#ifndef STREAM9_SORT_HPP
#define STREAM9_SORT_HPP

#include <ranges>

namespace stream9 {

using std::ranges::sort;

} // namespace stream9

#endif // STREAM9_SORT_HPP
