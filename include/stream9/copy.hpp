#ifndef STREAM9_COPY_HPP
#define STREAM9_COPY_HPP

#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/iterators/concepts.hpp>

#include <algorithm>
#include <iterator>

namespace stream9 {

template<iter::input_iterator I, iter::sentinel_for<I> S,
         iter::output_iterator<iter::reference_t<I>> O >
O
copy(I first, S last, O dest)
{
    return std::copy(first, last, dest);
}

template<input_range R, std::output_iterator<rng::reference_t<R>> O>
O
copy(R&& src, O dest)
{
    return std::copy(rng::begin(src), rng::end(src), dest);
}

} // namespace stream9

#endif // STREAM9_COPY_HPP
