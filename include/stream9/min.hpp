#ifndef STREAM9_MIN_HPP
#define STREAM9_MIN_HPP

#include "concepts.hpp"

namespace stream9 {

template<typename T, typename U = T>
concept less_than_comparable =
    requires (T const& x, U const& y) {
        { x < y } -> convertible_to<bool>;
    };

template<typename T, typename U = T>
concept nothrow_less_than_comparable =
    requires (T const& x, U const& y) {
        { x < y } noexcept -> convertible_to<bool>;
    };

template<less_than_comparable T>
T const&
min(T const& x, T const& y)
    noexcept(nothrow_less_than_comparable<T>)
{
    return x < y ? x : y;
}

} // namespace stream9

#endif // STREAM9_MIN_HPP
