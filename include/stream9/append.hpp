#ifndef STREAM9_APPEND_HPP
#define STREAM9_APPEND_HPP

#include "container/modifier/append.hpp"

namespace stream9 {

using container::append;

} // namespace stream9


#endif // STREAM9_APPEND_HPP
