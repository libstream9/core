#ifndef STREAM9_ERRORS_ERROR_CODE_HPP
#define STREAM9_ERRORS_ERROR_CODE_HPP

#include <concepts>
#include <system_error>

namespace stream9::errors {

enum class errc {
    ok = 0,
    invalid_argument,
    argument_out_of_domain,
    result_out_of_range,
    invalid_data,
    internal_error,
    unknown_error,
};

std::error_category const& error_category();

inline std::error_code
make_error_code(errc const e)
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::errors

namespace stream9 {

using errors::errc;
using errors::error_category;

} // namespace stream9

namespace std {

template<>
struct is_error_code_enum<stream9::errors::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_ERRORS_ERROR_CODE_HPP
