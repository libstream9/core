#ifndef STREAM9_ERRORS_RETHROW_ERROR_HPP
#define STREAM9_ERRORS_RETHROW_ERROR_HPP

#include "error_code.hpp"

#include <stream9/source_location.hpp>

#include <system_error>

#include <stream9/json.hpp>

namespace stream9::errors {

[[noreturn]] void
rethrow_error(source_location loc = {});

[[noreturn]] void
rethrow_error(json::object context,
              source_location loc = {});

[[deprecated]] [[noreturn]] void
rethrow_error(std::error_category const& current_category,
              source_location loc = {});

[[deprecated]] [[noreturn]] void
rethrow_error(std::error_category const& current_category,
              json::object context,
              source_location loc = {});

} // namespace stream9::errors

namespace stream9 {

using errors::rethrow_error;

} // namespace stream9

#endif // STREAM9_ERRORS_RETHROW_ERROR_HPP
