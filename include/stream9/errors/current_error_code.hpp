#ifndef STREAM9_ERRORS_CURRENT_ERROR_CODE_HPP
#define STREAM9_ERRORS_CURRENT_ERROR_CODE_HPP

#include "error.hpp"

#include <system_error>

namespace stream9::errors {

inline std::error_code
current_error_code()
{
    if (!std::current_exception()) {
        return {};
    }
    else {
        try {
            throw;
        }
        catch (error const& e) {
            return e.why();
        }
        catch (std::system_error const& e) {
            return e.code();
        }
        catch (...) {
            return {};
        }
    }
}

} // namespace stream9::errors

namespace stream9 {

using errors::current_error_code;

} // namespace stream9

#endif // STREAM9_ERRORS_CURRENT_ERROR_CODE_HPP
