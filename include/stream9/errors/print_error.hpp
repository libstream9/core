#ifndef STREAM9_ERRORS_PRINT_ERROR_HPP
#define STREAM9_ERRORS_PRINT_ERROR_HPP

#include "error.hpp"

#include <stream9/json.hpp>
#include <stream9/source_location.hpp>

#include <exception>
#include <iostream>

namespace stream9::errors {

void
print_error(std::ostream&,
            source_location where = {});

void
print_error(std::ostream&,
            json::object const& context,
            source_location where = {});

void
print_error(std::ostream&,
            error const&,
            source_location where = {});

void
print_error(std::ostream&,
            error const&,
            json::object const& context,
            source_location where = {});

void
print_error(std::ostream&,
            std::exception_ptr const&,
            source_location where = {});

void
print_error(std::ostream&,
            std::exception_ptr const&,
            json::object const& context,
            source_location where = {});

inline void
print_error(source_location where = {})
{
    print_error(std::cerr, std::move(where));
}

inline void
print_error(json::object const& context,
            source_location where = {})
{
    print_error(std::cerr, std::move(context), std::move(where));
}

inline void
print_error(error const& e,
            source_location where = {})
{
    print_error(std::cerr, e, std::move(where));
}

inline void
print_error(error const& e,
            json::object const& context,
            source_location where = {})
{
    print_error(std::cerr, e, context, std::move(where));
}

inline void
print_error(std::exception_ptr const& ex,
            source_location where = {})
{
    print_error(std::cerr, ex, std::move(where));
}

inline void
print_error(std::exception_ptr const& ex,
            json::object const& context,
            source_location where = {})
{
    print_error(std::cerr, ex, context, std::move(where));
}

std::ostream& operator<<(std::ostream&, error const&);

} // namespace stream9::errors

namespace stream9 {

using errors::print_error;

} // namespace stream9

#endif // STREAM9_ERRORS_PRINT_ERROR_HPP
