#ifndef STREAM9_ERRORS_EXCEPTION_CAST_HPP
#define STREAM9_ERRORS_EXCEPTION_CAST_HPP

#include <exception>
#include <type_traits>

namespace stream9::errors {

template<typename P>
    requires std::is_pointer_v<P>
P
exception_cast(std::exception_ptr const e = std::current_exception()) noexcept
{
    using T = std::remove_pointer_t<P>;

    try {
        std::rethrow_exception(e);
    }
    catch (T& ex) {
        return &ex;
    }
    catch (std::nested_exception const& ex) {
        return exception_cast<P>(ex.nested_ptr());
    }
    catch (...) {
        return nullptr;
    }
}

template<typename P1, typename P2>
    requires std::is_pointer_v<P1>
          && std::is_pointer_v<P2>
P1
exception_cast(P2 const e) noexcept
{
    if (e == nullptr) return nullptr;

    try {
        throw *e;
    }
    catch (...) {
        return exception_cast<P1>();
    }
}

} // namespace stream9::errors

namespace stream9 {

using errors::exception_cast;

} // namespace stream9

#endif // STREAM9_ERRORS_EXCEPTION_CAST_HPP
