#ifndef STREAM9_ERRORS_JSON_HPP
#define STREAM9_ERRORS_JSON_HPP

#include "error.hpp"

#include <exception>
#include <system_error>

#include <stream9/json.hpp>

namespace stream9::errors {

void tag_invoke(json::value_from_tag, json::value&, error const&);

} // namespace stream9::json

namespace stream9::json {

void tag_invoke(json::value_from_tag, json::value&, std::error_code const&);
void tag_invoke(json::value_from_tag, json::value&, std::exception const&);
void tag_invoke(json::value_from_tag, json::value&, std::exception_ptr const&);
void tag_invoke(json::value_from_tag, json::value& v, std::system_error const&);

} // namespace stream9::json

#endif // STREAM9_ERRORS_JSON_HPP
