#ifndef STREAM9_ERRORS_ERROR_HPP
#define STREAM9_ERRORS_ERROR_HPP

#include "error_code.hpp"
#include "namespace.hpp"

#include <stream9/json.hpp>
#include <stream9/source_location.hpp>

#include <exception>
#include <string>
#include <system_error>

namespace stream9::errors {

/*
 * @model std::copyable
 * @model std::equality_comparable
 */
class error : public std::exception
{
public:
    explicit error(std::error_code why,
          source_location where = {}) noexcept;

    error(std::error_code why,
          json::object context,
          source_location where = {}) noexcept;

    error(std::string what,
          std::error_code why,
          source_location where = {}) noexcept;

    error(std::string what,
          std::error_code why,
          json::object context,
          source_location where = {}) noexcept;

    // accessor
    char const* what() const noexcept override;

    std::error_code const& why() const noexcept;

    source_location const& where() const noexcept;

    json::object const& context() const noexcept;
    json::object& context() noexcept;

    // query
    std::error_category const& category() const noexcept;

    // equality
    bool operator==(error const&) const noexcept;

private:
    std::string m_what;
    std::error_code m_why;
    json::object m_context;
    source_location m_where;
};

[[noreturn]] void
throw_error(std::error_code why,
            source_location = {});

[[noreturn]] void
throw_error(std::error_code why,
            json::object context,
            source_location = {});

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            source_location = {});

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            json::object context,
            source_location = {});

// definition

inline std::error_code const& error::
why() const noexcept
{
    return m_why;
}

inline source_location const& error::
where() const noexcept
{
    return m_where;
}

inline json::object const& error::
context() const noexcept
{
    return m_context;
}

inline json::object& error::
context() noexcept
{
    return m_context;
}

inline std::error_category const& error::
category() const noexcept
{
    return m_why.category();
}

} // namespace stream9::errors

namespace stream9 {

using errors::error;
using errors::throw_error;

} // namespace stream9

#endif // STREAM9_ERRORS_ERROR_HPP
