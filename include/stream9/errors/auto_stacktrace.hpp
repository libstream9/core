#ifndef STREAM9_ERRORS_AUTO_STACKTRACE_HPP
#define STREAM9_ERRORS_AUTO_STACKTRACE_HPP

#include <stacktrace>

namespace stream9 {

std::stacktrace const&
stacktrace_at_throw() noexcept;

bool auto_stacktrace() noexcept;

void set_auto_stacktrace(bool) noexcept;

} // namespace stream9

#endif // STREAM9_ERRORS_AUTO_STACKTRACE_HPP
