#ifndef STREAM9_ERRORS_CURRENT_EXCEPTION_TYPE_HPP
#define STREAM9_ERRORS_CURRENT_EXCEPTION_TYPE_HPP

#include <typeinfo>

namespace stream9::errors {

std::type_info* current_exception_type() noexcept;

bool is_current_exception(std::type_info const&) noexcept;

template<typename T>
bool is_current_exception() noexcept
{
    return is_current_exception(typeid(T));
}

} // namespace stream9::errors

namespace stream9 {

using errors::current_exception_type;
using errors::is_current_exception;

} // namespace stream9

#endif // STREAM9_ERRORS_CURRENT_EXCEPTION_TYPE_HPP
