#ifndef STREAM9_NONMOVABLE_HPP
#define STREAM9_NONMOVABLE_HPP

namespace stream9 {

struct nonmovable
{
    nonmovable() = default;

    nonmovable(nonmovable const&) = delete;
    nonmovable& operator=(nonmovable const&) = delete;

    nonmovable(nonmovable&&) = delete;
    nonmovable& operator=(nonmovable&&) = delete;
};

} // namespace stream9

#endif // STREAM9_NONMOVABLE_HPP
