#include "optional/optional.hpp"
#include "optional/reference.hpp"
#include "optional/type_traits.hpp"

namespace stream9 {

template<typename T>
using opt = optional<T>;

} // namespace stream9
