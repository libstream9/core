#ifndef STREAM9_REF_HPP
#define STREAM9_REF_HPP

#include <type_traits>

namespace stream9 {

template<typename T>
class ref
{
public:
    // essnetial
    template<typename U>
    constexpr ref(U&& v) noexcept
        requires std::is_convertible_v<U, T&>
              && std::is_reference_v<U>;

    constexpr ref(ref const&) = default;
    constexpr ref& operator=(ref const&) = default;

    // accessor
    constexpr T& get() const noexcept;
    constexpr T& operator*() const noexcept;

    constexpr T* operator->() const noexcept;
    constexpr T* operator&() const noexcept;

    // conversion
    constexpr operator T& () const noexcept;

    // comparison
    constexpr bool operator==(ref const&) const noexcept = default;
    constexpr auto operator<=>(ref const&) const noexcept = default;

    template<typename U>
    constexpr bool operator==(U&& x) const noexcept
        requires std::is_convertible_v<U, T const&>
              && std::is_reference_v<U>
    {
        return m_ptr == &x;
    }

    template<typename U>
    constexpr bool operator<=>(U&& x) const noexcept
        requires std::is_convertible_v<U, T const&>
              && std::is_reference_v<U>
    {
        return m_ptr <=> &x;
    }

private:
    T* m_ptr; // non-null
};

template<typename T>
ref(T&) -> ref<T>;

template<typename T>
template<typename U>
constexpr ref<T>::
ref(U&& v) noexcept
    requires std::is_convertible_v<U, T&>
          && std::is_reference_v<U>
    : m_ptr { &v }
{}

template<typename T>
constexpr T& ref<T>::
get() const noexcept
{
    return *m_ptr;
}

template<typename T>
constexpr T& ref<T>::
operator*() const noexcept
{
    return get();
}

template<typename T>
constexpr T* ref<T>::
operator->() const noexcept
{
    return m_ptr;
}

template<typename T>
constexpr T* ref<T>::
operator&() const noexcept
{
    return m_ptr;
}

template<typename T>
constexpr ref<T>::
operator T& () const noexcept
{
    return get();
}

} // namespace stream9

#endif // STREAM9_REF_HPP
