#ifndef STREAM9_NULL_HPP
#define STREAM9_NULL_HPP

#include <concepts>
#include <type_traits>

namespace stream9 {

struct null_t {};

constexpr null_t null;

template<typename T>
struct null_traits
{
    static T
    construct()
        noexcept(noexcept(T(null)))
        requires requires { T(null); }
    {
        return T(null);
    }

    static bool
    is_null(T const& x)
        noexcept(noexcept(x.is_null()))
        requires requires (T const& x) {
            { x.is_null() } -> std::convertible_to<bool>;
        }
    {
        return x.is_null();
    }

    static void
    set_null(T& x)
        noexcept(noexcept(x.set_null()))
        requires requires (T& x) {
            x.set_null();
        }
    {
        x.set_null();
    }
};

template<typename T>
T
make_null()
    requires requires { { null_traits<T>::construct() } -> std::same_as<T>; }
{
    return null_traits<T>::construct();
}

template<typename T>
bool
is_null(T const& x)
    requires requires {
        { null_traits<T>::is_null(x)} -> std::convertible_to<bool>; }
{
    return null_traits<T>::is_null(x);
}

template<typename T>
void
set_null(T& x)
    noexcept(noexcept(null_traits<T>::set_null(x)))
    requires requires (T& x) { null_traits<T>::set_null(x); }
{
    null_traits<T>::set_null(x);
}

template<typename T>
void
set_null(T& x)
    noexcept(noexcept(null_traits<T>::construct())
                   && std::is_nothrow_move_assignable_v<T>)
    requires (!requires (T& x) { null_traits<T>::set_null(x); })
          && requires { null_traits<T>::construct(); }
          && std::is_move_assignable_v<T>
{
    x = null_traits<T>::construct();
}

template<typename T>
concept nullable = requires { stream9::make_null<T>(); }
                && requires (T const& x) { stream9::is_null(x); }
                && requires (T& x) { stream9::set_null(x); };

} // namespace stream9

#endif // STREAM9_NULL_HPP
