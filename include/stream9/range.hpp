#ifndef STREAM9_RANGE_HPP
#define STREAM9_RANGE_HPP

#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/ranges/namespace.hpp>

#include <cassert>
#include <iterator>

namespace stream9 {

template<std::input_or_output_iterator I, std::sentinel_for<I> S = I>
class range
{
public:
    // essentials
    template<rng::range R>
    range(R&& r) noexcept
        : m_first { rng::begin(r) }
        , m_last { rng::end(r) }
    {
        if constexpr (requires (I f, S l) { f <= l; }) {
            assert(m_first <= m_last);
        }
    }

    range(I f, S l)
        : m_first { std::move(f) }
        , m_last { std::move(l) }
    {
        if constexpr (requires (I f, S l) { f <= l; }) {
            assert(m_first <= m_last);
        }
    }

    operator bool () const noexcept { return !empty(); }

    I const& begin() const noexcept { return m_first; }
    S const& end() const noexcept { return m_last; }

    bool empty() const noexcept { return m_first == m_last; }

    auto
    size() const noexcept
        requires requires (I f, S l) { l - f; }
    {
        return m_last - m_first;
    }

    auto
    data() const noexcept
        requires std::contiguous_iterator<I>
    {
        return &*m_first;
    }

private:
    I m_first;
    S m_last;
};

template<rng::range R>
range(R) -> range<rng::iterator_t<R>, rng::sentinel_t<R>>;

template<std::size_t N, typename I, typename S>
constexpr auto
get(range<I, S> r) noexcept
    requires (N == 0 || N == 1)
{
    if constexpr (N == 0) {
        return r.begin();
    }
    else {
        return r.end();
    }
}

} // namespace stream9

namespace std {

template<typename I, typename S>
struct tuple_size<stream9::range<I, S>>
{
    static constexpr size_t value = 2;
};

template<typename I, typename S>
struct tuple_element<0, stream9::range<I, S>>
{
    using type = I;
};

template<typename I, typename S>
struct tuple_element<1, stream9::range<I, S>>
{
    using type = S;
};

} // namespace std

#endif // STREAM9_RANGE_HPP
