#ifndef STRINGS_STREAM_HPP
#define STRINGS_STREAM_HPP

#include "core/concepts.hpp"
#include "core/string_traits.hpp"

#include "streambuf.hpp"

#include <iostream>

namespace stream9::strings {

template<string S>
class istream : public std::basic_istream<str_char_t<S>>
{
    using base_t = std::basic_istream<str_char_t<S>>;
public:
    istream(S& s)
        : m_buf { s }
    {
        this->init(&m_buf);
    }

    istream(istream const&) = delete;

    istream(istream&& other)
        : base_t { std::move(other) }
        , m_buf { std::move(other.m_buf) }
    {
        this->set_rdbuf(&m_buf);
    }

    istream& operator=(istream const&) = delete;

    istream& operator=(istream&& other)
    {
        base_t::operator=(std::move(other));
        m_buf = std::move(other.m_buf);

        return *this;
    }

    ~istream() = default;

private:
    streambuf<S> m_buf;
};

template<string S>
class ostream : public std::basic_ostream<str_char_t<S>>
{
    using base_t = std::basic_ostream<str_char_t<S>>;
public:
    ostream(S& s)
        : m_buf { s }
    {
        this->init(&m_buf);
        *this << std::unitbuf;
    }

    ostream(ostream const&) = delete;

    ostream(ostream&& other)
        : base_t { std::move(other) }
        , m_buf { std::move(other.m_buf) }
    {
        this->set_rdbuf(&m_buf);
    }

    ostream& operator=(ostream const&) = delete;

    ostream& operator=(ostream&& other)
    {
        base_t::operator=(std::move(other));
        m_buf = std::move(other.m_buf);

        return *this;
    }

    ~ostream() = default;

private:
    streambuf<S> m_buf;
};

template<string S>
class iostream : public std::basic_iostream<str_char_t<S>>
{
    using base_t = std::basic_iostream<str_char_t<S>>;
public:
    iostream(S& s)
        : m_buf { s }
    {
        this->init(&m_buf);
        *this << std::unitbuf;
    }

    iostream(iostream const&) = delete;

    iostream(iostream&& other)
        : base_t { std::move(other) }
        , m_buf { std::move(other.m_buf) }
    {
        this->set_rdbuf(&m_buf);
    }

    iostream& operator=(iostream const&) = delete;

    iostream& operator=(iostream&& other)
    {
        base_t::operator=(std::move(other));
        m_buf = std::move(other.m_buf);

        return *this;
    }

    ~iostream() = default;

private:
    streambuf<S> m_buf;
};

template<typename T>
concept std_istream_streamable =
    requires (std::istream& is, T& v) {
        { is >> v } -> std::convertible_to<std::istream&>;
    };

template<typename T>
concept std_ostream_streamable =
    requires (std::ostream& os, T const& v) {
        { os << v } -> std::convertible_to<std::ostream&>;
    };

template<readable_string S, std_istream_streamable T>
istream<S>
operator>>(S& s, T& v)
{
    istream is { s };
    is >> v;
    return is;
}

template<writable_string S, std_ostream_streamable T>
ostream<S>
operator<<(S& s, T const& v)
{
    ostream os { s };
    os << v;
    return os;
}

} // namespace stream9::strings

#endif // STRINGS_STREAM_HPP
