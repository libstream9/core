#ifndef STRINGS_ACCESSOR_AT_HPP
#define STRINGS_ACCESSOR_AT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../error.hpp"

#include "begin.hpp"
#include "end.hpp"

namespace stream9::strings {

namespace _at {

    struct api
    {
        template<random_access_string S>
        reference_t<S>
        operator()(S&& s, str_index_t<S> n) const
        {
            auto f = str::begin(s);
            auto l = str::end(s);

            auto i = f + n;
            if (i < l) {
                return *i;
            }
            else {
                throw error {
                    errc::index_is_out_of_range, {
                        { "index", n },
                        { "size", l - f }
                    }
                };
            }
        }
    };

} // namespace _at

inline constexpr _at::api at;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_AT_HPP
