#ifndef STRINGS_ACCESSOR_BACK_HPP
#define STRINGS_ACCESSOR_BACK_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../error.hpp"
#include "../query/empty.hpp"

#include "end.hpp"

namespace stream9::strings {

namespace _back {

    struct api
    {
        template<bidirectional_string S>
        reference_t<S>
        operator()(S&& s) const
        {
            if (!str::empty(s)) {
                if constexpr (random_access_string<S>) {
                    return *(str::end(s) - 1);
                }
                else {
                    auto it = str::end(s);
                    --it;
                    return *it;
                }
            }
            else {
                throw error {
                    errc::try_to_access_inside_of_empty_string
                };
            }
        }
    };

} // namespace _back

inline constexpr _back::api back;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_BACK_HPP
