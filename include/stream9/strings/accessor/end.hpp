#ifndef STRINGS_ACCESSOR_END_HPP
#define STRINGS_ACCESSOR_END_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../detail/pointer.hpp"

#include <stream9/ranges/end.hpp>

#include <iterator>

namespace stream9::strings {

/*
 * end
 */
namespace _end {

    struct fn {

        template<character C>
        constexpr C*
        operator()(C* const& s) const
        {
            return detail::ptr_end(s);
        }

        template<character C, std::size_t N>
        constexpr C*
        operator()(C (&s)[N]) const
        {
            return detail::arr_end(s);
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::end(s);
        }

    };

} // namespace _end

inline constexpr _end::fn end;

/*
 * cend
 */
namespace _cend {

    struct fn {

        template<character C>
        constexpr C const*
        operator()(C const* const& s) const
        {
            return detail::ptr_end(s);
        }

        template<character C, std::size_t N>
        constexpr C const*
        operator()(C const (&s)[N]) const
        {
            return detail::arr_end(s);
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::cend(s);
        }

    };

} // namespace _cend

inline constexpr _cend::fn cend;

/*
 * rend
 */
namespace _rend {

    struct fn {

        template<character C>
        constexpr auto
        operator()(C* const& s) const
        {
            return std::make_reverse_iterator(s);
        }

        template<character C, std::size_t N>
        constexpr auto
        operator()(C (&s)[N]) const
        {
            return std::make_reverse_iterator(s);
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::rend(s);
        }

    };

} // namespace _rend

inline constexpr _rend::fn rend;

/*
 * crend
 */
namespace _crend {

    struct fn {

        template<character C>
        constexpr auto
        operator()(C const* const& s) const
        {
            return std::make_reverse_iterator(s);
        }

        template<character C, std::size_t N>
        constexpr auto
        operator()(C const (&s)[N]) const
        {
            return std::make_reverse_iterator(s);
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::crend(s);
        }

    };

} // namespace _crend

inline constexpr _crend::fn crend;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_END_HPP
