#ifndef STRINGS_ACCESSOR_FRONT_HPP
#define STRINGS_ACCESSOR_FRONT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../error.hpp"
#include "../query/empty.hpp"

#include "begin.hpp"

namespace stream9::strings {

namespace _front {

    struct api
    {
        template<readable_string S>
        reference_t<S>
        operator()(S&& s) const
        {
            if (!str::empty(s)) {
                return *str::begin(s);
            }
            else {
                throw error {
                    errc::try_to_access_inside_of_empty_string
                };
            }
        }
    };

} // namespace _front

inline constexpr _front::api front;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_FRONT_HPP
