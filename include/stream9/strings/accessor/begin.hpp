#ifndef STRINGS_ACCESSOR_BEGIN_HPP
#define STRINGS_ACCESSOR_BEGIN_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../detail/pointer.hpp"

#include <stream9/ranges/begin.hpp>

#include <iterator>

namespace stream9::strings {

/*
 * begin
 */
namespace _begin {

    struct fn {

        template<character C>
        constexpr C*
        operator()(C* const& s) const
        {
            return s;
        }

        template<character C, std::size_t N>
        constexpr C*
        operator()(C (&s)[N]) const
        {
            return s;
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::begin(s);
        }

    };

} // namespace _begin

inline constexpr _begin::fn begin;

/*
 * cbegin
 */
namespace _cbegin {

    struct fn {

        template<character C>
        constexpr C const*
        operator()(C const* const& s) const
        {
            return s;
        }

        template<character C, std::size_t N>
        constexpr C const*
        operator()(C const (&s)[N]) const
        {
            return s;
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::cbegin(s);
        }

    };

} // namespace _cbegin

inline constexpr _cbegin::fn cbegin;

/*
 * rbegin
 */
namespace _rbegin {

    struct fn {

        template<character C>
        constexpr auto
        operator()(C* const& s) const
        {
            return std::make_reverse_iterator(detail::ptr_end(s));
        }

        template<character C, std::size_t N>
        constexpr auto
        operator()(C (&s)[N]) const
        {
            return std::make_reverse_iterator(detail::arr_end(s));
        }

        template<character_range S>
            requires requires (S s) { rng::rbegin(s); }
        constexpr auto
        operator()(S&& s) const
        {
            return rng::rbegin(s);
        }

    };

} // namespace _rbegin

inline constexpr _rbegin::fn rbegin;

/*
 * crbegin
 */
namespace _crbegin {

    struct fn {

        template<character C>
        constexpr auto
        operator()(C const* const& s) const
        {
            return std::make_reverse_iterator(detail::ptr_end(s));
        }

        template<character C, std::size_t N>
        constexpr auto
        operator()(C const (&s)[N]) const
        {
            return std::make_reverse_iterator(detail::arr_end(s));
        }

        template<character_range S>
        constexpr auto
        operator()(S&& s) const
        {
            return rng::crbegin(s);
        }

    };

} // namespace _crbegin

inline constexpr _crbegin::fn crbegin;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_BEGIN_HPP
