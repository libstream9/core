#ifndef STRINGS_ACCESSOR_C_STR_HPP
#define STRINGS_ACCESSOR_C_STR_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

namespace stream9::strings {

namespace c_str_ {

    struct api {
        template<character C>
        constexpr C*
        operator()(C* const& s) const noexcept
        {
            return s;
        }

        template<character C, std::size_t N>
        constexpr C*
        operator()(C (&s)[N]) const noexcept
        {
            return s;
        }

        template<typename S>
            requires requires (S s) { s.c_str(); }
        constexpr auto
        operator()(S&& s) const
            noexcept(noexcept(s.c_str()))
        {
            return s.c_str();
        }
    };

} // namespace c_str_

constexpr c_str_::api c_str;

} // namespace stream9::strings

#endif // STRINGS_ACCESSOR_C_STR_HPP
