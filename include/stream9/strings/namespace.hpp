#ifndef STREAM9_STRINGS_NAMESPACE_HPP
#define STREAM9_STRINGS_NAMESPACE_HPP

namespace stream9::strings {}

namespace stream9 {

namespace st9 = stream9;
namespace str = strings;

} // namespace stream9

#endif // STREAM9_STRINGS_NAMESPACE_HPP
