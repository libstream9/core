#ifndef STREAM9_STRINGS_ERROR_HPP
#define STREAM9_STRINGS_ERROR_HPP

#include <stream9/errors.hpp>
#include <stream9/source_location.hpp>

namespace stream9::strings {

enum class errc {
    ok = 0,
    invalid_character,
    invalid_hex_character,
    invalid_bool_string,
    invalid_format_string,
    fail_to_convert_from_istream,
    not_terminated_with_null,
    index_is_out_of_range,
    not_enough_memory,
    invalid_pointer,
    invalid_range,
    invalid_iterator,
    try_to_access_inside_of_empty_string,
};

std::error_category const& error_category() noexcept;

inline std::error_code
make_error_code(errc const e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::strings

namespace std {

template<>
struct is_error_code_enum<stream9::strings::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_STRINGS_ERROR_HPP
