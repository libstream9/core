#ifndef STREAM9_STRINGS_STRING_VIEW_HPP
#define STREAM9_STRINGS_STRING_VIEW_HPP

#include "error.hpp"

#include "core/concepts.hpp"
#include "core/string_traits.hpp"

#include <stream9/null.hpp>
#include <stream9/safe_integer.hpp>

#include <compare>
#include <iterator>
#include <ostream>
#include <string_view>

namespace stream9 {

// Concept:
// - contiguous_string
// - nothrow_copy_constructible
// - nothrow_copy_assignable
// - nothrow_move_constructible
// - nothrow_move_assignable
// - nothrow_destructible
// - nothrow_swappable
//
// Invariant:
// - m_begin != nullptr
// - m_end != nullptr
// - m_begin <= m_end

template<str::character Ch>
class basic_string_view
{
public:
    using const_iterator = Ch const*;
    using iterator = Ch const*;
    using const_reference = Ch const&;
    using reference = Ch const&;
    using const_pointer = Ch const*;
    using pointer = Ch const*;
    using index_type = safe_integer<intmax_t, 0>;
    using size_type = safe_integer<intmax_t, 0>;

public:
    // essential
    template<str::contiguous_string S>
    constexpr
    basic_string_view(S const&) noexcept
        requires str::is_same_char_v<S, Ch>;

    template<str::contiguous_string S>
    constexpr
    basic_string_view&
    operator=(S const&) noexcept;

    template<std::contiguous_iterator I>
    constexpr
    basic_string_view(I first, I last) noexcept;

    template<std::contiguous_iterator I>
    constexpr
    basic_string_view(I first, size_type length) noexcept;

    constexpr basic_string_view(basic_string_view const&) = default;
    constexpr basic_string_view& operator=(basic_string_view const&) = default;

    constexpr basic_string_view(basic_string_view&&) = default;
    constexpr basic_string_view& operator=(basic_string_view&&) = default;

    constexpr ~basic_string_view() = default;

    // conversion
    constexpr operator std::basic_string_view<Ch>() const noexcept;

    // accessor
    constexpr const_iterator begin() const noexcept;
    constexpr const_iterator end() const noexcept;

    constexpr const_reference operator[](index_type) const;

    constexpr const_pointer data() const noexcept;

    // query
    constexpr size_type size() const noexcept;

    constexpr bool empty() const noexcept;

    // modifier
    constexpr void remove_prefix(size_type n) noexcept;

    constexpr void remove_suffix(size_type n) noexcept;

    // equality
    constexpr bool operator==(basic_string_view const&) const noexcept;

    template<str::readable_string S>
    constexpr
    bool
    operator==(S const& o) const noexcept
        requires str::is_same_char_v<S, Ch>;

    // order
    constexpr
    std::strong_ordering
    operator<=>(basic_string_view const& o) const noexcept;

    template<str::readable_string S>
    constexpr
    std::strong_ordering
    operator<=>(S const& o) const noexcept
        requires str::is_same_char_v<S, Ch>;

private:
    template<typename> friend class null_traits;

    constexpr basic_string_view(null_t) noexcept;
    constexpr bool is_null() const noexcept;
    constexpr void set_null() noexcept;

private:
    Ch const* m_begin;
    Ch const* m_end;
};

// template argument deduction guide
template<str::contiguous_string S>
basic_string_view(S&&) -> basic_string_view<str::character_t<S>>;

// ostream
template<typename C>
std::basic_ostream<C>&
operator<<(std::basic_ostream<C>&, basic_string_view<C> const&);

// alias
using string_view = basic_string_view<char>;

} // namespace stream9

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wliteral-suffix"
namespace stream9 {
namespace literals {
    inline constexpr string_view
    operator ""sv (char const* s, size_t len) noexcept
    {
        return string_view(s, len);
    }
} // namespace literals

using literals::operator ""sv;

} // namespace stream9
#pragma GCC diagnostic pop

namespace std {

template<typename C>
struct tuple_size<stream9::basic_string_view<C>>
{
    static constexpr auto value = 2;
};

template<typename C>
struct tuple_element<0, stream9::basic_string_view<C>>
{
    using type = typename stream9::basic_string_view<C>::const_iterator;
};

template<typename C>
struct tuple_element<1, stream9::basic_string_view<C>>
{
    using type = typename stream9::basic_string_view<C>::const_iterator;
};

} // namespace std

namespace stream9 {

template<size_t I>
constexpr auto
get(string_view s) noexcept
    requires (I < 2)
{
    if constexpr (I == 0) {
        return s.begin();
    }
    else {
        return s.end();
    }
}

} // namespace stream9

#endif // STREAM9_STRINGS_STRING_VIEW_HPP

#include "string_view.ipp"
