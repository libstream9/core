#ifndef STRINGS_CORE_STRING_TRAITS_HPP
#define STRINGS_CORE_STRING_TRAITS_HPP

#include "../namespace.hpp"

#include "concepts.hpp"
#include "meta.hpp"

#include <concepts>
#include <ranges>

namespace stream9::strings {

template<typename S>
struct string_traits;

template<typename CharPtr>
    requires std::is_pointer_v<_uncvref<CharPtr>>
struct string_traits<CharPtr>
{
    using reference_type = std::add_lvalue_reference_t<
                          std::remove_pointer_t<std::remove_cvref_t<CharPtr>> >;
    using char_type = std::remove_cvref_t<reference_type>;
    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;
    using index_type = difference_type;
};

template<typename CharArray>
    requires std::is_array_v<_uncvref<CharArray>>
struct string_traits<CharArray>
{
    using reference_type = std::add_lvalue_reference_t<
                     std::remove_extent_t<std::remove_reference_t<CharArray>> >;
    using char_type = std::remove_cvref_t<reference_type>;
    using difference_type = std::ptrdiff_t;
    using size_type = std::size_t;
    using index_type = difference_type;
};

template<character_range S>
    requires std::ranges::sized_range<S>
struct string_traits<S>
{
private:
    using iterator = iterator_t<_uncvref<S>>;
public:
    using reference_type = std::ranges::range_reference_t<S>;
    using char_type = std::iter_value_t<iterator>;
    using difference_type = std::iter_difference_t<iterator>;
    using size_type = decltype(std::ranges::size(std::declval<S>()));
    using index_type = difference_type;
};

template<character_range S>
    requires (!std::ranges::sized_range<S>)
struct string_traits<S>
{
private:
    using iterator = iterator_t<_uncvref<S>>;
public:
    using reference_type = std::ranges::range_reference_t<S>;
    using char_type = std::iter_value_t<iterator>;
    using difference_type = std::iter_difference_t<iterator>;
    using index_type = difference_type;
};

template<string S>
using str_char_t = typename string_traits<S>::char_type;

template<string S>
using str_index_t = typename string_traits<S>::index_type;

template<sized_string S>
using str_size_t = typename string_traits<S>::size_type;

template<string S>
using str_difference_t = typename string_traits<S>::difference_type;

template<string S, std::integral I>
str_difference_t<S>
to_difference(I n)
{
    //TODO safer
    return static_cast<str_difference_t<S>>(n);
}

template<string S, std::integral I>
str_size_t<S>
to_size(I n)
{
    //TODO safer
    return static_cast<str_size_t<S>>(n);
}

template<string S, std::integral I>
str_index_t<S>
to_index(I n)
{
    //TODO safer
    return static_cast<str_index_t<S>>(n);
}

/*
 * character_t
 */
template<typename> struct character_type;

template<string T>
struct character_type<T>
{
    using type = str_char_t<T>;
};

template<character T>
struct character_type<T>
{
    using type = _uncvref<T>;
};

template<typename T>
    requires (std::input_iterator<_uncvref<T>>
          &&  !character_pointer<T> )
struct character_type<T>
{
    using type = std::iter_value_t<_uncvref<T>>;
};

template<typename T>
using character_t = typename character_type<T>::type;

/*
 * is_same_char
 */
template<typename T, typename... Ts>
struct is_same_char : std::conjunction<
        std::is_same<character_t<T>, character_t<Ts>>... > {};

template<typename... Ts>
constexpr bool is_same_char_v = is_same_char<Ts...>::value;

/*
 * reference_t
 */
template<typename S>
using reference_t = typename string_traits<S>::reference_type;

} // namespace stream9::strings

#endif // STRINGS_CORE_STRING_TRAITS_HPP
