#ifndef STRINGS_CORE_CHAR_COMPARATOR_HPP
#define STRINGS_CORE_CHAR_COMPARATOR_HPP

#include "../concepts.hpp"

#include <cassert>
#include <concepts>
#include <locale>

namespace stream9::strings {

template<typename Fn, typename C>
concept char_comparator = character<C> && std::predicate<Fn, C, C>;

namespace detail {

    template<typename C>
    struct case_insensitive_base
    {
    private:
        std::ctype<C> const* m_facet; // non-null

    public:
        case_insensitive_base(std::locale const& loc = {})
            : m_facet { &std::use_facet<std::ctype<C>>(loc) }
        {
            assert(m_facet);
        }

        C toupper(C const c) const
        {
            return m_facet->toupper(c);
        }
    };

} // namespace detail

/*
 * equal_to
 */
template<character C>
struct equal_to
{
    bool
    operator()(C const c1, C const c2) const
    {
        return c1 == c2;
    }
};

template<character C>
struct iequal_to : detail::case_insensitive_base<C>
{
private:
    using base_t = detail::case_insensitive_base<C>;

public:
    using base_t::base_t;
    using base_t::operator=;

    bool
    operator()(C const c1, C const c2) const
    {
        return this->toupper(c1) == this->toupper(c2);
    }
};

/*
 * less
 */
template<character C>
struct less
{
    bool
    operator()(C const c1, C const c2) const
    {
        return c1 < c2;
    }
};

template<character C>
struct iless : detail::case_insensitive_base<C>
{
private:
    using base_t = detail::case_insensitive_base<C>;

public:
    using base_t::base_t;
    using base_t::operator=;

    bool
    operator()(C const c1, C const c2) const
    {
        return this->toupper(c1) < this->toupper(c2);
    }
};

/*
 * compare_three_way
 */
template<character C>
struct compare_three_way
{
    auto operator()(C const c1, C const c2) const noexcept
    {
        return c1 <=> c2;
    }
};

template<character C>
struct icompare_three_way : detail::case_insensitive_base<C>
{
    using detail::case_insensitive_base<C>::case_insensitive_base;

    auto operator()(C const c1, C const c2) const noexcept
    {
        return this->toupper(c1) <=> this->toupper(c2);
    }
};

} // namespace stream9::strings

#endif // STRINGS_CORE_CHAR_COMPARATOR_HPP
