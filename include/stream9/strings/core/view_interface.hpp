#ifndef STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP
#define STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP

#include "../namespace.hpp"

#include "concepts.hpp"
#include "string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

#include <algorithm>
#include <compare>
#include <concepts>
#include <ostream>
#include <ranges>
#include <type_traits>

#include <stream9/ranges/equal.hpp>

namespace stream9::strings {

struct view_interface_base {};

template<typename Derived>
    requires std::is_class_v<Derived>
          && std::same_as<Derived, std::remove_cv_t<Derived>>
class view_interface : public view_interface_base //TODO remove this class
{
public:

private:
    Derived& derived()
    {
        return static_cast<Derived&>(*this);
    }

    Derived const& derived() const
    {
        return static_cast<Derived const&>(*this);
    }
};

template<readable_string S1, readable_string S2>
bool operator==(S1 const& lhs, S2 const& rhs)
    requires std::derived_from<S1, view_interface_base>
          || std::derived_from<S2, view_interface_base>
{
    return rng::equal(str::begin(lhs), str::end(lhs),
                      str::begin(rhs), str::end(rhs) );
}

template<readable_string S1, readable_string S2>
auto operator<=>(S1 const& lhs, S2 const& rhs)
    requires std::derived_from<S1, view_interface_base>
          || std::derived_from<S2, view_interface_base>
{
    return std::lexicographical_compare_three_way(
        str::begin(lhs), str::end(lhs),
        str::begin(rhs), str::end(rhs)
    );
}

template<typename S>
    requires std::derived_from<_uncvref<S>, view_interface<_uncvref<S>>>
inline std::ostream&
operator<<(std::ostream& os, S const& s)
{
    if constexpr (contiguous_string<S> && sized_string<S>) {
        return os.write(s.data(), static_cast<std::streamsize>(str::size(s)));
    }
    else {
        for (auto const c: s) {
            os.put(c);
        }

        return os;
    }
}

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CORE_VIEW_INTERFACE_HPP
