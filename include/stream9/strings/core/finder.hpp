#ifndef STRINGS_CORE_FINDER_HPP
#define STRINGS_CORE_FINDER_HPP

#include "concepts.hpp"
#include "string_range.hpp"

#include <concepts>

namespace stream9::strings {

struct forward_finder_tag {};
struct backward_finder_tag {};

template<typename T>
struct finder_traits
{
    using direction = forward_finder_tag;
};

template<typename T>
    requires requires { typename T::direction; }
struct finder_traits<T>
{
    using direction = typename T::direction;
};

template<typename T, typename S>
concept finder =
    forward_string<S> &&
    requires (T fn, S s) {
        { fn(s) } -> std::same_as<string_range<iterator_t<S>>>;
    };

template<typename T>
concept forward_finder = std::same_as<
            typename finder_traits<_uncvref<T>>::direction,
            forward_finder_tag >;

template<typename T>
concept backward_finder = std::same_as<
            typename finder_traits<_uncvref<T>>::direction,
            backward_finder_tag >;

} // namespace stream9::strings

#endif // STRINGS_CORE_FINDER_HPP
