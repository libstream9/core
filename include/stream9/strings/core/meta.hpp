#ifndef STRINGS_CORE_META_HPP
#define STRINGS_CORE_META_HPP

#include <type_traits>

namespace stream9::strings {

template<typename T>
using _uncv = std::remove_cv_t<T>;

template<typename T>
using _unref = std::remove_reference_t<T>;

template<typename T>
using _uncvref = std::remove_cvref_t<T>;

template<typename T, typename... Ts>
struct is_same : std::conjunction<std::is_same<T, Ts>...> {};

} // namespace stream9::strings

#endif // STRINGS_CORE_META_HPP
