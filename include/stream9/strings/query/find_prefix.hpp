#ifndef STREAM9_STRINGS_QUERY_FIND_PREFIX_HPP
#define STREAM9_STRINGS_QUERY_FIND_PREFIX_HPP

#include "../namespace.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"
#include "../error.hpp"

#include <stream9/concepts.hpp>

namespace stream9::strings {

namespace find_prefix_ {

    struct api
    {
        template<forward_string T1, forward_string T2>
        static string_range<iterator_t<T1>>
        operator()(T1& s1, T2 const& prefix)
            noexcept(nothrow_equality_comparable<reference_t<T1>, reference_t<T2>>)
            requires equality_comparable<reference_t<T1>, reference_t<T2>>
        {
            try {
                using str::begin, str::end;

                auto i1 = begin(s1); auto e1 = end(s1);
                auto i2 = begin(prefix); auto e2 = end(prefix);

                for (; i1 != e1 && i2 != e2; ++i1, ++i2) {
                    if (*i1 != *i2) break;
                }

                if (i2 == e2) {
                    return { begin(s1), i1 };
                }
                else {
                    return { begin(s1), begin(s1) };
                }
            }
            catch (...) { rethrow_error(); }
        }
    };

} // namespace find_prefix_

inline constexpr find_prefix_::api find_prefix;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_FIND_PREFIX_HPP
