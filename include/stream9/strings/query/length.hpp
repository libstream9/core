#ifndef STRINGS_QUERY_LENGTH_HPP
#define STRINGS_QUERY_LENGTH_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "size.hpp"

namespace stream9::strings {

namespace length_ {

    struct api {

        template<string S>
        constexpr auto
        operator()(S&& s) const
        {
            return to_index<S>(str::size(s));
        }

    };

} // namespace size_

constexpr length_::api length;

} // namespace stream9::strings


#endif // STRINGS_QUERY_LENGTH_HPP
