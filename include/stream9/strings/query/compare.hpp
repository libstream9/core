#ifndef STRINGS_QUERY_COMPARE_HPP
#define STRINGS_QUERY_COMPARE_HPP

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"

#include <concepts>

namespace stream9::strings {

namespace _compare {

    template<typename S1, typename S2>
    inline constexpr bool has_member = requires (S1 s1, S2 s2) {
        { s1.compare(s2) } -> std::convertible_to<bool>;
    };

    template<typename S1, typename S2>
    int
    compare(S1&& s1, S2&& s2)
    {
        auto [it1, end1] = string_range(s1);
        auto [it2, end2] = string_range(s2);

        for (; it1 != end1 && it2 != end2; ++it1, ++it2) {
            if (*it1 > *it2) {
                return 1;
            }
            else if (*it1 < *it2) {
                return -1;
            }
        }

        if (it1 != end1) {
            return 1;
        }
        else if (it2 != end2) {
            return -1;
        }
        else {
            return 0;
        }
    }

    struct api
    {
        template<forward_string S1, forward_string S2>
        int
        operator()(S1&& s1, S2&& s2) const
        {
            if constexpr (has_member<S1, S2>) {
                return s1.compare(s2);
            }
            else if constexpr (has_member<S2, S1>) {
                return -1 * s2.compare(s1);
            }
            else {
                return _compare::compare(s1, s2);
            }
        }
    };

} // namespace _compare

inline constexpr _compare::api compare;

} // namespace stream9::strings

#endif // STRINGS_QUERY_COMPARE_HPP
