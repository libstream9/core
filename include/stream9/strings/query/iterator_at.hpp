#ifndef STRINGS_QUERY_ITERATOR_AT_HPP
#define STRINGS_QUERY_ITERATOR_AT_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"

#include <concepts>

namespace stream9::strings {

namespace _iterator_at {

    struct api
    {
        template<random_access_string S, std::integral IndexT>
        iterator_t<S>
        operator()(S&& s, IndexT const i) const
        {
            return str::begin(s) + to_difference<S>(i);
        }
    };

} // namespace _iterator_at

inline constexpr _iterator_at::api iterator_at;

} // namespace stream9::strings

#endif // STRINGS_QUERY_ITERATOR_AT_HPP
