#ifndef STRINGS_QUERY_FIND_FIRST_HPP
#define STRINGS_QUERY_FIND_FIRST_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include "../finder/first_finder.hpp"
#include "../finder/regex_finder.hpp"

#include <functional>
#include <locale>
#include <type_traits>

namespace stream9::strings {

/*
 * find_first
 */
namespace _find_first {

    struct api
    {
        template<forward_string S, character C>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s, C const c) const
        {
            str::first_finder fn { c };
            return fn(s);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        string_range<iterator_t<S1>>
        operator()(S1&& s1, S2&& s2) const
        {
            str::first_finder fn { s2 };
            return fn(s1);
        }

        template<forward_string S, classifier<str_char_t<S>> C>
        string_range<iterator_t<S>>
        operator()(S&& s, C&& c) const
        {
            str::first_finder<str_char_t<S>, _unref<C>> fn { c };
            return fn(s);
        }

        template<bidirectional_string S, regexp R>
        string_range<iterator_t<S>>
        operator()(S&& s, R&& e) const
        {
            if constexpr (std::is_trivially_copyable_v<_unref<R>>) {
                str::regex_finder fn { e };
                return fn(s);
            }
            else {
                str::regex_finder fn { std::cref(e) };
                return fn(s);
            }
        }

        template<forward_string S, finder<S> FinderT>
        string_range<iterator_t<S>>
        operator()(S&& s, FinderT&& fn) const
        {
            return fn(s);
        }
    };

} // namespace _find_first

inline constexpr _find_first::api find_first;

/*
 * ifind_first
 */
namespace _ifind_first {

    struct api
    {
        template<readable_string S, character C>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s, C const c, std::locale const& loc = {}) const
        {
            str::first_finder fn { c, str::iequal_to<C>(loc) };
            return fn(s);
        }

        template<readable_string S1, readable_string S2>
            requires is_same_char_v<S1, S2>
        string_range<iterator_t<S1>>
        operator()(S1&& s1, S2&& s2, std::locale const& loc = {}) const
        {
            str::first_finder fn { s2, str::iequal_to<str_char_t<S1>>(loc) };
            return fn(s1);
        }
    };

} // namespace _ifind_first

inline constexpr _ifind_first::api ifind_first;

} // namespace stream9::strings

#endif // STRINGS_QUERY_FIND_FIRST_HPP
