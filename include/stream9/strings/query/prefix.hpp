#ifndef STREAM9_STRINGS_QUERY_PREFIX_HPP
#define STREAM9_STRINGS_QUERY_PREFIX_HPP

#include "substr.hpp"

#include "../namespace.hpp"

namespace stream9::strings {

namespace prefix_ {

    struct api
    {
        template<random_access_string S>
        string_range<iterator_t<S>>
        operator()(S&& s, str_size_t<S> n) const noexcept
        // [[expects: 0 <= n]]
        {
            return substr(s, 0, n);
        }
    };

} // namespace prefix_

inline constexpr prefix_::api prefix;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_PREFIX_HPP
