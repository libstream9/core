#ifndef STRINGS_QUERY_FIND_LAST_HPP
#define STRINGS_QUERY_FIND_LAST_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../finder/last_finder.hpp"

#include <locale>

namespace stream9::strings {

/*
 * find_last
 */
namespace _find_last {

    struct api
    {
        template<bidirectional_string S, character C>
        string_range<iterator_t<S>>
        operator()(S&& s, C c) const
            requires is_same_char_v<S, C>
        {
            str::last_finder fn { c };
            return fn(s);
        }

        template<bidirectional_string S1, bidirectional_string S2>
            requires is_same_char_v<S1, S2>
        string_range<iterator_t<S1>>
        operator()(S1&& s1, S2&& s2) const
        {
            str::last_finder fn { s2 };
            return fn(s1);
        }

        template<bidirectional_string S, classifier<str_char_t<S>> C>
        string_range<iterator_t<S>>
        operator()(S&& s, C&& c) const
        {
            str::last_finder<str_char_t<S>, _unref<C>> fn { c };
            return fn(s);
        }
    };

} // namespace _find_last

inline constexpr _find_last::api find_last;

/*
 * ifind_last
 */
namespace _ifind_last {

    struct api
    {
        template<bidirectional_string S, character C>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s, C const c, std::locale const& loc = {}) const
        {
            str::last_finder fn { c, str::iequal_to<C>(loc) };
            return fn(s);
        }

        template<bidirectional_string S1, bidirectional_string S2>
            requires is_same_char_v<S1, S2>
        string_range<iterator_t<S1>>
        operator()(S1&& s1, S2&& s2, std::locale const& loc = {}) const
        {
            str::last_finder fn { s2, str::iequal_to<str_char_t<S1>>(loc) };
            return fn(s1);
        }
    };

} // namespace _ifind_last

inline constexpr _ifind_last::api ifind_last;

} // namespace stream9::strings

#endif // STRINGS_QUERY_FIND_LAST_HPP
