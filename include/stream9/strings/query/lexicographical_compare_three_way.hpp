#ifndef STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
#define STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP

#include "../namespace.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/c_str.hpp"
#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include <stream9/ranges/lexicographical_compare_three_way.hpp>

#include <cstring>
#include <functional>
#include <locale>

namespace stream9::strings {

namespace lexicographical_compare_three_way_ {

    struct api
    {
        template<contiguous_string S1, contiguous_string S2>
            requires is_same_char_v<S1, S2>
                  && std::same_as<_uncvref<str_char_t<S1>>, char>
        auto
        operator()(S1&& s1, S2&& s2) const noexcept
        {
            if constexpr (rng::sized_range<S1> && rng::sized_range<S2>) {
                return rng::lexicographical_compare_three_way(
                    str::begin(s1), str::end(s1),
                    str::begin(s2), str::end(s2)
                );
            }
            else if constexpr (terminated_string<S1> && terminated_string<S2>) {
                auto const p1 = str::c_str(s1);
                auto const p2 = str::c_str(s2);

                if (p1 && p2) {
                    return std::strcmp(p1, p2) <=> 0;
                }
                else {
                    return p1 <=> p2;
                }
            }
            else {
                auto const i1 = str::data(s1);
                auto const e1 = i1 + str::size(s1);
                auto const i2 = str::data(s2);
                auto const e2 = i2 + str::size(s2);

                return rng::lexicographical_compare_three_way(
                    i1, e1,
                    i2, e2
                );
            }
        }

        template<readable_string S1, readable_string S2,
                 class Comp = str::compare_three_way<str_char_t<S1>> >
            requires is_same_char_v<S1, S2>
                  && (!std::same_as<_uncvref<str_char_t<S1>>, char>)
        auto
        operator()(S1&& s1, S2&& s2, Comp comp = {} ) const noexcept
        {
            return rng::lexicographical_compare_three_way(
                str::begin(s1), str::end(s1),
                str::begin(s2), str::end(s2),
                std::ref(comp) );
        }
    };

} // namespace lexicographical_compare_three_way_

inline constexpr lexicographical_compare_three_way_::api lexicographical_compare_three_way;

namespace ilexicographical_compare_three_way_ {

    struct api
    {
        template<readable_string S1, readable_string S2,
                 class Comp = str::icompare_three_way<str_char_t<S1>> >
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s1, S2&& s2,
                   std::locale const& loc = std::locale()) const noexcept
        {
            auto comp = str::icompare_three_way<str_char_t<S1>>(loc);

            return rng::lexicographical_compare_three_way(
                str::begin(s1), str::end(s1),
                str::begin(s2), str::end(s2),
                std::ref(comp) );
        }
    };

} // namespace ilexicographical_compare_three_way_

inline constexpr ilexicographical_compare_three_way_::api ilexicographical_compare_three_way;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
