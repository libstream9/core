#ifndef STRINGS_QUERY_SUBSTR_HPP
#define STRINGS_QUERY_SUBSTR_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "iterator_at.hpp"

#include <concepts>

#include <stream9/iterators/next.hpp>

namespace stream9::strings {

namespace _substr {

    template<typename S, typename IndexT, typename SizeT>
    string_range<iterator_t<S>>
    substr(S&& s, IndexT const pos, SizeT const n)
    {
        auto first = str::iterator_at(s, pos);
        auto last = iter::next(first, to_difference<S>(n), str::end(s));

        return { first, last };
    }

    template<typename S, typename IndexT>
    string_range<iterator_t<S>>
    substr(S&& s, IndexT const pos)
    {
        auto first = str::iterator_at(s, pos);
        auto last = str::end(s);

        return { first, last };
    }

    struct api
    {
        template<random_access_string S>
        string_range<iterator_t<S>>
        operator()(S&& s, str_index_t<S> pos, str_size_t<S> n) const
        // [[expects: 0 <= pos < str::size(s) ]]
        // [[expects: 0 <= n]]
        {
            return substr(s, pos, n);
        }

        template<random_access_string S>
        string_range<iterator_t<S>>
        operator()(S&& s, str_index_t<S> pos) const
        // [[expects: 0 <= pos < str::size(s) ]]
        {
            return substr(s, pos);
        }
    };

} // namespace _substr

inline constexpr _substr::api substr;

} // namespace stream9::strings

#endif // STRINGS_QUERY_SUBSTR_HPP
