#ifndef STRINGS_QUERY_ENDS_WITH_HPP
#define STRINGS_QUERY_ENDS_WITH_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../core/char/comparator.hpp"

#include "../accessor/back.hpp"
#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include "empty.hpp"

#include <locale>

namespace stream9::strings {

namespace _ends_with {

    template<typename S1, typename T>
    inline constexpr bool has_member =
        string<S1> &&
        requires (S1 s1, T s2) {
            s1.ends_with(s2);
        };

    /*
     * with character
     */
    template<typename S, typename C, typename ComparatorT>
    bool
    ends_with_char(S&& s, C const c, ComparatorT fn)
    {
        if (str::empty(s)) return false;

        return fn(str::back(s), c);
    }

    template<typename S, typename C>
    bool
    ends_with_char(S&& s, C const c)
    {
        if constexpr (has_member<S, C>) {
            return s.ends_with(c);
        }
        else {
            return ends_with_char(s, c, str::equal_to<C>());
        }
    }

    /*
     * with substring
     */
    template<typename S1, typename S2, typename PredicateT>
    bool
    ends_with_str(S1&& s1, S2&& s2, PredicateT comp)
    {
        auto it1 = str::rbegin(s1);
        auto last1 = str::rend(s1);
        auto it2 = str::rbegin(s2);
        auto last2 = str::rend(s2);

        for (; it1 != last1 && it2 != last2; ++it1, ++it2) {
            if (!comp(*it1, *it2)) return false;
        }

        return it2 == last2;
    }

    template<typename S1, typename S2>
    bool
    ends_with_str(S1&& s1, S2&& s2)
    {
        if constexpr (has_member<S1, S2>) {
            return s1.ends_with(s2);
        }
        else {
            return ends_with_str(s1, s2, str::equal_to<str_char_t<S1>>());
        }
    }

    struct api
    {
        template<bidirectional_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c) const
        {
            return ends_with_char(s, c);
        }

        template<bidirectional_string S, character C, typename ComparatorT>
            requires is_same_char_v<S, C>
                  && char_comparator<ComparatorT, C>
        bool
        operator()(S&& s, C const c, ComparatorT comp) const
        {
            return ends_with_char(s, c, comp);
        }

        template<bidirectional_string S1, bidirectional_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2) const
        {
            return ends_with_str(s1, s2);
        }

        template<bidirectional_string S1, bidirectional_string S2, typename ComparatorT>
            requires is_same_char_v<S1, S2>
                  && char_comparator<ComparatorT, str_char_t<S1>>
        bool
        operator()(S1&& s1, S2&& s2, ComparatorT comp) const
        {
            return ends_with_str(s1, s2, comp);
        }
    };

} // namespace _ends_with

inline constexpr _ends_with::api ends_with;

namespace _iends_with {

    struct api
    {
        template<bidirectional_string S, character C>
            requires is_same_char_v<S, C>
        bool
        operator()(S&& s, C const c, std::locale const& loc = {}) const
        {
            auto comp = iequal_to<C>(loc);
            return str::ends_with(s, c, comp);
        }

        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        bool
        operator()(S1&& s1, S2&& s2, std::locale const& loc = {}) const
        {
            auto comp = iequal_to<str_char_t<S1>>(loc);
            return str::ends_with(s1, s2, comp);
        }
    };

} // namespace _iends_with

inline constexpr _iends_with::api iends_with;

} // namespace stream9::strings

#endif // STRINGS_QUERY_ENDS_WITH_HPP
