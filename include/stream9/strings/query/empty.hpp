#ifndef STRINGS_QUERY_EMPTY_HPP
#define STRINGS_QUERY_EMPTY_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../detail/pointer.hpp"

#include <stream9/ranges/empty.hpp>

namespace stream9::strings {

namespace empty_ {

    struct api {

        template<character C>
        constexpr bool
        operator()(C* const& s) const
        {
            return *s == null_char_v<C>;
        }

        template<character C, std::size_t N>
        constexpr bool
        operator()(C (&s)[N]) const
        {
            return detail::arr_length(s) == 0;
        }

        template<character C, std::size_t N>
        constexpr bool
        operator()(C (&&s)[N]) const
        {
            return detail::arr_length(s) == 0;
        }

        template<character_range S>
        constexpr bool
        operator()(S&& s) const
        {
            return rng::empty(s);
        }

    };

} // namespace empty_

constexpr empty_::api empty;

} // namespace stream9::strings

#endif // STRINGS_QUERY_EMPTY_HPP
