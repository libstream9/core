#ifndef STREAM9_STRINGS_QUERY_SUFFIX_HPP
#define STREAM9_STRINGS_QUERY_SUFFIX_HPP

#include "substr.hpp"

#include "../namespace.hpp"

namespace stream9::strings {

namespace suffix_ {

    struct api
    {
        template<random_access_string S>
        static string_range<iterator_t<S>>
        operator()(S&& s, str_size_t<S> n) noexcept
        // [[expects: 0 <= n]]
        {
            if (str::size(s) < n) return s;
            else {
                auto i = str::iterator_at(s, str::size(s) - n);
                return { i, str::end(s) };
            }
        }
    };

} // namespace suffix_

inline constexpr suffix_::api suffix;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_SUFFIX_HPP

