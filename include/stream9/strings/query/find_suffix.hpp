#ifndef STREAM9_STRINGS_QUERY_FIND_SUFFIX_HPP
#define STREAM9_STRINGS_QUERY_FIND_SUFFIX_HPP

#include "../namespace.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"

namespace stream9::strings {

/*
 * find_suffix
 */
namespace find_suffix_ {

    struct api
    {
        template<bidirectional_string S1, bidirectional_string S2>
        string_range<iterator_t<S1>>
        operator()(S1& s1, S2 const& suffix) const noexcept
        {
            auto f1 = str::rbegin(s1);
            auto l1 = str::rend(s1);
            auto f2 = str::rbegin(suffix);
            auto l2 = str::rend(suffix);

            for (; f1 != l1 && f2 != l2; ++f1, ++f2) {
                if (*f1 != *f2) break;
            }

            if (f2 == l2) {
                return { f1.base(), str::end(s1) };
            }
            else {
                return { str::end(s1), str::end(s1) };
            }
        }
    };

} // namespace find_suffix_

inline constexpr find_suffix_::api find_suffix;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_QUERY_FIND_SUFFIX_HPP
