#ifndef STREAM9_STRINGS_STRING_HPP
#define STREAM9_STRINGS_STRING_HPP

#include "namespace.hpp"
#include "stream.hpp"

#include "core/concepts.hpp"
#include "core/string_range.hpp"

#include <stream9/optional.hpp>
#include <stream9/safe_integer.hpp>
#include <stream9/string_view.hpp>
#include <stream9/strings/error.hpp>

#include <iostream>
#include <string>
#include <string_view>
#include <type_traits>

namespace stream9 {

template<typename> struct basic_string_base;

template<str::character Ch>
class basic_string : basic_string_base<Ch>
{
    static_assert(!std::is_const_v<Ch>);

    using base_type = std::basic_string<Ch>;
public:
    using value_type = Ch;
    using pointer = Ch*;
    using const_pointer = Ch const*;
    using iterator = char*;
    using const_iterator = char const*;
    using size_type = safe_integer<intmax_t, 0>;
    using index_type = safe_integer<intmax_t>;
    using reference = Ch&;
    using const_reference = Ch const&;

public:
    // essentials
    basic_string() noexcept = default;

    basic_string(Ch) noexcept;

    basic_string& operator=(Ch) noexcept;

    template<str::readable_string S>
    basic_string(S const&)
        requires str::is_same_char_v<Ch, S>;

    template<str::readable_string S>
    basic_string& operator=(S const&)
        requires str::is_same_char_v<Ch, S>;

    template<std::input_iterator I>
    basic_string(I first, I last)
        requires str::is_same_char_v<Ch, I>;

    template<std::contiguous_iterator I>
    basic_string(I first, size_type length)
        requires str::is_same_char_v<Ch, I>;

    basic_string(base_type&& s) noexcept;
    basic_string& operator=(base_type&& s) noexcept;

    basic_string(std::nullptr_t) = delete;
    basic_string& operator=(std::nullptr_t) = delete;

    basic_string(basic_string const&) = default;
    basic_string& operator=(basic_string const&) = default;

    basic_string(basic_string&&) noexcept = default;
    basic_string& operator=(basic_string&&) noexcept = default;

    ~basic_string() noexcept = default;

    template<typename C>
    friend void swap(basic_string<C>&, basic_string<C>&) noexcept;

    // conversion
    operator std::basic_string_view<Ch> () const noexcept;

    explicit operator base_type () const& noexcept;
    operator base_type () && noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    iterator begin() noexcept;
    iterator end() noexcept;

    Ch const& operator[](index_type i) const;
    Ch& operator[](index_type i);

    Ch const* c_str() const noexcept; // non-null

    Ch const* data() const noexcept; // non-null
    Ch* data() noexcept; // non-null

    // query
    size_type size() const noexcept;

    bool empty() const noexcept;

    size_type capacity() const noexcept;

    // modifier
    iterator insert(Ch);

    template<str::readable_string S>
        str::string_range<iterator>
    insert(S const&)
        requires (str::is_same_char_v<Ch, S>);

    iterator insert(const_iterator, Ch);

    template<str::readable_string S>
        str::string_range<iterator>
    insert(const_iterator, S const&)
        requires (str::is_same_char_v<Ch, S>);

    iterator append(Ch);

    template<str::readable_string S>
        str::string_range<iterator>
    append(S const&)
        requires (str::is_same_char_v<Ch, S>);

    iterator prepend(Ch);

    template<str::readable_string S>
        str::string_range<iterator>
    prepend(S const&)
        requires (str::is_same_char_v<Ch, S>);

    iterator erase(const_iterator) noexcept;
    iterator erase(const_iterator, const_iterator) noexcept;

    size_type remove_prefix(size_type n) noexcept;
    size_type remove_suffix(size_type n) noexcept;

    void clear() noexcept;

    void resize(size_type);
    void resize(size_type, Ch);

    void reserve(size_type);

    void shrink_to_fit();

    // comparison
    bool operator==(basic_string const&) const noexcept = default;

    template<str::readable_string S>
    bool operator==(S const& s) const noexcept
        requires str::is_same_char_v<Ch, S>;

    auto operator<=>(basic_string const&) const noexcept = default;

    template<str::readable_string S>
    auto operator<=>(S const& s) const noexcept
        requires str::is_same_char_v<Ch, S>;

    template<typename T>
    auto operator<=>(T const& s) const
        requires (!str::readable_string<T>) = delete;

    template<str::character Ch2, typename Traits>
    friend std::basic_ostream<Ch2, Traits>&
    operator<<(std::basic_ostream<Ch2, Traits>&, basic_string<Ch2> const&);

    template<str::character Ch2, typename Traits>
    friend std::basic_istream<Ch2, Traits>&
    operator>>(std::basic_istream<Ch2, Traits>&, basic_string<Ch2>&);

private:
    base_type m_str;
};

template<str::character Ch>
basic_string<Ch>&
operator+=(basic_string<Ch>&, Ch);

template<str::character Ch, str::readable_string S>
basic_string<Ch>&
operator+=(basic_string<Ch>&, S const&)
    requires str::is_same_char_v<Ch, S>;

template<str::readable_string S>
basic_string<str::character_t<S>>
operator+(S&&, str::character_t<S>);

template<str::readable_string S>
basic_string<str::character_t<S>>
operator+(str::character_t<S>, S const&);

template<str::readable_string S1, str::readable_string S2>
basic_string<str::character_t<S1>>
operator+(S1&&, S2&&)
    requires str::is_same_char_v<S1, S2>;

template<str::character Ch, str::std_ostream_streamable T>
str::ostream<basic_string<Ch>>
operator<<(basic_string<Ch>&, T const&);

template<str::character Ch, str::std_istream_streamable T>
str::istream<basic_string<Ch>>
operator>>(basic_string<Ch>&, T&);

// alias
using string = basic_string<char>;

} // namespace stream9

#include <utility>

namespace std {

template<typename C>
struct tuple_size<stream9::basic_string<C>>
{
    static constexpr auto value = 2;
};

template<typename C>
struct tuple_element<0, stream9::basic_string<C>>
{
    using type = typename stream9::basic_string<C>::const_iterator;
};

template<typename C>
struct tuple_element<1, stream9::basic_string<C>>
{
    using type = typename stream9::basic_string<C>::const_iterator;
};

} // namespace std

namespace stream9 {

template<size_t I, typename Ch>
auto
get(basic_string<Ch> const& s) noexcept
    requires (I < 2)
{
    if constexpr (I == 0) {
        return s.begin();
    }
    else {
        return s.end();
    }
}

} // namespace stream9

#endif // STREAM9_STRINGS_STRING_HPP

#include "string.ipp"
