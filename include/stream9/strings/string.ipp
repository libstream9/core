#ifndef STREAM9_STRINGS_STRING_IPP
#define STREAM9_STRINGS_STRING_IPP

#include "accessor/at.hpp"

namespace stream9 {

template<typename Ch>
struct basic_string_base
{
    using base_type = std::basic_string<Ch>;

    Ch*
    do_insert_ch(base_type& s, Ch const* pos, Ch c)
    {
        try {
            typename base_type::const_iterator i { pos };

            return s.insert(i, c).base();
        }
        catch (...) {
            rethrow_error();
        }
    }

    template<typename S>
    str::string_range<Ch*>
    do_insert_str(base_type& s1, Ch const* pos, S const& s2)
    {
        try {
            typename base_type::iterator i1 { const_cast<Ch*>(pos) };

            if constexpr (str::sized_string<S>) {
                i1 = s1.insert(i1, str::begin(s2), str::end(s2));
                return { i1.base(), i1.base() + str::size(s2) };
            }
            else {
                auto e2 = str::end(s2);
                for (auto i2 = str::begin(s2); i2 != e2; ++i2) {
                    i1 = s1.insert(i1, *i2);
                    ++i1;
                }
                return { s1.begin().base(), i1.base() };
            }
        }
        catch (...) {
            rethrow_error();
        }
    }

    bool operator==(basic_string_base const&) const noexcept = default;
    auto operator<=>(basic_string_base const&) const noexcept = default;
};

template<str::character Ch>
basic_string<Ch>::
basic_string(Ch c) noexcept
    : m_str { c }
{}

template<str::character Ch>
basic_string<Ch>& basic_string<Ch>::
operator=(Ch c) noexcept
{
    m_str = c;
    return *this;
}

template<str::character Ch>
template<str::readable_string S>
basic_string<Ch>::
basic_string(S const& s)
    requires str::is_same_char_v<Ch, S>
{
    try {
        if constexpr (str::contiguous_string<S>) {
            m_str.assign(str::data(s), static_cast<base_type::size_type>(str::size(s)));
        }
        else {
            if constexpr (str::sized_string<S>) {
                m_str.reserve(str::size(s));
            }
            for (auto c: s) {
                m_str.push_back(c);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
basic_string<Ch>& basic_string<Ch>::
operator=(S const& s)
    requires str::is_same_char_v<Ch, S>
{
    try {
        if constexpr (str::contiguous_string<S>) {
            m_str.assign(str::data(s), static_cast<base_type::size_type>(str::size(s)));
        }
        else {
            m_str.assign(str::begin(s), str::end(s));
        }

        return *this;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<std::input_iterator I>
basic_string<Ch>::
basic_string(I first, I last)
    requires str::is_same_char_v<Ch, I>
    try : m_str { first, last }
{}
catch (...) {
    rethrow_error();
}

template<str::character Ch>
template<std::contiguous_iterator I>
basic_string<Ch>::
basic_string(I first, size_type length)
    requires str::is_same_char_v<Ch, I>
    try : m_str { first, first + length }
{}
catch (...) {
    rethrow_error();
}

template<str::character Ch>
basic_string<Ch>::
basic_string(base_type&& s) noexcept
    : m_str { std::move(s) }
{}

template<str::character Ch>
basic_string<Ch>& basic_string<Ch>::
operator=(base_type&& s) noexcept
{
    m_str = std::move(s);
    return *this;
}

template<typename C>
void
swap(basic_string<C>& s1, basic_string<C>& s2) noexcept
{
    s1.m_str.swap(s2.m_str);
}

template<str::character Ch>
basic_string<Ch>::
operator std::basic_string_view<Ch> () const noexcept
{
    return m_str;
}

template<str::character Ch>
basic_string<Ch>::
operator base_type () const& noexcept
{
    return m_str;
}

template<str::character Ch>
basic_string<Ch>::
operator base_type () && noexcept
{
    return std::move(m_str);
}

template<str::character Ch>
basic_string<Ch>::const_iterator basic_string<Ch>::
begin() const noexcept
{
    return m_str.begin().base();
}

template<str::character Ch>
basic_string<Ch>::const_iterator basic_string<Ch>::
end() const noexcept
{
    return m_str.end().base();
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
begin() noexcept
{
    return m_str.begin().base();
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
end() noexcept
{
    return m_str.end().base();
}

template<str::character Ch>
Ch const& basic_string<Ch>::
operator[](index_type i) const
{
    return str::at(*this, i);
}

template<str::character Ch>
Ch& basic_string<Ch>::
operator[](index_type i)
{
    return str::at(*this, i);
}

template<str::character Ch>
Ch const* basic_string<Ch>::
c_str() const noexcept
{
    return m_str.c_str();
}

template<str::character Ch>
Ch const* basic_string<Ch>::
data() const noexcept
{
    return m_str.data();
}

template<str::character Ch>
Ch* basic_string<Ch>::
data() noexcept
{
    return m_str.data();
}

template<str::character Ch>
basic_string<Ch>::size_type basic_string<Ch>::
size() const noexcept
{
    return m_str.size();
}

template<str::character Ch>
bool basic_string<Ch>::
empty() const noexcept
{
    return m_str.empty();
}

template<str::character Ch>
basic_string<Ch>::size_type basic_string<Ch>::
capacity() const noexcept
{
    return m_str.capacity();
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
insert(Ch c)
{
    try {
        return this->do_insert_ch(m_str, end(), c);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
str::string_range<typename basic_string<Ch>::iterator> basic_string<Ch>::
insert(S const& s)
    requires (str::is_same_char_v<Ch, S>)
{
    try {
        return this->do_insert_str(m_str, end(), s);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
insert(const_iterator pos, Ch c)
{
    try {
        return this->do_insert_ch(m_str, pos, c);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
str::string_range<typename basic_string<Ch>::iterator> basic_string<Ch>::
insert(const_iterator pos, S const& s)
    requires (str::is_same_char_v<Ch, S>)
{
    try {
        return this->do_insert_str(m_str, pos, s);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
append(Ch c)
{
    try {
        return this->do_insert_ch(m_str, m_str.end().base(), c);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
str::string_range<typename basic_string<Ch>::iterator> basic_string<Ch>::
append(S const& s)
    requires (str::is_same_char_v<Ch, S>)
{
    try {
        return this->do_insert_str(m_str, m_str.end().base(), s);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
prepend(Ch c)
{
    try {
        return this->do_insert_ch(m_str, m_str.begin().base(), c);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
str::string_range<typename basic_string<Ch>::iterator> basic_string<Ch>::
prepend(S const& s)
    requires (str::is_same_char_v<Ch, S>)
{
    try {
        return this->do_insert_str(m_str, m_str.begin().base(), s);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
erase(const_iterator pos) noexcept
{
    typename base_type::const_iterator i { pos };

    return m_str.erase(i).base();
}

template<str::character Ch>
basic_string<Ch>::iterator basic_string<Ch>::
erase(const_iterator first, const_iterator last) noexcept
{
    typename base_type::const_iterator f { first };
    typename base_type::const_iterator l { last };

    return m_str.erase(f, l).base();
}

template<str::character Ch>
basic_string<Ch>::size_type basic_string<Ch>::
remove_prefix(size_type n) noexcept
{
    if (n < size()) {
        auto f = m_str.begin();
        auto l = f + n;
        m_str.erase(f, l);
        return n;
    }
    else {
        auto n = size();
        clear();
        return n;
    }
}

template<str::character Ch>
basic_string<Ch>::size_type basic_string<Ch>::
remove_suffix(size_type n) noexcept
{
    if (n < size()) {
        auto f = m_str.end() - n;
        auto l = m_str.end();
        m_str.erase(f, l);
        return n;
    }
    else {
        n = size();
        clear();
        return n;
    }
}

template<str::character Ch>
void basic_string<Ch>::
clear() noexcept
{
    m_str.clear();
}

template<str::character Ch>
void basic_string<Ch>::
resize(size_type n)
{
    try {
        m_str.resize(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
void basic_string<Ch>::
resize(size_type n, Ch c)
{
    try {
        m_str.resize(n, c);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
void basic_string<Ch>::
reserve(size_type n)
{
    try {
        m_str.reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
void basic_string<Ch>::
shrink_to_fit()
{
    try {
        m_str.shrink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch>
template<str::readable_string S>
bool basic_string<Ch>::
operator==(S const& s) const noexcept
    requires str::is_same_char_v<Ch, S>
{
    return std::equal(begin(), end(), str::begin(s), str::end(s));
}

template<str::character Ch>
template<str::readable_string S>
auto basic_string<Ch>::
operator<=>(S const& s) const noexcept
    requires str::is_same_char_v<Ch, S>
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        str::begin(s), str::end(s)
    );
}

/*
 * free functions
 */
template<str::character Ch>
basic_string<Ch>&
operator+=(basic_string<Ch>& s, Ch c)
{
    try {
        s.append(c);

        return s;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch, str::readable_string S>
basic_string<Ch>&
operator+=(basic_string<Ch>& s1, S const& s2)
    requires str::is_same_char_v<Ch, S>
{
    try {
        s1.append(s2);

        return s1;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::readable_string S>
basic_string<str::character_t<S>>
operator+(S&& s, str::character_t<S> c)
{
    try {
        basic_string<str::character_t<S>> rv { std::forward<S>(s) };
        rv.append(c);

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::readable_string S>
basic_string<str::character_t<S>>
operator+(str::character_t<S> c, S const& s)
{
    try {
        basic_string<str::character_t<S>> rv = c;
        rv.append(s);

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::readable_string S1, str::readable_string S2>
basic_string<str::character_t<S1>>
operator+(S1&& s1, S2&& s2)
    requires str::is_same_char_v<S1, S2>
{
    try {
        basic_string<str::character_t<S1>> rv { std::forward<S1>(s1) };
        rv.append(s2);

        return rv;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch, typename Traits>
std::basic_ostream<Ch, Traits>&
operator<<(std::basic_ostream<Ch, Traits>& os, basic_string<Ch> const& s)
{
    try {
        return os << s.m_str;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch, typename Traits>
std::basic_istream<Ch, Traits>&
operator>>(std::basic_istream<Ch, Traits>& is, basic_string<Ch>& s)
{
    try {
        return is >> s.m_str;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch, str::std_ostream_streamable T>
str::ostream<basic_string<Ch>>
operator<<(basic_string<Ch>& s, T const& v)
{
    try {
        str::ostream<basic_string<Ch>> os { s };
        os << v;
        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

template<str::character Ch, str::std_istream_streamable T>
str::istream<basic_string<Ch>>
operator>>(basic_string<Ch>& s, T& v)
{
    try {
        str::istream<basic_string<Ch>> is { s };
        is >> v;
        return is;
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9

#endif // STREAM9_STRINGS_STRING_IPP
