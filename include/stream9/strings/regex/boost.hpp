#ifndef STRINGS_REGEX_BOOST_HPP
#define STRINGS_REGEX_BOOST_HPP

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/regex.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include <boost/regex.hpp>

namespace stream9::strings {

template<typename CharT, typename Traits>
struct regex_adaptor<boost::basic_regex<CharT, Traits>>
{
    using char_type = CharT;

    template<bidirectional_string S>
        requires is_same_char_v<S, CharT>
    static string_range<iterator_t<S>>
    find(S&& s, boost::basic_regex<CharT, Traits> const& e)
    {
        boost::match_results<iterator_t<S>> result;

        auto const match =
            boost::regex_search(str::begin(s), str::end(s), result, e);
        if (match) {
            assert(!result.empty());

            return { result[0].first, result[0].second };
        }
        else {
            auto const last = str::end(s);

            return { last, last };
        }
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_BOOST_HPP
