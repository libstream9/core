#ifndef STRINGS_REGEX_POSIX_HPP
#define STRINGS_REGEX_POSIX_HPP

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/regex.hpp"

#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/iterator_at.hpp"
#include "../query/size.hpp"

#include <regex.h>

namespace stream9::strings {

template<>
struct regex_adaptor<regex_t>
{
    using char_type = char;

    template<contiguous_string S>
        requires is_same_char_v<S, char>
    static string_range<iterator_t<S>>
    find(S&& s, regex_t const& re)
    {
        namespace str = strings;

        auto ptr = str::data(s);
        auto len = str::length(s);

        if (len == 0) {
            auto const last = str::end(s);
            return { last, last };
        }

        std::string buf;

        if (ptr[len] != '\0') {
            buf.assign(ptr, static_cast<size_t>(len));
            ptr = buf.data();
        }

        regmatch_t m;
        int constexpr flag = 0;

        for (auto i = 0; i < len; ++i) {
            int const rc = ::regexec(&re, ptr + i, 1, &m, flag);

            if (rc == 0) {
                auto const first = str::iterator_at(s, i + m.rm_so);
                auto const last = str::iterator_at(s, i + m.rm_eo);

                return { first, last };
            }
        }

        auto const last = str::end(s);
        return { last, last };
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_POSIX_HPP
