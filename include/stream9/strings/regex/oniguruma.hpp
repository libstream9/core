#ifndef STRINGS_REGEX_ONIGURUMA_HPP
#define STRINGS_REGEX_ONIGURUMA_HPP

#include "../core/concepts.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/length.hpp"

#include <oniguruma.h>

namespace stream9::strings {

template<>
struct regex_adaptor<regex_t*>
{
    using char_type = char;

    class region
    {
    public:
        region()
            : m_ptr { ::onig_region_new() }
        {}

        ~region()
        {
            ::onig_region_free(m_ptr, 1);
        }

        auto get() const { return m_ptr; }

        auto operator->() const { return m_ptr; }

    private:
        OnigRegion* m_ptr;
    };

    template<contiguous_string S>
        requires is_same_char_v<S, char>
    static string_range<iterator_t<S>>
    find(S&& s, regex_t* const re)
    {
        region r;
        auto const begin = reinterpret_cast<unsigned char const*>(str::data(s));
        auto const end = begin + str::length(s);

        auto const rv = ::onig_search(
                    re, begin, end, begin, end, r.get(), ONIG_OPTION_NONE);
        if (rv >= 0) {
            auto const first = str::begin(s) + r->beg[0];
            auto const last = str::begin(s) + r->end[0];
            return { first, last };
        }
        else if (rv == ONIG_MISMATCH) {
            auto const last = str::end(s);
            return { last, last };
        }
        else {
            //TODO throw error
            throw "TODO error";
        }
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_ONIGURUMA_HPP
