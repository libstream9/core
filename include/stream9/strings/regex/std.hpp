#ifndef STRINGS_REGEX_STD_HPP
#define STRINGS_REGEX_STD_HPP

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/regex.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include <cassert>
#include <regex>

namespace stream9::strings {

template<typename CharT, typename Traits>
struct regex_adaptor<std::basic_regex<CharT, Traits>>
{
    using char_type = CharT;

    template<bidirectional_string S>
        requires is_same_char_v<S, CharT>
    static string_range<iterator_t<S>>
    find(S&& s, std::basic_regex<CharT, Traits> const& e)
    {
        std::match_results<iterator_t<S>> result;

        auto const match =
            std::regex_search(str::begin(s), str::end(s), result, e);
        if (match) {
            assert(!result.empty());

            return { result[0].first, result[0].second };
        }
        else {
            auto const last = str::end(s);

            return { last, last };
        }
    }
};

} // namespace stream9::strings

#endif // STRINGS_REGEX_STD_HPP
