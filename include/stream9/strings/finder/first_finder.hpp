#ifndef STRINGS_FINDER_FIRST_FINDER_HPP
#define STRINGS_FINDER_FIRST_FINDER_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/char/comparator.hpp"
#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/data.hpp"
#include "../accessor/end.hpp"
#include "../query/iterator_at.hpp"
#include "../query/size.hpp"

#include <algorithm>
#include <functional>
#include <iterator>

#include <stream9/iterators/next.hpp>

namespace stream9::strings {

namespace _first_finder {

    template<typename T>
    inline constexpr bool std_string_ch = requires (T s, str_char_t<T> c) {
        s.find(c) == s.npos;
    };

    template<typename T>
    inline constexpr bool std_string_str = requires (T s, str_char_t<T>* ptr) {
        s.find(ptr, 0, 0) == s.npos;
    };

    template<typename Arg1, typename Arg2> class first_finder;

    /*
     * find character
     */
    template<typename S, typename C, typename ComparatorT>
    string_range<iterator_t<S>>
    find_char(S&& s, C const c, ComparatorT comp)
    {
        using std::ranges::find_if;

        string_range s_ = s;
        auto last = str::end(s);

        auto it = find_if(s_,
            [&](auto ch) { return comp(c, ch); }
        );
        if (it == str::end(s_)) {
            return { last, last };
        }
        else {
            auto next = iter::next(it);

            return { it, next };
        }
    }

    template<typename S, typename C>
    string_range<iterator_t<S>>
    find_char(S&& s, C const c)
    {
        if constexpr (std_string_ch<S>) {
            auto const last = str::end(s);
            auto const idx = s.find(c);
            if (idx == s.npos) {
                return { last, last };
            }
            else {
                auto const first = iterator_at(s, idx);
                auto const next = first + 1;

                return { first, next };
            }
        }
        else {
            return find_char(s, c, str::equal_to<C>());
        }
    }

    /*
     * public interface
     */
    template<character C>
    class first_finder<C, void>
    {
    public:
        first_finder(C const target)
            : m_target { target }
        {}

        template<forward_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s) const
        {
            return find_char(s, m_target);
        }

    private:
        C m_target;
    };

    template<character C>
    first_finder(C) -> first_finder<C, void>;

    template<character C, typename ComparatorT>
        requires char_comparator<ComparatorT, C>
    class first_finder<C, ComparatorT>
    {
    public:
        first_finder(C const target, ComparatorT comp)
            : m_target { target }
            , m_comp { comp }
        {}

        template<forward_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s) const
        {
            return find_char(s, m_target, m_comp);
        }

    private:
        C m_target;
        ComparatorT m_comp;
    };

    template<character C, typename ComparatorT>
    first_finder(C, ComparatorT) -> first_finder<C, ComparatorT>;

    /*
     * find by classifier
     */
    template<character C, classifier<C> Fn>
    class first_finder<C, Fn>
    {
    public:
        first_finder(Fn pred)
            : m_pred { std::move(pred) }
        {}

        template<forward_string S>
            requires is_same_char_v<S, C>
        string_range<iterator_t<S>>
        operator()(S&& s_) const
        {
            using std::ranges::find_if;

            string_range s = s_;
            auto it1 = find_if(s, m_pred);
            auto last = str::end(s);

            if (it1 == last) {
                return { last, last };
            }
            else {
                auto it2 = std::next(it1);

                return { it1, it2 };
            }
        }

    private:
        std::function<bool(C)> m_pred;
    };

    template<typename Fn>
        requires classifier<Fn, char>
    first_finder(Fn) -> first_finder<char, Fn>;

    /*
     * find substring
     */
    template<typename S1, typename S2, typename ComparatorT>
    string_range<iterator_t<S1>>
    find_substr(S1&& s1, S2&& s2, ComparatorT comp)
    {
        using std::ranges::search;

        string_range s1_ = s1;
        string_range s2_ = s2;

        return search(s1_, s2_, comp);
    }

    template<typename S1, typename S2>
    string_range<iterator_t<S1>>
    find_substr(S1&& s1, S2&& s2)
    {
        if constexpr (std_string_str<S1> && contiguous_string<S2>) {
            auto const n = str::size(s2);
            auto const idx = s1.find(str::data(s2), 0, to_size<S1>(n));
            if (idx == s1.npos) {
                auto const last = str::end(s1);

                return { last, last };
            }
            else {
                auto const first = iterator_at(s1, idx);
                auto const last = std::next(first, to_difference<S1>(n));

                return { first, last };
            }
        }
        else {
            return find_substr(s1, s2, str::equal_to<str_char_t<S1>>());
        }
    }

    /*
     * public interface
     */
    template<forward_string S>
    class first_finder<S, void>
    {
    public:
        template<forward_string T>
        first_finder(T&& pattern)
            : m_pattern { pattern }
        {}

        template<forward_string T>
        string_range<iterator_t<T>>
        operator()(T&& s) const
        {
            return find_substr(s, m_pattern);
        }

    private:
        string_range<iterator_t<S>> m_pattern;
    };

    template<forward_string S>
    first_finder(S&&) -> first_finder<_unref<S>, void>;

    template<forward_string S, typename ComparatorT>
        requires char_comparator<ComparatorT, str_char_t<S>>
    class first_finder<S, ComparatorT>
    {
    public:
        template<forward_string T>
        first_finder(T&& pattern, ComparatorT comp)
            : m_pattern { pattern }
            , m_comp { comp }
        {}

        template<forward_string T>
        string_range<iterator_t<T>>
        operator()(T&& s) const
        {
            return find_substr(s, m_pattern, m_comp);
        }

    private:
        string_range<iterator_t<S>> m_pattern;
        ComparatorT m_comp;
    };

    template<forward_string S, typename ComparatorT>
    first_finder(S&&, ComparatorT) -> first_finder<_unref<S>, ComparatorT>;

} // namespace _first_finder

using _first_finder::first_finder;

} // namespace stream9::strings

#endif // STRINGS_FINDER_FIRST_FINDER_HPP
