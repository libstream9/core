#ifndef STRINGS_FINDER_REGEX_FINDER_HPP
#define STRINGS_FINDER_REGEX_FINDER_HPP

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../accessor/end.hpp"

namespace stream9::strings {

namespace _regex_finder {

    template<regexp R>
    class regex_finder
    {
    public:
        regex_finder(R e)
            : m_regex { std::move(e) }
        {}

        template<bidirectional_string T>
            requires is_same_char_v<T, R>
        string_range<iterator_t<T>>
        operator()(T&& s) const
        {
            auto const r = regex_adaptor<_uncvref<R>>::find(s, m_regex);

            if (r) {
                return r;
            }
            else {
                auto const last = str::end(s);
                return { last, last };
            }
        }

    private:
        R m_regex;
    };

} // namespace _regex_finder

using _regex_finder::regex_finder;

} // namespace stream9::strings

#endif // STRINGS_FINDER_REGEX_FINDER_HPP
