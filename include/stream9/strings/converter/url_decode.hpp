#ifndef STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP
#define STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP

#include "../error.hpp"
#include "../namespace.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"
#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"
#include "../query/size.hpp"

#include <string>
#include <system_error>

namespace stream9::strings {

namespace url_decode_ {

    inline int
    hex_to_int(char const c)
    {
        if ('0' <= c && c <= '9') {
            return c - '0';
        }
        else if ('A' <= c && c <= 'Z') {
            return 10 + c - 'A';
        }
        else if ('a' <= c && c <= 'z') {
            return 10 + c - 'a';
        }
        else {
            throw_error(errc::invalid_hex_character, {
                { "c", c },
            });
        }
    }

    struct api
    {
        template<str::readable_string S>
        std::basic_string<str_char_t<S>>
        operator()(S&& s) const
        {
            using Ch = str_char_t<S>;

            try {
                std::basic_string<Ch> result;
                result.reserve(str::size(s));

                for (auto it = str::begin(s); it != str::end(s);) {
                    auto const c = *it;
                    if (c != '%') {
                        result.push_back(c);
                        ++it;
                    }
                    else {
                        int decoded;

                        ++it;
                        decoded = hex_to_int(*it) << 4;

                        ++it;
                        decoded |= hex_to_int(*it);

                        result.push_back(static_cast<Ch>(decoded));
                        ++it;
                    }
                }
                return result;
            }
            catch (...) {
                rethrow_error({ { "s", s }, }); //TODO convert s_ to utf8
            }
        }
    };

} // namespace url_decode_

inline constexpr url_decode_::api url_decode;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONVERTER_URL_DECODE_HPP
