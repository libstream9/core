#ifndef STREAM9_STRINGS_CONVERTER_URL_ENCODE_HPP
#define STREAM9_STRINGS_CONVERTER_URL_ENCODE_HPP

#include "../error.hpp"
#include "../namespace.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include <string>

#include <stream9/errors.hpp>

namespace stream9::strings {

namespace url_encode_ {

    inline bool
    is_unreserved(char const c) noexcept
    {
        if ('a' <= c && c <= 'z') return true;
        if ('A' <= c && c <= 'Z') return true;
        if ('0' <= c && c <= '9') return true;
        if (c == '-' || c == '_' || c == '.' || c == '~') return true;

        return false;
    }

    template<typename Ch>
    inline void
    append_percent_encode_char(std::basic_string<Ch>& s, Ch const c) noexcept
    {
        static Ch const tbl[] {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9',
            'A', 'B', 'C', 'D', 'E', 'F',
        };

        s.push_back('%');
        s.push_back(tbl[(c >> 4) & 0x0f]);
        s.push_back(tbl[(c & 0x0f)]);
    }

    struct api
    {
        template<str::readable_string S>
        std::basic_string<str_char_t<S>>
        operator()(S&& s_) const
        {
            using Ch = str_char_t<S>;

            try {
                str::string_range s { s_ };
                std::basic_string<Ch> result;
                result.reserve(s.size());

                for (auto const c: s) {
                    if (is_unreserved(c)) {
                        result.push_back(c);
                    }
                    else {
                        append_percent_encode_char(result, c);
                    }
                }

                return result;
            }
            catch (...) {
                rethrow_error({ { "s", s_ }, }); //TODO convert s_ to utf8
            }
        }
    };

} // namespace url_encode_

inline constexpr url_encode_::api url_encode;

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONVERTER_URL_ENCODE_HPP
