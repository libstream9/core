#ifndef STRINGS_MODIFIER_APPEND_HPP
#define STRINGS_MODIFIER_APPEND_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../accessor/end.hpp"
#include "../modifier/insert.hpp"

namespace stream9::strings {

namespace _append {

    template<typename S, typename C>
    concept can_invoke_insert_with_ch =
        requires (S&& s, C c) {
            str::insert(s, str::end(s), c);
        };

    template<typename S1, typename S2>
    concept can_invoke_insert_with_str =
        requires (S1&& s1, S2&& s2) {
            str::insert(s1, str::end(s1), s2);
        };

    template<typename S1, typename C>
    concept has_member_append_ch =
        requires (S1& s1, C c) {
            s1.append(c);
        };

    template<typename S1, typename S2>
    concept has_member_append_str =
        requires (S1& s1, S2 const& s2) {
            s1.append(s2);
        };

    struct api
    {
        template<string S, character C>
        static decltype(auto)
        operator()(S& s, C c)
            requires is_same_char_v<S, C>
                  && has_member_append_ch<S, C>
        {
            return s.append(c);
        }

        template<string S1, string S2>
        static decltype(auto)
        operator()(S1& s1, S2 const& s2)
            requires is_same_char_v<S1, S2>
                  && has_member_append_str<S1, S2>
        {
            return s1.append(s2);
        }

        template<string S, character C>
        static void
        operator()(S& s, C c)
            requires is_same_char_v<S, C>
                  && (!has_member_append_ch<S, C>)
                  && can_invoke_insert_with_ch<S, C>
        {
            str::insert(s, str::end(s), c);
        }

        template<string S1, string S2>
        static void
        operator()(S1& s1, S2&& s2)
            requires is_same_char_v<S1, S2>
                  && (!has_member_append_str<S1, S2>)
                  && can_invoke_insert_with_str<S1, S2>
        {
            str::insert(s1, str::end(s1), s2);
        }
    };

} // namespace _append

inline constexpr _append::api append;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_APPEND_HPP
