#ifndef STRINGS_MODIFIER_TO_LOWER_HPP
#define STRINGS_MODIFIER_TO_LOWER_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../accessor/data.hpp"
#include "../query/size.hpp"

#include "../detail/ctype_facet.hpp"

#include <locale>

namespace stream9::strings {

namespace _to_lower {

    template<contiguous_string S>
    void
    to_lower(S& s, std::locale const& loc)
    {
        auto& f = detail::ctype_facet<S>(loc);

        auto* const first = str::data(s);
        auto* const last = first + str::size(s);

        f.tolower(first, last);
    }

    template<typename S>
        requires (!contiguous_string<S>)
    void
    to_lower(S& s, std::locale const& loc)
    {
        auto& f = detail::ctype_facet<S>(loc);

        for (auto& c: s) {
            c = f.tolower(c);
        }
    }

    struct api
    {
        template<writable_string S>
        void
        operator()(S& s, std::locale const& loc = {}) const
        {
            to_lower(s, loc);
        }
    };

} // namespace _to_lower

inline constexpr _to_lower::api to_lower;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TO_LOWER_HPP
