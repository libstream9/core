#ifndef STRINGS_MODIFIER_TRIM_RIGHT_HPP
#define STRINGS_MODIFIER_TRIM_RIGHT_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include "remove_suffix.hpp"

#include <algorithm>
#include <concepts>
#include <locale>

namespace stream9::strings {

namespace _trim_right {

    template<typename S, typename I>
    concept has_remove_suffix =
        requires (S s, I i) {
            str::remove_suffix(s, i);
        };

    template<typename S, typename PredicateT>
    str_size_t<S>
    count_suffix(S&& s, PredicateT pred)
    {
        auto f = rbegin(s);
        auto l = rend(s);
        str_size_t<S> cnt = 0;

        for (; f != l; ++f) {
            if (!pred(*f)) break;
            ++cnt;
        }

        return cnt;
    }

    struct api
    {
        template<bidirectional_string S>
        void
        operator()(S& s, std::locale const& loc = {}) const
        {
            operator()(s, str::is_space<str_char_t<S>>(loc));
        }

        template<bidirectional_string S>
        void
        operator()(S& s, str_char_t<S> c) const
        {
            operator()(s, [c](auto c1){ return c1 == c; });
        }

        template<bidirectional_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
                  && has_remove_suffix<S, str_size_t<S>>
        void
        operator()(S& s, PredicateT pred) const
        {
            auto n = count_suffix(s, pred);

            str::remove_suffix(s, n);
        }
    };

} // namespace _trim_right

inline constexpr _trim_right::api trim_right;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TRIM_RIGHT_HPP
