#ifndef STREAM9_STRINGS_MODIFIER_PREPEND_HPP
#define STREAM9_STRINGS_MODIFIER_PREPEND_HPP

#include "insert.hpp"

#include "../accessor/begin.hpp"
#include "../core/concepts.hpp"
#include "../namespace.hpp"

#include <stream9/errors.hpp>

namespace stream9::strings {

template<string S1, string S2>
void
prepend(S1& s1, S2&& s2)
{
    try {
        str::insert(s1, str::begin(s1), s2);
    }
    catch (...) {
        rethrow_error();
    }
}

template<string S1, character C>
void
prepend(S1& s1, C c)
{
    try {
        str::insert(s1, str::begin(s1), c);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9::strings

#endif // STREAM9_STRINGS_MODIFIER_PREPEND_HPP
