#ifndef STRINGS_MODIFIER_TO_UPPER_HPP
#define STRINGS_MODIFIER_TO_UPPER_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../accessor/data.hpp"
#include "../query/size.hpp"

#include "../detail/ctype_facet.hpp"

#include <locale>

namespace stream9::strings {

/*
 * to_upper
 */
namespace _to_upper {

    template<contiguous_string S>
    void
    to_upper(S& s, std::locale const& loc)
    {
        auto& f = detail::ctype_facet<S>(loc);

        auto* const first = str::data(s);
        auto* const last = first + str::size(s);

        f.toupper(first, last);
    }

    template<typename S>
        requires (!contiguous_string<S>)
    void
    to_upper(S& s, std::locale const& loc)
    {
        auto& f = detail::ctype_facet<S>(loc);

        for (auto& c: s) {
            c = f.toupper(c);
        }
    }

    struct api
    {
        template<writable_string S>
        void
        operator()(S& s, std::locale const& loc = {}) const
        {
            to_upper(s, loc);
        }
    };

} // namespace _to_upper

inline constexpr _to_upper::api to_upper;

} // namespace stream9::strings

#endif // STRINGS_MODIFIER_TO_UPPER_HPP
