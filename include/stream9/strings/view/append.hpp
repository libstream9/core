#ifndef STRINGS_VIEW_APPEND_HPP
#define STRINGS_VIEW_APPEND_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"

#include "../accessor/end.hpp"

#include "insert.hpp"

namespace stream9::strings::views {

namespace _append {

    template<typename S, typename C>
    inline constexpr bool has_insert_char =
        requires (S& s, C c) {
            str::views::insert(s, str::end(s), c);
        };

    template<typename S1, typename S2>
    inline constexpr bool has_insert_str =
        requires (S1& s1, S2&& s2) {
            str::views::insert(s1, str::end(s1), s2);
        };

    struct api
    {
        template<string S, character C>
            requires has_insert_char<S, C>
        auto
        operator()(S& s, C const c) const
        {
            return str::views::insert(s, str::end(s), c);
        }

        template<string S1, string S2>
            requires has_insert_str<S1, S2>
        auto
        operator()(S1& s1, S2&& s2) const
        {
            return str::views::insert(s1, str::end(s1), s2);
        }
    };

} // namespace _append

inline constexpr _append::api append;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_APPEND_HPP
