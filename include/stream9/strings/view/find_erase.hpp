#ifndef STRINGS_VIEW_FIND_ERASE_HPP
#define STRINGS_VIEW_FIND_ERASE_HPP

#include "../core/concepts.hpp"
#include "../core/finder.hpp"
#include "../core/regex.hpp"
#include "../core/string_range.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include "find_replace.hpp"

namespace stream9::strings::views {

namespace _find_erase {

    struct api
    {
        template<forward_string S1, forward_string S2>
            requires is_same_char_v<S1, S2>
        auto
        operator()(S1&& s, S2&& keyword) const
        {
            return views::find_replace(s, keyword, "");
        }

        template<forward_string S, regexp R>
            requires is_same_char_v<S, R>
        auto
        operator()(S&& s, R&& e) const
        {
            return views::find_replace(s, std::forward<R>(e), "");
        }

        template<forward_string S, typename FinderT>
            requires finder<FinderT, S>
        auto
        operator()(S&& s, FinderT&& f) const
        {
            return views::find_replace(s, std::forward<FinderT>(f), "");
        }
    };

} // namespace _find_erase

inline constexpr _find_erase::api find_erase;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_FIND_ERASE_HPP
