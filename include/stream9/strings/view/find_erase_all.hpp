#ifndef STRINGS_VIEW_FIND_ERASE_ALL_HPP
#define STRINGS_VIEW_FIND_ERASE_ALL_HPP

#include "../core/concepts.hpp"
#include "../core/regex.hpp"

#include "find_replace_all.hpp"

#include <utility>

namespace stream9::strings::views {

namespace _find_erase_all {

    struct api
    {
        template<forward_string S, forward_string KeywordT>
            requires is_same_char_v<S, KeywordT>
        auto
        operator()(S&& s1, KeywordT&& s2) const
        {
            return str::views::find_replace_all(s1, s2, "");
        }

        template<bidirectional_string S, regexp R>
            requires is_same_char_v<S, R>
        auto
        operator()(S&& s, R&& e) const
        {
            return str::views::find_replace_all(s, std::forward<R>(e), "");
        }

        template<forward_string S, typename FinderT>
            requires finder<FinderT, S>
        auto
        operator()(S&& s1, FinderT&& find) const
        {
            return str::views::find_replace_all(s1, std::forward<FinderT>(find), "");
        }
    };

} // namespace _find_erase_all

inline constexpr _find_erase_all::api find_erase_all;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_FIND_ERASE_ALL_HPP
