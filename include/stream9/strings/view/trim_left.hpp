#ifndef STRINGS_VIEW_TRIM_LEFT_HPP
#define STRINGS_VIEW_TRIM_LEFT_HPP

#include "../namespace.hpp"

#include "../core/char/classifier.hpp"
#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/end.hpp"

#include <algorithm>
#include <concepts>
#include <locale>

namespace stream9::strings::views {

namespace _trim_left {

    struct api
    {
        template<forward_string S>
        string_range<iterator_t<S>>
        operator()(S&& s, std::locale const& loc = {}) const
        {
            return operator()(s, is_space<str_char_t<S>>(loc));
        }

        template<forward_string S, typename PredicateT>
            requires std::predicate<PredicateT, str_char_t<S>>
        string_range<iterator_t<S>>
        operator()(S&& s_, PredicateT pred) const
        {
            string_range s = s_;
            auto it = std::ranges::find_if_not(s, pred);

            return { it, str::end(s) };
        }
    };

} // namespace _trim

inline constexpr _trim_left::api trim_left;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_TRIM_LEFT_HPP
