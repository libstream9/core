#ifndef STRINGS_VIEW_REMOVE_SUFFIX_HPP
#define STRINGS_VIEW_REMOVE_SUFFIX_HPP

#include "../namespace.hpp"

#include "../core/concepts.hpp"
#include "../core/string_range.hpp"
#include "../core/string_traits.hpp"

#include "../accessor/begin.hpp"
#include "../accessor/end.hpp"

#include <stream9/iterators/next.hpp>

#include <concepts>

namespace stream9::strings::views {

namespace _remove_suffix {

    struct api
    {
        template<forward_string S>
        auto
        operator()(S&& s, str_size_t<S> n) const
        // [[expects: n >= 0]]
        // [[expects: n <= str::size(s)]]
        {
            auto const first = str::begin(s);
            auto const last = iter::next(str::begin(s),
                    to_difference<S>(str::size(s)) - to_difference<S>(n));

            return string_range { first, last };
        }

        template<forward_string S, std::forward_iterator I>
            requires std::convertible_to<I, iterator_t<_unref<S> const>>
        auto
        operator()(S&& s, I it) const
        {
            return string_range { str::begin(s), it };
        }
    };

} // namespace _remove_suffix

inline constexpr _remove_suffix::api remove_suffix;

} // namespace stream9::strings::views

#endif // STRINGS_VIEW_REMOVE_SUFFIX_HPP
