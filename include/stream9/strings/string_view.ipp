#ifndef CORE_STREAM9_STRINGS_STRING_VIEW_IPP
#define CORE_STREAM9_STRINGS_STRING_VIEW_IPP

#include "accessor/at.hpp"
#include "accessor/begin.hpp"
#include "accessor/end.hpp"

#include <algorithm>
#include <cassert>

namespace stream9 {

template<str::character Ch>
template<str::contiguous_string S>
constexpr
basic_string_view<Ch>::
basic_string_view(S const& s) noexcept
    requires str::is_same_char_v<S, Ch>
    : m_begin { std::to_address(str::begin(s)) }
    , m_end { std::to_address(str::end(s)) }
{
    assert(m_begin != nullptr);
    assert(m_end != nullptr);
    assert(m_begin <= m_end);
}

template<str::character Ch>
template<str::contiguous_string S>
constexpr
basic_string_view<Ch>& basic_string_view<Ch>::
operator=(S const& s) noexcept
{
    m_begin = std::to_address(str::begin(s));
    m_end = std::to_address(str::end(s));

    assert(m_begin != nullptr);
    assert(m_end != nullptr);
    assert(m_begin <= m_end);

    return *this;
}

template<str::character Ch>
template<std::contiguous_iterator I>
constexpr
basic_string_view<Ch>::
basic_string_view(I f, I l) noexcept
    : m_begin { std::to_address(f) }
    , m_end { std::to_address(l) }
{
    assert(m_begin != nullptr);
    assert(m_end != nullptr);
    assert(m_begin <= m_end);
}

template<str::character Ch>
template<std::contiguous_iterator I>
constexpr
basic_string_view<Ch>::
basic_string_view(I f, size_type length) noexcept
    : m_begin { std::to_address(f) }
    , m_end { m_begin + length }
{
    assert(m_begin != nullptr);
    assert(m_end != nullptr);
    assert(m_begin <= m_end);
}

template<str::character Ch>
constexpr
basic_string_view<Ch>::
operator std::basic_string_view<Ch>() const noexcept
{
    return { m_begin, m_end };
}

template<str::character Ch>
constexpr
basic_string_view<Ch>::const_iterator basic_string_view<Ch>::
begin() const noexcept
{
    return m_begin;
}
template<str::character Ch>
constexpr
basic_string_view<Ch>::const_iterator basic_string_view<Ch>::
end() const noexcept
{
    return m_end;
}

template<str::character Ch>
constexpr
basic_string_view<Ch>::const_reference basic_string_view<Ch>::
operator[](index_type i) const
{
    return str::at(*this, i);
}

template<str::character Ch>
constexpr basic_string_view<Ch>::const_pointer basic_string_view<Ch>::
data() const noexcept
{
    return m_begin;
}

template<str::character Ch>
constexpr basic_string_view<Ch>::size_type basic_string_view<Ch>::
size() const noexcept
{
    return m_end - m_begin;
}

template<str::character Ch>
constexpr bool basic_string_view<Ch>::
empty() const noexcept
{
    return m_end == m_begin;
}

template<str::character Ch>
constexpr void basic_string_view<Ch>::
remove_prefix(size_type n) noexcept
{
    auto f = m_begin + n;
    if (f > m_end) {
        m_begin = m_end;
    }
    else {
        m_begin = f;
    }
}

template<str::character Ch>
constexpr void basic_string_view<Ch>::
remove_suffix(size_type n) noexcept
{
    auto l = m_end - n;
    if (l < m_begin) {
        m_end = m_begin;
    }
    else {
        m_end = l;
    }
}

template<str::character Ch>
constexpr bool basic_string_view<Ch>::
operator==(basic_string_view const& o) const noexcept
{
    return std::equal(m_begin, m_end, o.m_begin, o.m_end);
}

template<str::character Ch>
template<str::readable_string S>
constexpr bool basic_string_view<Ch>::
operator==(S const& o) const noexcept
    requires str::is_same_char_v<S, Ch>
{
    return std::equal(m_begin, m_end, str::begin(o), str::end(o));
}

template<str::character Ch>
constexpr std::strong_ordering basic_string_view<Ch>::
operator<=>(basic_string_view const& o) const noexcept
{
    return std::lexicographical_compare_three_way(
        m_begin, m_end, o.m_begin, o.m_end
    );
}

template<str::character Ch>
template<str::readable_string S>
constexpr std::strong_ordering basic_string_view<Ch>::
operator<=>(S const& o) const noexcept
    requires str::is_same_char_v<S, Ch>
{
    return std::lexicographical_compare_three_way(
        m_begin, m_end,
        str::begin(o), str::end(o)
    );
}

template<str::character Ch>
constexpr basic_string_view<Ch>::
basic_string_view(null_t) noexcept
    : m_begin { nullptr }
    , m_end { nullptr }
{}

template<str::character Ch>
constexpr bool basic_string_view<Ch>::
is_null() const noexcept
{
    return m_begin == nullptr;
}

template<str::character Ch>
constexpr void basic_string_view<Ch>::
set_null() noexcept
{
    m_begin = nullptr;
    m_end = nullptr;
}

template<typename C>
std::basic_ostream<C>&
operator<<(std::basic_ostream<C>& os, basic_string_view<C> const& s)
{
    return os << std::basic_string_view<C>(s);
}

} // namespace stream9

#endif // CORE_STREAM9_STRINGS_STRING_VIEW_IPP
