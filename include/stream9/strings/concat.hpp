#ifndef STREAM9_STRINGS_CONCAT_HPP
#define STREAM9_STRINGS_CONCAT_HPP

#include "error.hpp"
#include "modifier/append.hpp"
#include "namespace.hpp"

#include "core/concepts.hpp"

#include <stream9/string.hpp>

namespace stream9::strings {

namespace concat_ {

    struct api
    {
        template<string S1, string S2>
            requires is_same_char_v<S1, S2>
        basic_string<character_t<S1>>
        operator()(S1 const& s1, S2 const& s2) const
        {
            try {
                basic_string<character_t<S1>> s { s1 };
                append(s, s2);
                return s;
            }
            catch (...) {
                rethrow_error({
                    { "s1", s1 },
                    { "s2", s2 },
                });
            }
        }
    };

} // namespace concat_

inline constexpr concat_::api concat;

template<string S1, string S2>
    requires std::same_as<character_t<S1>, character_t<S2>>
basic_string<character_t<S1>>
operator+(S1 const& s1, S2 const& s2)
{
    return concat(s1, s2);
}

} // namespace stream9::strings

#endif // STREAM9_STRINGS_CONCAT_HPP
