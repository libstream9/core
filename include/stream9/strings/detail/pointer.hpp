#ifndef STRINGS_DETAIL_POINTER_HPP
#define STRINGS_DETAIL_POINTER_HPP

#include "../core/char/concepts.hpp"

#include <string>

namespace stream9::strings {

namespace detail {

    template<character C>
    auto
    ptr_length(C* const& s) noexcept
    {
        return std::char_traits<C>::length(s);
    }

    template<character C>
    C*
    ptr_end(C* const& s) noexcept
    {
        return s + ptr_length(s);
    }

    template<character C, std::size_t N>
    constexpr C*
    arr_end(C (&s)[N]) noexcept
    {
        static_assert(N > 0);

        auto e = s + N;
        if (*(e - 1) == null_char_v<C>) {
            return e - 1;
        }
        else {
            return e;
        }
    }

    template<character C, std::size_t N>
    constexpr auto
    arr_length(C (&s)[N]) noexcept
    {
        return arr_end(s) - s;
    }

} // namespace detail

} // namespace stream9::strings

#endif // STRINGS_DETAIL_POINTER_HPP
