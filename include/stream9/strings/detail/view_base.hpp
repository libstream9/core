#ifndef STRINGS_DETAIL_VIEW_BASE_HPP
#define STRINGS_DETAIL_VIEW_BASE_HPP

#include "../namespace.hpp"

#include "../core/meta.hpp"

#include <concepts>
#include <ranges>

namespace stream9::strings {

    class view_base {};

    template<std::size_t N, typename T>
        requires (N < 2
               && std::derived_from<_uncvref<T>, view_base>)
    constexpr auto
    get(T const& v)
    {
        if constexpr (N == 0) {
            return rng::begin(v);
        } else {
            return rng::end(v);
        }
    }

} // namespace stream9::strings

namespace std {

    template<typename T>
        requires derived_from<T, stream9::strings::view_base>
    struct tuple_size<T>
          : std::integral_constant<size_t, 2> {};

    template<typename T>
        requires derived_from<T, stream9::strings::view_base>
    struct tuple_element<0, T>
    {
        using type = typename ranges::iterator_t<T>;
    };

    template<typename T>
        requires derived_from<T, stream9::strings::view_base>
    struct tuple_element<0, T const>
    {
        using type = typename ranges::iterator_t<T const>;
    };

    template<typename T>
        requires derived_from<T, stream9::strings::view_base>
    struct tuple_element<1, T>
    {
        using type = typename ranges::sentinel_t<T>;
    };

    template<typename T>
        requires derived_from<T, stream9::strings::view_base>
    struct tuple_element<1, T const>
    {
        using type = typename ranges::sentinel_t<T const>;
    };

}

#endif // STRINGS_DETAIL_VIEW_BASE_HPP
