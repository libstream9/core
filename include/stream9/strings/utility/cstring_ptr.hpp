#ifndef STREAM9_STRINGS_UTILITY_CSTRING_PTR_HPP
#define STREAM9_STRINGS_UTILITY_CSTRING_PTR_HPP

#include "../namespace.hpp"
#include "../error.hpp"

#include "../core/concepts.hpp"
#include "../accessor/data.hpp"
#include "../accessor/c_str.hpp"
#include "../query/size.hpp"

#include <algorithm>
#include <compare>
#include <concepts>
#include <cstdint>
#include <functional>
#include <memory>
#include <string>
#include <string_view>
#include <utility>

#include <stream9/null.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::strings {

template<character Ch>
class basic_cstring_ptr
{
public:
    using size_type = safe_integer<std::int64_t, 0>;
    using const_pointer = Ch const*;
    using const_iterator = Ch const*;

public:
    // essential
    basic_cstring_ptr(const_pointer);

    template<typename T>
    basic_cstring_ptr(T const&) noexcept
        requires std::is_invocable_r_v<const_pointer, decltype(str::c_str), T>;

    template<string T>
    basic_cstring_ptr(T const&)
        requires (!std::is_invocable_r_v<const_pointer, decltype(str::c_str), T>)
              && std::invocable<decltype(str::data), T>;

    basic_cstring_ptr(nullptr_t) = delete;

    basic_cstring_ptr(basic_cstring_ptr const&) noexcept;
    basic_cstring_ptr& operator=(basic_cstring_ptr const&) noexcept;

    basic_cstring_ptr(basic_cstring_ptr&&) noexcept;
    basic_cstring_ptr& operator=(basic_cstring_ptr&&) noexcept;

    ~basic_cstring_ptr() noexcept;

    template<typename Ch2>
    friend void swap(basic_cstring_ptr<Ch2>&, basic_cstring_ptr<Ch2>&) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    const_pointer data() const noexcept;
    const_pointer c_str() const noexcept;

    // query
    size_type size() const noexcept;

    // converter
    operator const_pointer () const noexcept;
    operator std::basic_string_view<Ch> () const noexcept;

    explicit operator std::basic_string<Ch> () const;

    // equality / ordering
    template<typename T>
    bool
    operator==(T const& o) const noexcept
        requires std::is_convertible_v<T, std::basic_string_view<Ch>>;

    template<typename T>
    std::strong_ordering
    operator<=>(T const& o) const noexcept
        requires std::is_convertible_v<T, std::basic_string_view<Ch>>;

private:
    static constexpr std::uint64_t length_max = (1ull << 62) - 1;

    std::uint64_t length() const noexcept { return m_length; }
    void set_length(std::uint64_t l) noexcept { m_length = l & length_max; }

    template<typename>
    friend class stream9::null_traits;

    basic_cstring_ptr(null_t) {}
    bool is_null() const noexcept { return m_data == nullptr; }
    void set_null() noexcept
    {
        this->~basic_cstring_ptr();
        m_data = nullptr;
        m_owner = 0;
        m_length = 0;
    }

private:
    const_pointer m_data = nullptr;
    std::uint64_t m_owner : 1 = 0;
    std::uint64_t m_length : 62;
};

static_assert(sizeof(basic_cstring_ptr<char>) == sizeof(char*) + sizeof(std::int64_t));

using cstring_ptr = basic_cstring_ptr<char>;

template<character Ch>
std::basic_ostream<Ch>&
operator<<(std::basic_ostream<Ch>&, basic_cstring_ptr<Ch> const&);

template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(const_pointer p)
    : m_data { p }
{
    if (m_data == nullptr) {
        throw error {
            errc::invalid_pointer, {
                { "p", "null" },
            }
        };
    }
    set_length(str::size(p));
}

template<character Ch>
template<typename T>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(T const& s) noexcept
    requires std::is_invocable_r_v<const_pointer, decltype(str::c_str), T>
    : m_data { str::c_str(s) }
{
    if constexpr (std::invocable<decltype(str::size), T>) {
        set_length(str::size(s));
    }
    else {
        set_length(str::size(m_data));
    }
}

template<character Ch>
template<string T>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(T const& s)
    requires (!std::is_invocable_r_v<const_pointer, decltype(str::c_str), T>)
          && std::invocable<decltype(str::data), T>
{
    try {
        auto p = str::data(s);
        auto len = str::size(s);

        if (*(p + len) == Ch()) {
            m_data = p;
            set_length(len);
        }
        else {
            m_data = new Ch[static_cast<std::size_t>(len+1)];
            auto data = const_cast<Ch*>(m_data);

            std::memcpy(data, p, len);
            data[len] = Ch();
            set_length(len);
            m_owner = 1;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(basic_cstring_ptr const& o) noexcept
    : m_data { o.m_data }
    , m_length { o.m_length }
{}

template<character Ch>
basic_cstring_ptr<Ch>& basic_cstring_ptr<Ch>::
operator=(basic_cstring_ptr const& o) noexcept
{
    this->~basic_cstring_ptr();
    m_data = o.m_data;
    m_length = o.m_length;

    return *this;
}

template<character Ch>
basic_cstring_ptr<Ch>::
basic_cstring_ptr(basic_cstring_ptr&& o) noexcept
    : m_data { o.m_data }
    , m_owner { o.m_owner }
    , m_length { o.m_length }
{
    o.m_owner = 0;
}

template<character Ch>
basic_cstring_ptr<Ch>& basic_cstring_ptr<Ch>::
operator=(basic_cstring_ptr&& o) noexcept
{
    this->~basic_cstring_ptr();
    m_data = o.m_data;
    m_owner = o.m_owner;
    m_length = o.m_length;

    o.m_owner = 0;

    return *this;
}

template<character Ch>
basic_cstring_ptr<Ch>::
~basic_cstring_ptr() noexcept
{
    if (m_data && m_owner) {
        delete[] m_data;
    }
}

template<typename Ch2>
void
swap(basic_cstring_ptr<Ch2>& x, basic_cstring_ptr<Ch2>& y) noexcept
{
    using std::swap;
    swap(x.m_data, y.m_data);
    swap(x.m_owner, y.m_owner);
    swap(x.m_length, y.m_length);
}

template<character Ch>
basic_cstring_ptr<Ch>::const_iterator basic_cstring_ptr<Ch>::
begin() const noexcept
{
    return data();
}

template<character Ch>
basic_cstring_ptr<Ch>::const_iterator basic_cstring_ptr<Ch>::
end() const noexcept
{
    return data() + length();
}

template<character Ch>
basic_cstring_ptr<Ch>::const_pointer basic_cstring_ptr<Ch>::
data() const noexcept
{
    return m_data;
}

template<character Ch>
basic_cstring_ptr<Ch>::const_pointer basic_cstring_ptr<Ch>::
c_str() const noexcept
{
    return data();
}

template<character Ch>
basic_cstring_ptr<Ch>::size_type basic_cstring_ptr<Ch>::
size() const noexcept
{
    return length();
}

template<character Ch>
basic_cstring_ptr<Ch>::
operator const_pointer () const noexcept
{
    return c_str();
}

template<character Ch>
basic_cstring_ptr<Ch>::
operator std::basic_string_view<Ch> () const noexcept
{
    return { data(), length() };
}

template<character Ch>
basic_cstring_ptr<Ch>::
operator std::basic_string<Ch> () const
{
    return { data(), length() };
}

template<character Ch>
template<typename T>
bool basic_cstring_ptr<Ch>::
operator==(T const& o) const noexcept
    requires std::is_convertible_v<T, std::basic_string_view<Ch>>
{
    return std::basic_string_view<Ch>(data(), length()) == std::basic_string_view<Ch>(o);
}

template<character Ch>
template<typename T>
std::strong_ordering basic_cstring_ptr<Ch>::
operator<=>(T const& o) const noexcept
    requires std::is_convertible_v<T, std::basic_string_view<Ch>>
{
    return std::basic_string_view<Ch>(data(), length()) <=> std::basic_string_view<Ch>(o);
}

template<character Ch>
std::basic_ostream<Ch>&
operator<<(std::basic_ostream<Ch>& os, basic_cstring_ptr<Ch> const& s)
{
    return os << std::basic_string_view<Ch>(s);
}

} // namespace stream9::strings

namespace stream9 {

using strings::cstring_ptr;

} // namespace stream9

#endif // STREAM9_STRINGS_UTILITY_CSTRING_PTR_HPP
