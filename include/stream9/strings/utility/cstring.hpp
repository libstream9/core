#ifndef STREAM9_STRINGS_UTILITY_CSTRING_HPP
#define STREAM9_STRINGS_UTILITY_CSTRING_HPP

#include "cstring_view.hpp"

#include "../error.hpp"
#include "../core/char/concepts.hpp"
#include "../query/lexicographical_compare_three_way.hpp"

#include <cstring>
#include <cstdlib>
#include <iosfwd>
#include <iterator>

#include <stream9/iterators.hpp>

namespace stream9::strings {

template<character> class cstring_iterator;

template<character CharT, typename Traits = std::char_traits<CharT>>
class basic_cstring
{
public:
    using size_type = std::size_t;
    using traits_type = Traits;
    using pointer = CharT*;
    using const_pointer = CharT const*;
    using reference = CharT const&;
    using const_reference = CharT const&;
    using iterator = cstring_iterator<CharT>;
    using const_iterator = cstring_iterator<CharT const>;
    using sentinel = std::default_sentinel_t;
    using comparison_category = typename Traits::comparison_category;

public:
    // essential
    basic_cstring() = default;

    basic_cstring(size_type n);

    basic_cstring(const_pointer s);

    template<std::size_t N>
    basic_cstring(CharT (&s)[N]);

    basic_cstring(std::basic_string_view<CharT, Traits> const& s);

    template<typename T>
        requires requires (T s) {
            { s.c_str() } -> std::convertible_to<CharT const*>;
        }
    basic_cstring(T const& s);

    ~basic_cstring() noexcept;

    // accessor
    iterator begin() noexcept { return m_ptr; }
    sentinel end() noexcept { return {}; }

    const_iterator begin() const noexcept { return m_ptr; }
    sentinel end() const noexcept { return {}; }

    const_iterator cbegin() const noexcept { return m_ptr; }
    sentinel cend() const noexcept { return {}; }

    reference operator[](size_type pos) noexcept { return m_ptr[pos]; }
    const_reference operator[](size_type pos) const noexcept { return m_ptr[pos]; }

    reference front() noexcept { return operator[](0); }
    const_reference front() const noexcept { return operator[](0); }

    pointer data() noexcept { return m_ptr; }
    const_pointer data() const noexcept { return m_ptr; }

    const_pointer c_str() const noexcept { return m_ptr; }

    // query
    size_type size() const noexcept;
    bool empty() const noexcept;

    // operator
    bool operator==(basic_cstring const&) const noexcept;
    bool operator==(basic_cstring_view<CharT, Traits> const&) const noexcept;
    bool operator==(const_pointer) const noexcept;
    bool operator==(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    bool operator==(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    comparison_category operator<=>(basic_cstring const&) const noexcept;
    comparison_category operator<=>(basic_cstring_view<CharT, Traits> const&) const noexcept;
    comparison_category operator<=>(const_pointer) const noexcept;
    comparison_category operator<=>(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    comparison_category operator<=>(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    // conversion
    operator std::basic_string_view<CharT, Traits> () const noexcept;

    operator basic_cstring_view<CharT, Traits> () const noexcept;

    template<typename Allocator>
    explicit operator std::basic_string<CharT, Traits, Allocator> () const noexcept;

private:
    pointer m_ptr = nullptr;
};

using cstring = basic_cstring<char>;

template<typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os,
           basic_cstring<CharT, Traits> const& s)
{
    return os << s.c_str();
}

template<character CharT>
class cstring_iterator : public iter::iterator_facade<
                                        cstring_iterator<CharT>,
                                        std::contiguous_iterator_tag,
                                        CharT&>
{
public:
    cstring_iterator() = default;

    constexpr cstring_iterator(CharT* ptr) noexcept
        : m_ptr { ptr }
    {}

public:
    friend class iter::iterator_core_access;

    constexpr CharT& dereference() const noexcept { return *m_ptr; }

    constexpr void increment() noexcept { ++m_ptr; }

    constexpr void decrement() noexcept { --m_ptr; }

    constexpr void advance(std::ptrdiff_t const n) noexcept { m_ptr += n; }

    constexpr std::ptrdiff_t distance_to(cstring_iterator const& other) const noexcept
    {
        return other.m_ptr - m_ptr;
    }

    constexpr bool equal(cstring_iterator const& other) const noexcept
    {
        return m_ptr == other.m_ptr;
    }

    constexpr auto compare(cstring_iterator const& other) const noexcept
    {
        return m_ptr <=> other.m_ptr;
    }

    constexpr bool equal(std::default_sentinel_t const s) const noexcept
    {
        return compare(s) == std::strong_ordering::equivalent;
    }

    constexpr auto compare(std::default_sentinel_t) const noexcept
    {
        if (*m_ptr == CharT()) {
            return std::strong_ordering::equivalent;
        }
        else {
            return std::strong_ordering::greater;
        }
    }

private:
    CharT* m_ptr = nullptr;
};

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
basic_cstring(size_type const n)
{
    auto const len = n + 1;

    m_ptr = std::malloc(len);
    if (m_ptr == nullptr) {
        throw error {
            errc::not_enough_memory, {
                { "n", n }
            }
        };
    }

    std::memset(m_ptr, 0, len);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
basic_cstring(const_pointer const s)
{
    m_ptr = strdup(s);
    if (m_ptr == nullptr) {
        throw error {
            errc::not_enough_memory, {
                { "s", s },
            }
        };
    }
}

template<character CharT, typename Traits>
template<std::size_t N>
basic_cstring<CharT, Traits>::
basic_cstring(CharT (&s)[N])
{
    m_ptr = strndup(s, N - 1);
    if (m_ptr == nullptr) {
        throw error {
            errc::not_enough_memory, {
                { "s", std::string_view(s, N - 1) },
            }
        };
    }
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
basic_cstring(std::basic_string_view<CharT, Traits> const& s)
{
    m_ptr = strndup(s.data(), s.size());
    if (m_ptr == nullptr) {
        throw error {
            errc::not_enough_memory, {
                { "s", s },
            }
        };
    }
}

template<character CharT, typename Traits>
template<typename T>
    requires requires (T s) {
        { s.c_str() } -> std::convertible_to<CharT const*>;
    }
basic_cstring<CharT, Traits>::
basic_cstring(T const& s)
{
    m_ptr = strdup(s.c_str());
    if (m_ptr == nullptr) {
        throw error {
            errc::not_enough_memory, {
                { "s", s.c_str() },
            }
        };
    }
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
~basic_cstring() noexcept
{
    if (m_ptr) ::free(m_ptr);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::size_type basic_cstring<CharT, Traits>::
size() const noexcept
{
    if (m_ptr == nullptr) {
        return 0;
    }
    else {
        return std::strlen(m_ptr);
    }
}

template<character CharT, typename Traits>
bool basic_cstring<CharT, Traits>::
empty() const noexcept
{
    if (m_ptr == nullptr) {
        return true;
    }
    else {
        return *m_ptr == CharT();
    }
}

template<character CharT, typename Traits>
bool basic_cstring<CharT, Traits>::
operator==(basic_cstring const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
bool basic_cstring<CharT, Traits>::
operator==(basic_cstring_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
bool basic_cstring<CharT, Traits>::
operator==(const_pointer const other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
bool basic_cstring<CharT, Traits>::
operator==(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
template<typename Allocator>
bool basic_cstring<CharT, Traits>::
operator==(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::comparison_category basic_cstring<CharT, Traits>::
operator<=>(basic_cstring const& other) const noexcept
{
    return str::lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::comparison_category basic_cstring<CharT, Traits>::
operator<=>(basic_cstring_view<CharT, Traits> const& other) const noexcept
{
    return str::lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::comparison_category basic_cstring<CharT, Traits>::
operator<=>(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return str::lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::comparison_category basic_cstring<CharT, Traits>::
operator<=>(const_pointer const other) const noexcept
{
    return str::lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
template<typename Allocator>
basic_cstring<CharT, Traits>::comparison_category basic_cstring<CharT, Traits>::
operator<=>(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return str::lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
operator basic_cstring_view<CharT, Traits> () const noexcept
{
    if (m_ptr) {
        return m_ptr;
    }
    else {
        return {};
    }
}

template<character CharT, typename Traits>
basic_cstring<CharT, Traits>::
operator std::basic_string_view<CharT, Traits> () const noexcept
{
    if (m_ptr) {
        return m_ptr;
    }
    else {
        return {};
    }
}

template<character CharT, typename Traits>
template<typename Allocator>
basic_cstring<CharT, Traits>::
operator std::basic_string<CharT, Traits, Allocator> () const noexcept
{
    if (m_ptr) {
        return m_ptr;
    }
    else {
        return {};
    }
}

} // namespace stream9::strings

namespace std::ranges {

template<typename CharT, typename Traits>
inline constexpr bool disable_sized_range<
    stream9::strings::basic_cstring<CharT, Traits> > = true;

} // namespace std::ranges

#endif // STREAM9_STRINGS_UTILITY_CSTRING_HPP
