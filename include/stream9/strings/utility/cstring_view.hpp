#ifndef STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP
#define STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP

#include "../error.hpp"
#include "../core/char/concepts.hpp"
#include "../query/lexicographical_compare_three_way.hpp"

#include <concepts>
#include <cstddef>
#include <iosfwd>
#include <iterator>
#include <ranges>

#include <stream9/iterators.hpp>
#include <stream9/null.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::strings {

template<character> class cstring_view_iterator;

template<character CharT, typename Traits = std::char_traits<CharT>>
class basic_cstring_view
{
public:
    using value_type = CharT;
    using traits_type = Traits;
    using const_pointer = CharT const*;
    using const_reference = CharT const&;
    using size_type = safe_integer<std::int64_t, 0>;
    using difference_type = safe_integer<std::int64_t>;
    using const_iterator = const_pointer;
    using sentinel = const_pointer;
    using comparison_category = typename Traits::comparison_category;

public:
    // essentials
    constexpr basic_cstring_view(const_pointer, size_type len);

    constexpr basic_cstring_view(const_pointer begin, const_pointer end);

    constexpr basic_cstring_view(const_pointer);

    basic_cstring_view(std::nullptr_t) = delete;

    explicit constexpr basic_cstring_view(std::basic_string_view<CharT, Traits> const&);

    template<typename T>
        requires requires (T s) {
            { s.c_str() } -> std::convertible_to<CharT const*>;
        }
    constexpr basic_cstring_view(T const&) noexcept;

    // accessor
    constexpr const_iterator begin() const noexcept { return m_begin; }
    constexpr const_iterator cbegin() const noexcept { return m_begin; }

    constexpr sentinel end() const noexcept { return m_end; }
    constexpr sentinel cend() const noexcept { return m_end; }

    constexpr const_reference operator[](std::integral auto pos) const;

    constexpr const_reference front() const noexcept { return operator[](0); }

    constexpr const_pointer data() const noexcept { return m_begin; }
    constexpr const_pointer c_str() const noexcept { return m_begin; }

    // query
    constexpr size_type size() const noexcept;
    constexpr bool empty() const noexcept;

    // modifier
    constexpr void remove_prefix(size_type n);

    // operator
    constexpr bool operator==(basic_cstring_view const&) const noexcept;
    constexpr bool operator==(const_pointer) const noexcept;
    constexpr bool operator==(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    constexpr bool operator==(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    constexpr comparison_category operator<=>(basic_cstring_view const&) const noexcept;
    constexpr comparison_category operator<=>(const_pointer) const noexcept;
    constexpr comparison_category operator<=>(std::basic_string_view<CharT, Traits> const&) const noexcept;
    template<typename Allocator>
    constexpr comparison_category operator<=>(std::basic_string<CharT, Traits, Allocator> const&) const noexcept;

    // conversion
    constexpr operator const_pointer () const noexcept { return m_begin; }
    constexpr operator std::basic_string_view<CharT, Traits> () const noexcept;

    template<typename Allocator>
    explicit constexpr operator std::basic_string<CharT, Traits, Allocator> () const noexcept;

private:
    template<typename>
    friend class stream9::null_traits;

    basic_cstring_view(null_t) : m_begin { nullptr }, m_end { nullptr } {}

    bool is_null() const noexcept { return m_begin == nullptr; }
    void set_null() noexcept { m_begin = nullptr; }

private:
    const_pointer m_begin; // non-null
    const_pointer m_end; // non-null
};

using cstring_view = basic_cstring_view<char>;

template<typename CharT, typename Traits>
inline std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os,
           basic_cstring_view<CharT, Traits> const& s)
{
    return os << s.c_str();
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(const_pointer s, size_type len)
    : m_begin { s }
    , m_end { m_begin + len }
{
    if (!m_begin) {
        throw_error(errc::invalid_pointer, {
            { "s", "nullptr" },
        });
    }
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(const_pointer begin, const_pointer end)
    : m_begin { begin }
    , m_end { end }
{
    if (!m_begin) {
        throw_error(errc::invalid_pointer, {
            { "begin", "nullptr" },
        });
    }
    if (!m_end) {
        throw_error(errc::invalid_pointer, {
            { "end", "nullptr" },
        });
    }
    if (!(m_begin <= m_end)) {
        throw_error(errc::invalid_range, {
            { "description", "begin > end" },
        });
    }
    if (*m_end != null_char_v<CharT>) {
        throw_error(errc::not_terminated_with_null, {
            { "[begin, end)", std::string_view(begin, end) }
        });
    }
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(const_pointer s)
    : m_begin { s }
{
    if (!m_begin) {
        throw_error(errc::invalid_pointer, {
            { "s", "nullptr" },
        });
    }
    m_end = m_begin + traits_type::length(m_begin);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(std::basic_string_view<CharT, Traits> const& s)
    : m_begin { s.data() }
    , m_end { s.end() }
{
    if (!m_begin) {
        throw_error(errc::invalid_pointer);
    }
    if (*m_end != null_char_v<CharT>) {
        throw_error(errc::not_terminated_with_null, {
            { "s", s },
        });
    }
}

template<character CharT, typename Traits>
template<typename T>
    requires requires (T s) {
        { s.c_str() } -> std::convertible_to<CharT const*>;
    }
constexpr basic_cstring_view<CharT, Traits>::
basic_cstring_view(T const& s) noexcept
    : m_begin { s.c_str() }
{
    if (!m_begin) {
        throw_error(errc::invalid_pointer, {
            { "s", "nullptr" }
        });
    }
    m_end = m_begin + traits_type::length(m_begin);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::const_reference basic_cstring_view<CharT, Traits>::
operator[](std::integral auto pos) const
{
    try {
        return m_begin[size_type(pos)];
    }
    catch (...) {
        rethrow_error({
            { "pos", pos },
        });
    }
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::size_type basic_cstring_view<CharT, Traits>::
size() const noexcept
{
    return static_cast<size_type>(m_end - m_begin);
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
empty() const noexcept
{
    return *m_begin == null_char_v<CharT>;
}

template<character CharT, typename Traits>
constexpr void basic_cstring_view<CharT, Traits>::
remove_prefix(size_type n)
{
    auto sz = size();
    if (n > sz) {
        throw_error(errc::index_is_out_of_range, {
            { "n", n },
            { "size", sz },
        });
    }

    m_begin += n;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(basic_cstring_view const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(const_pointer const other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr bool basic_cstring_view<CharT, Traits>::
operator==(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return (*this <=> other) == comparison_category::equivalent;
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(basic_cstring_view const& other) const noexcept
{
    return lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(const_pointer const other) const noexcept
{
    return lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(std::basic_string_view<CharT, Traits> const& other) const noexcept
{
    return lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_cstring_view<CharT, Traits>::comparison_category basic_cstring_view<CharT, Traits>::
operator<=>(std::basic_string<CharT, Traits, Allocator> const& other) const noexcept
{
    return lexicographical_compare_three_way(*this, other);
}

template<character CharT, typename Traits>
constexpr basic_cstring_view<CharT, Traits>::
operator std::basic_string_view<CharT, Traits> () const noexcept
{
    return { m_begin, m_end };
}

template<character CharT, typename Traits>
template<typename Allocator>
constexpr basic_cstring_view<CharT, Traits>::
operator std::basic_string<CharT, Traits, Allocator> () const noexcept
{
    return { m_begin, m_end };
}

} // namespace stream9::strings

namespace stream9 {

using strings::cstring_view;

} // namespace stream9

namespace std::ranges {

template<typename CharT, typename Traits>
inline constexpr bool enable_borrowed_range<
    stream9::strings::basic_cstring_view<CharT, Traits> > = true;

template<typename CharT, typename Traits>
inline constexpr bool enable_view<
    stream9::strings::basic_cstring_view<CharT, Traits> > = true;

} // namespace std::ranges

#endif // STREAM9_STRINGS_UTILITY_CSTRING_VIEW_HPP
