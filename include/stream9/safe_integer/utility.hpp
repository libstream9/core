#ifndef STREAM9_SAFE_INTEGER_UTILITY_HPP
#define STREAM9_SAFE_INTEGER_UTILITY_HPP

#include <utility>

namespace stream9::safe_integers::utility {

template<typename T>
constexpr T max(T&& val)
{
    return std::forward<T>(val);
}

template<typename T, typename... Rest>
constexpr auto max(T&& val1, T&& val2, Rest&&... rest)
{
    return (val1 > val2) ?
        max(val1, std::forward<Rest>(rest)...) :
        max(val2, std::forward<Rest>(rest)...);
}

template<typename T>
constexpr T min(T&& val)
{
    return std::forward<T>(val);
}

template<typename T, typename... Rest>
constexpr auto min(T&& val1, T&& val2, Rest&&... rest)
{
    return (val1 < val2) ?
        min(val1, std::forward<Rest>(rest)...) :
        min(val2, std::forward<Rest>(rest)...);
}

} // namespace stream9::safe_integers::utility

#endif // STREAM9_SAFE_INTEGER_UTILITY_HPP
