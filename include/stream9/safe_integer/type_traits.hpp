#ifndef STREAM9_SAFE_INTEGER_TYPE_TRAITS_HPP
#define STREAM9_SAFE_INTEGER_TYPE_TRAITS_HPP

#include <stream9/safe_integer/fwd.hpp>

#include <cstdint>
#include <type_traits>

namespace stream9::safe_integers {

template<class T>
struct is_safe_integer : std::false_type {};

template<class T, T Min, T Max>
struct is_safe_integer<safe_integer<T, Min, Max>> : std::true_type {};

template<typename T, typename... Ts>
inline auto constexpr is_safe_integer_v =
                    is_safe_integer_v<T> && is_safe_integer_v<Ts...>;

template<class T>
inline auto constexpr is_safe_integer_v<T> = is_safe_integer<T>::value;

template<typename T, typename... Ts>
inline auto constexpr is_integral_v =
                    is_integral_v<T> && is_integral_v<Ts...>;

template<typename T>
inline auto constexpr is_integral_v<T> =
                    !is_safe_integer_v<T> && std::is_integral_v<T>;

template<typename T, typename... Ts>
inline auto constexpr is_integer_v = is_integer_v<T> && is_integer_v<Ts...>;

template<typename T>
inline auto constexpr is_integer_v<T>
                        = is_safe_integer_v<T> || is_integral_v<T>;

template<typename T = int>
inline bool constexpr is_twos_complement_integer_v =
    std::is_signed_v<T> &&
    (std::numeric_limits<T>::min() == (-std::numeric_limits<T>::max() - 1));

template<typename T, typename U>
inline auto constexpr is_same_signedness_v =
                (std::is_signed_v<T> && std::is_signed_v<U>)
             || (std::is_unsigned_v<T> && std::is_unsigned_v<U>);

template<typename T> struct make_larger;

template<> struct make_larger<int8_t> { using type = int16_t; };
template<> struct make_larger<int16_t> { using type = int32_t; };
template<> struct make_larger<int32_t> { using type = int64_t; };
template<> struct make_larger<uint8_t> { using type = uint16_t; };
template<> struct make_larger<uint16_t> { using type = uint32_t; };
template<> struct make_larger<uint32_t> { using type = uint64_t; };

template<typename T>
using make_larger_t = typename make_larger<T>::type;

// in gcc, std::is_same_v<int64_t, long long int> is false, so this workaround
template<typename T, typename U,
    std::enable_if_t<
        std::is_integral_v<T> && std::is_integral_v<U> >* = nullptr >
inline bool constexpr is_same_integral_v =
    std::is_signed_v<T> == std::is_signed_v<U> && sizeof(T) == sizeof(U);

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_TYPE_TRAITS_HPP
