#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_HPP

#include <stream9/safe_integer/arithmetic/promote.hpp>
#include <stream9/safe_integer/arithmetic/negate.hpp>
#include <stream9/safe_integer/arithmetic/add.hpp>
#include <stream9/safe_integer/arithmetic/subtract.hpp>
#include <stream9/safe_integer/arithmetic/multiply.hpp>
#include <stream9/safe_integer/arithmetic/divide.hpp>
#include <stream9/safe_integer/arithmetic/modulo.hpp>

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_HPP
