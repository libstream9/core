#ifndef STREAM9_SAFE_INTEGER_JSON_HPP
#define STREAM9_SAFE_INTEGER_JSON_HPP

#include "safe_integer.hpp"

#include <stream9/json.hpp>

namespace stream9::safe_integers {

template<typename T, auto Min, auto Max>
void
tag_invoke(json::value_from_tag,
           json::value& v,
           safe_integer<T, Min, Max> const i)
{
    v = i.value();
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_JSON_HPP
