#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_NEGATE_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_NEGATE_HPP

#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/arithmetic/safe_convert.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/arithmetic_option.hpp>

#include <type_traits>
#include <limits>

namespace stream9::safe_integers {

namespace detail {

template<typename U, arithmetic_option options, typename T>
    requires is_integral_v<T>
          && std::is_signed_v<T>
          && is_integral_v<U> || std::is_same_v<U, void>
constexpr auto
safe_negate_signed(T const v)
{
    using result_t = std::conditional_t<std::is_same_v<U, void>, T, U>;

    if constexpr (is_twos_complement_integer_v<T>) {
        if (v == std::numeric_limits<T>::min()) {
            using unsigned_t = std::make_unsigned_t<T>;

            return safe_convert<result_t, options>(
                static_cast<unsigned_t>(std::numeric_limits<T>::max()) + 1
            );
        }
    }

    return safe_convert<result_t, options>(-v);
}

template<typename U, arithmetic_option options, typename T>
    requires is_integral_v<T>
          && std::is_unsigned_v<T>
          && is_integral_v<U> || std::is_same_v<U, void>
constexpr auto
safe_negate_unsigned(T const v)
{
    using value_t = make_larger_t<std::make_signed_t<T>>;

    using result_t = std::conditional_t<
        std::is_same_v<U, void>,
        value_t,
        U
    >;

    return safe_convert<result_t, options>(
        -static_cast<value_t>(v)
    );
}

inline constexpr arithmetic_option
invert_option(arithmetic_option const o)
{
    using opt = arithmetic_option;

    opt result {};

    if (test_option(o, opt::dont_check_overflow)) {
        result |= opt::dont_check_underflow;
    }
    if (test_option(o, opt::dont_check_underflow)) {
        result |= opt::dont_check_overflow;
    }
    if (test_option(o, opt::dont_check_zero_division)) {
        result |= opt::dont_check_zero_division;
    }

    return result;
}

template<typename U, arithmetic_option options>
    requires is_integral_v<U> || std::is_same_v<U, void>
constexpr auto
safe_negate_unsigned64(uint64_t const v)
{
    using result_t =
            std::conditional_t<std::is_same_v<U, void>, int64_t, U>;

    auto const oc = safe_convert<int64_t, invert_option(options)>(v);
    if (oc) {
        return safe_convert<result_t, options>(-(*oc));
    }
    else {
        if constexpr (check_underflow(options)) {
            return arithmetic_outcome<result_t> { arithmetic_errc::underflow };
        }
        else {

            return safe_convert<result_t, options>(result_t {});
        }
    }
}

} // namespace detail

template<typename U = void,
         arithmetic_option options = arithmetic_option::none,
         typename T>
    requires is_integral_v<T>
          && (is_integral_v<U> || std::is_same_v<U, void>)
constexpr auto
safe_negate(T const v) // -> arithmetic_outcome<result_t>
{
    using pT = integral_promote_t<T>;
    auto const v_ = static_cast<pT>(v);

    if constexpr (std::is_signed_v<pT>) {
        return detail::safe_negate_signed<U, options>(v_);
    }
    else if constexpr (is_same_integral_v<pT, uint64_t>) {
        return detail::safe_negate_unsigned64<U, options>(v_);
    }
    else {
        return detail::safe_negate_unsigned<U, options>(v_);
    }
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_NEGATE_HPP
