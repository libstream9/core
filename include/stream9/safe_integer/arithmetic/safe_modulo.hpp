#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MODULO_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MODULO_HPP

#include <stream9/safe_integer/arithmetic_option.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/arithmetic/safe_convert.hpp>
#include <stream9/safe_integer/arithmetic/safe_negate.hpp>

#include <cstdint>
#include <type_traits>

namespace stream9::safe_integers {

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_unsigned_v<T> && std::is_unsigned_v<U>)
constexpr arithmetic_outcome<V>
safe_modulo_private(T const lhs, U const rhs)
{
    return safe_convert<V, options>(lhs % rhs);
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_signed_v<T> && std::is_signed_v<U>)
constexpr arithmetic_outcome<V>
safe_modulo_private(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    // on 2's compliment system, MIN % -1 will overflow
    if constexpr (is_twos_complement_integer_v<common_t>) {
        if (lhs == std::numeric_limits<common_t>::min() && rhs == -1) {
            return safe_convert<V, options>(0);
        }
    }

    return safe_convert<V, options>(lhs % rhs);
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_signed_v<T> && std::is_unsigned_v<U>)
constexpr arithmetic_outcome<V>
safe_modulo_private(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    if constexpr (std::is_signed_v<common_t>) {
        return safe_convert<V, options>(lhs % rhs);
    }
    // common_t is unsigned
    else if (lhs >= 0) {
        return safe_convert<V, options>(static_cast<common_t>(lhs) % rhs);
    }
    else { // common_t is unsigned and lhs is negative
        if constexpr (!is_same_integral_v<common_t, uint64_t>) {
            // sizeof(T) <= sizeof(U) && U is smaller than uint64_t
            auto const larger_signed_rhs =
                static_cast<make_larger_t<
                            std::make_signed_t<U> >>(rhs);

            return safe_convert<V, options>(lhs % larger_signed_rhs);
        }
        else { // common_t is uint64_t which imply U is uint64_t
            auto const int64_max = static_cast<uint64_t>(INT64_MAX);

            if (rhs <= int64_max) {
                auto const rhs64 = static_cast<int64_t>(rhs);

                return safe_convert<V, options>(lhs % rhs64);
            }
            else { // rhs > INT64_MAX

                if constexpr (is_twos_complement_integer_v<int64_t>) {
                    if (lhs == INT64_MIN && rhs == int64_max + 1) {
                        return safe_convert<V, options>(0);
                    }
                    else { // rhs > abs(lhs)
                        return safe_convert<V, options>(lhs);
                    }
                }
                else {
                    return safe_convert<V, options>(
                        safe_abs(lhs) == rhs ? 0 : lhs);
                }
            }
        }
    }
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_unsigned_v<T> && std::is_signed_v<U>)
constexpr arithmetic_outcome<V>
safe_modulo_private(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    if constexpr (std::is_signed_v<common_t>) {
        return safe_convert<V, options>(lhs % rhs);
    }
    else {
        using unsigned_U = std::make_unsigned_t<U>;

        if (rhs > 0) {
            return safe_convert<V, options>(lhs % static_cast<unsigned_U>(rhs));
        }
        else {
            auto const pos_rhs = safe_negate<unsigned_U>(rhs);
            assert(pos_rhs);

            return safe_convert<V, options>(lhs % *pos_rhs);
        }
    }
}

template<typename V, arithmetic_option options = arithmetic_option::none,
         typename T, typename U>
    requires is_integral_v<T, U, V>
constexpr arithmetic_outcome<V>
safe_modulo(T const lhs_, U const rhs_)
{
    auto const lhs = integral_promotion(lhs_);
    auto const rhs = integral_promotion(rhs_);

    if constexpr (check_zero_division(options)) {
        if (rhs == 0) {
            return arithmetic_errc::divide_by_zero;
        }
    }

    return safe_modulo_private<V, options>(lhs, rhs);
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_MODULO_HPP
