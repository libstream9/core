#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_DIVIDE_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_DIVIDE_HPP

#include <stream9/safe_integer/arithmetic/safe_divide.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/fwd.hpp>
#include <stream9/safe_integer/type_traits.hpp>
#include <stream9/safe_integer/utility.hpp>

#include <type_traits>
#include <limits>

namespace stream9::safe_integers {

template<typename T, typename U>
class divide_type;


template<typename T, typename U>
    requires is_safe_integer_v<T, U>
constexpr auto // arithmetic_outcome<safe_integer>
divide(T const lhs, U const rhs)
{
    using result_t = typename divide_type<T, U>::type;
    using value_t = typename result_t::value_type;

    auto make_option = [&] {
        auto result = arithmetic_option::none;

        if constexpr (T::is_left_bounded()) {
            result |= arithmetic_option::dont_check_underflow;
        }

        if constexpr (T::is_right_bounded()) {
            result |= arithmetic_option::dont_check_overflow;
        }

        if constexpr (less(0, U::min()) || less(U::max(), 0)) {
            result |= arithmetic_option::dont_check_zero_division;
        }

        return result;
    };

    auto const rv =
            safe_divide<value_t, make_option()>(lhs.value(), rhs.value());
    if (rv) {
        return arithmetic_outcome<result_t> { rv.value() };
    }
    else {
        return arithmetic_outcome<result_t> { rv.error() };
    }
}


template<typename T, typename U>
class divide_type
{
private:
    using T_t = typename T::value_type;
    using U_t = typename U::value_type;
    using value_t = arithmetic_common_t<T_t, U_t>;

    static_assert(!(U::min() == 0 && U::max() == 0), "Zero division guaranteed");

    template<typename T1, typename T2>
    static auto constexpr divide(T1 const lhs, T2 const rhs)
    {
        return safe_divide<value_t>(lhs, rhs);
    }

    static auto constexpr boundary()
    {
        auto constexpr t_min = integral_promotion(T::min());
        auto constexpr t_max = integral_promotion(T::max());
        auto u_min = integral_promotion(U::min());
        auto u_max = integral_promotion(U::max());

        if (u_min == 0 && u_max > 0) {
            u_min = 1;
        }
        else if (std::is_signed_v<U> && u_min < 0 && u_max == 0) {
            u_max = -1;
        }

        if (less_equal(0, t_min) && less_equal(t_min, t_max)) {
            if (less(0, u_min) && less_equal(u_min, u_max)) {
                return std::make_pair(
                    divide(t_min, u_max),
                    divide(t_max, u_min)
                );
            }
            else if (less(u_min, 0) && less(0, u_max)) {
                return std::make_pair(
                    divide(t_max, -1),
                    divide(t_max, 1)
                );
            }
            else { // u_min <= u_max < 0
                return std::make_pair(
                    divide(t_max, u_max),
                    divide(t_min, u_min)
                );
            }
        }
        else if (less(t_min, 0) && less_equal(0, t_max)) {
            if (less(0, u_min) && less_equal(u_min, u_max)) {
                return std::make_pair(
                    divide(t_min, u_min),
                    divide(t_max, u_min)
                );
            }
            else if (less(u_min, 0) && less(0, u_max)) {
                return std::make_pair(
                    utility::min(divide(t_min, 1), divide(t_max, -1)),
                    utility::max(divide(t_max, 1), divide(t_min, -1))
                );
            }
            else { // u_min <= u_max < 0
                return std::make_pair(
                    divide(t_max, u_max),
                    divide(t_min, u_max)
                );
            }
        }
        else { // t_min <= t_max <= 0
            if (less(0, u_min) && less_equal(u_min, u_max)) {
                return std::make_pair(
                    divide(t_min, u_min),
                    divide(t_max, u_max)
                );
            }
            else if (less(u_min, 0) && less_equal(0, u_max)) {
                return std::make_pair(
                    divide(t_min, 1),
                    divide(t_min, -1)
                );
            }
            else { // u_min <= u_max < 0
                return std::make_pair(
                    divide(t_max, u_min),
                    divide(t_min, u_max)
                );
            }
        }
    }

    static auto constexpr m_boundary = boundary();

public:
    using type = safe_integer<
        value_t,
        m_boundary.first.value_or(std::numeric_limits<value_t>::min()),
        m_boundary.second.value_or(std::numeric_limits<value_t>::max())
    >;
};

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_DIVIDE_HPP
