#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_DIVIDE_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_DIVIDE_HPP

#include <stream9/safe_integer/arithmetic/safe_convert.hpp>
#include <stream9/safe_integer/arithmetic_option.hpp>
#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/conversion.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <cstdint>
#include <type_traits>

namespace stream9::safe_integers {

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_unsigned_v<T> && std::is_unsigned_v<U>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    return safe_convert<V, options>(lhs / rhs);
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_signed_v<T> && std::is_signed_v<U>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    if constexpr (check_underflow(options)) {
        if (std::is_unsigned_v<V> && ((lhs < 0) ^ (rhs < 0))) {
            return arithmetic_errc::underflow;
        }
    }

    using common_t = arithmetic_common_t<T, U>;

    // on 2's compliment system, MIN / -1 will overflow
    if constexpr (is_twos_complement_integer_v<common_t>) {
        if (lhs == std::numeric_limits<common_t>::min() && rhs == -1) {
            static_assert(std::is_signed_v<common_t>);

            return safe_convert<V, options>(
                static_cast<std::make_unsigned_t<common_t>>(
                    std::numeric_limits<common_t>::max()
                ) + 1u
            );
        }
    }

    return safe_convert<V, options>(lhs / rhs);
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_signed_v<T> && std::is_unsigned_v<U> && std::is_signed_v<V>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    using common_t = arithmetic_common_t<T, U>;

    if constexpr (std::is_signed_v<common_t>) {
        return safe_convert<V, options>(lhs / rhs);
    }
    else if (lhs >= 0) {
        return safe_convert<V, options>(static_cast<common_t>(lhs) / rhs);
    }
    else { // common_t is unsigned and lhs is negative
        if constexpr (!std::is_same_v<common_t, uint64_t>) {
            // sizeof(T) <= sizeof(U) && U is smaller than uint64_t
            auto const larger_signed_rhs =
                static_cast<make_larger_t<
                            std::make_signed_t<U> >>(rhs);

            return safe_convert<V, options>(lhs / larger_signed_rhs);
        }
        else { // common_t is uint64_t which imply U is uint64_t
            auto const int64_max = static_cast<uint64_t>(INT64_MAX);

            if (rhs <= int64_max) {
                auto const rhs64 = static_cast<int64_t>(rhs);

                return safe_convert<V, options>(lhs / rhs64);
            }
            else { // rhs > INT64_MAX

                if (is_twos_complement_integer_v<int64_t> &&
                    lhs == INT64_MIN && rhs == int64_max + 1)
                {
                    return safe_convert<V, options>(-1);
                }
                else { // rhs > abs(lhs)
                    return safe_convert<V, options>(0);
                }
            }
        }
    }
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_signed_v<T> && std::is_unsigned_v<U> && std::is_unsigned_v<V>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    if constexpr (check_underflow(options)) {
        if (lhs < 0) {
            return arithmetic_errc::underflow;
        }
    }

    using unsigned_T = std::make_unsigned_t<T>;

    return safe_divide_private<V, options>(static_cast<unsigned_T>(lhs), rhs);
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_unsigned_v<T> && std::is_signed_v<U> && std::is_signed_v<V>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    if (rhs > 0) {
        using unsigned_U = std::make_unsigned_t<U>;

        return safe_divide_private<V, options>(
                                    lhs, static_cast<unsigned_U>(rhs));
    }
    else { // rhs is negative
        using common_t = arithmetic_common_t<T, U>;

        if constexpr (std::is_signed_v<common_t>) {
            return safe_convert<V, options>(lhs / rhs);
        }
        // common_t is unsigned
        else if constexpr (!std::is_same_v<common_t, uint64_t>) { //TODO is_smaller_v<common_t, uint64_t>
            // T is smaller than uint64_t
            auto const larger_signed_lhs =
                static_cast<make_larger_t<std::make_signed_t<T>>>(lhs);

            return safe_convert<V, options>(larger_signed_lhs / rhs);
        }
        else { // common_t is uint64_t which imply T is uint64_t
            auto const int64_max = static_cast<uint64_t>(INT64_MAX);

            if (lhs <= int64_max) {
                auto const signed_lhs = static_cast<int64_t>(lhs);

                return safe_convert<V, options>(signed_lhs / rhs);
            }
            else { // lhs > INT64_MAX

                if constexpr (is_twos_complement_integer_v<int64_t>) {
                    if (rhs == INT64_MIN) {
                        // INT64_MAX < lhs <= UINT64_MAX
                        //                 <= 2 * INT64_MAX
                        // INT64_MAX + 1 / INT64_MIN
                        //               / -(INT64_MAX + 1) = -1
                        // UINT64_MAX    / INT64_MIN
                        // 2 * INT64_MAX /
                        //               / -(INT64_MAX + 1) = -1
                        return safe_convert<V, options>(-1);
                    }
                    else if (rhs == -1) {
                        if constexpr (check_underflow(options)) {
                            if (lhs != int64_max + 1u) {
                                return arithmetic_errc::underflow;
                            }
                        }

                        return safe_convert<V, options>(INT64_MIN);
                    }
                    else { // INT64_MIN < rhs < -1
                        auto const abs_rhs = static_cast<uint64_t>(
                                                  -static_cast<int64_t>(rhs) );

                        auto const abs_result =
                                        static_cast<int64_t>(lhs / abs_rhs);

                        return safe_convert<V, options>(-abs_result);
                    }
                }
                else { // assume -rhs won't overflow
                    auto const abs = lhs / -rhs;

                    if constexpr (check_overflow(options)) {
                        if (abs > int64_max) {
                            return arithmetic_errc::underflow;
                        }
                    }

                    return safe_convert<V, options>(
                                                -static_cast<int64_t>(abs));
                }
            }
        }
    }
}

template<typename V, arithmetic_option options,
         typename T, typename U>
    requires is_integral_v<T, U, V>
          && (std::is_unsigned_v<T> && std::is_signed_v<U> && std::is_unsigned_v<V>)
constexpr arithmetic_outcome<V>
safe_divide_private(T const lhs, U const rhs)
{
    if constexpr (check_underflow(options)) {
        if (rhs < 0) {
            return arithmetic_errc::underflow;
        }
    }

    using unsigned_U = std::make_unsigned_t<U>;

    return safe_divide_private<V, options>(lhs, static_cast<unsigned_U>(rhs));
}

template<typename V, arithmetic_option options = arithmetic_option::none,
         typename T, typename U>
    requires is_integral_v<T, U, V>
constexpr arithmetic_outcome<V>
safe_divide(T const lhs_, U const rhs_)
{
    auto const lhs = integral_promotion(lhs_);
    auto const rhs = integral_promotion(rhs_);

    if constexpr (check_zero_division(options)) {
        if (rhs == 0) {
            return arithmetic_errc::divide_by_zero;
        }
    }

    return safe_divide_private<V, options>(lhs, rhs);
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_DIVIDE_HPP
