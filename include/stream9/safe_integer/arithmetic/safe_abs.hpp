#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_ABS_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_ABS_HPP

#include <stream9/safe_integer/type_traits.hpp>

#include <type_traits>

namespace stream9::safe_integers {

template<typename T>
    requires is_integral_v<T>
constexpr auto
safe_abs(T const v)
{
    using unsigned_T = std::make_unsigned_t<T>;

    if (v >= 0) {
        return static_cast<unsigned_T>(v);
    }
    else if (is_twos_complement_integer_v<T> &&
                                        v == std::numeric_limits<T>::min())
    {
        return static_cast<unsigned_T>(std::numeric_limits<T>::max()) + 1u;
    }
    else {
        return static_cast<unsigned_T>(-v);
    }
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_ABS_HPP
