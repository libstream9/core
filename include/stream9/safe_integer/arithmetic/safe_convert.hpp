#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_CONVERT_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_CONVERT_HPP

#include <stream9/safe_integer/arithmetic_outcome.hpp>
#include <stream9/safe_integer/arithmetic_option.hpp>
#include <stream9/safe_integer/comparison.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <concepts>
#include <limits>

namespace stream9::safe_integers {

template<typename U,
         arithmetic_option options = arithmetic_option::none,
         typename T >
    requires is_integral_v<T, U>
constexpr arithmetic_outcome<U>
safe_convert(T const v)
{
    if constexpr (std::same_as<U, bool>) {
        return v != 0;
    }

    auto constexpr t_min = std::numeric_limits<T>::min();
    auto constexpr t_max = std::numeric_limits<T>::max();
    auto constexpr u_min = std::numeric_limits<U>::min();
    auto constexpr u_max = std::numeric_limits<U>::max();

    if constexpr (check_underflow(options) && less(t_min, u_min)) {
        if (less(v, u_min)) {
            return arithmetic_errc::underflow;
        }
    }

    if constexpr (check_overflow(options) && greater(t_max, u_max)) {
        if (greater(v, u_max)) {
            return arithmetic_errc::overflow;
        }
    }

    return static_cast<U>(v);
}

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_SAFE_CONVERT_HPP
