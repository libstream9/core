#ifndef STREAM9_SAFE_INTEGER_ARITHMETIC_OUTCOME_HPP
#define STREAM9_SAFE_INTEGER_ARITHMETIC_OUTCOME_HPP

#include <stream9/safe_integer/comparison.hpp>
#include <stream9/safe_integer/outcome.hpp>
#include <stream9/safe_integer/type_traits.hpp>

#include <cassert>
#include <iosfwd>

namespace stream9::safe_integers {

enum class arithmetic_errc {
    ok = 0,
    overflow,
    underflow,
    divide_by_zero
};

template<typename T>
class arithmetic_outcome : public outcome<T, arithmetic_errc>
{
    using parent_t = outcome<T, arithmetic_errc>;
public:
    using value_type = T;

public:
    using parent_t::parent_t;

    constexpr arithmetic_outcome(arithmetic_errc const errc) noexcept
        : parent_t(errc)
    {
        assert(errc != arithmetic_errc::ok);
    }

    template<typename U>
    constexpr bool operator<(arithmetic_outcome<U> const other) const noexcept
    {
        if (this->has_value()) {
            if (other.has_value()) {
                return less(this->value(), other.value());
            }
            else {
                // overflow is biggest, othe errors are smallest
                return other.error() == arithmetic_errc::overflow;
            }
        }
        else {
            if (other.has_value()) {
                // overflow is biggest, othe errors are smallest
                return this->error() != arithmetic_errc::overflow;
            }
            else {
                if (this->error() == arithmetic_errc::overflow) {
                    // nothing is bigger than overflow
                    return false;
                }
                else { // this->error() is smallest
                    return other.error() == arithmetic_errc::overflow;
                }
            }
        }
    }

    template<typename U>
    constexpr bool operator>(arithmetic_outcome<U> const other) const noexcept
    {
        return other < *this;
    }
};

std::ostream& operator<<(std::ostream&, arithmetic_errc);

} // namespace stream9::safe_integers

#endif // STREAM9_SAFE_INTEGER_ARITHMETIC_OUTCOME_HPP
