#ifndef STREAM9_NUMBER_HPP
#define STREAM9_NUMBER_HPP

#include "safe_integer.hpp"

#include <cstdint>

namespace stream9 {

template<typename T = ::intmax_t,
         T Maximum = std::numeric_limits<T>::max()>
using natural = safe_integer<T, 0, Maximum>;

template<typename T = ::intmax_t,
         T Minimum = std::numeric_limits<T>::min(),
         T Maximum = std::numeric_limits<T>::max()>
using integer = safe_integer<T, Minimum, Maximum>;

} // namespace stream9

#endif // STREAM9_NUMBER_HPP
