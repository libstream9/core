#ifndef STREAM9_OUTCOME_TYPE_TRAITS_HPP
#define STREAM9_OUTCOME_TYPE_TRAITS_HPP

#include "outcome.hpp"

#include <concepts>
#include <type_traits>

namespace stream9 {

template<typename T, typename E>
using outcome_value_t = decltype(std::declval<outcome<T, E>>().value());

template<typename T, typename E>
using outcome_error_t = decltype(std::declval<outcome<T, E>>().error());

template<typename, typename> struct common_error_type; //TODO move to stream9::errors

template<typename E>
struct common_error_type<E, E>
{
    using type = E;
};

template<typename E1, typename E2>
    requires (!std::is_error_code_enum_v<E1> || !std::is_error_code_enum_v<E2>)
          && requires { typename std::common_type_t<E1, E2>; }
struct common_error_type<E1, E2>
{
    using type = std::common_type_t<E1, E2>;
};

template<typename E1, typename E2>
    requires std::is_error_code_enum_v<E1>
          && std::is_error_code_enum_v<E2>
struct common_error_type<E1, E2>
{
    using type = std::error_code;
};

template<typename E>
    requires std::is_error_code_enum_v<E>
struct common_error_type<E, std::error_code>
{
    using type = std::error_code;
};

template<typename E>
    requires std::is_error_code_enum_v<E>
struct common_error_type<std::error_code, E>
{
    using type = std::error_code;
};

template<typename E1, typename E2>
using common_error_type_t = common_error_type<E1, E2>::type;

} // namespace stream9

namespace std {

template<typename T1, typename E1, typename T2, typename E2>
    requires requires {
        typename std::common_type_t<T1, T2>;
        typename stream9::common_error_type_t<E1, E2>;
    }
struct common_type<stream9::outcome<T1, E1>, stream9::outcome<T2, E2>>
{
    using type = stream9::outcome<
                    std::common_type_t<T1, T2>,
                    stream9::common_error_type_t<E1, E2> >;
};

template<typename T1, typename E1, typename T2>
    requires requires {
        typename std::common_type_t<T1, T2>;
    }
struct common_type<stream9::outcome<T1, E1>, T2>
{
    using type = stream9::outcome<std::common_type_t<T1, T2>, E1>;
};

template<typename T1, typename E1, typename E2>
    requires requires {
        typename stream9::common_error_type_t<E1, E2>;
    }
struct common_type<stream9::outcome<T1, E1>, E2>
{
    using type = stream9::outcome<T1, stream9::common_error_type_t<E1, E2>>;
};

} // namespace std

#endif // STREAM9_OUTCOME_TYPE_TRAITS_HPP
