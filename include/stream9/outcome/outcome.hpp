#ifndef STREAM9_OUTCOME_OUTCOME_HPP
#define STREAM9_OUTCOME_OUTCOME_HPP

#include "storage.hpp"

#include <stream9/errors.hpp>
#include <stream9/namespace.hpp>
#include <stream9/string.hpp>

#include <compare>
#include <concepts>
#include <system_error>
#include <type_traits>
#include <utility>

namespace stream9 {

template<typename T>
concept outcome_value = (std::is_object_v<T> && !std::is_const_v<T>);

template<typename E>
concept outcome_error = std::is_object_v<E> && !std::is_const_v<E>
                     && std::is_nothrow_default_constructible_v<E>
                     && std::is_nothrow_move_constructible_v<E>
                     && std::is_nothrow_move_assignable_v<E>
                     && std::is_nothrow_swappable_v<E>;

template<typename>
struct is_outcome : std::false_type {};

template<typename T>
constexpr auto is_outcome_v = is_outcome<T>::value;

struct error_tag {};
struct value_tag {};

/*
 * @model std::default_initializable if T is one
 * @model std::copyable if both T and E are one
 * @model std::movable if both T and E are one
 * @model std::equality_comparable if both T and E are one
 * @model std::three_way_comparable if both T and E are one
 */
template<typename T, typename E> class outcome;

template<outcome_value T, outcome_error E>
class outcome<T, E>
{
public:
    // essentials
    constexpr outcome()
        noexcept(std::is_nothrow_default_constructible_v<T>)
        requires std::is_default_constructible_v<T>;

    template<typename X>
    constexpr outcome(X&& x)
        noexcept(std::is_nothrow_constructible_v<T, X>)
        requires std::constructible_from<T, X>
        : m_value { std::forward<X>(x) }
        , m_has_value { true }
    {}

    template<typename... X>
    constexpr outcome(value_tag, X&&... value)
        noexcept(std::is_nothrow_constructible_v<T, X...>)
        requires std::constructible_from<T, X...>
        : m_value { std::forward<X>(value)... }
        , m_has_value { true }
    {}

    template<typename X>
    constexpr outcome(X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_errc { x }
    {}

    template<typename... X>
    constexpr outcome(error_tag, X&&... x)
        noexcept(std::is_nothrow_constructible_v<E, X...>)
        requires std::constructible_from<E, X...>
        : m_errc { std::forward<X>(x)... }
    {}

    template<typename X, typename Y>
    constexpr outcome(X&& x, Y&& y)
        noexcept(std::is_nothrow_constructible_v<T, X> && std::is_nothrow_constructible_v<E, Y>)
        requires std::constructible_from<T, X>
              && std::constructible_from<E, Y>
        : m_value { std::forward<X>(x) }
        , m_errc { std::forward<Y>(y) }
        , m_has_value { true }
    {}

    outcome(outcome const&) = default;
    outcome& operator=(outcome const&) = default;

    outcome(outcome&&) = default;
    outcome& operator=(outcome&&) = default;

    template<typename T2, typename E2>
    constexpr outcome(outcome<T2, E2> const&)
        noexcept(std::is_nothrow_constructible_v<T, T2 const&>
              && std::is_nothrow_constructible_v<E, E2 const&>)
        requires std::is_constructible_v<T, T2 const&>
              && std::is_constructible_v<E, E2 const&>;

    template<typename T2, typename E2>
    constexpr outcome(outcome<T2, E2>&&)
        noexcept(std::is_nothrow_constructible_v<T, T2&&>
              && std::is_nothrow_constructible_v<E, E2&&>)
        requires std::is_object_v<T2>
              && std::is_constructible_v<T, T2&&>
              && std::is_constructible_v<E, E2&&>;

    constexpr ~outcome() noexcept;

    // accessor
    template<typename X>
    constexpr auto&&
    value(this X&& self)
    {
        return std::forward<X>(self).m_value.value();
    }

    template<typename X>
    constexpr auto&&
    error(this X&& self)
    {
        return std::forward<X>(self).m_errc;
    }

    // query
    template<typename Self>
    auto&&
    or_throw(this Self&& self, source_location loc = {})
        requires (std::constructible_from<st9::error, E>)
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw st9::error {
                self.m_errc,
                std::move(loc)
            };
        }
    }

    template<typename Self, typename Fn>
    auto&&
    or_throw(this Self&& self, Fn&& fn)
        requires std::invocable<Fn, E>
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw fn(self.error());
        }
    }

    [[noreturn]] void
    throw_error(source_location loc = {})
    {
        throw st9::error {
            m_errc,
            std::move(loc),
        };
    }

    constexpr bool has_value() const noexcept;
    constexpr bool has_error() const noexcept;

    template<typename X>
    auto&&
    operator*(this X&& self)
    {
        return std::forward<X>(self).m_value.value();
    }

    constexpr T const* operator->() const noexcept;
    constexpr T* operator->() noexcept;

    explicit constexpr operator bool() const noexcept;

    // modifier
    void swap(outcome&)
        noexcept(std::is_nothrow_swappable_v<T>);

    // comparison
    template<typename T2, typename E2>
    bool operator==(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() == other.value())
                 && noexcept(this->error() == other.error()) )
        requires std::equality_comparable_with<T, T2>
              && std::equality_comparable_with<E, E2>;

    template<typename E2>
    bool operator==(E2 const& e) const
        noexcept(noexcept(this->error() == e))
        requires (!is_outcome_v<E2>)
              && std::equality_comparable_with<E, E2>;

    template<typename T2, typename E2>
    std::partial_ordering
    operator<=>(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() <=> other.value())
                 && noexcept(this->error() <=> other.error()) )
        requires std::three_way_comparable_with<T, T2>
              && std::three_way_comparable_with<E, E2>;

    template<typename E2>
    std::partial_ordering
    operator<=>(E2 const& e) const
        noexcept(noexcept(this->error() <=> e))
        requires (!is_outcome_v<E2>)
              && std::three_way_comparable_with<E, E2>;

private:
    outcome_::storage<T> m_value;
    E m_errc {};
    bool m_has_value : 1 = false;
};

template<typename T, outcome_error E>
class outcome<T&, E>
{
public:
    // essentials
    outcome() = delete;

    constexpr outcome(T& ref) noexcept;

    outcome(T&& ref) = delete;
    outcome& operator=(T&& ref) = delete;

    constexpr outcome(value_tag, T& ref) noexcept
        : m_ptr { &ref }
    {}

    template<typename X>
    constexpr outcome(X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_errc { std::forward<X>(x) }
    {}

    template<typename... X>
    constexpr outcome(error_tag, X&&... x)
        noexcept(std::is_nothrow_constructible_v<E, X...>)
        requires std::constructible_from<E, X...>
        : m_errc { std::forward<X>(x)... }
    {}

    template<typename X>
    constexpr outcome(T& ref, X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_ptr { &ref }
        , m_errc { std::forward<X>(x) }
    {}

    outcome(outcome const&) = default;
    outcome& operator=(outcome const&) = default;

    outcome(outcome&&) = default;
    outcome& operator=(outcome&&) = default;

    template<typename T2, typename E2>
    constexpr outcome(outcome<T2, E2> const&)
        noexcept(std::is_nothrow_constructible_v<E, E2 const&>)
        requires std::is_convertible_v<T2, T&>
              && std::is_constructible_v<E, E2 const&>;

    template<typename T2, typename E2>
    constexpr outcome(outcome<T2, E2>&&)
        noexcept(std::is_nothrow_constructible_v<E, E2&&>)
        requires std::is_convertible_v<T2, T&>
              && std::is_constructible_v<E, E2&&>;

    ~outcome() = default;

    // accessor
    constexpr T& value() const noexcept;

    template<typename X>
    auto&&
    error(this X&& self)
    {
        return std::forward<X>(self).m_errc;
    }

    // query
    template<typename Self>
    auto&&
    or_throw(this Self&& self, source_location loc = {})
        requires (std::constructible_from<st9::error, E>)
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw st9::error {
                self.m_errc,
                std::move(loc)
            };
        }
    }

    template<typename Self, typename Fn>
    auto&&
    or_throw(this Self&& self, Fn&& fn)
        requires std::invocable<Fn, E>
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw fn(self.error());
        }
    }

    constexpr bool has_value() const noexcept;
    constexpr bool has_error() const noexcept;

    constexpr T& operator*() const noexcept;

    constexpr T* operator->() const noexcept;

    explicit constexpr operator bool() const noexcept;

    // modifier
    void swap(outcome&) noexcept;

    // comparison
    template<typename T2, typename E2>
    bool operator==(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() == other.value())
                 && noexcept(this->error() == other.error()) )
        requires std::equality_comparable_with<T, T2>
              && std::equality_comparable_with<E, E2>;

    template<typename E2>
    bool operator==(E2 const& e) const
        noexcept(noexcept(this->error() == e))
        requires (!is_outcome_v<E2>)
              && std::equality_comparable_with<E, E2>;

    template<typename T2, typename E2>
    std::partial_ordering
    operator<=>(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() <=> other.value())
                 && noexcept(this->error() <=> other.error()) )
        requires std::three_way_comparable_with<T, T2>
              && std::three_way_comparable_with<E, E2>;

    template<typename E2>
    std::partial_ordering
    operator<=>(E2 const& e) const
        noexcept(noexcept(this->error() <=> e))
        requires (!is_outcome_v<E2>)
              && std::three_way_comparable_with<E, E2>;

private:
    T* m_ptr = nullptr; // non-null
    E m_errc {};
};

template<typename T, outcome_error E>
class outcome<T&&, E>
{
public:
    // essentials
    outcome() = delete;

    outcome(T& ref) = delete;
    outcome& operator=(T& ref) = delete;

    constexpr outcome(T&& ref) noexcept;

    constexpr outcome(value_tag, T&& ref)
        : m_ptr { &ref }
    {}

    template<typename X>
    constexpr outcome(X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_errc { std::forward<X>(x) }
    {}

    template<typename... X>
    constexpr outcome(error_tag, X&&... x)
        noexcept(std::is_nothrow_constructible_v<E, X...>)
        requires std::constructible_from<E, X...>
        : m_errc { std::forward<X>(x)... }
    {}

    template<typename X>
    constexpr outcome(T&& ref, X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_ptr { &ref }
        , m_errc { std::forward<X>(x) }
    {}

    outcome(outcome const&) = default;
    outcome& operator=(outcome const&) = default;

    outcome(outcome&&) = default;
    outcome& operator=(outcome&&) = default;

    template<typename T2, typename E2>
    constexpr outcome(outcome<T2, E2> const&)
        noexcept(std::is_nothrow_copy_constructible_v<E>)
        requires std::is_convertible_v<T2, T&&>
              && std::is_constructible_v<E, E2 const&>;

    ~outcome() = default;

    // accessor
    constexpr T&& value() const noexcept;

    template<typename X>
    auto&&
    error(this X&& self)
    {
        return std::forward<X>(self).m_errc;
    }

    // query
    template<typename Self>
    auto&&
    or_throw(this Self&& self, source_location loc = {})
        requires (std::constructible_from<st9::error, E>)
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw st9::error {
                self.m_errc,
                std::move(loc)
            };
        }
    }

    template<typename Self, typename Fn>
    auto&&
    or_throw(this Self&& self, Fn&& fn)
        requires std::invocable<Fn, E>
    {
        if (!self.has_error()) {
            return std::forward<Self>(self).value();
        }
        else {
            throw fn(self.error());
        }
    }

    constexpr bool has_value() const noexcept;
    constexpr bool has_error() const noexcept;

    constexpr T&& operator*() const noexcept;

    explicit constexpr operator bool() const noexcept;

    // modifier
    void swap(outcome&) noexcept;

    // comparison
    template<typename T2, typename E2>
    bool operator==(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() == other.value())
                 && noexcept(this->error() == other.error()) )
        requires std::equality_comparable_with<T, T2>
              && std::equality_comparable_with<E, E2>;

    template<typename E2>
    bool operator==(E2 const& e) const
        noexcept(noexcept(this->error() == e))
        requires (!is_outcome_v<E2>)
              && std::equality_comparable_with<E, E2>;

    template<typename T2, typename E2>
    std::partial_ordering
    operator<=>(outcome<T2, E2> const& other) const
        noexcept(noexcept(this->value() <=> other.value())
                 && noexcept(this->error() <=> other.error()) )
        requires std::three_way_comparable_with<T, T2>
              && std::three_way_comparable_with<E, E2>;

    template<typename E2>
    std::partial_ordering
    operator<=>(E2 const& e) const
        noexcept(noexcept(this->error() <=> e))
        requires (!is_outcome_v<E2>)
              && std::three_way_comparable_with<E, E2>;

private:
    T* m_ptr = nullptr;
    E m_errc {};
};

template<outcome_error E>
class outcome<void, E>
{
public:
    // essentials
    outcome() = default;

    template<typename X>
    constexpr outcome(X&& x)
        noexcept(std::is_nothrow_constructible_v<E, X>)
        requires std::constructible_from<E, X>
        : m_errc { std::forward<X>(x) }
    {}

    outcome(outcome const&) = default;
    outcome& operator=(outcome const&) = default;

    outcome(outcome&&) = default;
    outcome& operator=(outcome&&) = default;

    template<typename E2>
    constexpr outcome(outcome<void, E2> const&)
        noexcept(std::is_nothrow_constructible_v<E, E2 const&>)
        requires std::is_constructible_v<E, E2 const&>;

    template<typename E2>
    constexpr outcome(outcome<void, E2>&&)
        noexcept(std::is_nothrow_constructible_v<E, E2&&>)
        requires std::is_constructible_v<E, E2&&>;

    ~outcome() = default;

    // accessor
    template<typename X>
    auto&&
    error(this X&& self)
    {
        return std::forward<X>(self).m_errc;
    }

    template<typename Self>
    void
    or_throw(this Self&& self, source_location loc = {})
        requires (std::constructible_from<st9::error, E>)
    {
        if (self.has_error()) {
            throw st9::error {
                self.m_errc,
                std::move(loc)
            };
        }
    }

    template<typename Self, typename Fn>
    void
    or_throw(this Self&& self, Fn&& fn)
        requires std::invocable<Fn, E>
    {
        if (self.has_error()) {
            throw fn(self.error());
        }
    }

    // query
    constexpr bool has_value() const noexcept;
    constexpr bool has_error() const noexcept;

    explicit constexpr operator bool() const noexcept;

    // modifier
    constexpr void swap(outcome&) noexcept;

    // comparison
    template<typename E2>
    constexpr bool operator==(outcome<void, E2> const& other) const
        noexcept(noexcept(this->error() == other.error()))
        requires std::equality_comparable_with<E, E2>;

    template<typename E2>
    constexpr bool operator==(E2 const& e) const
        noexcept(noexcept(this->error() == e))
        requires (!is_outcome_v<E2>)
              && std::equality_comparable_with<E, E2>;

    template<typename E2>
    constexpr std::partial_ordering
    operator<=>(outcome<void, E2> const& other) const
        noexcept(noexcept(this->error() <=> other.error()))
        requires std::three_way_comparable_with<E, E2>;

    template<typename E2>
    constexpr std::partial_ordering
    operator<=>(E2 const& e) const
        noexcept(noexcept(this->error() <=> e))
        requires (!is_outcome_v<E2>)
              && std::three_way_comparable_with<E, E2>;

private:
    E m_errc {};
};

template<typename T, typename E>
struct is_outcome<outcome<T, E>>
    : std::true_type {};

} // namespace stream9

#endif // STREAM9_OUTCOME_OUTCOME_HPP

#include "type_traits.hpp"
#include "outcome.ipp"
