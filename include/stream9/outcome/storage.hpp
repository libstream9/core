#ifndef STREAM9_OUTCOME_STORAGE_HPP
#define STREAM9_OUTCOME_STORAGE_HPP

#include <concepts>
#include <cstddef>
#include <type_traits>
#include <utility>

namespace stream9::outcome_ {

template<typename T>
class storage
{
public:
    // essentials
    constexpr storage() noexcept // partially-formed
        requires (!std::default_initializable<T>)
    {}

    constexpr storage()
        noexcept(std::is_nothrow_default_constructible_v<T>)
        requires std::default_initializable<T>
    {
        new (m_buf) T {};
    }

    constexpr storage(T const& v)
        noexcept(std::is_nothrow_copy_constructible_v<T>)
        requires std::copy_constructible<T>
    {
        new (m_buf) T { v };
    }

    constexpr storage(T&& v)
        noexcept(std::is_nothrow_move_constructible_v<T>)
        requires std::move_constructible<T>
    {
        new (m_buf) T { std::move(v) };
    }

    template<typename... Args>
    constexpr storage(Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        requires std::constructible_from<T, Args...>
    {
        new (m_buf) T { std::forward<Args>(args)... };
    }

    constexpr storage(storage const& other)
        noexcept(std::is_nothrow_copy_constructible_v<T>)
        requires std::is_copy_constructible_v<T>
    {
        new (m_buf) T { other.value() };
    }

    constexpr storage& operator=(storage const& other)
        noexcept(std::is_nothrow_copy_assignable_v<T>)
        requires std::is_copy_assignable_v<T>
    {
        value() = other.value();
        return *this;
    }

    constexpr storage(storage&& other)
        noexcept(std::is_nothrow_move_constructible_v<T>)
        requires std::is_move_constructible_v<T>
    {
        new (m_buf) T { std::move(other.value()) };
    }

    constexpr storage& operator=(storage&& other)
        noexcept(std::is_nothrow_move_assignable_v<T>)
        requires std::is_move_assignable_v<T>
    {
        value() = std::move(other.value());
        return *this;
    }

    ~storage() = default;

    // accessor
    constexpr T const& value() const& noexcept
    {
        return *reinterpret_cast<T const*>(m_buf);
    }

    constexpr T& value() & noexcept
    {
        return *reinterpret_cast<T*>(m_buf);
    }

    constexpr T const&& value() const&& noexcept
    {
        return static_cast<T const&&>(*reinterpret_cast<T const*>(m_buf));
    }

    constexpr T&& value() && noexcept
    {
        return static_cast<T&&>(*reinterpret_cast<T*>(m_buf));
    }

    constexpr operator T const& () const& noexcept
    {
        return value();
    }

    constexpr operator T& () & noexcept
    {
        return value();
    }

    constexpr operator T const&& () const&& noexcept
    {
        return value();
    }

    constexpr operator T&& () && noexcept
    {
        return value();
    }

    // modifier
    constexpr void swap(storage& other) noexcept
    {
        using std::swap;
        swap(value(), other.value());
    }

    constexpr void emplace(T const& v)
        noexcept(std::is_nothrow_copy_constructible_v<T>)
        requires std::copy_constructible<T>
    {
        new (m_buf) T { v };
    }

    constexpr void emplace(T&& v)
        noexcept(std::is_nothrow_move_constructible_v<T>)
        requires std::move_constructible<T>
    {
        new (m_buf) T { std::move(v) };
    }

    constexpr void destroy() noexcept
    {
        value().~T();
    }

public:
    alignas(T) std::byte m_buf[sizeof(T)];
};

template<typename T>
    requires std::is_trivial_v<T>
class storage<T>
{
public:
    // essentials
    storage() noexcept = default;

    constexpr storage(T const& v) noexcept
        : m_value { v } {}

    constexpr storage(T&& v) noexcept
        : m_value { std::move(v) } {}

    template<typename... Args>
    constexpr storage(Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        requires std::constructible_from<T, Args...>
        : m_value { std::forward<Args>(args)... } {}

    storage(storage const&) = default;
    storage& operator=(storage const&) = default;

    storage(storage&&) = default;
    storage& operator=(storage&&) = default;

    ~storage() = default;

    // accessor
    template<typename X>
    constexpr auto&&
    value(this X&& self)
    {
        return std::forward<X>(self).m_value;
    }

    constexpr operator T const& () const& noexcept
    {
        return value();
    }

    constexpr operator T& () & noexcept
    {
        return value();
    }

    constexpr operator T const&& () const&& noexcept
    {
        return value();
    }

    constexpr operator T&& () && noexcept
    {
        return value();
    }

    // modifier
    constexpr void swap(storage& other) noexcept
    {
        using std::swap;
        swap(value(), other.value());
    }

    constexpr void destroy() noexcept {}

public:
    T m_value;
};

} // namespace stream9::outcome_

#endif // STREAM9_OUTCOME_STORAGE_HPP
