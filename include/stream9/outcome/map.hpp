#ifndef CORE_STREAM9_OUTCOME_MAP_HPP
#define CORE_STREAM9_OUTCOME_MAP_HPP

#include "outcome.hpp"
#include "type_traits.hpp"

#include <concepts>
#include <system_error>
#include <type_traits>

namespace stream9 {

template<typename T, typename E>
struct outcome_map_result
{
    using type = outcome<T, E>;
};

template<typename T, typename E>
struct outcome_map_result<outcome<T, E>, E>
{
    using type = outcome<T, E>;
};

template<typename E1, typename T2, typename E2>
struct outcome_map_result<outcome<T2, E2>, E1>
{
    using type = std::common_type_t<outcome<T2, E2>, E1>;
};

template<typename T, typename E, typename F>
using outcome_map_result_t =
    outcome_map_result<std::invoke_result_t<F, T const&>, E>::type;

template<typename T, typename E, typename F>
    requires std::invocable<F, T const&>
outcome_map_result_t<T, E, F>
operator|(outcome<T, E> const& o, F func)
{
    if (o.has_value()) {
        return func(o.value());
    }
    else {
        return o.error();
    }
}

} // namespace stream9

#endif // CORE_STREAM9_OUTCOME_MAP_HPP
