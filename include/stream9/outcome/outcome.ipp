#ifndef STREAM9_OUTCOME_OUTCOME_IPP
#define STREAM9_OUTCOME_OUTCOME_IPP

#include "outcome.hpp"

#include <cassert>
#include <compare>
#include <concepts>
#include <type_traits>
#include <utility>

namespace stream9 {

namespace outcome_ {

    template<typename T1, typename E1, typename T2, typename E2>
    bool
    equal(outcome<T1, E1> const& o1, outcome<T2, E2> const& o2)
        noexcept(noexcept(o1.value() == o2.value())
                 && noexcept(o1.error() == o2.error()) )
    {
        if (o1.has_value()) {
            return o2.has_value() ? o1.value() == o2.value() : false;
        }
        else {
            return o2.has_value() ? false : o1.error() == o2.error();
        }
    }

    template<typename T1, typename E1, typename E2>
    bool
    equal(outcome<T1, E1> const& o1, E2 const& e2)
        noexcept(noexcept(o1.error() == e2))
    {
        if (!o1.has_error()) {
            return false;
        }
        else {
            return o1.error() == e2;
        }
    }

    template<typename T1, typename E1, typename T2, typename E2>
    std::partial_ordering
    compare_three_way(outcome<T1, E1> const& o1, outcome<T2, E2> const& o2)
        noexcept(noexcept(o1.value() <=> o2.value())
              && noexcept(o1.error() <=> o2.error()) )
    {
        if (o1.has_value() && o2.has_value()) {
            return o1.value() <=> o2.value();
        }
        else if (!o1.has_value() && !o2.has_value()) {
            return o1.error() <=> o2.error();
        }
        else {
            return std::partial_ordering::unordered;
        }
    }

    template<typename T1, typename E1, typename E2>
    std::partial_ordering
    compare_three_way(outcome<T1, E1> const& o1, E2 const& e2)
        noexcept(noexcept(o1.error() <=> e2))
    {
        if (!o1.has_error()) {
            return std::partial_ordering::unordered;
        }
        else {
            return o1.error() <=> e2;
        }
    }

} // namespace outcome_

/*
 * class outcome<T, E>
 */
template<outcome_value T, outcome_error E>
constexpr outcome<T, E>::
outcome()
    noexcept(std::is_nothrow_default_constructible_v<T>)
    requires std::is_default_constructible_v<T>
    : m_value {}
    , m_has_value { true }
{}

template<outcome_value T, outcome_error E>
template<typename T2, typename E2>
constexpr outcome<T, E>::
outcome(outcome<T2, E2> const& o)
    noexcept(std::is_nothrow_constructible_v<T, T2 const&>
          && std::is_nothrow_constructible_v<E, E2 const&>)
    requires std::is_constructible_v<T, T2 const&>
          && std::is_constructible_v<E, E2 const&>
{
    if (o.has_value()) {
        if (m_has_value) {
            m_value.value() = o.value();
        }
        else {
            m_value.emplace(o.value());
        }
    }
    else { // !o.m_has_value
        if (m_has_value) {
            m_value.destroy();
        }
        else {
            // nop
        }
    }
    m_has_value = o.has_value();

    m_errc = o.m_errc;
}

template<outcome_value T, outcome_error E>
template<typename T2, typename E2>
constexpr outcome<T, E>::
outcome(outcome<T2, E2>&& o)
    noexcept(std::is_nothrow_constructible_v<T, T2&&>
          && std::is_nothrow_constructible_v<E, E2&&>)
    requires std::is_object_v<T2>
          && std::is_constructible_v<T, T2&&>
          && std::is_constructible_v<E, E2&&>
{
    if (o.has_value()) {
        if (m_has_value) {
            m_value.value() = std::move(o.value());
        }
        else {
            m_value.emplace(std::move(o.value()));
        }
    }
    else { // !o.m_has_value
        if (m_has_value) {
            m_value.destroy();
        }
        else {
            // nop
        }
    }
    m_has_value = o.has_value();

    m_errc = std::move(o.error());
}

template<outcome_value T, outcome_error E>
constexpr outcome<T, E>::
~outcome() noexcept
{
    if (has_value()) {
        m_value.destroy();
    }
}

template<outcome_value T, outcome_error E>
constexpr bool outcome<T, E>::
has_value() const noexcept
{
    return m_has_value;
}

template<outcome_value T, outcome_error E>
constexpr bool outcome<T, E>::
has_error() const noexcept
{
    return m_errc != E();
}

template<outcome_value T, outcome_error E>
constexpr T const* outcome<T, E>::
operator->() const noexcept
{
    return &value();
}

template<outcome_value T, outcome_error E>
constexpr T* outcome<T, E>::
operator->() noexcept
{
    return &value();
}

template<outcome_value T, outcome_error E>
constexpr outcome<T, E>::
operator bool() const noexcept
{
    return !has_error();
}

template<outcome_value T, outcome_error E>
void outcome<T, E>::
swap(outcome& o)
    noexcept(std::is_nothrow_swappable_v<T>)
{
    using std::swap;

    if (m_has_value) {
        if (o.m_has_value) {
            swap(m_value.value(), o.m_value.value());
        }
        else {
            o.m_value.emplace(std::move(m_value.value()));
        }
    }
    else {
        if (o.m_has_value) {
            m_value.emplace(std::move(o.m_value.value()));
        }
        else {
            // nop
        }
    }
    swap(m_has_value, o.m_has_value);

    swap(m_errc, o.m_errc);
}

template<outcome_value T, outcome_error E>
template<typename T2, typename E2>
bool outcome<T, E>::
operator==(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() == other.value())
             && noexcept(this->error() == other.error()) )
    requires std::equality_comparable_with<T, T2>
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, other);
}

template<outcome_value T, outcome_error E>
template<typename E2>
bool outcome<T, E>::
operator==(E2 const& e) const
    noexcept(noexcept(this->error() == e))
    requires (!is_outcome_v<E2>)
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, e);
}

template<outcome_value T, outcome_error E>
template<typename T2, typename E2>
std::partial_ordering outcome<T, E>::
operator<=>(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() <=> other.value())
             && noexcept(this->error() <=> other.error()) )
    requires std::three_way_comparable_with<T, T2>
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, other);
}

template<outcome_value T, outcome_error E>
template<typename E2>
std::partial_ordering outcome<T, E>::
operator<=>(E2 const& e) const
    noexcept(noexcept(this->error() <=> e))
    requires (!is_outcome_v<E2>)
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, e);
}

/*
 * class outcome<T&, E>
 */
template<typename T, outcome_error E>
constexpr outcome<T&, E>::
outcome(T& ref) noexcept
    : m_ptr { &ref } {}

template<typename T, outcome_error E>
template<typename T2, typename E2>
constexpr outcome<T&, E>::
outcome(outcome<T2, E2> const& o)
    noexcept(std::is_nothrow_constructible_v<E, E2 const&>)
    requires std::is_convertible_v<T2, T&>
          && std::is_constructible_v<E, E2 const&>
{
    if (o.has_value()) {
        m_ptr = &o.value();
    }
    if (o.has_error()) {
        m_errc = o.error();
    }
}

template<typename T, outcome_error E>
template<typename T2, typename E2>
constexpr outcome<T&, E>::
outcome(outcome<T2, E2>&& o)
    noexcept(std::is_nothrow_constructible_v<E, E2&&>)
    requires std::is_convertible_v<T2, T&>
          && std::is_constructible_v<E, E2&&>
{
    if (o.has_value()) {
        m_ptr = &o.value();
    }
    if (o.has_error()) {
        m_errc = std::move(o.error());
    }
}

template<typename T, outcome_error E>
constexpr T& outcome<T&, E>::
value() const noexcept
{
    return *m_ptr;
}

template<typename T, outcome_error E>
constexpr bool outcome<T&, E>::
has_value() const noexcept
{
    return m_ptr != nullptr;
}

template<typename T, outcome_error E>
constexpr bool outcome<T&, E>::
has_error() const noexcept
{
    return m_errc != E();
}

template<typename T, outcome_error E>
constexpr T& outcome<T&, E>::
operator*() const noexcept
{
    return value();
}

template<typename T, outcome_error E>
constexpr T* outcome<T&, E>::
operator->() const noexcept
{
    return &value();
}

template<typename T, outcome_error E>
constexpr outcome<T&, E>::
operator bool() const noexcept
{
    return !has_error();
}

template<typename T, outcome_error E>
void outcome<T&, E>::
swap(outcome& other) noexcept
{
    using std::swap;
    swap(m_ptr, other.m_ptr);
    swap(m_errc, other.m_errc);
}

template<typename T, outcome_error E>
template<typename T2, typename E2>
bool outcome<T&, E>::
operator==(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() == other.value())
             && noexcept(this->error() == other.error()) )
    requires std::equality_comparable_with<T, T2>
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, other);
}

template<typename T, outcome_error E>
template<typename E2>
bool outcome<T&, E>::
operator==(E2 const& e) const
    noexcept(noexcept(this->error() == e))
    requires (!is_outcome_v<E2>)
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, e);
}

template<typename T, outcome_error E>
template<typename T2, typename E2>
std::partial_ordering outcome<T&, E>::
operator<=>(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() <=> other.value())
             && noexcept(this->error() <=> other.error()) )
    requires std::three_way_comparable_with<T, T2>
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, other);
}

template<typename T, outcome_error E>
template<typename E2>
std::partial_ordering outcome<T&, E>::
operator<=>(E2 const& e) const
    noexcept(noexcept(this->error() <=> e))
    requires (!is_outcome_v<E2>)
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, e);
}

/*
 * class outcome<T&&, E>
 */
template<typename T, outcome_error E>
constexpr outcome<T&&, E>::
outcome(T&& ref) noexcept
    : m_ptr { &ref } {}

template<typename T, outcome_error E>
template<typename T2, typename E2>
constexpr outcome<T&&, E>::
outcome(outcome<T2, E2> const& o)
    noexcept(std::is_nothrow_copy_constructible_v<E>)
    requires std::is_convertible_v<T2, T&&>
          && std::is_constructible_v<E, E2 const&>
{
    if (o.has_value()) {
        m_ptr = &o.value();
    }
    if (o.has_error()) {
        m_errc = o.error();
    }
}

template<typename T, outcome_error E>
constexpr T&& outcome<T&&, E>::
value() const noexcept
{
    return static_cast<T&&>(*m_ptr);
}

template<typename T, outcome_error E>
constexpr bool outcome<T&&, E>::
has_value() const noexcept
{
    return m_ptr != nullptr;
}

template<typename T, outcome_error E>
constexpr bool outcome<T&&, E>::
has_error() const noexcept
{
    return m_errc != E();
}

template<typename T, outcome_error E>
constexpr T&& outcome<T&&, E>::
operator*() const noexcept
{
    return value();
}

template<typename T, outcome_error E>
constexpr outcome<T&&, E>::
operator bool() const noexcept
{
    return !has_error();
}

template<typename T, outcome_error E>
void outcome<T&&, E>::
swap(outcome& other) noexcept
{
    using std::swap;
    swap(m_ptr, other.m_ptr);
    swap(m_errc, other.m_errc);
}

template<typename T, outcome_error E>
template<typename T2, typename E2>
bool outcome<T&&, E>::
operator==(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() == other.value())
             && noexcept(this->error() == other.error()) )
    requires std::equality_comparable_with<T, T2>
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, other);
}

template<typename T, outcome_error E>
template<typename E2>
bool outcome<T&&, E>::
operator==(E2 const& e) const
    noexcept(noexcept(this->error() == e))
    requires (!is_outcome_v<E2>)
          && std::equality_comparable_with<E, E2>
{
    return outcome_::equal(*this, e);
}

template<typename T, outcome_error E>
template<typename T2, typename E2>
std::partial_ordering outcome<T&&, E>::
operator<=>(outcome<T2, E2> const& other) const
    noexcept(noexcept(this->value() <=> other.value())
             && noexcept(this->error() <=> other.error()) )
    requires std::three_way_comparable_with<T, T2>
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, other);
}

template<typename T, outcome_error E>
template<typename E2>
std::partial_ordering outcome<T&&, E>::
operator<=>(E2 const& e) const
    noexcept(noexcept(this->error() <=> e))
    requires (!is_outcome_v<E2>)
          && std::three_way_comparable_with<E, E2>
{
    return outcome_::compare_three_way(*this, e);
}

/*
 * class outcome<void, E>
 */
template<outcome_error E>
template<typename E2>
constexpr outcome<void, E>::
outcome(outcome<void, E2> const& other)
    noexcept(std::is_nothrow_constructible_v<E, E2 const&>)
    requires std::is_constructible_v<E, E2 const&>
    : m_errc { other.error() } {}

template<outcome_error E>
template<typename E2>
constexpr outcome<void, E>::
outcome(outcome<void, E2>&& other)
    noexcept(std::is_nothrow_constructible_v<E, E2&&>)
    requires std::is_constructible_v<E, E2&&>
    : m_errc { std::move(other).error() } {}

template<outcome_error E>
constexpr bool outcome<void, E>::
has_value() const noexcept
{
    return false;
}

template<outcome_error E>
constexpr bool outcome<void, E>::
has_error() const noexcept
{
    return m_errc != E();
}

template<outcome_error E>
constexpr outcome<void, E>::
operator bool() const noexcept
{
    return !has_error();
}

template<outcome_error E>
constexpr void outcome<void, E>::
swap(outcome& other) noexcept
{
    using std::swap;
    swap(m_errc, other.m_errc);
}

template<outcome_error E>
template<typename E2>
constexpr bool outcome<void, E>::
operator==(outcome<void, E2> const& other) const
    noexcept(noexcept(this->error() == other.error()))
    requires std::equality_comparable_with<E, E2>
{
    return error() == other.error();
}

template<outcome_error E>
template<typename E2>
constexpr bool outcome<void, E>::
operator==(E2 const& e) const
    noexcept(noexcept(this->error() == e))
    requires (!is_outcome_v<E2>)
          && std::equality_comparable_with<E, E2>
{
    return error() == e;
}

template<outcome_error E>
template<typename E2>
constexpr std::partial_ordering outcome<void, E>::
operator<=>(outcome<void, E2> const& other) const
    noexcept(noexcept(this->error() <=> other.error()))
    requires std::three_way_comparable_with<E, E2>
{
    return error() <=> other.error();
}

template<outcome_error E>
template<typename E2>
constexpr std::partial_ordering outcome<void, E>::
operator<=>(E2 const& e) const
    noexcept(noexcept(this->error() <=> e))
    requires (!is_outcome_v<E2>)
          && std::three_way_comparable_with<E, E2>
{
    return error() <=> e;
}

template<typename T>
std::ostream&
operator<<(std::ostream& os, T const& o)
    requires (is_outcome_v<T>)
{
    os << "{ ";

    if (o.has_value()) {
        os << *o;
    }
    else {
        os << "null";
    }

    os << ", \"" << o.error() << "\" }";

    return os;
}

} // namespace stream9

#endif // STREAM9_OUTCOME_OUTCOME_IPP
