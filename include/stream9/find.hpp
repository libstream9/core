#ifndef STREAM9_FIND_HPP
#define STREAM9_FIND_HPP

#include "ranges/find.hpp"

namespace stream9 {

using ranges::find;

} // namespace stream9

#endif // STREAM9_FIND_HPP
