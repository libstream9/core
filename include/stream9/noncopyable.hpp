#ifndef STREAM9_NONCOPYABLE_HPP
#define STREAM9_NONCOPYABLE_HPP

namespace stream9 {

struct noncopyable
{
    noncopyable() = default;

    noncopyable(noncopyable const&) = delete;
    noncopyable& operator=(noncopyable const&) = delete;

    noncopyable(noncopyable&&) = default;
    noncopyable& operator=(noncopyable&&) = default;
};

} // namespace stream9

#endif // STREAM9_NONCOPYABLE_HPP
