#ifndef STREAM9_COMPARE_HPP
#define STREAM9_COMPARE_HPP

#include <compare>

namespace stream9 {

inline constexpr std::partial_ordering
negate(std::partial_ordering o) noexcept
{
    using T = std::partial_ordering;
    if (o == T::less) return T::greater;
    if (o == T::equivalent) return T::equivalent;
    if (o == T::greater) return T::less;
    else return T::unordered;
}

inline constexpr std::weak_ordering
negate(std::weak_ordering o) noexcept
{
    using T = std::weak_ordering;
    if (o == T::less) return T::greater;
    if (o == T::greater) return T::less;
    else return T::equivalent;
}

inline constexpr std::strong_ordering
negate(std::strong_ordering o) noexcept
{
    using T = std::strong_ordering;
    if (o == T::less) return T::greater;
    if (o == T::equal) return T::equal;
    if (o == T::greater) return T::less;
    else return T::equivalent;
}

} // namespace stream9

#endif // STREAM9_COMPARE_HPP
