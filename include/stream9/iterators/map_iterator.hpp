#ifndef STREAM9_ITERATORS_MAP_ITERATOR_HPP
#define STREAM9_ITERATORS_MAP_ITERATOR_HPP

#include "iterator_adaptor.hpp"

#include <concepts>
#include <functional>
#include <iterator>
#include <type_traits>

namespace stream9::iterators {

template<std::input_iterator I, typename Proj>
    requires std::is_invocable_v<Proj, std::iter_reference_t<I>>
class map_iterator : public iterator_adaptor<
                                map_iterator<I, Proj>,
                                I,
                                typename std::iterator_traits<I>::iterator_category,
                                std::invoke_result_t<Proj, std::iter_reference_t<I>>
                            >
{
public:
    map_iterator() = default;

    map_iterator(I it, Proj proj)
        noexcept(std::is_nothrow_copy_constructible_v<I>
              && std::is_nothrow_move_constructible_v<Proj>)
        : map_iterator::adaptor_type { it }
        , m_proj { std::move(proj) }
    {}

private:
    friend class iterator_core_access;

    decltype(auto)
    dereference() const
        noexcept(noexcept(*this->base()))
    {
        return std::invoke(m_proj, *this->base());
    }

    auto
    distance_to(I const& o) const
    {
        return o - this->base();
    }

    using map_iterator::adaptor_type::distance_to;

private:
    Proj m_proj;
};

} // namespace stream9::iterators

namespace stream9 {

using iterators::map_iterator;

} // namespace stream9

#endif // STREAM9_ITERATORS_MAP_ITERATOR_HPP
