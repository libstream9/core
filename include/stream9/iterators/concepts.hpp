#ifndef STREAM9_ITERATORS_CONCEPTS_HPP
#define STREAM9_ITERATORS_CONCEPTS_HPP

#include "namespace.hpp"

#include <iterator>

namespace stream9::iterators {

using std::input_or_output_iterator;
using std::sentinel_for;
using std::input_iterator;
using std::output_iterator;
using std::forward_iterator;
using std::bidirectional_iterator;
using std::random_access_iterator;
using std::contiguous_iterator;

template<typename T>
using value_t = std::iter_value_t<T>;

template<typename T>
using reference_t = std::iter_reference_t<T>;

template<typename T>
using const_reference_t = std::iter_const_reference_t<T>;

template<typename T>
using rvalue_reference_t = std::iter_rvalue_reference_t<T>;

template<typename T>
using difference_t = std::iter_difference_t<T>;

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_CONCEPTS_HPP
