#ifndef STREAM9_ITERATORS_ITERATOR_FACADE_HPP
#define STREAM9_ITERATORS_ITERATOR_FACADE_HPP

#include "type_traits.hpp"

#include "iterator_facade/iterator_core_access.hpp"

#include <concepts>
#include <iterator>

namespace stream9::iterators {

struct iterator_facade_base {};

template<typename Derived,
         typename Category,
         typename Reference,
         typename Value = std::remove_reference_t<Reference>,
         typename Difference = std::ptrdiff_t >
    requires iterator_category_tag<Category>
          && (!std::is_reference_v<Value>)
          && std::signed_integral<Difference>
class iterator_facade : public iterator_facade_base
{
public:
    using iterator_category = Category;
    using value_type = Value;
    using reference = Reference;
    using difference_type = Difference;
    using pointer = std::conditional_t<std::is_reference_v<reference>,
                        std::add_pointer_t<std::remove_reference_t<reference>>,
                        void >;

private:
    template<typename Tag>
    static constexpr bool is_derived_from_category_v =
        std::derived_from<iterator_category, Tag>;

public:
    constexpr iterator_facade() noexcept
    {
        check_requirement();
    }

    constexpr decltype(auto)
    operator*() const
        noexcept(noexcept(iterator_core_access::dereference(derived())))
        requires requires (Derived& i) {
                    iterator_core_access::dereference(i);
                 }
    {
        return iterator_core_access::dereference(derived());
    }

    constexpr decltype(auto)
    operator[](difference_type n) const
        noexcept(noexcept(*(derived() + n)))
        requires requires (Derived const& i, difference_type n) { *(i + n); }
    {
        return *(derived() + n);
    }

    constexpr auto
    operator->() const
        noexcept(noexcept(*derived()))
        requires requires (Derived const& i) { *i; }
              && is_derived_from_category_v<std::forward_iterator_tag>
              && std::is_reference_v<reference>
    {
        return std::addressof(*derived());
    }

    constexpr Derived&
    operator++()
        noexcept(noexcept(iterator_core_access::increment(derived())))
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        iterator_core_access::increment(derived());

        return derived();
    }

    constexpr Derived
    operator++(int)
        noexcept(noexcept(++derived()))
        requires requires (Derived& i) {
                    iterator_core_access::increment(i);
                 }
    {
        auto tmp = derived();
        ++derived();
        return tmp;
    }

    constexpr Derived&
    operator--()
        noexcept(noexcept(iterator_core_access::decrement(derived())))
        requires requires (Derived& i) {
                     iterator_core_access::decrement(i);
                 }
    {
        iterator_core_access::decrement(derived());

        return derived();
    }

    constexpr Derived
    operator--(int)
        noexcept(noexcept(--derived()))
        requires requires (Derived& i) { --i; }
    {
        auto tmp = derived();
        --derived();
        return tmp;
    }

    constexpr Derived&
    operator+=(difference_type n)
        noexcept(noexcept(iterator_core_access::advance(derived(), n)))
        requires requires (Derived& i, difference_type n) {
                     iterator_core_access::advance(i, n);
                 }
    {
        iterator_core_access::advance(derived(), n);

        return derived();
    }

    constexpr Derived
    operator+(difference_type n) const
        noexcept(std::is_nothrow_copy_constructible_v<Derived>
                 && noexcept(std::declval<Derived&>() += n) )
        requires requires (Derived& i, difference_type n) { i += n; }
    {
        auto tmp = derived();
        tmp += n;
        return tmp;
    }

    constexpr Derived&
    operator-=(difference_type n)
        noexcept(noexcept(iterator_core_access::advance(derived(), -n)))
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type n) {
                  iterator_core_access::advance(i, -n);
              }
    {
        iterator_core_access::advance(derived(), -n);

        return derived();
    }

    constexpr Derived
    operator-(difference_type n) const
        noexcept(noexcept(std::declval<Derived>() -= n))
        requires is_derived_from_category_v<std::random_access_iterator_tag>
              && requires (Derived& i, difference_type n) { i -= n; }
    {
        auto tmp = derived();
        tmp -= n;
        return tmp;
    }

private:
    constexpr Derived&
    derived() noexcept
    {
        return static_cast<Derived&>(*this);
    }

    constexpr Derived const&
    derived() const noexcept
    {
        return static_cast<Derived const&>(*this);
    }

    static constexpr void check_requirement() noexcept
    {
        if constexpr (is_derived_from_category_v<std::input_iterator_tag>) {
            static_assert(requires (Derived& d) {
                { iterator_core_access::dereference(d) }
                                    -> std::same_as<Reference>;
            });

            static_assert(requires (Derived& d) {
                iterator_core_access::increment(d);
            });
        }
        if constexpr (is_derived_from_category_v<std::forward_iterator_tag>) {
            static_assert(requires (Derived& d) {
                { iterator_core_access::equal(d, d) } -> std::same_as<bool>;
            });
        }
        if constexpr (is_derived_from_category_v<std::bidirectional_iterator_tag>) {
            static_assert(requires (Derived& d) {
                iterator_core_access::decrement(d);
            });
        }
        if constexpr (is_derived_from_category_v<std::random_access_iterator_tag>) {
            static_assert(requires (Derived& d, Difference n) {
                iterator_core_access::advance(d, n);
            });
            static_assert(requires (Derived const& d1, Derived const& d2) {
                { iterator_core_access::distance_to(d1, d2) }
                                                -> std::same_as<Difference>;
            });
            static_assert(requires (Derived const& d1, Derived const& d2) {
                { iterator_core_access::compare(d1, d2) }
                            -> std::same_as<std::strong_ordering>;
            });
        }
    }
};

template<std::input_iterator I>
constexpr I
operator+(std::iter_difference_t<I> n, I const& i)
    noexcept(noexcept(i + n))
    requires requires (I& i, std::iter_difference_t<I> n) {
        iterator_core_access::advance(i, n);
    }
{
    return i + n;
}

template<std::input_iterator I1, std::input_iterator I2>
constexpr auto
operator-(I1 const& i1, I2 const& i2)
    noexcept(noexcept(iterator_core_access::distance_to(i2, i1)))
    requires requires (I1 const& i1, I2 const& i2) {
        iterator_core_access::distance_to(i2, i1);
    }
{
    return iterator_core_access::distance_to(i2, i1);
}

template<typename I1, typename I2>
constexpr bool
operator==(I1 const& lhs, I2 const& rhs)
    noexcept(noexcept(iterator_core_access::equal(lhs, rhs)))
    requires requires (I1 const& lhs, I2 const& rhs) {
                 iterator_core_access::equal(lhs, rhs);
             }
{
    return iterator_core_access::equal(lhs, rhs);
}

template<typename I1, typename I2>
constexpr std::strong_ordering
operator<=>(I1 const& lhs, I2 const& rhs)
    noexcept(noexcept(iterator_core_access::compare(lhs, rhs)))
    requires requires (I1 const& lhs, I2 const& rhs) {
                 iterator_core_access::compare(lhs, rhs);
             }
{
    return iterator_core_access::compare(lhs, rhs);
}

} // namespace stream9::iterators

namespace stream9 {

using iterators::iterator_facade;
using iterators::iterator_core_access;

} // namespace stream9

namespace std {

template<std::derived_from<stream9::iterators::iterator_facade_base> T>
struct pointer_traits<T>
{
    static auto
    to_address(T i)
    {
        return i.operator->();
    }
};

} // namespace std

#endif // STREAM9_ITERATORS_ITERATOR_FACADE_HPP
