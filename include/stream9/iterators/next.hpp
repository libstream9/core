#ifndef STREAM9_ITERATORS_NEXT_HPP
#define STREAM9_ITERATORS_NEXT_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::iterators {

using std::ranges::next;

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_NEXT_HPP
