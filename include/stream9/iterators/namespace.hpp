#ifndef STREAM9_ITERATORS_NAMESPACE_HPP
#define STREAM9_ITERATORS_NAMESPACE_HPP

namespace stream9::iterators {}

namespace stream9 {

namespace iter { using namespace iterators; }

} // namespace stream9

#endif // STREAM9_ITERATORS_NAMESPACE_HPP
