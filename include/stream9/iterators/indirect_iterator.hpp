#ifndef STREAM9_ITERATORS_INDIRECT_ITERATOR_HPP
#define STREAM9_ITERATORS_INDIRECT_ITERATOR_HPP

#include "iterator_adaptor.hpp"

#include <concepts>
#include <iterator>
#include <type_traits>

namespace stream9::iterators {

template<std::input_iterator T>
    requires std::indirectly_readable<std::iter_reference_t<T>>
class indirect_iterator : public iterator_adaptor<
                            indirect_iterator<T>,
                            T,
                            typename std::iterator_traits<T>::iterator_category,
                            decltype(**std::declval<T>()) >
{
public:
    indirect_iterator() = default;

    indirect_iterator(T const i)
        noexcept(std::is_nothrow_constructible_v<T>)
        : indirect_iterator::adaptor_type { i }
    {}

private:
    friend class iterator_core_access;

    decltype(auto)
    dereference() const
        noexcept(noexcept(*this->base()))
    {
        return **this->base();
    }
};

} // namespace stream9::iterators

namespace stream9 {

using iterators::indirect_iterator;

} // namespace stream9

#endif // STREAM9_ITERATORS_INDIRECT_ITERATOR_HPP
