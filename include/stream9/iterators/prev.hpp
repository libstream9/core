#ifndef STREAM9_ITERATORS_PREV_HPP
#define STREAM9_ITERATORS_PREV_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::iterators {

using std::ranges::prev;

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_PREV_HPP
