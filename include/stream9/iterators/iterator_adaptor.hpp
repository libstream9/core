#ifndef STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP
#define STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP

#include "iterator_facade.hpp"
#include "type_traits.hpp"

#include <compare>
#include <concepts>
#include <iterator>

#include <stream9/compare.hpp>

namespace stream9::iterators {

struct iterator_adaptor_base {};

/*
 * @model std::input_iterator
 */
template<typename Derived,
         std::input_iterator Base,
         typename Category = std::iterator_traits<Base>::iterator_category,
         typename Reference = std::iterator_traits<Base>::reference,
         typename Value = std::remove_reference_t<Reference>,
         typename Difference = std::iterator_traits<Base>::difference_type >
    requires iterator_category_tag<Category>
          && (!std::is_reference_v<Value>)
          && std::signed_integral<Difference>
class iterator_adaptor : public iterator_facade<
                               Derived, Category, Reference, Value, Difference>
                       , public iterator_adaptor_base
{
protected:
    using facade_type = iterator_facade<Derived, Category, Reference, Value, Difference>;
    using adaptor_type = iterator_adaptor<
                        Derived, Base, Category, Reference, Value, Difference>;

    template<typename T>
    static constexpr bool is_adapted = std::derived_from<T, iterator_facade_base>;

public:
    // essentials
    iterator_adaptor() = default;

    iterator_adaptor(Base const& it)
        noexcept(std::is_nothrow_copy_constructible_v<Base>)
        : m_base { it }
    {}

    Base const& base() const noexcept { return m_base; }
    Base&       base() noexcept { return m_base; }

protected:
    friend class iterator_core_access;

    constexpr decltype(auto)
    dereference() const
        noexcept(noexcept(*m_base))
    {
        return *m_base;
    }

    constexpr void
    increment()
        noexcept(noexcept(++m_base))
        requires requires (Base i) { ++i; }
    {
        ++m_base;
    }

    constexpr void
    decrement()
        noexcept(noexcept(--m_base))
        requires requires (Base i) { --i; }
    {
        --m_base;
    }

    constexpr void
    advance(std::iter_difference_t<iterator_adaptor> const n)
        noexcept(noexcept(m_base += n))
        requires requires (Base i) { i += 1; }
    {
        m_base += n;
    }

    constexpr auto
    distance_to(Derived const& o) const
        noexcept(noexcept(o.m_base - m_base))
        requires requires (Base i) { i - i; }
    {
        return o.m_base - m_base;
    }

    constexpr bool
    equal(Derived const& o) const
        noexcept(noexcept(m_base == o.m_base))
        requires std::equality_comparable<Base>
    {
        return m_base == o.m_base;
    }

    template<typename U>
    constexpr bool
    equal(U const& o) const
        noexcept(noexcept(m_base == o))
        requires (!std::same_as<U, Derived>)
              && (!is_adapted<Base> && !is_adapted<U>)
              && requires (Base b, U o) {
                    { b == o } -> std::convertible_to<bool>;
                 }
    {
        return m_base == o;
    }

    template<typename U>
    constexpr bool
    equal(U const& o) const
        noexcept(noexcept(iterator_core_access::equal(o, m_base)))
        requires (!std::same_as<U, Derived>)
              && (!is_adapted<Base> && is_adapted<U>)
              && requires (Base b, U o) {
                    iterator_core_access::equal(o, b);
                 }
    {
        return iterator_core_access::equal(o, m_base);
    }

    template<typename U>
    constexpr bool
    equal(U const& o) const
        noexcept(noexcept(iterator_core_access::equal(m_base, o)))
        requires (!std::same_as<U, Derived>)
              && (is_adapted<Base> && !is_adapted<U>)
              && requires (Base b, U o) {
                    iterator_core_access::equal(b, o);
                 }
    {
        return iterator_core_access::equal(m_base, o);
    }

    template<typename U>
    constexpr bool
    equal(U const& o) const
        noexcept(noexcept(iterator_core_access::equal(m_base, o)))
        requires (!std::same_as<U, Derived>)
              && (is_adapted<Base> && is_adapted<U>)
              && requires (Base b, U o) {
                    iterator_core_access::equal(b, o);
                 }
    {
        return iterator_core_access::equal(m_base, o);
    }

    constexpr auto
    compare(Derived const& o) const
        noexcept(noexcept(m_base <=> o.m_base))
        requires std::three_way_comparable<Base>
    {
        return m_base <=> o.m_base;
    }

    constexpr std::strong_ordering
    compare(Derived const& o) const
        noexcept(noexcept(m_base < o.m_base) && noexcept(m_base == o.m_base))
        requires (!std::three_way_comparable<Base>)
              && requires (Base i) {
                  { i < i } -> std::convertible_to<bool>;
                  { i == i } -> std::convertible_to<bool>;
              }
    {
        if (m_base < o.m_base) {
            return std::strong_ordering::less;
        }
        else if (o.m_base < m_base) {
            return std::strong_ordering::greater;
        }
        else {
            assert(m_base == o.m_base);
            return std::strong_ordering::equal;
        }
    }

    template<typename U>
    constexpr auto
    compare(U const& o) const
        noexcept(noexcept(m_base <=> o))
        requires (!std::same_as<U, Derived>)
              && (!is_adapted<Base> && !is_adapted<U>)
              && std::three_way_comparable_with<Base, U>
    {
        return m_base <=> o;
    }

    template<typename U>
    constexpr std::strong_ordering
    compare(U const& o) const
        noexcept(noexcept(m_base < o) && noexcept(m_base == o))
        requires (!std::same_as<U, Derived>)
              && (!is_adapted<Base> && !is_adapted<U>)
              && (!std::three_way_comparable<Base>)
              && requires (Base i, U j) {
                  { i < j } -> std::convertible_to<bool>;
                  { j < i } -> std::convertible_to<bool>;
                  { i == j } -> std::convertible_to<bool>;
              }
    {
        if (m_base < o) {
            return std::strong_ordering::less;
        }
        else if (o < m_base) {
            return std::strong_ordering::greater;
        }
        else {
            assert(o == m_base);
            return std::strong_ordering::equal;
        }
    }

    template<typename U>
    constexpr auto
    compare(U const& o) const
        noexcept(noexcept(iterator_core_access::compare(o, m_base)))
        requires (!std::same_as<U, Derived>)
              && (!is_adapted<Base> && is_adapted<U>)
              && requires (Base b, U o) {
                  iterator_core_access::compare(o, b);
              }
    {
        return negate(iterator_core_access::compare(o, m_base));
    }

    template<typename U>
    constexpr auto
    compare(U const& o) const
        noexcept(noexcept(iterator_core_access::compare(m_base, o)))
        requires (!std::same_as<U, Derived>)
              && (is_adapted<Base> && !is_adapted<U>)
              && requires (Base b, U o) {
                  iterator_core_access::compare(b, o);
              }
    {
        return iterator_core_access::compare(m_base, o);
    }

    template<typename U>
    constexpr auto
    compare(U const& o) const
        noexcept(noexcept(iterator_core_access::compare(m_base, o)))
        requires (!std::same_as<U, Derived>)
              && (is_adapted<Base> && is_adapted<U>)
              && requires (Base b, U o) {
                  iterator_core_access::compare(b, o);
              }
    {
        return iterator_core_access::compare(m_base, o);
    }

private:
    Base m_base;
};

} // namespace stream9::iterators

namespace stream9 {

using iterators::iterator_adaptor;

} // namespace stream9

#endif // STREAM9_ITERATORS_ITERATOR_ADAPTOR_HPP
