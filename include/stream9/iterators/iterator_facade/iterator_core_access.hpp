#ifndef STREAM9_ITERATORS_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP
#define STREAM9_ITERATORS_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP

#include <concepts>
#include <compare>

namespace stream9::iterators {

class iterator_core_access
{
public:
    template<typename Derived>
    static constexpr decltype(auto)
    dereference(Derived const& d)
        noexcept(noexcept(d.dereference()))
        requires requires (Derived const& d) {
            { d.dereference() } -> std::same_as<typename Derived::reference>;
        }
    {
        return d.dereference();
    }

    template<typename Derived>
    static constexpr void
    increment(Derived& d)
        noexcept(noexcept(d.increment()))
        requires requires (Derived& d) { d.increment(); }
    {
        d.increment();
    }

    template<typename Derived>
    static constexpr void
    decrement(Derived& d)
        noexcept(noexcept(d.decrement()))
        requires requires (Derived& d) { d.decrement(); }
    {
        d.decrement();
    }

    template<typename I, typename N>
    static constexpr void
    advance(I& i, N n)
        noexcept(noexcept(i.advance(n)))
        requires requires (I& i, N n) { i.advance(n); }
    {
        i.advance(n);
    }

    template<typename I1, typename I2>
    static constexpr auto
    distance_to(I1 const& i1, I2 const& i2)
        noexcept(noexcept(i1.distance_to(i2)))
        requires requires (I1 const& i1, I2 const& i2) { i1.distance_to(i2); }
    {
        return i1.distance_to(i2);
    }

    template<typename I1, typename I2>
    static constexpr auto
    distance_to(I1 const& i1, I2 const& i2)
        noexcept(noexcept(i2.distance_to(i1)))
        requires (!requires (I1 const& i1, I2 const& i2) { i1.distance_to(i2); })
              && requires (I1 const& i1, I2 const& i2) { i2.distance_to(i1); }
    {
        return i2.distance_to(i1) * (-1);
    }

    template<typename I1, typename I2>
    static constexpr bool
    equal(I1 const& i1, I2 const& i2)
        noexcept(noexcept(i1.equal(i2)))
        requires requires (I1 const& i1, I2 const& i2) {
            { i1.equal(i2) } -> std::same_as<bool>;
        }
    {
        return i1.equal(i2);
    }

    template<typename I1, typename I2>
    static constexpr std::strong_ordering
    compare(I1 const& i1, I2 const& i2)
        noexcept(noexcept(i1.compare(i2)))
        requires requires (I1 const& i1, I2 const& i2) {
            { i1.compare(i2) } -> std::same_as<std::strong_ordering>;
        }
    {
        return i1.compare(i2);
    }
};

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_ITERATOR_FACADE_ITERATOR_CORE_ACCESS_HPP
