#ifndef STREAM9_ITERATORS_FUNCTION_OUTPUT_ITERATOR_HPP
#define STREAM9_ITERATORS_FUNCTION_OUTPUT_ITERATOR_HPP

#include <cstddef>
#include <iterator>
#include <utility>

namespace stream9 {

template<typename Func>
class function_output_iterator
{
public:
    using iterator_category = std::output_iterator_tag;
    using value_type = void;
    using reference = void;
    using pointer = void;
    using difference_type = std::ptrdiff_t;

public:
    // essentials
    function_output_iterator() = default; // partially-formed

    function_output_iterator(Func func)
        : m_func { std::move(func) }
    {}

    template<typename U>
    function_output_iterator& operator=(U&& v)
    {
        m_func(std::forward<U>(v));
        return *this;
    }

    // operator
    function_output_iterator& operator*() { return *this; }
    function_output_iterator& operator++() { return *this; }
    function_output_iterator& operator++(int) { return *this; }

private:
    Func m_func;
};

} // namespace stream9

#endif // STREAM9_ITERATORS_FUNCTION_OUTPUT_ITERATOR_HPP
