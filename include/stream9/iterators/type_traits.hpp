#ifndef STREAM9_ITERATORS_TYPE_TRAITS_HPP
#define STREAM9_ITERATORS_TYPE_TRAITS_HPP

#include <concepts>
#include <iterator>
#include <type_traits>

namespace stream9::iterators {

template<typename T>
concept iterator_category_tag =
       std::derived_from<T, std::input_iterator_tag>
    || std::derived_from<T, std::output_iterator_tag>;

} // namespace stream9::iterators

#endif // STREAM9_ITERATORS_TYPE_TRAITS_HPP
