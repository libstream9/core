#ifndef STREAM9_ITERATORS_MOVE_ITERATOR_HPP
#define STREAM9_ITERATORS_MOVE_ITERATOR_HPP

#include "iterator_adaptor.hpp"

#include <concepts>
#include <iterator>
#include <type_traits>

namespace stream9 {

template<std::input_iterator I>
class move_iterator : public iterator_adaptor<move_iterator<I>, I,
                                typename std::iterator_traits<I>::iterator_category,
                                std::iter_rvalue_reference_t<I> >
{
public:
    // essential
    move_iterator() = default;

    move_iterator(I it)
        noexcept(std::is_nothrow_copy_constructible_v<I>)
        : move_iterator::adaptor_type { it }
    {}

    move_iterator(move_iterator const&) = default;
    move_iterator& operator=(move_iterator const&) = default;

    move_iterator(move_iterator&&) = default;
    move_iterator& operator=(move_iterator&&) = default;

    ~move_iterator() = default;

private:
    friend class iterator_core_access;

    decltype(auto)
    dereference() const
        noexcept(noexcept(*this->base()))
    {
        return std::ranges::iter_move(this->base());
    }
};

} // namespace stream9

#endif // STREAM9_ITERATORS_MOVE_ITERATOR_HPP
