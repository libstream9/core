#ifndef STREAM9_ITERATORS_ZIP_ITERATOR_HPP
#define STREAM9_ITERATORS_ZIP_ITERATOR_HPP

#include "namespace.hpp"
#include "iterator_facade.hpp"
#include "type_traits.hpp"

#include <iterator>
#include <tuple>

namespace stream9::iterators {

namespace zip_detail {

    struct dummy {};

    template<typename T>
    class sentinel_wrapper
        : public iter::iterator_facade<sentinel_wrapper<T>,
                                       std::input_iterator_tag,
                                       dummy >
    {
    public:
        sentinel_wrapper() = default;
        sentinel_wrapper(T const& s) : m_sentinel { s } {};

        operator T& () & { return m_sentinel; }
        operator T const& () const& { return m_sentinel; }

    private:
        friend class iter::iterator_core_access;

        decltype(auto) dereference() const
        {
            return dummy {};
        }

        void increment()
        {}

    private:
        T m_sentinel;
    };

    template<typename T>
    using normalize_t =
        std::conditional_t<std::input_iterator<T>,
                           T,
                           sentinel_wrapper<T> >;

    template<typename... Is>
    using reference_t =
        std::tuple<std::iter_reference_t<normalize_t<Is>>...>;

    template<typename... Is>
    using value_t = reference_t<Is...>;

    template<typename... Is>
    using common_category_t =
        std::common_type_t<typename normalize_t<Is>::iterator_category...>;

    template<typename... Is>
    using tuple_t = std::tuple<normalize_t<Is>...>;

    template<typename T1, typename T2>
    using larger_type_t = std::conditional_t<(sizeof(T1) > sizeof(T2)), T1, T2>;

    template<typename...>
    struct largest_type;

    template<typename T>
    struct largest_type<T> { using type = T; };

    template<typename T1, typename T2, typename... Rest>
    struct largest_type<T1, T2, Rest...>
    {
        using type = largest_type<larger_type_t<T1, T2>, Rest...>::type;
    };

    template<typename... Ts>
    using largest_type_t = largest_type<Ts...>::type;

    template<typename T>
    struct diff
    {
        using type = ptrdiff_t;
    };

    template<std::input_iterator T>
    struct diff<T>
    {
        using type = std::iter_difference_t<T>;
    };

    template<typename T>
    using diff_t = diff<T>::type;

    template<typename... Is>
    using difference_t = largest_type<diff_t<Is>...>::type;

} // namespace detail

template<typename... Is>
class zip_iterator : public iter::iterator_facade<zip_iterator<Is...>,
                                zip_detail::common_category_t<Is...>,
                                zip_detail::reference_t<Is...>,
                                zip_detail::value_t<Is...>,
                                zip_detail::difference_t<Is...>
                            >
{
private:
    template<typename T>
    static constexpr bool is_iterator_category_derived_from_v =
        std::derived_from<typename zip_iterator::iterator_category, T>;

    template<typename... Ts>
              requires (sizeof...(Ts) == sizeof...(Is))
    static constexpr bool is_sentinel_v = (... && std::sentinel_for<Ts, Is>);

public:
    zip_iterator() = default;

    zip_iterator(Is const&... is)
        : m_iterators { is... }
    {}

private:
    friend class iter::iterator_core_access;

    template<typename... Ts> friend class zip_iterator;

    decltype(auto) dereference() const
    {
        auto fn = [](auto&&... is) {
            return zip_detail::reference_t<Is...>(*is...);
        };

        return std::apply(fn, m_iterators);
    }

    void increment()
    {
        auto fn = [](auto&&... is) {
            (..., ++is);
        };

        std::apply(fn, m_iterators);
    }

    void decrement()
        requires is_iterator_category_derived_from_v<std::bidirectional_iterator_tag>
    {
        auto fn = [](auto&&... is) {
            (..., --is);
        };

        std::apply(fn, m_iterators);
    }

    void advance(std::iter_difference_t<zip_iterator> const n)
        requires is_iterator_category_derived_from_v<std::random_access_iterator_tag>
    {
        auto fn = [&](auto&&... is) {
            auto fn = [](auto&& i, auto n) { i += n; };

            (..., fn(is, n));
        };

        std::apply(fn, m_iterators);
    }

    std::iter_difference_t<zip_iterator>
        distance_to(zip_iterator const& other) const
            requires is_iterator_category_derived_from_v<std::random_access_iterator_tag>
    {
        auto min_distance = [&] <size_t...I> (std::index_sequence<I...>) {
            using diff_t = std::iter_difference_t<zip_iterator>;
            diff_t result = std::numeric_limits<diff_t>::max();

            auto min = [&](auto d) {
                if (d < result) result = d;
            };

            (min(std::get<I>(other.m_iterators) - std::get<I>(m_iterators)), ...);

            return result;
        };

        return min_distance(std::index_sequence_for<Is...>());
    }

    template<typename... Ts>
    bool equal(zip_iterator<Ts...> const& other) const
        requires is_sentinel_v<Ts...>
    {
        auto is_any_of_equal = [&] <size_t... I> (std::index_sequence<I...>) {
            return ((std::get<I>(m_iterators) ==
                     std::get<I>(other.m_iterators) ) || ...);
        };

        return is_any_of_equal(std::index_sequence_for<Is...>());
    }

    std::strong_ordering
        compare(zip_iterator const& other) const
    {
        return std::get<0>(other.m_iterators) <=> std::get<0>(m_iterators);
    }

    template<std::size_t I>
    friend std::tuple_element_t<I, zip_iterator> const&
    get(zip_iterator const& it)
    {
        return std::get<I>(it.m_iterators);
    }

    template<std::size_t I>
    friend std::tuple_element_t<I, zip_iterator>&
    get(zip_iterator& it)
    {
        return std::get<I>(it.m_iterators);
    }

private:
    zip_detail::tuple_t<Is...> m_iterators;
};

} // namespace stream9::iterators

namespace std {

template<typename... Is>
struct tuple_size<stream9::iterators::zip_iterator<Is...>>
    : integral_constant<size_t, sizeof...(Is)>
{};

template<size_t I, typename... Is>
struct tuple_element<I, stream9::iterators::zip_iterator<Is...>>
    : tuple_element<I, tuple<Is...>>
{};

} // namespace std

#endif // STREAM9_ITERATORS_ZIP_ITERATOR_HPP
