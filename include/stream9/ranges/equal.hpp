#ifndef STREAM9_RANGES_EQUAL_HPP
#define STREAM9_RANGES_EQUAL_HPP

#include "namespace.hpp"

#include <algorithm>

namespace stream9::ranges {

using std::ranges::equal;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_EQUAL_HPP
