#ifndef STREAM9_RANGES_FRONT_HPP
#define STREAM9_RANGES_FRONT_HPP

#include "begin.hpp"
#include "namespace.hpp"

namespace stream9::ranges {

namespace _front {

    struct api {
        template<typename T>
            requires requires (T&& r) {
                *rng::begin(r);
            }
        decltype(auto)
        operator()(T&& r) const //TODO error handling
        {
            return *rng::begin(r);
        }
    };

} // namespace _front

inline constexpr _front::api front;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_FRONT_HPP
