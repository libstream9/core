#ifndef STREAM9_RANGES_COUNT_HPP
#define STREAM9_RANGES_COUNT_HPP

#include <algorithm>

namespace stream9 {

using std::ranges::count;
using std::ranges::count_if;

} // namespace stream9

#endif // STREAM9_RANGES_COUNT_HPP
