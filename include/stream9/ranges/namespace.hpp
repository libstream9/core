#ifndef STREAM9_RANGES_NAMESPACE_HPP
#define STREAM9_RANGES_NAMESPACE_HPP

#include <stream9/namespace.hpp>

namespace stream9::ranges {}

namespace stream9 {

namespace rng = ranges;

} // namespace stream9

#endif // STREAM9_RANGES_NAMESPACE_HPP
