#ifndef STREAM9_RANGES_PREFIX_HPP
#define STREAM9_RANGES_PREFIX_HPP

#include "begin.hpp"
#include "end.hpp"
#include "concepts.hpp"
#include "namespace.hpp"
#include "../range.hpp"

#include <stream9/iterators/next.hpp>
#include <stream9/number.hpp>

namespace stream9::ranges {

template<forward_range R>
st9::range<iterator_t<R>>
prefix(R&& v, integer<difference_t<R>, 0> n) noexcept
{
    auto f = begin(v);
    auto l = iter::next(f, n, end(v));

    return { f, l };
}

} // namespace stream9::ranges

#endif // STREAM9_RANGES_PREFIX_HPP
