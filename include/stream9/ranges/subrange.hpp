#ifndef STREAM9_RANGES_SUBRANGE_HPP
#define STREAM9_RANGES_SUBRANGE_HPP

#include "begin.hpp"
#include "end.hpp"
#include "concepts.hpp"
#include "namespace.hpp"
#include "../range.hpp"

#include <stream9/iterators/next.hpp>
#include <stream9/number.hpp>

namespace stream9::ranges {

template<forward_range R>
st9::range<iterator_t<R>>
subrange(R&& v,
         difference_t<R> offset,
         integer<difference_t<R>, 0> n) noexcept
{
    auto e = end(v);
    auto f = offset < 0 ? iter::next(e, offset, begin(v))
                        : iter::next(begin(v), offset, e);
    auto l = iter::next(f, n, e);

    return { f, l };
}

template<forward_range R>
st9::range<iterator_t<R>>
subrange(R&& v,
         difference_t<R> offset) noexcept
{
    auto l = end(v);
    auto f = offset < 0 ? iter::next(l, offset, begin(v))
                        : iter::next(begin(v), offset, l);

    return { f, l };
}

} // namespace stream9::ranges

#endif // STREAM9_RANGES_SUBRANGE_HPP
