#ifndef STREAM9_RANGES_UPPER_BOUND_HPP
#define STREAM9_RANGES_UPPER_BOUND_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "namespace.hpp"

#include <stream9/less.hpp>

#include <algorithm>
#include <concepts>
#include <functional>
#include <iterator>

namespace stream9::ranges {

namespace upper_bound_ {

    using std::forward_iterator;
    using std::identity;
    using std::sentinel_for;

    template<typename I, typename T, typename Proj, typename Comp>
    concept comparable_with =
        requires (I i, T const& v, Proj proj, Comp comp) {
            { comp(proj(*i), v) } -> std::convertible_to<bool>;
        };

    struct api
    {
        template<forward_iterator I, sentinel_for<I> S,
                 typename T,
                 typename Proj = identity,
                 typename Comp = less >
        constexpr I
        operator()(I first, S last,
                   T const& value,
                   Comp comp = {}, Proj proj = {}) const
            requires comparable_with<I, T, Proj, Comp>
        {
            using std::ranges::distance;
            using std::ranges::next;

            auto len = distance(first, last);

            while (len > 0) {
                auto half = len / 2;
                auto middle = next(first, half);

                if (std::invoke(comp, value, std::invoke(proj, *middle))) {
                    len = half;
                } else {
                    first = middle;
                    ++first;
                    len = len - half - 1;
                }
            }

            return first;
        }

        template<forward_range R,
                 typename T,
                 typename Proj = identity,
                 typename Comp = less >
        constexpr iterator_t<R>
        operator()(R&& r,
                   T const& value,
                   Comp comp = {}, Proj proj = {}) const
            requires comparable_with<iterator_t<R>, T, Proj, Comp>
        {
            return operator()(rng::begin(r), rng::end(r),
                              value, std::move(comp), std::move(proj) );
        }
    };

} // namespace upper_bound_

inline constexpr upper_bound_::api upper_bound;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_UPPER_BOUND_HPP
