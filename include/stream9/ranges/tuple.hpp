#ifndef STREAM9_RANGES_TUPLE_HPP
#define STREAM9_RANGES_TUPLE_HPP

#include <ranges>

namespace std {

template<ranges::range T>
struct tuple_size<T>
{
    static constexpr auto value = 2;
};

template<ranges::range T>
struct tuple_size<T const>
{
    static constexpr auto value = 2;
};

template<ranges::range T>
struct tuple_element<0, T>
{
    using type = ranges::iterator_t<T>;
};

template<ranges::range T>
struct tuple_element<0, T const>
{
    using type = ranges::iterator_t<T const>;
};

template<ranges::range T>
struct tuple_element<1, T>
{
    using type = ranges::sentinel_t<T>;
};

template<ranges::range T>
struct tuple_element<1, T const>
{
    using type = ranges::sentinel_t<T const>;
};

} // namespace std

namespace stream9 {

template<size_t I, std::ranges::range T>
auto
get(T& t) noexcept
    requires (I == 0 || I == 1)
{
    if constexpr (I == 0) {
        return t.begin();
    }
    else if constexpr (I == 1) {
        return t.end();
    }
}

} // namespace stream9

#endif // STREAM9_RANGES_TUPLE_HPP
