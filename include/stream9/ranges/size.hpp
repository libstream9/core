#ifndef STREAM9_RANGES_SIZE_HPP
#define STREAM9_RANGES_SIZE_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::ranges {

using std::ranges::size;
using std::ranges::ssize;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_SIZE_HPP
