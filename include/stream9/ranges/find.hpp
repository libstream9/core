#ifndef STREAM9_RANGES_FIND_HPP
#define STREAM9_RANGES_FIND_HPP

#include "concepts.hpp"
#include "linear_find.hpp"
#include "namespace.hpp"

#include <concepts>
#include <iterator>
#include <utility>

namespace stream9::ranges {

namespace find_ {

    using std::convertible_to;
    using std::input_iterator;
    using std::sentinel_for;

    template<typename R, typename T>
    concept member_find_invocable =
        requires (R&& r, T const& v) {
            { r.find(v) } -> convertible_to<iterator_t<R const&>>;
        };

    template<typename R, typename T>
    concept linear_find_invocable1 =
        requires (R&& r, T const& v) {
            rng::linear_find(r, v);
        };

    template<typename I, typename S, typename T>
    concept linear_find_invocable2 =
        requires (I f, S l, T const& v) {
            rng::linear_find(f, l, v);
        };

    template<typename R, typename T, typename Proj>
    concept linear_find_invocable3 =
        requires (R&& r, T const& v, Proj proj) {
            rng::linear_find(r, v, proj);
        };

    template<typename I, typename S, typename T, typename Proj>
    concept linear_find_invocable4 =
        requires (I f, S l, T const& v, Proj p) {
            rng::linear_find(f, l, v, std::move(p));
        };

    struct api
    {
        /* find by value */
        template<input_range R, typename T>
        constexpr iterator_t<R const>
        operator()(R&& r, T const& v) const
            noexcept(noexcept(r.find(v)))
            requires member_find_invocable<R, T>
        {
            return r.find(v);
        }

        template<input_range R, typename T>
        constexpr iterator_t<R const>
        operator()(R&& r, T const& v) const
            noexcept(noexcept(rng::linear_find(std::forward<R>(r), v)))
            requires (!member_find_invocable<R, T>)
                  && linear_find_invocable1<R, T>
        {
            return rng::linear_find(std::forward<R>(r), v);
        }

        template<input_iterator I, sentinel_for<I> S, typename T>
        constexpr auto
        operator()(I first, S last, T const& v) const
            noexcept(noexcept(rng::linear_find(first, last, v)))
            requires linear_find_invocable2<I, S, T>
        {
            return rng::linear_find(first, last, v);
        }

        /* find by value with projection */ //TODO invoke member fn if projection matches
        template<input_range R, typename T, typename Proj>
        constexpr iterator_t<R const>
        operator()(R&& r, T const& v, Proj proj) const
            noexcept(noexcept(rng::linear_find(std::forward<R>(r), v, std::move(proj))))
            requires linear_find_invocable3<R, T, Proj>
        {
            return rng::linear_find(std::forward<R>(r), v, std::move(proj));
        }

        template<input_iterator I, sentinel_for<I> S, typename T, typename Proj>
        constexpr auto
        operator()(I first, S last, T const& v, Proj proj) const
            noexcept(noexcept(rng::linear_find(first, last, v)))
            requires linear_find_invocable4<I, S, T, Proj>
        {
            return rng::linear_find(first, last, v, std::move(proj));
        }
    };

} // namespace find_

inline constexpr find_::api find;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_FIND_HPP
