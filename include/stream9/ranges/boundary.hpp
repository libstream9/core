#ifndef STREAM9_RANGES_BOUNDARY_HPP
#define STREAM9_RANGES_BOUNDARY_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "namespace.hpp"

#include <utility>

namespace stream9::ranges {

namespace _boundary {

    struct api
    {
        template<rng::range R>
        auto
        operator()(R&& r) const
            requires std::is_lvalue_reference_v<R>
        {
            return std::make_pair(
                rng::begin(std::forward<R>(r)),
                rng::end(std::forward<R>(r))
            );
        }
    };

} // namespace _boundary

inline constexpr _boundary::api boundary;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_BOUNDARY_HPP
