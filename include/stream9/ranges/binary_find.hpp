#ifndef STREAM9_RANGES_BINARY_FIND_HPP
#define STREAM9_RANGES_BINARY_FIND_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "lower_bound.hpp"
#include "namespace.hpp"
#include "upper_bound.hpp"

#include <functional>
#include <iterator>

namespace stream9::ranges {

namespace binary_find_first_ {

    using std::ranges::less;

    template<class I, class S, class T, class Proj, class Comp>
    concept lower_bound_invocable =
        requires (I i, S s, T const& v, Comp c, Proj p) {
            rng::lower_bound(i, s, v, c, p);
        };

    struct api {

        template<forward_range R, class T,
                 class Proj = std::identity, class Comp = less >
        constexpr iterator_t<R>
        operator()(R&& r, T const& value, Comp comp = {}, Proj proj = {}) const
        {
            auto first = rng::begin(r);
            auto last = rng::end(r);

            return operator()(first, last, value, comp, proj);
        }

        template<std::forward_iterator I, std::sentinel_for<I> S,
                 class T,
                 class Proj = std::identity, class Comp = less >
            requires lower_bound_invocable<I, S, T, Proj, Comp>
        constexpr I
        operator()(I first, S last, T const& value,
                   Comp comp = {}, Proj proj = {}) const
        {
            auto it = rng::lower_bound(first, last, value, comp, proj);

            if (it != last && !comp(value, proj(*it))) {
                return it;
            }
            else {
                return last;
            }
        }

    };

} // namespace binary_find_first_

inline constexpr binary_find_first_::api binary_find;
inline constexpr binary_find_first_::api binary_find_first;

namespace binary_find_last_ {

    using std::ranges::less;

    template<class I, class S, class T, class Proj, class Comp>
    concept upper_bound_invocable =
        requires (I i, S s, T const& v, Comp c, Proj p) {
            rng::upper_bound(i, s, v, c, p);
        };

    struct api {

        template<forward_range R, class T,
                 class Proj = std::identity, class Comp = less >
        constexpr iterator_t<R>
        operator()(R&& r, T const& value, Comp comp = {}, Proj proj = {}) const
        {
            auto first = rng::begin(r);
            auto last = rng::end(r);

            return operator()(first, last, value, comp, proj);
        }

        template<std::forward_iterator I, std::sentinel_for<I> S,
                 class T,
                 class Proj = std::identity, class Comp = less >
            requires upper_bound_invocable<I, S, T, Proj, Comp>
        constexpr I
        operator()(I first, S last, T const& value,
                   Comp comp = {}, Proj proj = {}) const
        {
            if (first == last) return last;

            auto it = rng::upper_bound(first, last, value, comp, proj);
            if (it == first) return last;

            --it;

            if (!comp(proj(*it), value)) {
                return it;
            }
            else {
                return last;
            }
        }

    };

} // namespace binary_find_last_

inline constexpr binary_find_last_::api binary_find_last;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_BINARY_FIND_HPP
