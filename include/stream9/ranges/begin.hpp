#ifndef STREAM9_RANGES_BEGIN_HPP
#define STREAM9_RANGES_BEGIN_HPP

#include <ranges>

namespace stream9::ranges {

using std::ranges::begin;
using std::ranges::cbegin;
using std::ranges::rbegin;
using std::ranges::crbegin;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_BEGIN_HPP
