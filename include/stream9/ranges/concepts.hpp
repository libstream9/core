#ifndef STREAM9_RANGES_CONCEPTS_HPP
#define STREAM9_RANGES_CONCEPTS_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::ranges {

using std::ranges::range;

using std::ranges::input_range;
using std::ranges::output_range;
using std::ranges::forward_range;
using std::ranges::bidirectional_range;
using std::ranges::random_access_range;
using std::ranges::contiguous_range;

using std::ranges::sized_range;

using std::ranges::iterator_t;
using std::ranges::sentinel_t;

template<typename R>
using value_t = std::ranges::range_value_t<R>;

template<typename R>
using reference_t = std::ranges::range_reference_t<R>;

template<typename R>
using size_t = std::ranges::range_size_t<R>;

template<typename R>
using difference_t = std::ranges::range_difference_t<R>;

} // namespace stream9::ranges

namespace stream9 {

// range categories
using ranges::input_range;
using ranges::output_range;
using ranges::forward_range;
using ranges::bidirectional_range;
using ranges::random_access_range;
using ranges::contiguous_range;

} // namespace stream9

#endif // STREAM9_RANGES_CONCEPTS_HPP
