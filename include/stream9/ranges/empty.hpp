#ifndef STREAM9_RANGES_EMPTY_HPP
#define STREAM9_RANGES_EMPTY_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::ranges {

using std::ranges::empty;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_EMPTY_HPP
