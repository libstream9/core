#ifndef STREAM9_RANGES_BACK_HPP
#define STREAM9_RANGES_BACK_HPP

#include "end.hpp"
#include "namespace.hpp"

#include <stream9/iterators/prev.hpp>

namespace stream9::ranges {

namespace _back {

    struct api {
        template<typename T>
            requires requires (T&& r) {
                *iter::prev(rng::end(r));
            }
        decltype(auto)
        operator()(T&& r) const //TODO error handling
        {
            return *iter::prev(rng::end(r));
        }
    };

} // namespace _back

inline constexpr _back::api back;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_BACK_HPP
