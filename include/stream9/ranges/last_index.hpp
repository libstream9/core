#ifndef STREAM9_RANGES_LAST_INDEX_HPP
#define STREAM9_RANGES_LAST_INDEX_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"

namespace stream9::ranges {

namespace _last_index {

    struct api
    {
        template<rng::random_access_range T>
        rng::difference_t<T>
        operator()(T&& r) const
        {
            return rng::end(r) - rng::begin(r) - 1;
        }
    };

} // namespace _last_index

inline constexpr _last_index::api last_index;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_LAST_INDEX_HPP
