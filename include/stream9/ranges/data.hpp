#ifndef STREAM9_RANGES_DATA_HPP
#define STREAM9_RANGES_DATA_HPP

#include "namespace.hpp"

#include <ranges>

namespace stream9::ranges {

using std::ranges::data;
using std::ranges::cdata;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_DATA_HPP
