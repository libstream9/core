#ifndef STREAM9_RANGES_SUFFIX_HPP
#define STREAM9_RANGES_SUFFIX_HPP

#include "begin.hpp"
#include "end.hpp"
#include "concepts.hpp"
#include "namespace.hpp"
#include "../range.hpp"

#include <stream9/iterators/prev.hpp>
#include <stream9/number.hpp>

namespace stream9::ranges {

template<bidirectional_range R>
st9::range<iterator_t<R>>
suffix(R&& v, integer<difference_t<R>, 0> n) noexcept
{
    auto f = iter::prev(end(v), n, begin(v));
    auto l = end(v);

    return { f, l };
}

} // namespace stream9::ranges

#endif // STREAM9_RANGES_SUFFIX_HPP
