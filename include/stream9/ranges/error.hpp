#ifndef STREAM9_RANGES_ERROR_HPP
#define STREAM9_RANGES_ERROR_HPP

#include <string>
#include <system_error>

#include <stream9/errors.hpp>

namespace stream9::ranges {

enum class errc {
    out_of_range,
};

inline std::error_category const&
error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept override
        {
            return "stream9::ranges";
        }

        std::string message(int const e) const override
        {
            switch (static_cast<errc>(e)) {
                using enum errc;
                case out_of_range:
                    return "out of range";
            }
            return "unknown error";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(errc const e) noexcept
{
    return { static_cast<int>(e), error_category() };
}

} // namespace stream9::ranges

namespace std {

template<>
struct is_error_code_enum<stream9::ranges::errc>
    : true_type {};

} // namespace std

#endif // STREAM9_RANGES_ERROR_HPP
