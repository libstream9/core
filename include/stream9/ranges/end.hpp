#ifndef STREAM9_RANGES_END_HPP
#define STREAM9_RANGES_END_HPP

#include <ranges>

namespace stream9::ranges {

using std::ranges::end;
using std::ranges::cend;
using std::ranges::rend;
using std::ranges::crend;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_END_HPP
