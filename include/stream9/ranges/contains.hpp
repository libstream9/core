#ifndef STREAM9_RANGES_CONTAINS_HPP
#define STREAM9_RANGES_CONTAINS_HPP

#include "concepts.hpp"
#include "end.hpp"
#include "find.hpp"
#include "linear_find.hpp"
#include "namespace.hpp"

#include <algorithm>
#include <concepts>
#include <functional>
#include <ranges>

namespace stream9::ranges {

namespace contains_ {

    using std::convertible_to;
    using std::identity;

    template<class R, class T>
    concept has_find =
        requires (R&& r, T const& v) {
            { rng::find(r, v) }
                -> convertible_to<iterator_t<R const&>>;
        };

    template<class R, class T, class Proj>
    concept has_find_with_projection =
        requires (R&& r, T const& v, Proj p) {
            { rng::linear_find(r, v, p) }
                -> convertible_to<iterator_t<R const&>>;
        };

    struct api
    {
        template<input_range R, class T>
        constexpr bool
        operator()(R&& r, T const& value) const
            noexcept(noexcept(rng::find(r, value)))
            requires has_find<R, T>
        {
            return rng::find(r, value) != rng::end(r);
        }

        template<input_range R, class T, class Proj = identity>
        constexpr bool
        operator()(R&& r, T const& value, Proj proj) const
            noexcept(noexcept(rng::linear_find(r, value, proj)))
            requires has_find_with_projection<R, T, Proj>
        {
            return rng::linear_find(r, value, proj) != rng::end(r);
        }
    };

} // namespace contains_

inline constexpr contains_::api contains;

} // namespace stream9::ranges

namespace stream9 {

using ranges::contains;

} // namespace stream9

#endif // STREAM9_RANGES_CONTAINS_HPP
