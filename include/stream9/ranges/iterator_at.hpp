#ifndef STREAM9_RANGES_ITERATOR_AT_HPP
#define STREAM9_RANGES_ITERATOR_AT_HPP

#include "begin.hpp"
#include "concepts.hpp"

#include <concepts>

namespace stream9::ranges {

namespace _iterator_at {

    struct api
    {
        template<rng::random_access_range T, std::integral I>
        rng::iterator_t<T>
        operator()(T&& r, I n) const //TODO error check
        {
            return rng::begin(r) + static_cast<rng::difference_t<T>>(n);
        }
    };

} // namespace _iterator_at

inline constexpr _iterator_at::api iterator_at;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_ITERATOR_AT_HPP
