#ifndef STREAM9_RANGES_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
#define STREAM9_RANGES_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "namespace.hpp"

#include <compare>
#include <functional>
#include <iterator>
#include <ranges>
#include <utility>

namespace stream9::ranges {

namespace lexicographical_compare_three_way_ {

    template<typename Comp, typename Proj1, typename Proj2,
             std::input_iterator I1, std::input_iterator I2>
    using comparison_category_t = std::invoke_result_t<
        Comp,
        std::invoke_result_t<Proj1, std::iter_reference_t<I1>>,
        std::invoke_result_t<Proj2, std::iter_reference_t<I2>> >;

    template<typename I1, typename S1, typename I2, typename S2,
             class Proj1, class Proj2, class Comp >
    auto
    impl(I1 const first1, S1 const last1,
         I2 const first2, S2 const last2,
         Comp comp = {},
         Proj1 proj1 = {}, Proj2 proj2 = {}) noexcept
    {
        using result_t = comparison_category_t<Comp, Proj1, Proj2, I1, I2>;

        auto i1 = first1;
        auto i2 = first2;

        while (true) {
            if (i1 == last1) {
                if (i2 == last2) {
                    return result_t::equivalent;
                }
                else {
                    return result_t::less;
                }
            }
            else if (i2 == last2) {
                return result_t::greater;
            }

            auto const c1 = std::invoke(proj1, *i1);
            auto const c2 = std::invoke(proj2, *i2);

            auto const cat = std::invoke(comp, c1, c2);
            if (std::is_neq(cat)) {
                return cat;
            }

            ++i1, ++i2;
        }
    }

    struct api
    {
        // by iterator
        template<std::input_iterator I1, std::sentinel_for<I1> S1,
                 std::input_iterator I2, std::sentinel_for<I2> S2,
                 class Proj1 = std::identity,
                 class Proj2 = std::identity,
                 class Comp = std::compare_three_way >
        comparison_category_t<Comp, Proj1, Proj2, I1, I2>
        operator()(I1 first1, S1 last1,
                   I2 first2, S2 last2,
                   Comp comp = {},
                   Proj1 proj1 = {}, Proj2 proj2 = {}) const noexcept
            requires std::convertible_to<
                comparison_category_t<Comp, Proj1, Proj2, I1, I2>,
                std::partial_ordering >
        {
            return (impl)(first1, last1, first2, last2,
                std::ref(comp), std::ref(proj1), std::ref(proj2) );
        }

        // by range
        template<input_range R1, input_range R2,
                 class Proj1 = std::identity,
                 class Proj2 = std::identity,
                 class Comp = std::compare_three_way >
        comparison_category_t<Comp, Proj1, Proj2,
                              rng::iterator_t<R1>,
                              rng::iterator_t<R2> >
        operator()(R1&& r1, R2&& r2,
                   Comp comp = {},
                   Proj1 proj1 = {}, Proj2 proj2 = {}) const noexcept
            requires std::convertible_to<
                comparison_category_t<Comp, Proj1, Proj2,
                                      rng::iterator_t<R1>,
                                      rng::iterator_t<R2> >,
                std::partial_ordering >
        {
            return (impl)(
                rng::begin(r1), rng::end(r1),
                rng::begin(r2), rng::end(r2),
                std::ref(comp), std::ref(proj1), std::ref(proj2) );
        }
    };

} // namespace lexicographical_compare_three_way_

inline constexpr lexicographical_compare_three_way_::api lexicographical_compare_three_way;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_LEXICOGRAPHICAL_COMPARE_THREE_WAY_HPP
