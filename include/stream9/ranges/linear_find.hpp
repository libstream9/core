#ifndef STREAM9_RANGES_LINEAR_FIND_HPP
#define STREAM9_RANGES_LINEAR_FIND_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "namespace.hpp"

#include <concepts>
#include <iterator>
#include <functional>

namespace stream9::ranges {

namespace linear_find_ {

    using std::input_iterator;
    using std::sentinel_for;

    struct api
    {
        template<input_range R, typename T, typename Proj = std::identity>
        iterator_t<R>
        operator()(R&& r, T const& v, Proj proj = {}) const
            //TODO noexcept
            requires requires (reference_t<R> v1, T const& v2) {
                { std::invoke(proj, v1) == v2 } -> std::convertible_to<bool>;
            }
        {
            auto f = begin(r);
            auto l = end(r);

            for (; f != l; ++f) {
                if (std::invoke(proj, *f) == v) {
                    return f;
                }
            }

            return f;
        }

        template<input_iterator I, sentinel_for<I> S, typename T, typename Proj = std::identity>
        I
        operator()(I first, S last, T const& v, Proj proj = {}) const
            //TODO noexcept
            requires requires (I i, T const& v2) {
                { std::invoke(proj, *i) == v2 } -> std::convertible_to<bool>;
            }
        {
            for (; first != last; ++first) {
                if (std::invoke(proj, *first) == v) {
                    break;
                }
            }

            return first;
        }
    };

} // namespace linear_find_

inline constexpr linear_find_::api linear_find;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_LINEAR_FIND_HPP
