#ifndef STREAM9_RANGES_INDEX_AT_HPP
#define STREAM9_RANGES_INDEX_AT_HPP

#include "concepts.hpp"
#include "begin.hpp"

namespace stream9::ranges {

namespace _index_at {

    struct api
    {
        template<rng::random_access_range T>
        rng::difference_t<T>
        operator()(T&& r, rng::iterator_t<T> it) const
        {
            return it - rng::begin(r);
        }
    };

} // namespace _index_at

inline constexpr _index_at::api index_at;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_INDEX_AT_HPP
