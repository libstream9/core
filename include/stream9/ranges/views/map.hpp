#ifndef STREAM9_RANGES_MAP_HPP
#define STREAM9_RANGES_MAP_HPP

#include "../namespace.hpp"
#include "../begin.hpp"
#include "../end.hpp"
#include "../concepts.hpp"

#include <iterator>
#include <ranges>
#include <type_traits>

#include <stream9/iterators/map_iterator.hpp>

namespace stream9::ranges::views {

template<input_range R, typename Proj>
    requires std::is_invocable_v<Proj, rng::reference_t<R>>
class map
{
public:
    using base_iterator = rng::iterator_t<R>;
    using base_sentinel = rng::sentinel_t<R>;

public:
    map(R& r, Proj p)
        : m_begin { rng::begin(r) }
        , m_end { rng::end(r) }
        , m_proj { std::move(p) }
    {}

    map_iterator<base_iterator, Proj>
    begin() const
    {
        return { m_begin, m_proj };
    }

    map_iterator<base_sentinel, Proj>
    end() const
        requires std::input_iterator<base_sentinel>
    {
        return { m_end, m_proj };
    }

    base_sentinel
    end() const
        requires (!std::input_iterator<base_sentinel>)
    {
        return m_end;
    }

private:
    base_iterator m_begin;
    base_sentinel m_end;
    Proj m_proj;
};

} // namespace stream9::ranges::views

#endif // STREAM9_RANGES_MAP_HPP
