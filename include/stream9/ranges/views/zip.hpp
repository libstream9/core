#ifndef STREAM9_RANGES_VIEWS_ZIP_HPP
#define STREAM9_RANGES_VIEWS_ZIP_HPP

#include "../begin.hpp"
#include "../concepts.hpp"
#include "../end.hpp"
#include "../namespace.hpp"

#include <ranges>

#include <stream9/iterators/zip_iterator.hpp>

namespace stream9::ranges::views {

namespace _zip {

    using std::ranges::subrange;

    struct api
    {
        template<input_range... Rs>
        auto
        operator()(Rs&&... r) const
        {
            return subrange(
                iter::zip_iterator(rng::begin(r)...),
                iter::zip_iterator(rng::end(r)...)
            );
        }
    };

} // namespace _zip

inline constexpr _zip::api zip;

} // namespace stream9::ranges::views

#endif // STREAM9_RANGES_VIEWS_ZIP_HPP
