#ifndef STREAM9_RANGES_VIEWS_MOVE_HPP
#define STREAM9_RANGES_VIEWS_MOVE_HPP

#include "../begin.hpp"
#include "../concepts.hpp"
#include "../end.hpp"
#include "../namespace.hpp"

#include <stream9/iterators/move_iterator.hpp>

namespace stream9::ranges::views {

template<typename> class move;

template<input_range R>
class move
{
public:
    using base_iterator = rng::iterator_t<R>;
    using base_sentinel = rng::sentinel_t<R>;

public:
    move(R& r)
        : m_begin { rng::begin(r) }
        , m_end { rng::end(r) }
    {}

    move_iterator<base_iterator>
    begin() const
    {
        return m_begin;
    }

    move_iterator<base_iterator>
    end() const
        requires std::input_iterator<base_sentinel>
    {
        return m_end;
    }

    base_sentinel
    end() const
        requires (!std::input_iterator<base_sentinel>)
    {
        return m_end;
    }

private:
    base_iterator m_begin;
    base_sentinel m_end;
};

} // namespace stream9::ranges::views

#endif // STREAM9_RANGES_VIEWS_MOVE_HPP
