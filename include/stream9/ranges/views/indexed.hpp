#ifndef STREAM9_RANGES_VIEWS_INDEXED_HPP
#define STREAM9_RANGES_VIEWS_INDEXED_HPP

#include "../begin.hpp"
#include "../concepts.hpp"
#include "../end.hpp"
#include "../namespace.hpp"

#include "zip.hpp"

#include <stream9/iterators/counting_iterator.hpp>

#include <ranges>

namespace stream9::ranges::views {

namespace _indexed {

    struct api
    {
        template<input_range R>
        auto
        operator()(R&& r, difference_t<R> init = 0) const
        {
            //TODO This doesn't work because gcc return __int128 as
            // iota_view::difference_type and rest of stdlib doesn't support
            // __int128. It should be able to work in gcc 10.3
            // https://gcc.gnu.org/bugzilla/show_bug.cgi?id=96042
            //return zip(rng::views::iota(init), std::forward<R>(r));

            using index_iterator
                = iter::counting_iterator<difference_t<R>>;

            index_iterator idx_it { init };

            auto value_it = rng::begin(r);
            std::advance(value_it, init);

            return std::ranges::subrange {
                iter::zip_iterator(idx_it, value_it),
                iter::zip_iterator(std::default_sentinel, rng::end(r))
            };
        }
    };

} // namespace _indexed

inline constexpr _indexed::api indexed;

} // namespace stream9::ranges::views

#endif // STREAM9_RANGES_VIEWS_INDEXED_HPP
