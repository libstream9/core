#ifndef STREAM9_RANGES_AT_HPP
#define STREAM9_RANGES_AT_HPP

#include "concepts.hpp"
#include "begin.hpp"
#include "error.hpp"
#include "size.hpp"

#include <stream9/iterators/next.hpp>

namespace stream9::ranges {

namespace _at {

    struct api {
        template<forward_range T>
            requires requires (T&& r, difference_t<T> n) {
                *iter::next(rng::begin(r), n);
                rng::ssize(r);
            }
        decltype(auto)
        operator()(T&& r, difference_t<T> n) const
        {
            if (0 <= n && n < ssize(r)) {
                return *iter::next(rng::begin(r), n);
            }
            else {
                throw_error(errc::out_of_range, {
                    { "size", rng::ssize(r) },
                    { "n", n },
                });
            }
        }
    };

} // namespace _at

inline constexpr _at::api at;

} // namespace stream9::ranges

#endif // STREAM9_RANGES_AT_HPP
