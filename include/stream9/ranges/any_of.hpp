#ifndef STREAM9_RANGES_ANY_OF_HPP
#define STREAM9_RANGES_ANY_OF_HPP

#include <algorithm>

namespace stream9 {

using std::ranges::any_of;

} // namespace stream9

#endif // STREAM9_RANGES_ANY_OF_HPP
