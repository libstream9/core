#ifndef STREAM9_RANGES_ACCUMULATE_HPP
#define STREAM9_RANGES_ACCUMULATE_HPP

#include "begin.hpp"
#include "concepts.hpp"
#include "end.hpp"
#include "namespace.hpp"

#include <functional>
#include <numeric>
#include <type_traits>

namespace stream9 {

template<typename Op, typename T>
concept binary_operation = requires (T v, Op op) {
    { op(v, v) } -> std::convertible_to<T>;
};

template<typename Op, typename T, typename U>
concept projection = requires (T v, Op op) {
    { op(v) } -> std::convertible_to<U>;
};

template<input_range R>
rng::value_t<R>
accumulate(R&& r)
    requires binary_operation<std::plus<rng::value_t<R>>, rng::value_t<R>>
          && std::is_default_constructible_v<rng::value_t<R>>
{
    rng::value_t<R> init {};
    return std::accumulate(rng::begin(r), rng::end(r), std::move(init));
}

template<input_range R, typename T>
    requires std::same_as<T, rng::value_t<R>>
          && binary_operation<std::plus<T>, T>
T
accumulate(R&& r, T init)
{
    return std::accumulate(rng::begin(r), rng::end(r), std::move(init));
}

template<input_range R, typename P, typename T>
T
accumulate(R&& r, T init, P proj)
    requires projection<P, rng::value_t<R>, T>
          && binary_operation<std::plus<T>, T>
{
    for (auto const& e: r) {
        init = std::move(init) + proj(e);
    }

    return init;
}

template<input_range R, typename P, typename T, typename Op>
T
accumulate(R&& r, T init, P proj, Op op)
    requires projection<P, rng::value_t<R>, T>
          && binary_operation<Op, T>
{
    for (auto const& e: r) {
        init = op(std::move(init), proj(e));
    }

    return init;
}

} // namespace stream9

#endif // STREAM9_RANGES_ACCUMULATE_HPP
