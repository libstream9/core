#ifndef STREAM9_ORDERING_IS_COMPARABLE_HPP
#define STREAM9_ORDERING_IS_COMPARABLE_HPP

#include <concepts>

#include "less.hpp"

namespace stream9 {

template<typename T, typename U = T, typename Compare = less>
concept is_comparable_v =
    requires (T const& x, U const& y, Compare comp) {
        { comp(x, y) } -> std::convertible_to<bool>;
    };

template<typename T, typename U = T, typename Compare = less>
concept is_nothrow_comparable_v =
    noexcept(Compare()(std::declval<T>(), std::declval<U>()));

} // namespace stream9

#endif // STREAM9_ORDERING_IS_COMPARABLE_HPP
