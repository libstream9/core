#ifndef STREAM9_ORDERING_LESS_HPP
#define STREAM9_ORDERING_LESS_HPP

namespace stream9 {

struct less
{
    template<typename T, typename U>
    constexpr bool
    operator()(T const& x, U const& y) const
        noexcept(noexcept(x < y))
        requires (!(std::is_array_v<T> && std::is_array_v<U>))
              && requires (T const& x, U const& y) {
                    { x < y } -> std::convertible_to<bool>;
                 }
    {
        return x < y;
    }
};

} // namespace stream9

#endif // STREAM9_ORDERING_LESS_HPP
