#ifndef STREAM9_TRANSITIVE_ARROW_HPP
#define STREAM9_TRANSITIVE_ARROW_HPP

namespace stream9 {

template<typename T>
concept has_transitive_arrow = requires (T v) {
    T::transitive_arrow == true;
    { v.operator->() };
};

} // namespace stream9

#endif // STREAM9_TRANSITIVE_ARROW_HPP
