#ifndef STREAM9_NOTHROW_SWAP_HPP
#define STREAM9_NOTHROW_SWAP_HPP

#include <type_traits>
#include <utility>

namespace stream9 {

namespace swap_ {

    using std::declval;
    using std::swap;

    template<typename T>
    concept has_adl_swap =
        requires (T& x, T& y) {
            swap(x, y);
        }
        && noexcept(swap(declval<T&>(), declval<T&>()));

    struct api
    {
        template<typename T>
        void
        operator()(T& x, T& y) const noexcept
            requires has_adl_swap<T>
        {
            swap(x, y);
        }

        template<typename T>
        void
        operator()(T& x, T& y) const noexcept
            requires (!has_adl_swap<T>)
                  && std::is_nothrow_move_constructible_v<T>
                  && std::is_nothrow_move_assignable_v<T>
        {
            T tmp { std::move(x) };
            x = std::move(y);
            y = std::move(tmp);
        }

        template<typename T>
        void
        operator()(T& x, T& y) const noexcept
            requires (!has_adl_swap<T>)
                  && std::is_nothrow_move_constructible_v<T>
                  && (!std::is_nothrow_move_assignable_v<T>)
        {
            T tmp { std::move(x) };
            x.~T();
            ::new(&x) T { std::move(y) };
            y.~T();
            ::new(&y) T { std::move(tmp) };
        }
    };

} // namespace swap_

inline constexpr swap_::api nothrow_swap;

template<typename T>
inline constexpr bool is_nothrow_swappable_v =
    requires (T& x, T& y, swap_::api swap) {
        swap(x, y);
    };

} // namespace stream9

#endif // STREAM9_NOTHROW_SWAP_HPP
