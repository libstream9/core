#ifndef STREAM9_ARRAY_VIEW_HPP
#define STREAM9_ARRAY_VIEW_HPP

#include <cstddef>
#include <iterator>
#include <ranges>

#include <stream9/null.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

template<typename T>
class array_view
{
public:
    using size_type = safe_integer<std::ptrdiff_t, 0>;
    using pointer = T*;
    using reference = T&;
    using iterator = pointer;
    using difference_type = safe_integer<std::ptrdiff_t>;

public:
    // essential
    template<std::size_t N>
    constexpr array_view(T (&arr)[N]) noexcept;

    template<std::size_t N>
    constexpr array_view(T (&arr)[N], size_type offset) noexcept;

    template<std::size_t N>
    constexpr array_view(T (&arr)[N], size_type offset, size_type length) noexcept;

    template<std::ranges::contiguous_range R>
    constexpr array_view(R&& range) noexcept
        requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>;

    template<std::ranges::contiguous_range R>
    constexpr array_view(R&& range, size_type offset) noexcept
        requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>;

    template<std::ranges::contiguous_range R>
    constexpr array_view(R&& range, size_type offset, size_type length) noexcept
        requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>;

    template<std::contiguous_iterator It>
    constexpr array_view(It first, size_type count) noexcept
        requires std::is_convertible_v<std::iter_reference_t<It>, reference>;

    template<std::contiguous_iterator It>
    constexpr array_view(It first, It last) noexcept
        requires std::is_convertible_v<std::iter_reference_t<It>, reference>;

    array_view(array_view const&) = default;
    array_view& operator=(array_view const&) = default;

    array_view(array_view&&) = default;
    array_view& operator=(array_view&&) = default;

    ~array_view() = default;

    // accessor
    constexpr reference front() const;

    constexpr reference back() const;

    constexpr reference operator[](size_type idx) const;

    constexpr pointer data() const noexcept;

    constexpr iterator begin() const noexcept;

    constexpr iterator end() const noexcept;

    // query
    constexpr size_type size() const noexcept;

    constexpr bool empty() const noexcept;

    // modifier
    constexpr void advance_begin(difference_type);
    constexpr void advance_begin(difference_type, iterator left_boundary);

    constexpr void advance_end(difference_type);
    constexpr void advance_end(difference_type, iterator right_boundary);

private:
    template<typename>
    friend class stream9::null_traits;

    array_view(null_t) noexcept : m_first { (pointer)(-1) }, m_last { nullptr } {}

    bool is_null() const noexcept { return m_first == (pointer)(-1); }
    void set_null() noexcept { m_first = (pointer)(-1); }

private:
    pointer m_first;
    pointer m_last;
};

enum class array_view_errc {
    ok = 0,
    reference_empty_view,
    index_out_of_range,
};

template<std::ranges::contiguous_range R>
array_view(R)
    -> array_view<std::remove_reference_t<std::ranges::range_reference_t<R>>>;

template<std::ranges::contiguous_range R>
array_view(R, std::integral auto)
    -> array_view<std::remove_reference_t<std::ranges::range_reference_t<R>>>;

template<std::ranges::contiguous_range R>
array_view(R, std::integral auto, std::integral auto)
    -> array_view<std::remove_reference_t<std::ranges::range_reference_t<R>>>;

template<std::contiguous_iterator I>
array_view(I, std::integral auto)
    -> array_view<std::remove_reference_t<std::iter_reference_t<I>>>;

template<std::contiguous_iterator I>
array_view(I, I)
    -> array_view<std::remove_reference_t<std::iter_reference_t<I>>>;

} // namespace stream9

namespace std::ranges {

template<typename T>
inline constexpr bool enable_view<stream9::array_view<T>> = true;

template<typename T>
inline constexpr bool enable_borrowed_range<stream9::array_view<T>> = true;

} // namespace std::ranges

namespace std {

template<>
struct is_error_code_enum<stream9::array_view_errc> : true_type {};

} // namespace std

/*
 * structured binding
 */
#include <utility>

namespace std {

template<typename C>
struct tuple_size<stream9::array_view<C>>
{
    static constexpr auto value = 2;
};

template<typename C>
struct tuple_element<0, stream9::array_view<C>>
{
    using type = typename stream9::array_view<C>::iterator;
};

template<typename C>
struct tuple_element<1, stream9::array_view<C>>
{
    using type = typename stream9::array_view<C>::iterator;
};

} // namespace std

namespace stream9 {

template<size_t I, typename Ch>
auto
get(array_view<Ch> const& s) noexcept
    requires (I < 2)
{
    if constexpr (I == 0) {
        return s.begin();
    }
    else {
        return s.end();
    }
}

} // namespace stream9

#endif // STREAM9_ARRAY_VIEW_HPP

#include "container/array_view.ipp"
