#ifndef STREAM9_PROJECTION_HPP
#define STREAM9_PROJECTION_HPP

#include <type_traits>
#include <utility>

namespace stream9 {

template<typename Proj, typename T>
concept projection = std::is_invocable_v<Proj, T>;

template<typename T, typename Proj>
using projected_t = decltype(Proj()(std::declval<T>()));

template<auto field>
struct project
{
    // member variable
    template<typename T>
    decltype(auto)
    operator()(T&& v) const noexcept
        requires requires (T&& v) { std::forward<T>(v).*field; }
    {
        return std::forward<T>(v).*field;
    }

    // member function
    template<typename T, typename... Args>
    decltype(auto)
    operator()(T&& v, Args&&... args) const
        noexcept(noexcept((std::forward<T>(v).*field)(std::forward<Args>(args)...)))
        requires requires (T&& v, Args... args) {
            (std::forward<T>(v).*field)(std::forward<Args>(args)...);
        }
    {
        return (std::forward<T>(v).*field)(std::forward<Args>(args)...);
    }
};

} // namespace stream9

#endif // STREAM9_PROJECTION_HPP
