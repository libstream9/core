#ifndef STREAM9_CONCEPTS_HPP
#define STREAM9_CONCEPTS_HPP

#include <concepts>
#include <utility>

namespace stream9 {

using std::same_as;
using std::convertible_to;

template<typename From, typename To>
concept nothrow_convertible_to = std::is_nothrow_convertible_v<From, To>;

template<typename T, typename... Args>
concept constructible =
    requires (Args&&... args) {
        T { std::forward<Args>(args)... };
    };

template<typename T, typename... Args>
concept nothrow_constructible =
    requires (Args&&... args) {
        { T { std::forward<Args>(args)... } } noexcept;
    };

template<typename T>
concept default_constructible = constructible<T>;

template<typename T>
concept nothrow_default_constructible = nothrow_constructible<T>;

template<typename T>
concept copy_constructible = constructible<T, T const&>;

template<typename T>
concept nothrow_copy_constructible = nothrow_constructible<T, T const&>;

template<typename T>
concept move_constructible = constructible<T, T>;

template<typename T>
concept nothrow_move_constructible = nothrow_constructible<T, T>;

template<typename T1, typename T2>
concept assignable =
    requires (T1&& v1, T2&& v2) {
        std::forward<T1>(v1) = std::forward<T2>(v2);
    };

template<typename T1, typename T2>
concept nothrow_assignable =
    requires (T1&& v1, T2&& v2) {
        { std::forward<T1>(v1) = std::forward<T2>(v2) } noexcept;
    };

template<typename T>
concept copy_assignable = assignable<T&, T const&>;

template<typename T>
concept nothrow_copy_assignable = nothrow_assignable<T&, T const&>;

template<typename T>
concept move_assignable = assignable<T&, T>;

template<typename T>
concept nothrow_move_assignable = nothrow_assignable<T&, T>;

template<typename T>
concept destructible =
    requires (T v) {
        v.~T();
    };

template<typename T>
concept nothrow_destructible =
    requires (T v) {
        { v.~T() } noexcept;
    };

template<typename T>
concept copyable = copy_constructible<T> && copy_assignable<T>;

template<typename T>
concept nothrow_copyable = nothrow_copy_constructible<T> && nothrow_copy_assignable<T>;

template<typename T>
concept movable = move_constructible<T> && move_assignable<T>;

template<typename T>
concept nothrow_movable = nothrow_move_constructible<T> && nothrow_move_assignable<T>;

template<typename Fn, typename... Args>
concept callable = requires (Fn&& fn, Args&&... args) {
    std::forward<Fn>(fn)(std::forward<Args>(args)...);
};

template<typename Fn, typename... Args>
concept nothrow_callable = requires (Fn&& fn, Args&&... args) {
    { std::forward<Fn>(fn)(std::forward<Args>(args)...) } noexcept;
};

template<typename Fn, typename R, typename... Args>
concept callable_r = requires (Fn&& fn, Args&&... args) {
    { std::forward<Fn>(fn)(std::forward<Args>(args)...) } -> convertible_to<R>;
};

template<typename Fn, typename R, typename... Args>
concept nothrow_callable_r = requires (Fn&& fn, Args&&... args) {
    { std::forward<Fn>(fn)(std::forward<Args>(args)...) } noexcept -> convertible_to<R>;
};

template<typename T, typename... Args>
concept predicate = callable_r<T, bool, Args...>;

template<typename T, typename... Args>
concept nothrow_predicate = nothrow_callable_r<T, bool, Args...>;

using std::integral;

template<typename T, typename U = T>
concept equality_comparable =
    requires (T&& x, U&& y) {
        { x == y } -> convertible_to<bool>;
        { x != y } -> convertible_to<bool>;
        { y == x } -> convertible_to<bool>;
        { y != x } -> convertible_to<bool>;
    };

template<typename T, typename U = T>
concept nothrow_equality_comparable =
    requires (T&& x, U&& y) {
        { x == y } noexcept -> convertible_to<bool>;
        { x != y } noexcept -> convertible_to<bool>;
        { y == x } noexcept -> convertible_to<bool>;
        { y != x } noexcept -> convertible_to<bool>;
    };

template<typename T, typename U = T>
concept comparable = equality_comparable<T, U>
    && requires (T&& x, U&& y) {
           { x < y } -> convertible_to<bool>;
           { x <= y } -> convertible_to<bool>;
           { x > y } -> convertible_to<bool>;
           { x >= y } -> convertible_to<bool>;
           { y < x } -> convertible_to<bool>;
           { y <= x } -> convertible_to<bool>;
           { y > x } -> convertible_to<bool>;
           { y >= x } -> convertible_to<bool>;
       };

template<typename T, typename U = T>
concept nothrow_comparable = nothrow_equality_comparable<T, U>
    && requires (T&& x, U&& y) {
           { x < y } noexcept -> convertible_to<bool>;
           { x <= y } noexcept -> convertible_to<bool>;
           { x > y } noexcept -> convertible_to<bool>;
           { x >= y } noexcept -> convertible_to<bool>;
           { y < x } noexcept -> convertible_to<bool>;
           { y <= x } noexcept -> convertible_to<bool>;
           { y > x } noexcept -> convertible_to<bool>;
           { y >= x } noexcept -> convertible_to<bool>;
       };

} // namespace stream9

#endif // STREAM9_CONCEPTS_HPP
