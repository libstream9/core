#ifndef STREAM9_JSON_OBJECT_ITERATOR_HPP
#define STREAM9_JSON_OBJECT_ITERATOR_HPP

#include "namespace.hpp"

#include <stream9/iterator_facade.hpp>

#include <compare>
#include <iterator>

namespace stream9::json {

template<typename It, typename Key, typename Val>
class object_iterator : public iterator_facade<
                                        object_iterator<It, Key, Val>,
                                        std::random_access_iterator_tag,
                                        std::pair<Key, Val&>, // reference
                                        std::pair<Key, Val&>, // value
                                        std::iter_difference_t<It> >
{
public:
    object_iterator() noexcept = default;

    object_iterator(It it) noexcept
        : m_it { it }
    {}

    template<typename It2, typename Key2, typename Val2> // for conversion to const_iterator
    object_iterator(object_iterator<It2, Key2, Val2> it) noexcept
        : m_it { it.base() }
    {}

    It base() const noexcept { return m_it; }

private:
    friend class stream9::iterator_core_access;

    std::pair<Key, Val&> dereference() const noexcept
    {
        return {
            m_it->key(),
            reinterpret_cast<Val&>(m_it->value())
        };
    }

    void increment() noexcept { ++m_it; }
    void decrement() noexcept { --m_it; }

    void advance(std::iter_difference_t<object_iterator> n) noexcept
    {
        m_it += n;
    }

    std::iter_difference_t<object_iterator>
        distance_to(object_iterator const& other) const noexcept
    {
        return other.m_it - m_it;
    }

    bool equal(object_iterator const& other) const noexcept
    {
        return other.m_it == m_it;
    }

    std::strong_ordering
        compare(object_iterator const& other) const noexcept
    {
        return other.m_it <=> m_it;
    }

private:
    It m_it;
};

} // namespace stream9::json

#endif // STREAM9_JSON_OBJECT_ITERATOR_HPP
