#ifndef STREAM9_JSON_HAS_TAG_INVOKE_HPP
#define STREAM9_JSON_HAS_TAG_INVOKE_HPP

#include <utility>

namespace stream9::json {

class value;
struct value_from_tag;

template<typename T>
concept has_tag_invoke =
    requires (value jv, T v) {
        tag_invoke(value_from_tag(), jv, v);
    };

} // namespace stream9::json

#endif // STREAM9_JSON_HAS_TAG_INVOKE_HPP
