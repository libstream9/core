#ifndef STREAM9_JSON_ARRAY_IPP
#define STREAM9_JSON_ARRAY_IPP

#include "array.hpp"

#include <algorithm>

namespace stream9::json {

/*
 * class value
 */
inline value::
value(array const& v, storage_ptr sp/*= {}*/)
    : m_base(static_cast<value const&>(v).base(), std::move(sp))
{}

inline value::
value(array&& v, storage_ptr sp/*= {}*/)
    : m_base(std::move(static_cast<value&>(v).base()), std::move(sp))
{}

inline value& value::
operator=(array const& v)
{
    m_base = static_cast<value const&>(v).base();
    return *this;
}

inline value& value::
operator=(array&& v)
{
    m_base = std::move(static_cast<value&>(v).base());
    return *this;
}

inline array const& value::
get_array() const noexcept
{
    return static_cast<array const&>(*this);
}

inline array& value::
get_array() noexcept
{
    return static_cast<array&>(*this);
}

inline array& value::
emplace_array()
{
    m_base.emplace_array();

    return static_cast<array&>(*this);
}

/*
 * class array
 */
inline array::
array()
    : value { boost::json::array_kind }
{}

inline array::
array(storage_ptr sp)
    : value { boost::json::array_kind, std::move(sp) }
{}

inline array::
array(array const& other, storage_ptr sp)
    : value { boost::json::array(other.base(), std::move(sp)) }
{}

inline array::
array(array&& other, storage_ptr sp)
    : value { boost::json::array(std::move(other.base()), std::move(sp)) }
{}

inline array::
array(size_type const count, storage_ptr sp/*= {}*/)
    : value { boost::json::array(count, std::move(sp)) }
{}

inline array::
array(size_type count, value const& v, storage_ptr sp/*= {}*/)
    : value { boost::json::array(count, v.base(), std::move(sp)) }
{}

template<std::input_iterator It>
    requires std::constructible_from<array::value_type, std::iter_value_t<It>>
array::
array(It const first, It const last, storage_ptr sp/*= {}*/)
    :value { boost::json::array(first, last, std::move(sp)) }
{}

inline array::
array(std::initializer_list<boost::json::value_ref> init, storage_ptr sp/*= {}*/)
    : value { boost::json::array(std::move(init), std::move(sp)) }
{}

/*
 * accessor
 */
inline value const& array::
at(size_type const pos) const
{
    return reinterpret_cast<value const&>(
        base().at(pos)
    );
}

inline value& array::
at(size_type const pos)
{
    return reinterpret_cast<value&>(
        base().at(pos)
    );
}

inline value const& array::
operator[](size_type const pos) const
{
    return reinterpret_cast<value const&>(
        base()[pos]
    );
}

inline value& array::
operator[](size_type const pos)
{
    return reinterpret_cast<value&>(
        base()[pos]
    );
}

inline value const& array::
front() const
{
    return reinterpret_cast<value const&>(
        base().front()
    );
}

inline value& array::
front()
{
    return reinterpret_cast<value&>(
        base().front()
    );
}

inline value const& array::
back() const
{
    return reinterpret_cast<value const&>(
        base().back()
    );
}

inline value& array::
back()
{
    return reinterpret_cast<value&>(
        base().back()
    );
}

inline value const* array::
data() const
{
    return reinterpret_cast<value const*>(
        base().data()
    );
}

inline value* array::
data()
{
    return reinterpret_cast<value*>(
        base().data()
    );
}

inline array::const_iterator array::
begin() const
{
    return base().begin();
}

inline array::iterator array::
begin()
{
    return base().begin();
}

inline array::const_iterator array::
end() const
{
    return base().end();
}

inline array::iterator array::
end()
{
    return base().end();
}

inline array::const_iterator array::
cbegin() const
{
    return base().cbegin();
}

inline array::const_iterator array::
cend() const
{
    return base().cend();
}

inline array::const_reverse_iterator array::
rbegin() const
{
    return base().rbegin();
}

inline array::reverse_iterator array::
rbegin()
{
    return base().rbegin();
}

inline array::const_reverse_iterator array::
rend() const
{
    return base().rend();
}

inline array::reverse_iterator array::
rend()
{
    return base().rend();
}

inline array::const_reverse_iterator array::
crbegin() const
{
    return base().crbegin();
}

inline array::const_reverse_iterator array::
crend() const
{
    return base().crend();
}

inline boost::json::array const& array::
base() const
{
    return value::base().get_array();
}

inline boost::json::array& array::
base()
{
    return value::base().get_array();
}

/*
 * comparison
 */
inline bool array::
empty() const
{
    return base().empty();
}

inline array::size_type array::
size() const
{
    return base().size();
}

inline array::difference_type array::
ssize() const
{
    return static_cast<difference_type>(
        base().size()
    );
}

inline array::size_type array::
max_size() const
{
    return base().max_size();
}

/*
 * modifier
 */
inline void array::
push_back(value const& v)
{
    base().push_back(v.base());
}

inline void array::
push_back(value&& v)
{
    base().push_back(std::move(v.base()));
}

template<typename Arg>
value& array::
emplace_back(Arg&& arg)
{
    return reinterpret_cast<value&>(
        base().emplace_back(std::forward<Arg>(arg))
    );
}

inline array::iterator array::
insert(const_iterator const pos, value const& v)
{
    return base().insert(pos.base(), v.base());
}

inline array::iterator array::
insert(const_iterator const pos, value&& v)
{
    return base().insert(pos.base(), std::move(v.base()));
}

inline array::iterator array::
insert(const_iterator const pos, size_type const count, value const& v)
{
    return base().insert(pos.base(), count, v.base());
}

inline array::iterator array::
insert(const_iterator const pos, size_type const count, value&& v)
{
    return base().insert(pos.base(), count, std::move(v.base()));
}

template<std::input_iterator It>
    requires std::constructible_from<array::value_type, std::iter_value_t<It>>
array::iterator array::
insert(const_iterator const pos, It const first, It const last)
{
    return base().insert(pos.base(), first, last);
}

inline array::iterator array::
insert(const_iterator const pos,
       std::initializer_list<boost::json::value_ref> init)
{
    return base().insert(pos.base(), std::move(init));
}

template<typename Arg>
array::iterator array::
emplace(const_iterator const pos, Arg&& arg)
{
    return base().emplace(pos.base(), std::forward<Arg>(arg));
}

inline void array::
pop_back()
{
    base().pop_back();
}

inline array::iterator array::
erase(const_iterator const pos)
{
    return base().erase(pos.base());
}

inline array::iterator array::
erase(const_iterator const first, const_iterator const last)
{
    return base().erase(first.base(), last.base());
}

inline void array::
clear()
{
    base().clear();
}

inline void array::
resize(size_type const sz)
{
    base().resize(sz);
}

inline void array::
resize(size_type const sz, value const& v)
{
    base().resize(sz, v.base());
}

inline void array::
reserve(size_type const sz)
{
    base().reserve(sz);
}

inline void array::
shrink_to_fit()
{
    base().shrink_to_fit();
}

} // namespace stream9::json

#endif // STREAM9_JSON_ARRAY_IPP
