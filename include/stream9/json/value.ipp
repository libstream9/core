#ifndef STREAM9_JSON_VALUE_IPP
#define STREAM9_JSON_VALUE_IPP

#include "value.hpp"

namespace stream9::json {

inline value::
value(storage_ptr sp) noexcept
    : m_base(std::move(sp))
{}

inline value::
value(value const& v, storage_ptr sp)
    : m_base(v.base(), std::move(sp))
{}

inline value::
value(value&& v, storage_ptr sp)
    : m_base(std::move(v.base()), std::move(sp))
{}

inline value::
value(std::nullptr_t const v, storage_ptr sp/*= {}*/) noexcept
    : m_base(v, std::move(sp))
{}

inline value::
value(std::integral auto const v, storage_ptr sp/*= {}*/) noexcept
    : m_base(v, std::move(sp))
{}

inline value::
value(double const v, storage_ptr sp/*= {}*/) noexcept
    : m_base(v, std::move(sp))
{}

template<std::convertible_to<std::string_view> T>
    requires (!has_tag_invoke<T>)
inline value::
value(T const& v, storage_ptr sp/*= {}*/)
    : m_base(std::string_view(v), std::move(sp))
{}

#if 0 // moved to "string.ipp"
inline value::
value(string const& v);

inline value::
value(string&& v);
#endif

#if 0 // moved to "array.ipp"
inline value::
value(array const& v);

inline value::
value(array&& v);
#endif

#if 0 // move to "object.ipp"
inline value::
value(object const& v);

inline value::
value(object&& v);
#endif

template<typename T>
    requires has_tag_invoke<T>
inline value::
value(T const& v, storage_ptr sp/*= {}*/)
    : value(value_from(v, std::move(sp)))
{}

inline value::
value(boost::json::string_kind_t k, storage_ptr sp/*= {}*/)
    : m_base(k, std::move(sp))
{}

inline value::
value(boost::json::array_kind_t k, storage_ptr sp/*= {}*/)
    : m_base(k, std::move(sp))
{}

inline value::
value(boost::json::object_kind_t k, storage_ptr sp/*= {}*/)
    : m_base(k, std::move(sp))
{}

inline value::
value(boost::json::value const& v, storage_ptr sp/*= {}*/)
    : m_base(v, std::move(sp))
{}

inline value::
value(boost::json::value&& v, storage_ptr sp/*= {}*/)
    : m_base(std::move(v), std::move(sp))
{}

inline value::
value(boost::json::string const& s, storage_ptr sp/*= {}*/)
    : m_base(s, std::move(sp))
{}

inline value::
value(boost::json::string&& s, storage_ptr sp/*= {}*/)
    : m_base(std::move(s), std::move(sp))
{}

inline value::
value(boost::json::array const& arr, storage_ptr sp/*= {}*/)
    : m_base(arr, std::move(sp))
{}

inline value::
value(boost::json::array&& arr, storage_ptr sp/*= {}*/)
    : m_base(std::move(arr), std::move(sp))
{}

inline value::
value(boost::json::object const& obj, storage_ptr sp/*= {}*/)
    : m_base(obj, std::move(sp))
{}

inline value::
value(boost::json::object&& obj, storage_ptr sp/*= {}*/)
    : m_base(std::move(obj), std::move(sp))
{}

inline value& value::
operator=(std::nullptr_t) noexcept
{
    m_base = nullptr;
    return *this;
}

inline value& value::
operator=(std::integral auto const i) noexcept
{
    m_base = i;
    return *this;
}

inline value& value::
operator=(double const d) noexcept
{
    m_base = d;
    return *this;
}

inline value& value::
operator=(std::convertible_to<std::string_view> auto const& s)
{
    m_base = std::string_view(s);
    return *this;
}

#if 0
// defined in "string.ipp"
inline value& value::
operator=(string const&);

inline value& value::
operator=(string&&);
#endif

#if 0
// defined in "array.ipp"
inline value& value::
operator=(array const&);

inline value& value::
operator=(array&&);
#endif

#if 0
// defined in "object.ipp"
inline value& value::
operator=(object const&);

inline value& value::
operator=(object&&);
#endif

inline value& value::
operator=(boost::json::value const& other)
{
    m_base = other;
    return *this;
}

inline value& value::
operator=(boost::json::value&& other)
{
    m_base = std::move(other);
    return *this;
}

inline value& value::
operator=(boost::json::string const& other)
{
    m_base = other;
    return *this;
}

inline value& value::
operator=(boost::json::string&& other)
{
    m_base = std::move(other);
    return *this;
}

inline value& value::
operator=(boost::json::array const& other)
{
    m_base = other;
    return *this;
}

inline value& value::
operator=(boost::json::array&& other)
{
    m_base = std::move(other);
    return *this;
}

inline value& value::
operator=(boost::json::object const& other)
{
    m_base = other;
    return *this;
}

inline value& value::
operator=(boost::json::object&& other)
{
    m_base = std::move(other);
    return *this;
}

inline json::kind value::
kind() const noexcept
{
    return m_base.kind();
}

inline bool value::
get_bool() const noexcept
{
    return m_base.get_bool();
}

inline bool& value::
get_bool() noexcept
{
    return m_base.get_bool();
}

inline int64_t value::
get_int64() const noexcept
{
    return m_base.get_int64();
}

inline int64_t& value::
get_int64() noexcept
{
    return m_base.get_int64();
}

inline uint64_t value::
get_uint64() const noexcept
{
    return m_base.get_uint64();
}

inline uint64_t& value::
get_uint64() noexcept
{
    return m_base.get_uint64();
}

inline double value::
get_double() const noexcept
{
    return m_base.get_double();
}

inline double& value::
get_double() noexcept
{
    return m_base.get_double();
}

#if 0 // moved to "string.ipp"
inline string const& value::
get_string() const noexcept;

inline string& value::
get_string() noexcept;
#endif

#if 0 // moved to "array.ipp"
inline array const& value::
get_array() const noexcept;

inline array& value::
get_array() noexcept;
#endif

#if 0 // moved to "object.ipp"
inline object const& value::
get_object() const noexcept;

inline object& value::
get_object() noexcept;
#endif

inline value::allocator_type value::
get_allocator() const noexcept
{
    return m_base.get_allocator();
}

inline storage_ptr const& value::
storage() const noexcept
{
    return m_base.storage();
}

inline bool value::
is_null() const noexcept
{
    return m_base.is_null();
}

inline bool value::
is_bool() const noexcept
{
    return m_base.is_bool();
}

inline bool value::
is_int64() const noexcept
{
    return m_base.is_int64();
}

inline bool value::
is_uint64() const noexcept
{
    return m_base.is_uint64();
}

inline bool value::
is_double() const noexcept
{
    return m_base.is_double();
}

inline bool value::
is_string() const noexcept
{
    return m_base.is_string();
}

inline bool value::
is_array() const noexcept
{
    return m_base.is_array();
}

inline bool value::
is_object() const noexcept
{
    return m_base.is_object();
}

inline bool value::
is_primitive() const noexcept
{
    return m_base.is_primitive();
}

inline bool value::
is_structured() const noexcept
{
    return m_base.is_structured();
}

inline void value::
emplace_null()
{
    return m_base.emplace_null();
}

inline bool& value::
emplace_bool()
{
    return m_base.emplace_bool();
}

inline int64_t& value::
emplace_int64()
{
    return m_base.emplace_int64();
}

inline uint64_t& value::
emplace_uint64()
{
    return m_base.emplace_uint64();
}

inline double& value::
emplace_double()
{
    return m_base.emplace_double();
}

#if 0 // moved to "string.ipp"
inline string& value::
emplace_string();
#endif

#if 0 // moved to "array.ipp"
inline array& value::
emplace_array();
#endif

#if 0 // moved to "object.ipp"
inline object& value::
emplace_object();
#endif

inline void value::
swap(value& other)
{
    m_base.swap(other.m_base);
}

inline void
swap(value& lhs, value& rhs)
{
    swap(lhs.m_base, rhs.m_base);
}

/*
 * class value conversion
 */
inline boost::json::value const& value::
base() const
{
    return m_base;
}

inline boost::json::value& value::
base()
{
    return m_base;
}

inline value::
operator boost::json::value const& () const
{
    return m_base;
}

inline value::
operator boost::json::value& ()
{
    return m_base;
}

template<typename T>
T value::
to_number(std::error_code& ec) const
{
    return m_base.to_number<T>(ec);
}

template<typename T>
T value::
to_number() const
{
    return m_base.to_number<T>();
}

inline bool value::
operator==(std::nullptr_t) const noexcept
{
    return is_null();
}

inline bool value::
operator==(std::integral auto other) const noexcept
{
    return base() == other;
}

inline bool value::
operator==(double const other) const noexcept
{
    return base() == other;
}

#if 0
// defined in string.ipp
inline bool value::
operator==(std::convertible_to<std::string_view> auto const& other) const noexcept;
#endif

inline bool value::
operator==(boost::json::value const& other) const noexcept
{
    return base() == other;
}

inline bool value::
operator==(boost::json::string const& other) const noexcept
{
    return base() == other;
}

inline bool value::
operator==(boost::json::array const& other) const noexcept
{
    return base() == other;
}

inline bool value::
operator==(boost::json::object const& other) const noexcept
{
    return base() == other;
}

template<typename T>
    requires std::same_as<std::remove_cvref_t<T>, value>
void
tag_invoke(boost::json::value_from_tag,
           boost::json::value& v1, T&& v2)
{
    v1 = std::forward<T>(v2);
}

} // namespace stream9::json

#endif // STREAM9_JSON_VALUE_IPP
