#ifndef STREAM9_JSON_OBJECT_IPP
#define STREAM9_JSON_OBJECT_IPP

#include "object.hpp"

namespace stream9::json {

/*
 * class value
 */
inline value::
value(object const& v, storage_ptr sp/*= {}*/)
    : m_base(static_cast<value const&>(v).base(), std::move(sp))
{}

inline value::
value(object&& v, storage_ptr sp/*= {}*/)
    : m_base(std::move(static_cast<value&>(v).base()), std::move(sp))
{}

inline value& value::
operator=(object const& v)
{
    m_base = static_cast<value const&>(v).base();
    return *this;
}

inline value& value::
operator=(object&& v)
{
    m_base = std::move(static_cast<value&>(v).base());
    return *this;
}

inline object const& value::
get_object() const noexcept
{
    return static_cast<object const&>(*this);
}

inline object& value::
get_object() noexcept
{
    return static_cast<object&>(*this);
}

inline object& value::
emplace_object()
{
    m_base.emplace_object();

    return static_cast<object&>(*this);
}

/*
 * class object
 */
inline object::
object()
    : value { boost::json::object_kind }
{}

inline object::
object(storage_ptr sp)
    : value { boost::json::object_kind, std::move(sp) }
{}

inline object::
object(object const& other, storage_ptr sp)
    : value { boost::json::object(other.base(), std::move(sp)) }
{}

inline object::
object(object&& other, storage_ptr sp)
    : value { boost::json::object(std::move(other.base()), std::move(sp)) }
{}

inline object::
object(size_type const count, storage_ptr sp/*= {}*/)
    : value { boost::json::object(count, std::move(sp)) }
{}

template<std::input_iterator It>
    requires std::constructible_from<object::base_value_type, std::iter_value_t<It>>
object::
object(It const first, It const last, size_type const min_capacity/*= 0*/,
       storage_ptr sp/*= {}*/)
    : value { boost::json::object(first, last, min_capacity, std::move(sp)) }
{}

inline object::
object(std::initializer_list<
            std::pair<std::string_view, boost::json::value_ref> > init,
       storage_ptr sp/*= {}*/ )
    : value { boost::json::object(std::move(init), std::move(sp)) }
{}

/*
 * class object accessor
 */
inline value const& object::
at(std::string_view const sv) const
{
    return reinterpret_cast<value const&>(
        base().at(sv)
    );
}

inline value& object::
at(std::string_view const sv)
{
    return reinterpret_cast<value&>(
        base().at(sv)
    );
}

inline value& object::
operator[](std::string_view const sv)
{
    return reinterpret_cast<value&>(
        base()[sv]
    );
}

inline object::
const_iterator object::begin() const
{
    return base().begin();
}

inline object::
iterator object::begin()
{
    return base().begin();
}

inline object::
const_iterator object::end() const
{
    return base().end();
}

inline object::
iterator object::end()
{
    return base().end();
}

inline object::
const_iterator object::cbegin() const
{
    return base().cbegin();
}

inline object::
const_iterator object::cend() const
{
    return base().cend();
}

inline object::
const_reverse_iterator object::rbegin() const
{
    return base().rbegin();
}

inline object::
reverse_iterator object::rbegin()
{
    return base().rbegin();
}

inline object::
const_reverse_iterator object::rend() const
{
    return base().rend();
}

inline object::
reverse_iterator object::rend()
{
    return base().rend();
}

inline object::
const_reverse_iterator object::crbegin() const
{
    return base().crbegin();
}

inline object::
const_reverse_iterator object::crend() const
{
    return base().crend();
}

inline boost::json::object const& object::
base() const
{
    return value::base().get_object();
}

inline boost::json::object& object::
base()
{
    return value::base().get_object();
}

/*
 * class object query
 */
inline bool object::
empty() const
{
    return base().empty();
}

inline object::size_type object::
size() const
{
    return base().size();
}

inline object::difference_type object::
ssize() const
{
    return static_cast<difference_type>(size());
}

inline object::size_type object::
capacity() const
{
    return base().capacity();
}

inline object::size_type object::
max_size() const
{
    return base().max_size();
}

inline object::size_type object::
count(std::string_view const key) const
{
    return base().count(key);
}

inline object::iterator object::
find(std::string_view const key)
{
    return base().find(key);
}

inline object::const_iterator object::
find(std::string_view const key) const
{
    return base().find(key);
}

inline bool object::
contains(std::string_view const key) const
{
    return base().contains(key);
}

/*
 * class object modifier
 */
template<typename P>
    requires std::constructible_from<object::base_value_type, P>
std::pair<object::iterator, bool> object::
insert(P&& p)
{
    return base().insert(std::forward<P>(p));
}

template<std::input_iterator It>
    requires std::constructible_from<object::base_value_type, std::iter_value_t<It>>
void object::
insert(It const first, It const last)
{
    base().insert(first, last);
}

inline void object::
insert(std::initializer_list<
            std::pair<std::string_view, boost::json::value_ref> > init)
{
    base().insert(std::move(init));
}

template<typename T>
std::pair<object::iterator, bool> object::
insert_or_assign(std::string_view const key, T&& value)
{
    return base().insert_or_assign(key, std::forward<T>(value));
}

template<typename Arg>
std::pair<object::iterator, bool> object::
emplace(std::string_view const key, Arg&& value)
{
    return base().emplace(key, std::forward<Arg>(value));
}

inline object::iterator object::
erase(const_iterator const it)
{
    return base().erase(it.base());
}

inline object::size_type object::
erase(std::string_view const key)
{
    return base().erase(key);
}

inline void object::
clear()
{
    return base().clear();
}

inline void object::
reserve(size_type const new_capacity)
{
    return base().reserve(new_capacity);
}

} // namespace stream9::json

#endif // STREAM9_JSON_OBJECT_IPP
