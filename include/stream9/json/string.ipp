#ifndef STREAM9_JSON_STRING_IPP
#define STREAM9_JSON_STRING_IPP

#include "value.hpp"
#include "string.hpp"

namespace stream9::json {

/*
 * value
 */
inline value::
value(string const& v, storage_ptr sp/*= {}*/)
    : m_base(static_cast<value const&>(v).base(), std::move(sp))
{}

inline value::
value(string&& v, storage_ptr sp/*= {}*/)
    : m_base(std::move(static_cast<value&>(v).base()), std::move(sp))
{}

inline value& value::
operator=(string const& v)
{
    m_base = static_cast<value const&>(v).base();
    return *this;
}

inline value& value::
operator=(string&& v)
{
    m_base = std::move(static_cast<value&>(v).base());
    return *this;
}

inline string const& value::
get_string() const noexcept
{
    return static_cast<string const&>(*this);
}

inline string& value::
get_string() noexcept
{
    return static_cast<string&>(*this);
}

inline string& value::
emplace_string()
{
    m_base.emplace_string();

    return static_cast<string&>(*this);
}

inline bool value::
operator==(std::convertible_to<std::string_view> auto const& other) const noexcept
{
    return is_string() && static_cast<std::string_view>(get_string()) == other;
}

/*
 * string
 */
inline string::
string()
    : value { boost::json::string_kind }
{}

inline string::
string(storage_ptr sp)
    : value { boost::json::string_kind, std::move(sp) }
{}

inline string::
string(string const& other, storage_ptr sp)
    : value { boost::json::string(other.base(), std::move(sp)) }
{}

inline string::
string(string&& other, storage_ptr sp)
    : value { boost::json::string(std::move(other.base()), std::move(sp)) }
{}

inline string::
string(std::convertible_to<std::string_view> auto const& s, storage_ptr sp)
    : value { std::string_view(s), std::move(sp) }
{}

inline string::
string(size_type const count, char const ch, storage_ptr sp/*= {}*/)
    : value { boost::json::string(count, ch, std::move(sp)) }
{}

/*
 * accessor
 */
inline char const& string::
at(size_type const pos) const
{
    return base().at(pos);
}

inline char& string::
at(size_type const pos)
{
    return base().at(pos);
}

inline char const& string::
operator[](size_type const pos) const
{
    return base()[pos];
}

inline char& string::
operator[](size_type const pos)
{
    return base()[pos];
}

inline char const& string::
front() const
{
    return base().front();
}

inline char& string::
front()
{
    return base().front();
}

inline char const& string::
back() const
{
    return base().back();
}

inline char& string::
back()
{
    return base().back();
}

inline char const* string::
data() const
{
    return base().data();
}

inline char* string::
data()
{
    return base().data();
}

inline char const* string::
c_str() const
{
    return base().data();
}

inline string::const_iterator string::
begin() const
{
    return base().begin();
}

inline string::iterator string::
begin()
{
    return base().begin();
}

inline string::const_iterator string::
end() const
{
    return base().end();
}

inline string::iterator string::
end()
{
    return base().end();
}

inline string::const_iterator string::
cbegin() const
{
    return base().cbegin();
}

inline string::const_iterator string::
cend() const
{
    return base().cend();
}

inline string::const_reverse_iterator string::
rbegin() const
{
    return base().rbegin();
}

inline string::reverse_iterator string::
rbegin()
{
    return base().rbegin();
}

inline string::const_reverse_iterator string::
rend() const
{
    return base().rend();
}

inline string::reverse_iterator string::
rend()
{
    return base().rend();
}

inline string::const_reverse_iterator string::
crbegin() const
{
    return base().crbegin();
}

inline string::const_reverse_iterator string::
crend() const
{
    return base().crend();
}

inline boost::json::string const& string::
base() const
{
    return value::base().get_string();
}

inline boost::json::string& string::
base()
{
    return value::base().get_string();
}

/*
 * query
 */
inline bool string::
empty() const
{
    return base().empty();
}

inline string::size_type string::
size() const
{
    return base().size();
}

inline string::difference_type string::
ssize() const
{
    return static_cast<difference_type>(base().size());
}

inline string::size_type string::
max_size() const
{
    return base().max_size();
}

inline string::size_type string::
capacity() const
{
    return base().capacity();
}

inline bool string::
starts_with(char const c) const
{
    return base().starts_with(c);
}

inline bool string::
starts_with(std::string_view const sv) const
{
    return base().starts_with(sv);
}

inline bool string::
ends_with(char const c) const
{
    return base().ends_with(c);
}

inline bool string::
ends_with(std::string_view const sv) const
{
    return base().ends_with(sv);
}

inline string::size_type string::
find(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().find(sv, pos);
}

inline string::size_type string::
find(char const c, size_type const pos/*= 0*/)
{
    return base().find(c, pos);
}

inline string::size_type string::
rfind(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().rfind(sv, pos);
}

inline string::size_type string::
rfind(char const c, size_type const pos/*= 0*/)
{
    return base().rfind(c, pos);
}

inline string::size_type string::
find_first_of(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().find_first_of(sv, pos);
}

inline string::size_type string::
find_last_of(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().find_last_of(sv, pos);
}

inline string::size_type string::
find_first_not_of(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().find_first_not_of(sv, pos);
}

inline string::size_type string::
find_first_not_of(char const c, size_type const pos/*= 0*/)
{
    return base().find_first_not_of(c, pos);
}

inline string::size_type string::
find_last_not_of(std::string_view const sv, size_type const pos/*= 0*/)
{
    return base().find_first_not_of(sv, pos);
}

inline string::size_type string::
find_last_not_of(char const c, size_type const pos/*= 0*/)
{
    return base().find_first_not_of(c, pos);
}

/*
 * modifier
 */
inline void string::
push_back(char const c)
{
    base().push_back(c);
}

inline string& string::
insert(size_type const pos, std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base().insert(pos, sv)
    );
}
inline string& string::
insert(size_type const pos, char const c)
{
    return reinterpret_cast<string&>(
        base().insert(pos, c)
    );
}

inline string& string::
insert(size_type const pos, size_type const count, char const c)
{
    return reinterpret_cast<string&>(
        base().insert(pos, count, c)
    );
}

template<std::input_iterator It>
string& string::
insert(size_type const pos, It const first, It const last)
{
    return reinterpret_cast<string&>(
        base().insert(pos, first, last)
    );
}

inline string& string::
assign(string const& s)
{
    return reinterpret_cast<string&>(
        base().assign(s)
    );
}

inline string& string::
assign(string&& sv)
{
    return reinterpret_cast<string&>(
        base().assign(std::move(sv))
    );
}

inline string& string::
assign(size_type const count, char const ch)
{
    return reinterpret_cast<string&>(
        base().assign(count, ch)
    );
}

inline string& string::
assign(char const* s, size_type const count)
{
    return reinterpret_cast<string&>(
        base().assign(s, count)
    );
}

inline string& string::
assign(char const* s)
{
    return reinterpret_cast<string&>(
        base().assign(s)
    );
}

inline string& string::
assign(std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base().assign(sv)
    );
}

template<std::input_iterator It>
inline string& string::
assign(It const first, It const last)
{
    return reinterpret_cast<string&>(
        base().assign(first, last)
    );
}

inline string& string::
append(size_type const count, char const c)
{
    return reinterpret_cast<string&>(
        base().append(count, c)
    );
}

inline string& string::
append(std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base().append(sv)
    );
}

template<std::input_iterator It>
string& string::
append(It const first, It const last)
{
    return reinterpret_cast<string&>(
        base().append(first, last)
    );
}

inline string& string::
operator+=(char const c)
{
    return reinterpret_cast<string&>(
        base() += c
    );
}

inline string& string::
operator+=(std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base() += sv
    );
}

inline string& string::
replace(size_type const pos,
        size_type const count, std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base().replace(pos, count, sv)
    );
}

inline string& string::
replace(const_iterator const first,
        const_iterator const last, std::string_view const sv)
{
    return reinterpret_cast<string&>(
        base().replace(first, last, sv)
    );
}

template<std::input_iterator It>
string& string::replace(const_iterator const first1, const_iterator const last1,
                        It const first2, It const last2)
{
    return reinterpret_cast<string&>(
        base().replace(first1, last1, first2, last2)
    );
}

inline string& string::
replace(size_type const pos, size_type const count,
        size_type const count2, char const ch)
{
    return reinterpret_cast<string&>(
        base().replace(pos,
            count, count2, ch)
    );
}

inline string& string::
replace(const_iterator const first, const_iterator const last,
        size_type const count, char const ch)
{
    return reinterpret_cast<string&>(
        base().replace(first, last, count, ch)
    );
}

inline void string::
pop_back()
{
    base().pop_back();
}

inline string& string::
erase(size_type const pos/*= 0*/, size_type const count/*= npos*/)
{
    return reinterpret_cast<string&>(
        base().erase(pos, count)
    );
}

inline string::iterator string::
erase(const_iterator const it)
{
    return base().erase(it);
}

inline string::iterator string::
erase(const_iterator const first, const_iterator const last)
{
    return base().erase(first, last);
}

inline void string::
clear()
{
    base().clear();
}

inline void string::
resize(size_type const sz)
{
    base().resize(sz);
}

inline void string::
resize(size_type const sz, char const c)
{
    base().resize(sz, c);
}

inline void string::
reserve(size_type const sz)
{
    base().reserve(sz);
}

inline void string::
shrink_to_fit()
{
    base().shrink_to_fit();
}

/*
 * command
 */
inline string::size_type string::
copy(char* const dest, size_type const count,
     size_type const pos/*= 0*/)
{
    return base().copy(dest, count, pos);
}

/*
 * conversion
 */
inline string::
operator std::string_view () const
{
    return base();
}

/*
 * comparison
 */
inline int string::
compare(std::string_view const sv) const
{
    return base().compare(sv);
}

inline std::strong_ordering string::
operator<=>(std::convertible_to<std::string_view> auto const& other) const noexcept
{
    return static_cast<std::string_view>(base()) <=> other;
}

} // namespace stream9::json

#endif // STREAM9_JSON_STRING_IPP
