#ifndef STREAM9_JSON_OBJECT_HPP
#define STREAM9_JSON_OBJECT_HPP

#include "object_iterator.hpp"
#include "value.hpp"

#include <initializer_list>
#include <string_view>
#include <utility>

namespace stream9::json {

class object : public value
{
public:
    using key_type = boost::json::object::key_type;
    using mapped_type = value;
    using allocator_type = value::allocator_type;
    using const_iterator = object_iterator<boost::json::object::const_iterator, key_type, value const>;
    using const_pointer = const_iterator::pointer;
    using const_reference = std::iter_reference_t<const_iterator>;
    using const_reverse_iterator = object_iterator<boost::json::object::const_reverse_iterator, key_type, value const>;
    using difference_type = std::iter_difference_t<const_iterator>;
    using iterator = object_iterator<boost::json::object::iterator, key_type, value>;
    using pointer = iterator::pointer;
    using reference = std::iter_reference_t<iterator>;
    using reverse_iterator = object_iterator<boost::json::object::reverse_iterator, key_type, value>;
    using size_type = boost::json::object::size_type;
    using value_type = std::pair<key_type, mapped_type&>;
    using base_value_type = boost::json::object::value_type;

public:
    object();
    explicit object(storage_ptr);

    object(object const&) = default;
    object(object const&, storage_ptr);

    object(object&&) = default;
    object(object&&, storage_ptr);

    object(size_type min_capacity, storage_ptr = {});

    template<std::input_iterator It>
        requires std::constructible_from<base_value_type, std::iter_value_t<It>>
    object(It first, It last, size_type min_capacity = 0, storage_ptr = {});

    object(std::initializer_list<
                std::pair<std::string_view, boost::json::value_ref> >,
           storage_ptr = {} );

    object& operator=(object const&) = default;
    object& operator=(object&&) = default;

    // accessor
    value const& at(std::string_view) const;
    value&       at(std::string_view);

    value&       operator[](std::string_view);

    const_iterator begin() const;
    iterator       begin();

    const_iterator end() const;
    iterator       end();

    const_iterator cbegin() const;
    const_iterator cend() const;

    const_reverse_iterator rbegin() const;
    reverse_iterator       rbegin();

    const_reverse_iterator rend() const;
    reverse_iterator       rend();

    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    boost::json::object const& base() const;
    boost::json::object&       base();

    // query
    bool empty() const;

    size_type size() const;
    difference_type ssize() const;

    size_type capacity() const;
    size_type max_size() const;

    size_type count(std::string_view key) const;

    iterator       find(std::string_view key);
    const_iterator find(std::string_view key) const;

    bool contains(std::string_view key) const;

    // modifier
    template<typename P>
        requires std::constructible_from<base_value_type, P>
    std::pair<iterator, bool>
        insert(P&&);

    template<std::input_iterator It>
        requires std::constructible_from<base_value_type, std::iter_value_t<It>>
    void insert(It first, It last);

    void insert(std::initializer_list<
                std::pair<std::string_view, boost::json::value_ref> >);

    template<typename T>
    std::pair<iterator, bool>
        insert_or_assign(std::string_view key, T&& value);

    template<typename Arg>
    std::pair<iterator, bool>
        emplace(std::string_view key, Arg&& value);

    iterator erase(const_iterator);
    size_type erase(std::string_view key);

    void clear();

    void reserve(size_type new_capacity);
};

} // namespace stream9::json

#include "object.ipp"

#endif // STREAM9_JSON_OBJECT_HPP
