#ifndef STREAM9_JSON_TYPE_TRAITS_HPP
#define STREAM9_JSON_TYPE_TRAITS_HPP

#include "namespace.hpp"

#include <concepts>
#include <type_traits>

namespace stream9::json {

class value;
class object;
class array;
class string;

template<typename T>
constexpr bool is_json_type_v =
       std::same_as<std::remove_cvref_t<T>, json::value>
    || std::same_as<std::remove_cvref_t<T>, json::object>
    || std::same_as<std::remove_cvref_t<T>, json::array>
    || std::same_as<std::remove_cvref_t<T>, json::string> ;

template<typename T1, typename T2>
concept same_without_cvref =
    std::same_as<std::remove_cvref_t<T1>, std::remove_cvref_t<T2>>;

} // namespace stream9::json

#endif // STREAM9_JSON_TYPE_TRAITS_HPP
