#ifndef STREAM9_JSON_STRING_HPP
#define STREAM9_JSON_STRING_HPP

#include "value.hpp"

#include <iterator>
#include <string_view>

namespace stream9::json {

class string : public value
{
public:
    using allocator_type = value::allocator_type;
    using const_iterator = boost::json::string::const_iterator;
    using const_pointer = boost::json::string::const_pointer;
    using const_reference = boost::json::string::const_reference;
    using const_reverse_iterator = boost::json::string::const_reverse_iterator;
    using difference_type = boost::json::string::difference_type;
    using iterator = boost::json::string::iterator;
    using pointer = boost::json::string::pointer;
    using reference = boost::json::string::reference;
    using reverse_iterator = boost::json::string::reverse_iterator;
    using size_type = boost::json::string::size_type;
    using value_type = boost::json::string::value_type;

    static constexpr auto npos = boost::json::string::npos;

public:
    string();
    explicit string(storage_ptr);

    string(string const&) = default;
    string(string const&, storage_ptr);

    string(string&&) = default;
    string(string&&, storage_ptr);

    string(std::convertible_to<std::string_view> auto const&, storage_ptr = {});

    string(size_type count, char ch, storage_ptr = {});

    string& operator=(string const&) = default;
    string& operator=(string&&) = default;

    // accessor
    char const& at(size_type pos) const;
    char&       at(size_type pos);

    char const& operator[](size_type pos) const;
    char&       operator[](size_type pos);

    char const& front() const;
    char&       front();

    char const& back() const;
    char&       back();

    char const* data() const;
    char*       data();

    char const* c_str() const;

    const_iterator begin() const;
    iterator       begin();

    const_iterator end() const;
    iterator       end();

    const_iterator cbegin() const;
    const_iterator cend() const;

    const_reverse_iterator rbegin() const;
    reverse_iterator       rbegin();

    const_reverse_iterator rend() const;
    reverse_iterator       rend();

    const_reverse_iterator crbegin() const;
    const_reverse_iterator crend() const;

    boost::json::string const& base() const;
    boost::json::string& base();

    // query
    bool empty() const;

    size_type       size() const;
    difference_type ssize() const;

    size_type       max_size() const;
    size_type       capacity() const;

    bool starts_with(char) const;
    bool starts_with(std::string_view) const;

    bool ends_with(char) const;
    bool ends_with(std::string_view) const;

    size_type find(std::string_view, size_type pos = 0);
    size_type find(char, size_type pos = 0);

    size_type rfind(std::string_view, size_type pos = 0);
    size_type rfind(char, size_type pos = 0);

    size_type find_first_of(std::string_view, size_type pos = 0);
    size_type find_last_of(std::string_view, size_type pos = 0);

    size_type find_first_not_of(std::string_view, size_type pos = 0);
    size_type find_first_not_of(char, size_type pos = 0);

    size_type find_last_not_of(std::string_view, size_type pos = 0);
    size_type find_last_not_of(char, size_type pos = 0);

    // modifier
    void push_back(char);

    string& insert(size_type pos, std::string_view);
    string& insert(size_type pos, char);
    string& insert(size_type pos, size_type count, char);

    template<std::input_iterator It>
    string& insert(size_type pos, It first, It last);

    string& assign(string const&);
    string& assign(string&&);
    string& assign(size_type count, char ch);
    string& assign(char const*, size_type count);
    string& assign(char const*);
    string& assign(std::string_view);

    template<std::input_iterator It>
    string& assign(It first, It last);

    string& append(size_type count, char);
    string& append(std::string_view);

    template<std::input_iterator It>
    string& append(It first, It last);

    string& operator+=(char);
    string& operator+=(std::string_view);

    string& replace(size_type pos, size_type count, std::string_view);
    string& replace(const_iterator first, const_iterator last, std::string_view);

    template<std::input_iterator It>
    string& replace(const_iterator first1, const_iterator last1,
                    It             first2,             It last2 );

    string& replace(size_type pos, size_type count,
                    size_type count2, char ch );

    string& replace(const_iterator first, const_iterator last,
                    size_type count, char ch );

    void pop_back();

    string& erase(size_type pos = 0, size_type count = npos);
    iterator erase(const_iterator);
    iterator erase(const_iterator first, const_iterator last);

    void clear();

    void resize(size_type);
    void resize(size_type, char);

    void reserve(size_type);
    void shrink_to_fit();

    // command
    size_type copy(char* dest, size_type count,
                         size_type pos = 0 );

    // conversion
    operator std::string_view () const;

    // comparison
    int compare(std::string_view) const;

    std::strong_ordering
        operator<=>(std::convertible_to<std::string_view> auto const&) const noexcept;
};

} // namespace stream9::json

#include "string.ipp"

#endif // STREAM9_JSON_STRING_HPP
