#ifndef STREAM9_JSON_FIND_HPP
#define STREAM9_JSON_FIND_HPP

#include "value.hpp"

#include <stream9/optional.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::json {

/*
 * template<typename... Rest>
 * opt<[type]&>
 * find_[name](value& v,
 *             std::string_view const key,
 *             Rest&&... rest) noexcept;
 *
 * template<typename... Rest>
 * opt<[type] const&>
 * find_[name](value const& v,
 *             std::string_view const key,
 *             Rest&&... rest) noexcept;
 *
 * template<typename... Rest>
 * opt<[type]&>
 * find_[name](value& v,
 *             safe_integer<std::size_t> const idx,
 *             Rest&&... rest) noexcept;
 *
 * template<typename... Rest>
 * opt<[type] const&>
 * find_[name](value const& v,
 *             safe_integer<std::size_t> const idx,
 *             Rest&&... rest) noexcept;
 *
 * (name, type) := {
 *      (bool, bool), (int64, int64_t), (uint64, uint64_t),
 *      (double, double), (string, string), (array, array), (object, object)
 *  }
 */

//
inline opt<value const&>
find_value(value const& v) noexcept
{
    return v;
}

inline opt<value&>
find_value(value& v) noexcept
{
    return v;
}

//
inline opt<value&>
find_value(value& v, std::string_view const key) noexcept
{
    if (!v.is_object()) return {};

    auto& obj = v.get_object();

    auto const it = obj.find(key);
    if (it == obj.end()) return {};

    return (*it).second;
}

inline opt<value const&>
find_value(value const& v, std::string_view const key) noexcept
{
    return find_value(const_cast<value&>(v), key);
}

//
inline opt<value&>
find_value(value& v, safe_integer<std::size_t> const idx) noexcept
{
    if (!v.is_array()) return {};

    auto& arr = v.get_array();

    if (idx >= arr.size()) return {};

    return arr[idx];
}

inline opt<value const&>
find_value(value const& v, safe_integer<std::size_t> const idx) noexcept
{
    return find_value(const_cast<value&>(v), idx);
}

//
template<typename... Rest>
opt<value&>
find_value(value& v,
           std::string_view const key,
           Rest&&... rest) noexcept
{
    auto const o_v = find_value(v, key);
    if (o_v) {
        return find_value(*o_v, std::forward<Rest>(rest)...);
    }
    else {
        return {};
    }
}

template<typename... Rest>
opt<value const&>
find_value(value const& v,
           std::string_view const key,
           Rest&&... rest) noexcept
{
    return find_value(const_cast<value&>(v),
                   key, std::forward<Rest>(rest)... );
}

template<typename... Rest>
opt<value&>
find_value(value& v,
           safe_integer<std::size_t> const idx,
           Rest&&... rest) noexcept
{
    auto const o_v = find_value(v, idx);
    if (o_v) {
        return find_value(*o_v, std::forward<Rest>(rest)...);
    }
    else {
        return {};
    }
}

template<typename... Rest>
opt<value const&>
find_value(value const& v,
           safe_integer<std::size_t> const idx,
           Rest&&... rest) noexcept
{
    return find_value(const_cast<value&>(v),
                   idx, std::forward<Rest>(rest)... );
}

#define FIND_X(x) find_ ## x
#define IS_X(x) is_ ## x
#define GET_X(x) get_ ## x

#define DEFINE_FUNCTIONS(name, kind) \
    inline opt<kind&> \
    FIND_X(name)(value& v) noexcept \
    { \
        if (v.IS_X(name)()) { \
            return v.GET_X(name)(); \
        } \
        else { \
            return {}; \
        } \
    } \
 \
    inline opt<kind const&> \
    FIND_X(name)(value const& v) noexcept \
    { \
        return FIND_X(name)(const_cast<value&>(v)); \
    } \
 \
    inline opt<kind&> \
    FIND_X(name)(value& v, std::string_view const key) noexcept \
    { \
        auto const o_v = find_value(v, key); \
        if (o_v) { \
            return FIND_X(name)(*o_v); \
        } \
        else { \
            return {}; \
        } \
    } \
 \
    inline opt<kind const&> \
    FIND_X(name)(value const& v, std::string_view const key) noexcept \
    { \
        return FIND_X(name)(const_cast<value&>(v), key); \
    } \
 \
    inline opt<kind&> \
    FIND_X(name)(value& v, safe_integer<std::size_t> const idx) noexcept \
    { \
        auto const o_v = find_value(v, idx); \
        if (o_v) { \
            return FIND_X(name)(*o_v); \
        } \
        else { \
            return {}; \
        } \
    } \
 \
    inline opt<kind const&> \
    FIND_X(name)(value const& v, safe_integer<std::size_t> const idx) noexcept \
    { \
        return FIND_X(name)(const_cast<value&>(v), idx); \
    } \
 \
    template<typename... Rest> \
    opt<kind&> \
    FIND_X(name)(value& v, std::string_view const key, Rest&&... rest) noexcept \
    { \
        auto const o_v = find_value(v, key, std::forward<Rest>(rest)...); \
        if (o_v) { \
            return FIND_X(name)(*o_v); \
        } \
        else { \
            return {}; \
        } \
    } \
 \
    template<typename... Rest> \
    opt<kind const&> \
    FIND_X(name)(value const& v, std::string_view const key, Rest&&... rest) noexcept \
    { \
        return FIND_X(name)(const_cast<value&>(v), \
                           key, std::forward<Rest>(rest)... ); \
    } \
 \
    template<typename... Rest> \
    opt<kind&> \
    FIND_X(name)(value& v, \
                safe_integer<std::size_t> const idx, \
                Rest&&... rest) noexcept \
    { \
        auto const o_v = find_value(v, idx, std::forward<Rest>(rest)...); \
        if (o_v) { \
            return FIND_X(name)(*o_v); \
        } \
        else { \
            return {}; \
        } \
    } \
 \
    template<typename... Rest> \
    opt<kind const&> \
    FIND_X(name)(value const& v, \
                safe_integer<std::size_t> const idx, \
                Rest&&... rest) noexcept \
    { \
        return FIND_X(name)(const_cast<value&>(v), \
                           idx, std::forward<Rest>(rest)... ); \
    } \
 \

DEFINE_FUNCTIONS(bool, bool)
DEFINE_FUNCTIONS(int64, int64_t)
DEFINE_FUNCTIONS(uint64, uint64_t)
DEFINE_FUNCTIONS(double, double)
DEFINE_FUNCTIONS(string, string)
DEFINE_FUNCTIONS(array, array)
DEFINE_FUNCTIONS(object, object)

#undef FIND_X
#undef IS_X
#undef GET_X
#undef DEFINE_FUNCTIONS

} // namespace stream9::json

#endif // STREAM9_JSON_FIND_HPP
