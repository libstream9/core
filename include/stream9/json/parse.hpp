#ifndef STREAM9_JSON_PARSE_HPP
#define STREAM9_JSON_PARSE_HPP

#include "boost_json.hpp"
#include "error.hpp"

#include <string>
#include <string_view>

namespace stream9::json {

class value;
using boost::json::parse_options;

json::value
    parse(std::string_view json,
          json::storage_ptr = {},
          json::parse_options const& = {});

json::value
    parse(std::istream& json,
          json::storage_ptr = {},
          json::parse_options const& = {});

} // namespace stream9::json

#include "value.hpp"

#endif // STREAM9_JSON_PARSE_HPP
