#ifndef STREAM9_JSON_ERROR_HPP
#define STREAM9_JSON_ERROR_HPP

#include "boost_json.hpp"

namespace stream9::json {

using errc = boost::json::error;

inline std::error_category const&
error_category() noexcept
{
    return boost::json::generic_category();
}

} // namespace stream9::json

#endif // STREAM9_JSON_ERROR_HPP
