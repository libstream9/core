#ifndef STREAM9_PAIR_HPP
#define STREAM9_PAIR_HPP

#include "namespace.hpp"

namespace stream9 {

template<typename T1, typename T2>
struct pair
{
    T1 first;
    T2 second;
};

} // namespace stream9

#endif // STREAM9_PAIR_HPP
