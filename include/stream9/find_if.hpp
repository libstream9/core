#ifndef STREAM9_RANGES_FIND_IF_HPP
#define STREAM9_RANGES_FIND_IF_HPP

#include <ranges>

namespace stream9 {

using std::ranges::find_if;

} // namespace stream9

#endif // STREAM9_RANGES_FIND_IF_HPP
