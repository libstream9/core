#ifndef STREAM9_SWAP_HPP
#define STREAM9_SWAP_HPP

#include <utility>

namespace stream9 {

using std::swap;

} // namespace stream9

#endif // STREAM9_SWAP_HPP
