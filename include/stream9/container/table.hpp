#ifndef STREAM9_CONTAINER_TABLE_HPP
#define STREAM9_CONTAINER_TABLE_HPP

#include "key_value.hpp"

#include <stream9/array.hpp>
#include <stream9/concepts.hpp>

#include <cstdint>
#include <initializer_list>

namespace stream9 {

struct table_traits
{
    using size_type = std::int64_t;

    std::int64_t minimum_capacity = 0;
    std::int64_t maximum_capacity = INT64_MAX;
};

template<typename Key,
         typename Value,
         typename Traits = table_traits >
class table
{
public:
    static_assert(nothrow_movable<Key>);
    static_assert(nothrow_destructible<Key>);
    static_assert(nothrow_movable<Value>);
    static_assert(nothrow_destructible<Value>);

    using entry = key_value<Key, Value>;

    using array_t = array<entry>;
    using size_type = Traits::size_type;

    using iterator = array_t::iterator;
    using const_iterator = array_t::const_iterator;

public:
    // essentials
    table() = default;

    table(std::initializer_list<entry>);

    table(table const&) = default;
    table& operator=(table const&) = default;
    table(table&&) noexcept = default;
    table& operator=(table&&) noexcept = default;

    ~table() noexcept = default;

    // accessor
    iterator begin() noexcept;
    iterator end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    template<typename K>
    Value& operator[](K const&)
        requires equality_comparable<Key, K>
              && constructible<Key, K>
              && default_constructible<Value>;

    iterator remove_const(const_iterator);

    // query
    size_type size() const noexcept;
    bool empty() const noexcept;

    template<typename K>
    const_iterator find(K const&) const
        noexcept(nothrow_equality_comparable<Key, K>)
        requires equality_comparable<Key, K>;

    template<typename K>
    iterator find(K const&)
        noexcept(nothrow_equality_comparable<Key, K>)
        requires equality_comparable<Key, K>;

    // modifier
    iterator insert(entry&&);

    iterator insert_at(const_iterator, entry&&);

    template<typename K, typename V>
    iterator
    emplace(K&& k, V&& v)
        requires constructible<Key, K>
              && constructible<Value, V>;

    template<typename K, typename V>
    iterator
    emplace_at(const_iterator, K&& k, V&& v)
        requires constructible<Key, K>
              && constructible<Value, V>;

    iterator erase(const_iterator) noexcept;
    iterator erase(const_iterator first, const_iterator last) noexcept;

    void clear() noexcept;

    void resize(size_type);

    void reserve(size_type);

    void shrink_to_fit();

    bool operator==(table const&) const
        noexcept(nothrow_equality_comparable<entry>)
        requires equality_comparable<entry>;

private:
    array_t m_entries;
};

template<typename K, typename V, typename T>
std::ostream& operator<<(std::ostream&, table<K, V, T> const&);

} // namespace stream9

#include "detail/table.ipp"

#endif // STREAM9_CONTAINER_TABLE_HPP
