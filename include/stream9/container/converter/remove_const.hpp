#ifndef STREAM9_CONTAINER_REMOVE_CONST_HPP
#define STREAM9_CONTAINER_REMOVE_CONST_HPP

#include "../namespace.hpp"

#include <concepts>

#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/concepts.hpp>

namespace stream9::container {

namespace remove_const_ {

    template<typename C>
    concept has_member_fn =
        requires (C& s) {
            { s.remove_const(rng::cbegin(s)) } -> std::same_as<rng::iterator_t<C>>;
        };

    template<typename C, typename I = rng::iterator_t<C const>>
    concept has_range_erase =
        requires (C a, I q1, I q2) {
            { a.erase(q1, q2) } -> std::same_as<rng::iterator_t<C>>;
        };

    struct api
    {
        template<typename Seq>
        rng::iterator_t<Seq>
        operator()(Seq& s, rng::iterator_t<Seq const> i) const
            noexcept(noexcept(s.remove_const(i)))
            requires has_member_fn<Seq>
        {
            return s.remove_const(i);
        }

        template<typename Seq>
        rng::iterator_t<Seq>
        operator()(Seq& s, rng::iterator_t<Seq const> i) const
            noexcept(noexcept(s.erase(i, i)))
            requires (!has_member_fn<Seq>)
                  && has_range_erase<Seq>
        {
            return s.erase(i, i);
        }
    };

} // namespace remove_const_

inline constexpr remove_const_::api remove_const;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_REMOVE_CONST_HPP
