#ifndef STREAM9_CONTAINER_CONCEPTS2_HPP
#define STREAM9_CONTAINER_CONCEPTS2_HPP

#include <concepts>
#include <ranges>
#include <utility>

namespace stream9::container {

using std::false_type;
using std::forward;
using std::input_iterator;
using std::ranges::input_range;
using std::ranges::iterator_t;
using std::same_as;
using std::sentinel_for;
using std::true_type;

template<typename C, typename T>
concept insertable_sequence =
    input_range<C> &&
    requires (C c, T&& v) {
        { c.insert(c.begin(), forward<T>(v)) } -> same_as<iterator_t<C>>;
    };

template<typename...>
struct is_range_insertable;

template<typename C, typename... Args>
concept range_insertable_sequence =
    is_range_insertable<C, Args...>::value;

template<typename C, typename... Args>
concept emplacable_sequence =
    input_range<C> &&
    requires (C c, Args&&... args) {
        { c.emplace(c.begin(), forward<Args>(args)...) }
                                                      -> same_as<iterator_t<C>>;
    };

//
template<typename...>
struct is_range_insertable : false_type {};

template<input_range C, input_range R>
    requires requires (C c, R v) {
        { c.insert(c.begin(), v.begin(), v.end()) } -> same_as<iterator_t<C>>;
    }
struct is_range_insertable<C, R> : true_type {};

template<input_range C, input_iterator I, sentinel_for<I> S>
    requires requires (C c, I i, S s) {
        { c.insert(c.begin(), i, s) } -> same_as<iterator_t<C>>;
    }
struct is_range_insertable<C, I, S> : true_type {};

} // namespace stream9::container

#endif // STREAM9_CONTAINER_CONCEPTS2_HPP
