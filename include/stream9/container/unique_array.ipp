#ifndef STREAM9_CONTAINER_UNIQUE_ARRAY_IPP
#define STREAM9_CONTAINER_UNIQUE_ARRAY_IPP

#include <ranges>
#include <utility>

#include <stream9/errors.hpp>
#include <stream9/ranges/binary_find.hpp>

namespace stream9 {

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::
unique_array(allocator_type const& alloc)
    : m_array { alloc }
{}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::
unique_array(std::initializer_list<T> il)
    try : m_array { std::move(il) }
{
    std::ranges::sort(m_array, C(), P());
    auto r = std::ranges::unique(m_array, {}, P());
    m_array.erase(r.begin(), r.end());
}
catch (...) {
    rethrow_error();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename... Us>
unique_array<T, C, P, Min, A>::
unique_array(Us&&... values)
    requires (is_insertable_v<Us> && ...)
    try : m_array { std::forward<Us>(values)... }
{
    std::ranges::sort(m_array, C(), P());
    auto r = std::ranges::unique(m_array, {}, P());
    m_array.erase(r.begin(), r.end());
}
catch (...) {
    rethrow_error();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::
unique_array::iterator unique_array<T, C, P, Min, A>::
begin() noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
end() noexcept
{
    return m_array.end();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_iterator unique_array<T, C, P, Min, A>::
begin() const noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_iterator unique_array<T, C, P, Min, A>::
end() const noexcept
{
    return m_array.end();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::reference unique_array<T, C, P, Min, A>::
at(size_type pos) noexcept
{
    return m_array.at(pos);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_reference unique_array<T, C, P, Min, A>::
at(size_type pos) const noexcept
{
    return m_array.at(pos);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::reference unique_array<T, C, P, Min, A>::
operator[](size_type pos) noexcept
{
    return m_array[pos];
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_reference unique_array<T, C, P, Min, A>::
operator[](size_type pos) const noexcept
{
    return m_array[pos];
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::reference unique_array<T, C, P, Min, A>::
front() noexcept
{
    return m_array.front();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_reference unique_array<T, C, P, Min, A>::
front() const noexcept
{
    return m_array.front();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::reference unique_array<T, C, P, Min, A>::
back() noexcept
{
    return m_array.back();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_reference unique_array<T, C, P, Min, A>::
back() const noexcept
{
    return m_array.back();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::pointer unique_array<T, C, P, Min, A>::
data() noexcept
{
    return m_array.data();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::const_pointer unique_array<T, C, P, Min, A>::
data() const noexcept
{
    return m_array.data();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
bool unique_array<T, C, P, Min, A>::
empty() const noexcept
{
    return m_array.empty();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::size_type unique_array<T, C, P, Min, A>::
size() const noexcept
{
    return m_array.size();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::iterator unique_array<T, C, P, M, A>::
find(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::const_iterator unique_array<T, C, P, M, A>::
find(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
find(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using ranges::binary_find;

    return binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::const_iterator unique_array<T, C, P, Min, A>::
find(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using ranges::binary_find;

    return binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::iterator unique_array<T, C, P, M, A>::
lower_bound(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::lower_bound;
    P proj {};
    return lower_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::const_iterator unique_array<T, C, P, M, A>::
lower_bound(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::lower_bound;
    P proj {};
    return lower_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
lower_bound(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::lower_bound;

    return lower_bound(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::const_iterator unique_array<T, C, P, Min, A>::
lower_bound(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::lower_bound;

    return lower_bound(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::iterator unique_array<T, C, P, M, A>::
upper_bound(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::upper_bound;
    P proj {};
    return upper_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
unique_array<T, C, P, M, A>::const_iterator unique_array<T, C, P, M, A>::
upper_bound(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::upper_bound;
    P proj {};
    return upper_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
upper_bound(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::upper_bound;

    return upper_bound(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::const_iterator unique_array<T, C, P, Min, A>::
upper_bound(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::upper_bound;

    return upper_bound(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
std::ranges::subrange<typename unique_array<T, C, P, M, A>::iterator> unique_array<T, C, P, M, A>::
equal_range(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::equal_range;
    P proj {};
    return equal_range(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
std::ranges::subrange<typename unique_array<T, C, P, M, A>::const_iterator> unique_array<T, C, P, M, A>::
equal_range(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::equal_range;
    P proj {};
    return equal_range(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
std::ranges::subrange<typename unique_array<T, C, P, Min, A>::iterator> unique_array<T, C, P, Min, A>::
equal_range(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::equal_range;

    return equal_range(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
std::ranges::subrange<typename unique_array<T, C, P, Min, A>::const_iterator> unique_array<T, C, P, Min, A>::
equal_range(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::equal_range;

    return equal_range(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::size_type unique_array<T, C, P, Min, A>::
capacity() const noexcept
{
    return m_array.capacity();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::size_type unique_array<T, C, P, Min, A>::
min_capacity() const noexcept
{
    return m_array.min_capacity();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::size_type unique_array<T, C, P, Min, A>::
max_capacity() const noexcept
{
    return m_array.max_capacity();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::allocator_type unique_array<T, C, P, Min, A>::
get_allocator() const noexcept
{
    return m_array.get_allocator();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
remove_const(const_iterator i)
{
    return m_array.remove_const(i);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
T& unique_array<T, C, P, Min, A>::insert_result_t::
operator*() const noexcept
{
    return *position;
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
T* unique_array<T, C, P, Min, A>::insert_result_t::
operator->() const noexcept
{
    return std::addressof(*position);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t::
operator bool () const noexcept
{
    return inserted;
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t::
operator iterator () const noexcept
{
    return position;
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert(T const& value)
{
    try {
        C comp;
        P proj;

        bool inserted = false;
        auto&& pv = std::invoke(proj, value);
        auto it = rng::lower_bound(*this, pv, comp, proj);
        if (it == end() || comp(pv, std::invoke(proj, *it))) {
            it = m_array.insert(it, value);
            inserted = true;
        }

        return { it, inserted };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert(T&& value)
{
    try {
        C comp;
        P proj;

        bool inserted = false;
        auto&& pv = std::invoke(proj, value);
        auto it = rng::lower_bound(*this, pv, comp, proj);
        if (it == end() || comp(pv, std::invoke(proj, *it))) {
            it = m_array.insert(it, std::move(value));
            inserted = true;
        }

        return { it, inserted };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert(U&& value)
    requires std::is_constructible_v<T, U>
{
    try {
        return insert(T { std::forward<U>(value) });
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert_or_replace(T const& value)
{
    try {
        C comp;
        P proj;

        bool inserted = false;
        auto&& pv = std::invoke(proj, value);
        auto it = rng::lower_bound(*this, pv, comp, proj);
        if (it == end() || comp(pv, std::invoke(proj, *it))) {
            it = m_array.insert(it, value);
            inserted = true;
        }
        else {
            *it = value;
        }

        return { it, inserted };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert_or_replace(T&& value)
{
    try {
        C comp;
        P proj;

        bool inserted = false;
        auto&& pv = std::invoke(proj, value);
        auto it = rng::lower_bound(*this, pv, comp, proj);
        if (it == end() || comp(pv, std::invoke(proj, *it))) {
            it = m_array.insert(it, std::move(value));
            inserted = true;
        }
        else {
            *it = std::move(value);
        }

        return { it, inserted };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<typename U>
unique_array<T, C, P, Min, A>::insert_result_t unique_array<T, C, P, Min, A>::
insert_or_replace(U&& value)
    requires std::is_constructible_v<T, U>
{
    try {
        return insert_or_replace(T { std::forward<U>(value) });
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
erase(const_iterator pos) noexcept
{
    return m_array.erase(pos);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
unique_array<T, C, P, Min, A>::iterator unique_array<T, C, P, Min, A>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_array.erase(first, last);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
void unique_array<T, C, P, Min, A>::
clear() noexcept
{
    m_array.clear();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
void unique_array<T, C, P, Min, A>::
reserve(size_type n)
{
    m_array.reserve(n);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
void unique_array<T, C, P, Min, A>::
shrink_to_fit()
{
    m_array.shrink_to_fit();
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
void unique_array<T, C, P, Min, A>::
swap(unique_array& o) noexcept
{
    m_array.swap(o);
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<std::ranges::input_range R>
bool unique_array<T, C, P, Min, A>::
operator==(R const& other) const
    noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
    requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return m_array == other;
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<std::ranges::input_range R>
auto unique_array<T, C, P, Min, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
    requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return m_array <=> other;
}

template<typename T, typename C, typename P, std::int64_t Min, typename A>
template<std::ranges::input_range R>
std::partial_ordering unique_array<T, C, P, Min, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
    requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
          && requires (T v1, std::ranges::range_reference_t<R> v2) {
              { v1 < v2 } -> std::convertible_to<bool>;
          }
{
    return m_array <=> other;
}

template<typename CharT, typename Traits,
         typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, unique_array<T2, C2, P2, M2, A2> const& a)
    requires requires (std::basic_ostream<CharT, Traits>& os, T2 v) { os << v; }
{
    return os << a.m_array;
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void
swap(unique_array<T, C, P, M, A>& lhs, unique_array<T, C, P, M, A>& rhs) noexcept
{
    lhs.swap(rhs);
}

namespace json {

template<typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
void
tag_invoke(value_from_tag, value& jv, stream9::unique_array<T2, C2, P2, M2, A2> const& a)
{
    jv = value_from(a.m_array);
}

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_UNIQUE_ARRAY_IPP
