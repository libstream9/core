#ifndef STREAM9_CONTAINER_CONCEPTS_HPP
#define STREAM9_CONTAINER_CONCEPTS_HPP

#include "namespace.hpp"

#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/end.hpp>

#include <concepts>

namespace stream9::container {

template<typename T>
concept erasable = rng::forward_range<T>
      && requires (T v) {
          { v.erase(rng::cbegin(v)) } -> std::convertible_to<rng::iterator_t<T>>;
      };

template<typename T>
concept nothrow_erasable = rng::forward_range<T>
      && requires (T v) {
          { v.erase(rng::cbegin(v)) } noexcept -> std::convertible_to<rng::iterator_t<T>>;
      };

template<typename T>
concept range_erasable = rng::forward_range<T>
      && requires (T v) {
          { v.erase(rng::cbegin(v), rng::cend(v)) } -> std::convertible_to<rng::iterator_t<T>>;
      };

template<typename T>
concept nothrow_range_erasable = rng::forward_range<T>
      && requires (T v) {
          { v.erase(rng::cbegin(v), rng::cend(v)) } noexcept -> std::convertible_to<rng::iterator_t<T>>;
      };

} // namespace stream9::container

#endif // STREAM9_CONTAINER_CONCEPTS_HPP
