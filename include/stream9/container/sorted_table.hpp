#ifndef STREAM9_CONTAINER_SORTED_TABLE_HPP
#define STREAM9_CONTAINER_SORTED_TABLE_HPP

#include "key_value.hpp"

#include <stream9/array.hpp>
#include <stream9/json.hpp>
#include <stream9/less.hpp>
#include <stream9/optional.hpp>
#include <stream9/range.hpp>

#include <compare>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <ostream>
#include <type_traits>

namespace stream9 {

struct sorted_table_traits
{
    template<typename T>
    using allocator = std::allocator<T>;

    using order = less;
    using size_type = std::int64_t;

    static constexpr auto min_capacity = std::numeric_limits<size_type>::min;
    static constexpr auto max_capacity = std::numeric_limits<size_type>::max;
};

template<typename Key, typename T,
         typename Traits = sorted_table_traits>
class sorted_table
{
public:
    using entry = key_value<Key, T>;

    using array_t = array<entry>;

    using size_type = Traits::size_type;

    using iterator = array_t::iterator;
    using const_iterator = array_t::const_iterator;

    template<typename K>
    static constexpr bool is_comparable_with_key_v =
        requires (Key const& x, K const& y) {
            { typename Traits::order()(x, y) } -> std::convertible_to<bool>;
        };

    template<typename K>
    static constexpr bool is_nothrow_comparable_with_key_v =
        noexcept(typename Traits::order()(std::declval<Key>(), std::declval<K>()));

    static_assert(std::is_nothrow_destructible_v<Key>);
    static_assert(std::is_nothrow_destructible_v<T>);
    static_assert(std::is_nothrow_move_constructible_v<Key>);
    static_assert(std::is_nothrow_move_constructible_v<T>);

public:
    // essentials
    sorted_table() noexcept;

    sorted_table(std::initializer_list<entry>);

    sorted_table(sorted_table const&) = default;
    sorted_table& operator=(sorted_table const&) = default;

    sorted_table(sorted_table&&) noexcept = default;
    sorted_table& operator=(sorted_table&&) noexcept = default;

    ~sorted_table() noexcept = default;

    void swap(sorted_table&) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    iterator begin() noexcept;

    const_iterator end() const noexcept;
    iterator end() noexcept;

    template<typename K>
    T& operator[](K const&)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_default_constructible_v<T>;

    iterator remove_const(const_iterator) noexcept;

    // query
    size_type size() const noexcept;

    template<typename K>
    const_iterator find(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator find(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    const_iterator lower_bound(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator lower_bound(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    const_iterator upper_bound(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator upper_bound(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    range<iterator>
    equal_range(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    range<const_iterator>
    equal_range(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    // modifier
    iterator insert(entry const&);

    iterator insert(entry&&);

    template<typename K, typename V>
    iterator emplace(K&& k, V&& v)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_constructible_v<T, V>;

    template<typename K, typename V>
    iterator emplace_hint(const_iterator hint, K&& k, V&& v)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_constructible_v<T, V>;

    iterator erase(const_iterator) noexcept;

    iterator erase(const_iterator first, const_iterator last) noexcept;

    template<typename K>
    size_type erase(K const& k)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    void clear() noexcept;

    void reserve(size_type);

    void shrink_to_fit();

    bool operator==(sorted_table const&) const
        noexcept(noexcept(std::declval<Key>() == std::declval<Key>())
              && noexcept(std::declval<T>() == std::declval<T>()) );

    auto operator<=>(sorted_table const&) const
        noexcept(noexcept(std::declval<Key>() <=> std::declval<Key>())
              && noexcept(std::declval<T>() <=> std::declval<T>()) );

private:
    array_t m_entries;
};

template<typename K, typename V, typename T>
std::ostream& operator<<(std::ostream&, sorted_table<K, V, T> const&);

namespace json {

template<typename K, typename V, typename T>
void tag_invoke(value_from_tag, value&, sorted_table<K, V, T> const&);

} // namespace json

} // namespace stream9

namespace std {

template<typename K, typename V, typename T>
struct tuple_size<stream9::sorted_table<K, V, T>>
{
    static constexpr auto value = 2;
};

template<typename K, typename V, typename T>
struct tuple_element<0, stream9::sorted_table<K, V, T>>
{
    using type = stream9::ranges::iterator_t<stream9::sorted_table<K, V, T>>;
};

template<typename K, typename V, typename T>
struct tuple_element<1, stream9::sorted_table<K, V, T>>
{
    using type = stream9::ranges::sentinel_t<stream9::sorted_table<K, V, T>>;
};

} // namespace std

#include "sorted_table.ipp"

#endif // STREAM9_CONTAINER_SORTED_TABLE_HPP
