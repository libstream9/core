#ifndef STREAM9_CONTAINER_ARRAY_HPP
#define STREAM9_CONTAINER_ARRAY_HPP

#include "array/partial_compare.hpp"
#include "array/base.hpp"

#include <cstdint>
#include <initializer_list>
#include <iterator>
#include <memory>
#include <ostream>
#include <ranges>
#include <type_traits>
#include <utility>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/nothrow_swap.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

/*
 * model std::contiguous_range
 */
template<typename T,
         std::int64_t MinCapacity = 0,
         typename Allocator = std::allocator<T> >
class array : array_::base<T, MinCapacity, Allocator>
{
    static_assert(!std::is_reference_v<T>);
    static_assert(!std::is_const_v<T>);
    static_assert(MinCapacity >= 0);
    //static_assert(std::is_nothrow_move_constructible_v<T>); // don't check these here, because is cause error on incomplete type
    //static_assert(std::is_nothrow_destructible_v<T>);
    static_assert(std::is_nothrow_default_constructible_v<Allocator>);
    static_assert(std::is_nothrow_move_constructible_v<Allocator>);
    static_assert(std::is_nothrow_destructible_v<Allocator>);
    static_assert(is_nothrow_swappable_v<Allocator>);

    using base_t = array_::base<T, MinCapacity, Allocator>;
public:
    using pointer = T*;
    using const_pointer = T const*;
    using reference = T&;
    using const_reference = T const&;
    using size_type = safe_integer<std::int64_t, 0>;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using allocator_type = Allocator;
    using value_type = T;

    template<typename>
    struct is_insertable : std::false_type {};

    template<typename U>
        requires std::is_constructible_v<T, U>
              || (rng::input_range<U>
                  && std::is_constructible_v<T, rng::reference_t<U>>)
    struct is_insertable<U> : std::true_type {};

    template<typename U>
    constexpr static bool is_insertable_v = is_insertable<U>::value;

    template<typename>
    struct is_nothrow_insertable : std::false_type {};

    template<typename U>
        requires std::is_nothrow_constructible_v<T, U>
              || (rng::input_range<U>
                  && rng::sized_range<U>
                  && std::is_nothrow_constructible_v<T, rng::reference_t<U>>)
    struct is_nothrow_insertable<U> : std::true_type {};

    template<typename U>
    constexpr static bool is_nothrow_insertable_v = is_insertable<U>::value;

public:
    // essential
    array() = default;

    array(allocator_type const& alloc);

    array(std::initializer_list<T>);

    template<typename... Us>
    array(Us&&... values)
        requires (is_insertable_v<Us> && ...);

    array(array const&) = default;
    array& operator=(array const&) = default;

    array(array&&) = default;
    array& operator=(array&&) = default;

    ~array() noexcept = default;

    // accessor
    iterator       begin() noexcept;
    iterator       end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    reference       at(size_type pos) noexcept;
    const_reference at(size_type pos) const noexcept;

    reference       operator[](size_type pos) noexcept;
    const_reference operator[](size_type pos) const noexcept;

    reference       front() noexcept;
    const_reference front() const noexcept;

    reference back() noexcept;
    const_reference back() const noexcept;

    pointer       data() noexcept;
    const_pointer data() const noexcept;

    // query
    bool empty() const noexcept;
    size_type size() const noexcept;

    size_type capacity() const noexcept;
    size_type min_capacity() const noexcept;
    size_type max_capacity() const noexcept;

    allocator_type get_allocator() const noexcept;

    iterator remove_const(const_iterator i);

    // modifier
    template<typename U>
    iterator
    insert(U&& value)
        requires std::is_constructible_v<T, U>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<rng::input_range R>
    auto //std::ranges::subrange<iterator>
    insert(R&& range)
        requires (!std::is_constructible_v<T, R>)
              && std::is_constructible_v<T, rng::reference_t<R>>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<std::input_iterator I, std::sentinel_for<I> S>
    auto //std::ranges::subrange<iterator>
    insert(I first, S last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<typename... Us>
    auto //std::ranges::subrange<iterator>
    insert(Us&&... values)
        requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...)
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<typename U>
    iterator
    insert(const_iterator pos, U&& value)
        requires std::is_constructible_v<T, U>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<rng::input_range R>
    auto //std::ranges::subrange<iterator>
    insert(const_iterator pos, R&& range)
        requires (!std::is_constructible_v<T, R>)
              && std::is_constructible_v<T, rng::reference_t<R>>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<std::input_iterator I, std::sentinel_for<I> S>
    auto //std::ranges::subrange<iterator>
    insert(const_iterator pos, I first, S last)
        requires std::is_constructible_v<T, std::iter_reference_t<I>>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<typename... Us>
    auto //std::ranges::subrange<iterator>
    insert(const_iterator pos, Us&&... values)
        requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...)
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<typename... Args>
    iterator
    emplace(Args&&... args)
        requires std::is_constructible_v<T, Args...>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    template<typename... Args>
    iterator
    emplace(const_iterator pos, Args&&... args)
        requires std::is_constructible_v<T, Args...>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    iterator
    erase(const_iterator pos) noexcept
        requires std::is_nothrow_destructible_v<T>;

    iterator
    erase(const_iterator first, const_iterator last) noexcept
        requires std::is_nothrow_destructible_v<T>;

    void
    clear() noexcept
        requires std::is_nothrow_destructible_v<T>;

    template<typename... Args>
    void
    resize(size_type count, Args&&... args)
        requires std::is_constructible_v<T, Args...>
              && std::is_nothrow_move_constructible_v<T>
              && std::is_nothrow_destructible_v<T>;

    void
    reserve(size_type n);

    void
    shrink_to_fit()
        requires std::is_nothrow_move_constructible_v<T>;

    void
    swap(array&) noexcept
        requires std::is_nothrow_move_constructible_v<T>;

    // comparison
    template<std::ranges::input_range R>
    bool
    operator==(R const& other) const
        noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
        requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    auto
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
        requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    std::partial_ordering
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
        requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
              && requires (T v1, std::ranges::range_reference_t<R> v2) {
                  { v1 < v2 } -> std::convertible_to<bool>;
              };
};

template<typename CharT, typename Traits,
         typename T2, std::int64_t C2, typename A2>
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, array<T2, C2, A2> const& a)
    requires requires (std::basic_ostream<CharT, Traits>& os, T2 v) { os << v; };

template<typename T, std::int64_t C, typename A>
void
swap(array<T, C, A>& lhs, array<T, C, A>& rhs) noexcept;

namespace json {

template<typename T, std::int64_t C, typename A>
void tag_invoke(value_from_tag, value& v, stream9::array<T, C, A> const& a);

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_ARRAY_HPP

#include "array/array.ipp"
