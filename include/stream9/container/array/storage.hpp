#ifndef STREAM9_CONTAINER_ARRAY_STORAGE_HPP
#define STREAM9_CONTAINER_ARRAY_STORAGE_HPP

#include "chunk.hpp"
#include "size_type.hpp"
#include "storage_base.hpp"

#include <cstddef>
#include <cstdint>
#include <utility>

#include <stream9/nothrow_swap.hpp>

namespace stream9::array_ {

template<typename T, std::int64_t C, typename A>
struct storage : storage_base<T, A>
{
    using pointer = T*;
    using const_pointer = T const*;

    pointer m_first = reinterpret_cast<pointer>(m_buf);
    pointer m_last = reinterpret_cast<pointer>(m_buf);
    pointer m_buf_last = reinterpret_cast<pointer>(m_buf + sizeof(T)*C);
    alignas(T) std::byte m_buf[sizeof(T) * C];

    storage() = default;

    storage(A const& a)
        : A { a }
    {}

    storage(storage const& other)
        : storage_base<T, A> { other }
    {
        if (other.is_using_internal_buffer()) {
            m_last = this->copy_uninitialized(
                                      other.m_first, other.m_last, m_first);
        }
        else {
            auto buf = this->allocate(other.m_last - other.m_first);

            m_last = this->copy_uninitialized(
                                  other.m_first, other.m_last, buf.m_first);
            swap_buffer(buf);
        }
    }

    storage& operator=(storage const& other)
    {
        storage tmp { other };
        do_swap(tmp);
        return *this;
    }

    storage(storage&& other) noexcept
        : storage_base<T, A> { std::move(other) }
    {
        if (other.is_using_internal_buffer()) {
            m_last = this->move_uninitialized(
                                      other.m_first, other.m_last, m_first);
        }
        else {
            m_first = other.m_first;
            m_last = other.m_last;
            m_buf_last = other.m_buf_last;
        }
        other.m_first = other.ibuf_first();
        other.m_last = other.m_first;
        other.m_buf_last = other.ibuf_last();
    }

    storage&
    operator=(storage&& other) noexcept
    {
        for (auto i = m_first; i != m_last; ++i) {
            this->destroy_at(i);
        }

        if (other.is_using_internal_buffer()) {
            if (!is_using_internal_buffer()) {
                this->deallocate(m_first, m_buf_last - m_first);
                m_first = ibuf_first();
                m_last = m_first;
                m_buf_last = ibuf_last();
            }
            m_last = this->move_uninitialized(
                                      other.m_first, other.m_last, m_first);
        }
        else {
            if (!is_using_internal_buffer()) {
                this->deallocate(m_first, m_buf_last - m_first);
            }
            m_first = other.m_first;
            m_last = other.m_last;
            m_buf_last = other.m_buf_last;
        }
        other.m_first = other.ibuf_first();
        other.m_last = other.m_first;
        other.m_buf_last = other.ibuf_last();

        return *this;
    }

    ~storage() noexcept
    {
        for (auto i = m_first; i != m_last; ++i) {
            this->destroy_at(i);
        }
        if (m_first != ibuf_first()) {
            this->deallocate(m_first, m_buf_last - m_first);
        }
    }

    pointer const& first() const noexcept { return m_first; }
    pointer const& last() const noexcept { return m_last; }
    pointer const& buf_last() const noexcept { return m_buf_last; }

    pointer& first() noexcept { return m_first; }
    pointer& last() noexcept { return m_last; }
    pointer& buf_last() noexcept { return m_buf_last; }

    pointer ibuf_first() { return reinterpret_cast<pointer>(m_buf); }
    pointer ibuf_last() { return ibuf_first() + C; }

    bool is_using_internal_buffer() const noexcept
    {
        return m_first == reinterpret_cast<const_pointer>(m_buf);
    }

    void
    do_swap(storage& other) noexcept
    {
        using stream9::nothrow_swap;

        if (is_using_internal_buffer()) {
            if (other.is_using_internal_buffer()) {
                auto i1 = m_first;
                auto i2 = other.m_first;
                for (; i1 != m_last && i2 != other.m_last; ++i1, ++i2) {
                    nothrow_swap(*i1, *i2);
                }

                if (i1 != m_last) { // i2 == other.m_last
                    other.m_last =
                        other.move_uninitialized(i1, m_last, i2);
                    m_last = i1;
                }
                else if (i2 != other.m_last) { // i1 == m_last
                    m_last =
                        this->move_uninitialized(i2, other.m_last, i1);
                    other.m_last = i2;
                }
            }
            else {
                auto other_buf_first = other.m_first;
                auto other_buf_last = other.m_buf_last;
                auto other_last = other.m_last;

                other.m_first = other.ibuf_first();
                other.m_last = other.move_uninitialized(
                                            m_first, m_last, other.m_first);
                other.m_buf_last = m_first + C;

                m_first = other_buf_first;
                m_buf_last = other_buf_last;
                m_last = other_last;
            }
        }
        else {
            if (other.is_using_internal_buffer()) {
                auto old_first = m_first;
                auto old_last = m_last;
                auto old_buf_last = m_buf_last;

                m_first = ibuf_first();
                m_last = this->move_uninitialized(
                                      other.m_first, other.m_last, m_first);
                m_buf_last = m_first + C;

                other.m_first = old_first;
                other.m_last = old_last;
                other.m_buf_last = old_buf_last;
            }
            else {
                nothrow_swap(m_first, other.m_first);
                nothrow_swap(m_last, other.m_last);
                nothrow_swap(m_buf_last, other.m_buf_last);
            }
        }
    }

    void
    swap_buffer(chunk<T, A>& buf) noexcept
    {
        using stream9::nothrow_swap;

        if (is_using_internal_buffer()) {
            m_first = buf.m_first;
            m_buf_last = m_first + buf.m_length;
            buf.m_first = nullptr;
            buf.m_length = 0;
        }
        else {
            size_type len = m_buf_last - m_first;

            nothrow_swap(m_first, buf.m_first);
            nothrow_swap(len, buf.m_length);

            m_buf_last = m_first + len;
        }
    }

    void
    do_shrink()
    {
        if (is_using_internal_buffer()) return;

        auto len = m_last - m_first;
        if (len <= C) {
            auto l = this->move_uninitialized(
                                             m_first, m_last, ibuf_first());

            this->deallocate(m_first, m_buf_last - m_first);

            m_first = ibuf_first();
            m_last = l;
            m_buf_last = ibuf_last();
        }
        else {
            auto buf = this->allocate(len);

            m_last = this->move_uninitialized(
                                              m_first, m_last, buf.m_first);
            swap_buffer(buf);
        }
    }
};

template<typename T, typename A>
struct storage<T, 0, A> : storage_base<T, A>
{
    using pointer = T*;
    using const_pointer = T const*;

    pointer m_first = nullptr;
    pointer m_last = nullptr;
    pointer m_buf_last = nullptr;

    storage() = default;

    storage(A const& a)
        : A { a }
    {}

    storage(storage const& other)
        : storage_base<T, A> { other }
    {
        auto len = other.m_last - other.m_first;
        auto buf = this->allocate(len);

        m_last = this->copy_uninitialized(
                                  other.m_first, other.m_last, buf.m_first);

        swap_buffer(buf);
    }

    storage&
    operator=(storage const& other)
    {
        storage tmp { other };
        do_swap(tmp);
        return *this;
    }

    storage(storage&& other) noexcept
        : storage_base<T, A> { std::move(other) }
    {
        m_first = other.m_first;
        m_last = other.m_last;
        m_buf_last = other.m_buf_last;

        other.m_first = nullptr;
        other.m_last = nullptr;
        other.m_buf_last = nullptr;
    }

    storage&
    operator=(storage&& other) noexcept
    {
        this->~storage();

        m_first = other.m_first;
        m_last = other.m_last;
        m_buf_last = other.m_buf_last;

        other.m_first = nullptr;
        other.m_last = nullptr;
        other.m_buf_last = nullptr;

        return *this;
    }

    ~storage() noexcept
    {
        for (auto i = m_first; i != m_last; ++i) {
            this->destroy_at(i);
        }
        this->deallocate(m_first, m_buf_last - m_first);
    }

    pointer const& first() const noexcept { return m_first; }
    pointer const& last() const noexcept { return m_last; }
    pointer const& buf_last() const noexcept { return m_buf_last; }

    pointer& first() noexcept { return m_first; }
    pointer& last() noexcept { return m_last; }
    pointer& buf_last() noexcept { return m_buf_last; }

    void do_swap(storage& other) noexcept
    {
        using stream9::nothrow_swap;

        nothrow_swap(m_first, other.m_first);
        nothrow_swap(m_last, other.m_last);
        nothrow_swap(m_buf_last, other.m_buf_last);
    }

    void swap_buffer(chunk<T, A>& buf) noexcept
    {
        using stream9::nothrow_swap;

        size_type len = this->buf_last() - this->first();

        nothrow_swap(this->first(), buf.m_first);
        nothrow_swap(len, buf.m_length);

        this->buf_last() = this->first() + len;
    }

    void do_shrink()
    {
        auto len = m_last - m_first;

        auto buf = this->allocate(len);

        m_last = this->move_uninitialized(m_first, m_last, buf.m_first);
        swap_buffer(buf);
    }
};

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_STORAGE_HPP
