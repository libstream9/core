#ifndef STREAM9_CONTAINER_ARRAY_SIZE_TYPE_HPP
#define STREAM9_CONTAINER_ARRAY_SIZE_TYPE_HPP

#include <stream9/safe_integer.hpp>

namespace stream9::array_ {

using size_type = safe_integer<std::int64_t, 0>;

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_SIZE_TYPE_HPP
