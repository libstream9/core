#ifndef STREAM9_CONTAINER_ARRAY_CHUNK_HPP
#define STREAM9_CONTAINER_ARRAY_CHUNK_HPP

#include "size_type.hpp"

#include <memory>

namespace stream9::array_ {

template<typename T, typename A>
struct chunk
{
    using pointer = T*;

    A& m_allocator;
    pointer m_first;
    size_type m_length;

    chunk(A& a, size_type n)
        : m_allocator { a }
        , m_first { std::allocator_traits<A>::allocate(a, n) }
        , m_length { n }
    {}

    ~chunk() noexcept
    {
        if (m_first) {
            std::allocator_traits<A>::deallocate(m_allocator, m_first, m_length);
        }
    }

    pointer first() const noexcept { return m_first; }
    size_type length() const noexcept { return m_length; }
};

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_CHUNK_HPP
