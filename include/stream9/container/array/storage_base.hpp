#ifndef STREAM9_CONTAINER_ARRAY_STORAGE_BASE_HPP
#define STREAM9_CONTAINER_ARRAY_STORAGE_BASE_HPP

#include <cstddef>
#include <memory>
#include <type_traits>

namespace stream9::array_ {

template<typename T, typename A>
struct storage_base : A
{
    using pointer = T*;

    storage_base() = default;

    storage_base(A const& a) noexcept
        : A { a }
    {}

    storage_base(storage_base const&) = default;
    storage_base& operator=(storage_base const&) = default;

    storage_base(storage_base&&) = default;
    storage_base& operator=(storage_base&&) = default;

    ~storage_base() = default;

    template<typename... Args>
    void
    construct_at(pointer p, Args&&... args)
    {
        std::allocator_traits<A>::construct(
                                 *this, p, std::forward<Args>(args)...);
    }

    void
    destroy_at(pointer p) noexcept
    {
        std::allocator_traits<A>::destroy(*this, p);
    }

    void
    deallocate(pointer p, size_type n) noexcept
    {
        std::allocator_traits<A>::deallocate(
                                     *this, p, static_cast<std::size_t>(n));
    }

    chunk<T, A>
    allocate(size_type n)
    {
        return { *this, n };
    }

    template<std::contiguous_iterator I>
    pointer
    copy_uninitialized(I first, I last, pointer dest)
        requires std::same_as<std::iter_value_t<I>, T>
              && std::is_trivially_copyable_v<T>
    {
        auto f = std::to_address(first);
        auto l = std::to_address(last);
        if (f != nullptr) {
            auto n = static_cast<std::size_t>(l - f);
            std::memcpy(dest, f, n * sizeof(T));
            dest += n;
        }

        return dest;
    }

    template<typename I, typename S>
    pointer
    copy_uninitialized(I first, S last, pointer dest)
    {
        auto tail = dest;
        try {
            for (auto i = first; i != last; ++i, ++tail) {
                construct_at(tail, *i);
            }
        }
        catch (...) {
            for (auto i = dest; i != tail; ++i) {
                destroy_at(i);
            }
            throw;
        }

        return tail;
    }

    // precondition: no overlap between [first, last) and [dest, dest + (last - first))
    pointer
    move_uninitialized(pointer first, pointer last, pointer dest) noexcept
    {
        if constexpr (std::is_trivially_copyable_v<T>) {
            if (first != nullptr) {
                auto n = static_cast<std::size_t>(last - first);
                std::memcpy(dest, first, n * sizeof(T));
                dest += n;
            }
        }
        else {
            for (; first != last; ++first, ++dest) {
                construct_at(dest, std::move(*first));
            }
        }

        return dest;
    }
};

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_STORAGE_BASE_HPP
