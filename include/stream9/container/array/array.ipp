#ifndef STREAM9_CONTAINER_ARRAY_ARRAY_IPP
#define STREAM9_CONTAINER_ARRAY_ARRAY_IPP

#include <algorithm>
#include <concepts>
#include <cstdint>
#include <limits>
#include <ostream>
#include <ranges>
#include <type_traits>
#include <utility>

#include <stream9/errors.hpp>
#include <stream9/json.hpp>
#include <stream9/move_iterator.hpp>
#include <stream9/ranges/concepts.hpp>

namespace stream9 {

template<typename T, std::int64_t C, typename A>
array<T, C, A>::
array(allocator_type const& alloc)
    : base_t { alloc }
{}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::
array(std::initializer_list<T> il)
{
    try {
        for (auto& v: il) {
            insert(end(), std::move(v));
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename... Us>
array<T, C, A>::
array(Us&&... values)
    requires (is_insertable_v<Us> && ...)
{
    try {
        insert(end(), std::forward<Us>(values)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::iterator array<T, C, A>::
begin() noexcept
{
    return this->first();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::iterator array<T, C, A>::
end() noexcept
{
    return this->last();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_iterator array<T, C, A>::
begin() const noexcept
{
    return this->first();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_iterator array<T, C, A>::
end() const noexcept
{
    return this->last();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::reference array<T, C, A>::
at(size_type pos) noexcept
{
    assert(pos < this->length());
    return *(this->first() + pos);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_reference array<T, C, A>::
at(size_type pos) const noexcept
{
    assert(pos < this->length());
    return *(this->first() + pos);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::reference array<T, C, A>::
operator[](size_type pos) noexcept
{
    assert(pos < this->length());
    return at(pos);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_reference array<T, C, A>::
operator[](size_type pos) const noexcept
{
    assert(pos < this->length());
    return at(pos);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::reference array<T, C, A>::
front() noexcept
{
    return *begin();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_reference array<T, C, A>::
front() const noexcept
{
    return *begin();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::reference array<T, C, A>::
back() noexcept
{
    return *(end() - 1);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_reference array<T, C, A>::
back() const noexcept
{
    return *(end() - 1);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::pointer array<T, C, A>::
data() noexcept
{
    return this->begin();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::const_pointer array<T, C, A>::
data() const noexcept
{
    return this->begin();
}

template<typename T, std::int64_t C, typename A>
bool array<T, C, A>::
empty() const noexcept
{
    return this->begin() == this->end();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::size_type array<T, C, A>::
size() const noexcept
{
    return this->length();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::size_type array<T, C, A>::
capacity() const noexcept
{
    return this->buf_length();
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::size_type array<T, C, A>::
min_capacity() const noexcept
{
    return C;
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::size_type array<T, C, A>::
max_capacity() const noexcept
{
    return std::numeric_limits<size_type>::max() / sizeof(T);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::allocator_type array<T, C, A>::
get_allocator() const noexcept
{
    return *this;
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::iterator array<T, C, A>::
remove_const(const_iterator i)
{
    auto i2 = const_cast<pointer>(i);
    assert(this->is_valid(i2));
    return i2;
}

template<typename T, std::int64_t C, typename A>
template<typename U>
array<T, C, A>::iterator array<T, C, A>::
insert(U&& v)
    requires std::is_constructible_v<T, U>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        return this->do_emplace(this->last(), std::forward<U>(v));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<rng::input_range R>
auto array<T, C, A>::
insert(R&& v)
    requires (!std::is_constructible_v<T, R>)
          && std::is_constructible_v<T, rng::reference_t<R>>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        return insert(end(), std::forward<R>(v));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<std::input_iterator I, std::sentinel_for<I> S>
auto array<T, C, A>::
insert(I first, S last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        return insert(end(), std::move(first), std::move(last));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename... Us>
auto array<T, C, A>::
insert(Us&&... values)
    requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...)
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        return insert(end(), std::forward<Us>(values)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename U>
array<T, C, A>::iterator array<T, C, A>::
insert(const_iterator pos, U&& v)
    requires std::is_constructible_v<T, U>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        auto i = const_cast<pointer>(pos);
        assert(this->is_valid(i));

        return this->do_emplace(i, std::forward<U>(v));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<rng::input_range R>
auto array<T, C, A>::
insert(const_iterator pos, R&& v)
    requires (!std::is_constructible_v<T, R>)
          && std::is_constructible_v<T, rng::reference_t<R>>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        auto i = const_cast<pointer>(pos);
        assert(this->is_valid(i));
        auto offset = i - this->first();

        auto n = this->do_insert(i, std::forward<R>(v));

        auto f = this->first() + offset;
        auto l = f + n;

        return std::ranges::subrange<iterator> { f, l };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<std::input_iterator I, std::sentinel_for<I> S>
auto array<T, C, A>::
insert(const_iterator pos, I first, S last)
    requires std::is_constructible_v<T, std::iter_reference_t<I>>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        auto i = const_cast<pointer>(pos);
        assert(this->is_valid(i));

        auto [f, n] = this->do_insert_range(i, first, last);
        return std::ranges::subrange<iterator> { f, f + n };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename... Us>
auto array<T, C, A>::
insert(const_iterator pos, Us&&... values)
    requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...)
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        auto i = const_cast<pointer>(pos);
        assert(this->is_valid(i));
        auto offset = i - this->first();
        size_type n;

        auto len = (array_::length(std::forward<Us>(values)) + ...);
        if constexpr ((is_nothrow_insertable_v<Us> && ...)) {
            this->do_reserve(this->length() + len);
            i = this->first() + offset;

            n = this->do_insert(i, std::forward<Us>(values)...);
        }
        else {
            array tmp;
            tmp.reserve(len);

            tmp.do_insert(tmp.end(), std::forward<Us>(values)...);

            n = this->do_insert(i, std::move(tmp));
        }

        auto f = this->first() + offset;
        auto l = f + n;

        return std::ranges::subrange<iterator> { f, l };
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename... Args>
array<T, C, A>::iterator array<T, C, A>::
emplace(Args&&... args)
    requires std::is_constructible_v<T, Args...>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        return emplace(end(), std::forward<Args>(args)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
template<typename... Args>
array<T, C, A>::iterator array<T, C, A>::
emplace(const_iterator pos, Args&&... args)
    requires std::is_constructible_v<T, Args...>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        auto i = const_cast<pointer>(pos);
        assert(this->is_valid(i));

        return this->do_emplace(i, std::forward<Args>(args)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::iterator array<T, C, A>::
erase(const_iterator pos) noexcept
    requires std::is_nothrow_destructible_v<T>
{
    pointer i = const_cast<pointer>(pos);
    assert(this->is_valid(i));

    return this->do_erase(i);
}

template<typename T, std::int64_t C, typename A>
array<T, C, A>::iterator array<T, C, A>::
erase(const_iterator first, const_iterator last) noexcept
    requires std::is_nothrow_destructible_v<T>
{
    pointer f = const_cast<pointer>(first);
    assert(this->is_valid(f));
    pointer l = const_cast<pointer>(last);
    assert(this->is_valid(l));

    if (f == l) return l;

    return this->do_erase_range(f, l);
}

template<typename T, std::int64_t C, typename A>
void array<T, C, A>::
clear() noexcept
    requires std::is_nothrow_destructible_v<T>
{
    this->do_clear();
}

template<typename T, std::int64_t C, typename A>
template<typename... Args>
void array<T, C, A>::
resize(size_type count, Args&&... args)
    requires std::is_constructible_v<T, Args...>
          && std::is_nothrow_move_constructible_v<T>
          && std::is_nothrow_destructible_v<T>
{
    try {
        this->do_resize(count, std::forward<Args>(args)...);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
void array<T, C, A>::
reserve(size_type n)
{
    try {
        this->do_reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
void array<T, C, A>::
shrink_to_fit()
    requires std::is_nothrow_move_constructible_v<T>
{
    try {
        this->do_shrink();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
void array<T, C, A>::
swap(array& other) noexcept
    requires std::is_nothrow_move_constructible_v<T>
{
    this->do_swap(other);
}

template<typename T, std::int64_t C, typename A>
template<std::ranges::input_range R>
bool array<T, C, A>::
operator==(R const& other) const
    noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
    requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return std::equal(begin(), end(), other.begin(), other.end());
}

template<typename T, std::int64_t C, typename A>
template<std::ranges::input_range R>
auto array<T, C, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
    requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        other.begin(), other.end()
    );
}

template<typename T, std::int64_t C, typename A>
template<std::ranges::input_range R>
std::partial_ordering array<T, C, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
    requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
          && requires (T v1, std::ranges::range_reference_t<R> v2) {
              { v1 < v2 } -> std::convertible_to<bool>;
          }
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        other.begin(), other.end(),
        array_::partial_compare()
    );
}

template<typename CharT, typename Traits,
         typename T2, std::int64_t C2, typename A2>
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, array<T2, C2, A2> const& a)
    requires requires (std::basic_ostream<CharT, Traits>& os, T2 v) { os << v; }
{
    try {
        bool first = true;

        os << '[';
        for (auto& v: a) {
            if (first) {
                first = false;
            }
            else {
                os << ", ";
            }

            os << v;
        }
        os << ']';

        return os;
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, std::int64_t C, typename A>
void
swap(array<T, C, A>& lhs, array<T, C, A>& rhs) noexcept
{
    lhs.swap(rhs);
}

namespace json {

template<typename T, std::int64_t C, typename A>
void tag_invoke(value_from_tag, value& v, stream9::array<T, C, A> const& a)
{
    try {
        auto& arr = v.emplace_array();

        for (auto const& e: a) {
            arr.push_back(e);
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_ARRAY_ARRAY_IPP
