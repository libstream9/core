#ifndef STREAM9_CONTAINER_ARRAY_PARTIAL_COMPARE_HPP
#define STREAM9_CONTAINER_ARRAY_PARTIAL_COMPARE_HPP

#include <compare>

namespace stream9::array_ {

struct partial_compare
{
    using is_transparent = std::true_type;

    template<typename T, typename U>
    std::partial_ordering
    operator()(T const& t, U const& u) const
    {
        if (t < u) {
            return std::partial_ordering::less;
        }
        else if (u < t) {
            return std::partial_ordering::greater;
        }
        else if (u == t) {
            return std::partial_ordering::equivalent;
        }
        else {
            return std::partial_ordering::unordered;
        }
    }
};

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_PARTIAL_COMPARE_HPP
