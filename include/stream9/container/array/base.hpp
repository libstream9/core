#ifndef STREAM9_CONTAINER_ARRAY_BASE_HPP
#define STREAM9_CONTAINER_ARRAY_BASE_HPP

#include "chunk.hpp"
#include "size_type.hpp"
#include "storage.hpp"

#include <algorithm>
#include <cstdint>
#include <iterator>
#include <utility>

#include <stream9/move_iterator.hpp>

#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/ranges/size.hpp>

namespace stream9::array_ {

template<typename T, std::int64_t C, typename A>
struct base : storage<T, C, A>
{
    using pointer = T*;
    using const_pointer = T const*;

    base() = default;

    base(A const& a) noexcept
        : storage<T, C, A> { a }
    {}

    base(base const&) = default;
    base& operator=(base const&) = default;

    base(base&&) = default;
    base& operator=(base&&) = default;

    size_type
    length() const noexcept
    {
        return this->last() - this->first();
    }

    size_type
    buf_length() const noexcept
    {
        return this->buf_last() - this->first();
    }

    size_type
    next_length(size_type request) const noexcept
    {
        try {
            auto const current = buf_length();

            if (current == 0) return request;

            size_type next = current * 2;

            while (next < request) {
                next *= 2;
            }

            return next;
        }
        catch (...) {
            return buf_length();
        }
    }

    bool
    has_capacity(size_type n) const noexcept
    {
        return this->last() + n <= this->buf_last();
    }

    bool
    is_valid(pointer p) const noexcept
    {
        return (this->first() <= p && p <= this->last());
    }

    // precondition: no overlap between [first, last) and [dest, dest + (last - first))
    void
    shift_backward(pointer i, size_type n)
    {
        auto new_i = i + n;
        if (new_i < this->last()) {
            auto mid = this->last() - n;
            this->move_uninitialized(mid, this->last(), this->last());
            std::move_backward(i, mid, this->last());
        }
        else {
            this->move_uninitialized(i, this->last(), new_i);
        }
    }

    template<typename... Args>
    pointer
    do_emplace(pointer i, Args&&... args)
    {
        if (has_capacity(1)) {
            if (i != this->last()) {
                if constexpr (std::is_nothrow_constructible_v<T, Args...>) {
                    shift_backward(i, 1);
                    this->construct_at(i, std::forward<Args>(args)...);
                }
                else {
                    T tmp { std::forward<Args>(args)... };
                    shift_backward(i, 1);
                    this->construct_at(i, std::move(tmp));
                }
            }
            else {
                this->construct_at(i, std::forward<Args>(args)...);
            }

            ++this->last();

            return i;
        }
        else {
            auto buf = this->allocate(next_length(this->buf_length() + 1));

            auto m = buf.first() + (i - this->first());
            this->construct_at(m, std::forward<Args>(args)...);

            this->move_uninitialized(this->first(), i, buf.first());
            this->last() = this->move_uninitialized(i, this->last(), m + 1);
            this->swap_buffer(buf);

            return m;
        }
    }

    size_type do_insert(pointer) { return 0; }

    template<typename U, typename... Ts>
    size_type
    do_insert(pointer i, U&& v, Ts&&... rest)
        requires std::is_constructible_v<T, U>
    {
        auto j = do_emplace(i, std::forward<U>(v));
        size_type n = 1;

        n += do_insert(j + 1, std::forward<Ts>(rest)...);

        return n;
    }

    template<rng::input_range R, typename... Ts>
    size_type
    do_insert(pointer i, R&& r, Ts&&... rest)
        requires (!std::is_constructible_v<T, R>)
              && std::is_constructible_v<T, rng::reference_t<R>>
    {
        size_type n;

        if constexpr (std::is_lvalue_reference_v<R>) {
            auto [f, n1] = do_insert_range(i, rng::begin(r), rng::end(r));
            i = f + n1;
            n = n1;
        }
        else {
            auto [f, n1] = do_insert_range(i, move_iterator(rng::begin(r)), rng::end(r));
            i = f + n1;
            n = n1;
        }

        n += do_insert(i, std::forward<Ts>(rest)...);

        return n;
    }

    template<typename I, std::sized_sentinel_for<I> S>
    std::pair<pointer, size_type>
    do_insert_range(pointer i, I first, S last)
    {
        auto len = last - first;
        if (len == 0) return { i, 0 };

        pointer n_first;

        if (this->has_capacity(len) && i != nullptr) {
            if (i != this->last()) {
                using U = std::iter_reference_t<I>;
                if constexpr (std::is_nothrow_constructible_v<T, U>) {
                    shift_backward(i, len); //nothrow
                    this->copy_uninitialized(first, last, i);
                    n_first = i;
                }
                else {
                    auto l = this->copy_uninitialized(first, last, this->last());
                    std::rotate(i, this->last(), l); //nothrow
                    n_first = i;
                }
            }
            else {
                this->copy_uninitialized(first, last, i);
                n_first = i;
            }

            this->last() += len;
        }
        else {
            auto buf =
                this->allocate(next_length(this->buf_length() + len));

            auto m1 = buf.first() + (i - this->first());
            auto m2 = this->copy_uninitialized(first, last, m1);

            // noexcept from here
            this->move_uninitialized(this->first(), i, buf.first());
            auto l = this->move_uninitialized(i, this->last(), m2);

            this->swap_buffer(buf);
            this->last() = l;

            n_first = m1;
        }

        return { n_first, len };
    }

    template<typename I, typename S>
    std::pair<pointer, size_type>
    do_insert_range(pointer i, I first, S last)
    {
        auto off = i - this->first();
        size_type n = 0;

        base tmp;
        for (; first != last; ++first, ++n) {
            tmp.do_emplace(tmp.last(), *first);
        }
        // noexcept from here
        do_insert_range(i,
            move_iterator(tmp.first()),
            move_iterator(tmp.last()) );

        return { this->first() + off, n };
    }

    pointer
    do_erase(pointer i) noexcept
    {
        if (i == nullptr) return i;

        if (i + 1 == this->last()) {
            this->destroy_at(i);
            this->last() = i;
        }
        else {
            this->last() = std::move(i + 1, this->last(), i);
        }

        return i;
    }

    // precondition: first() <= f < l <= last()
    pointer
    do_erase_range(pointer f, pointer l) noexcept
    {
        this->last() = std::move(l, this->last(), f);

        for (auto i = this->last(); i < l; ++i) {
            this->destroy_at(i);
        }

        return f;
    }

    void
    do_clear() noexcept
    {
        for (auto i = this->first(); i != this->last(); ++i) {
            this->destroy_at(i);
        }

        this->last() = this->first();
    }

    template<typename... Args>
    void
    do_resize(size_type n, Args&&... args)
    {
        if (n <= this->length()) {
            auto i = this->first() + n;
            do_erase_range(i, this->last());
        }
        else {
            do_reserve(n); // This will not be rollbacked on exception that might be thrown later.

            auto new_last = this->first() + n;
            for (auto p = this->last(); p != new_last; ++p) {
                this->construct_at(p, std::forward<Args>(args)...);
            }

            this->last() = new_last;
        }
    }

    void
    do_reserve(size_type n)
    {
        if (this->buf_length() >= n) return;

        auto buf = this->allocate(n);

        auto l = this->move_uninitialized(
                                  this->first(), this->last(), buf.first());

        this->swap_buffer(buf);
        this->last() = l;
    }

};

template<typename T>
constexpr size_t
length(T&& v)
{
    if constexpr (std::ranges::sized_range<T>) {
        return rng::size(v);
    }
    else {
        return 1;
    }
}

} // namespace stream9::array_

#endif // STREAM9_CONTAINER_ARRAY_BASE_HPP
