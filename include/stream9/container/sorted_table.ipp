#ifndef STREAM9_CONTAINER_SORTED_TABLE_IPP
#define STREAM9_CONTAINER_SORTED_TABLE_IPP

#include <stream9/errors.hpp>
#include <stream9/projection.hpp>
#include <stream9/ranges/binary_find.hpp>
#include <stream9/ranges/lower_bound.hpp>
#include <stream9/ranges/upper_bound.hpp>

#include <algorithm>

namespace stream9 {

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::
sorted_table() noexcept = default;

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::
sorted_table(std::initializer_list<entry> il)
    try : m_entries { std::move(il) }
{
    typename Traits::order o {};
    project<&entry::key> p {};

    std::ranges::sort(m_entries, o, p);
}
catch (...) {
    rethrow_error();
}

template<typename Key, typename T, typename Traits>
void sorted_table<Key, T, Traits>::
swap(sorted_table& o) noexcept
{
    m_entries.swap(o.m_entries);
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::const_iterator sorted_table<Key, T, Traits>::
begin() const noexcept
{
    return m_entries.begin();
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
begin() noexcept
{
    return m_entries.begin();
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::const_iterator sorted_table<Key, T, Traits>::
end() const noexcept
{
    return m_entries.end();
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
end() noexcept
{
    return m_entries.end();
}

template<typename Key, typename T, typename Traits>
template<typename K>
T& sorted_table<Key, T, Traits>::
operator[](K const& k)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_default_constructible_v<T>
{
    try {
        auto it = lower_bound(k);
        if (it == end() || it->key != k) {
            auto it2 = m_entries.insert(it, entry { k, T() });
            return it2->value;
        }
        else {
            return it->value;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
remove_const(const_iterator it) noexcept
{
    return m_entries.remove_const(it);
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::size_type sorted_table<Key, T, Traits>::
size() const noexcept
{
    return m_entries.size();
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::const_iterator sorted_table<Key, T, Traits>::
find(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::binary_find(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
find(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::binary_find(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::const_iterator sorted_table<Key, T, Traits>::
lower_bound(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::lower_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
lower_bound(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::lower_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::const_iterator sorted_table<Key, T, Traits>::
upper_bound(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::upper_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
upper_bound(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::upper_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
range<typename sorted_table<Key, T, Traits>::iterator> sorted_table<Key, T, Traits>::
equal_range(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    auto f = rng::lower_bound(m_entries, k, o, p);

    auto l = f;
    for (; l != m_entries.end(); ++l) {
        if (l->key != k) break;
    }

    return { f, l };
}

template<typename Key, typename T, typename Traits>
template<typename K>
range<typename sorted_table<Key, T, Traits>::const_iterator> sorted_table<Key, T, Traits>::
equal_range(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    auto f = rng::lower_bound(m_entries, k, o, p);

    auto l = f;
    for (; l != m_entries.end(); ++l) {
        if (l->key != k) break;
    }

    return { f, l };
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
insert(entry const& e)
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it = rng::upper_bound(m_entries, e.key, o, p);
        return m_entries.insert(it, e);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
insert(entry&& e)
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it = rng::upper_bound(m_entries, e.key, o, p);
        return m_entries.insert(it, std::move(e));
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
template<typename K, typename V>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
emplace(K&& k, V&& v)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_constructible_v<T, V>
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it = rng::upper_bound(m_entries, k, o, p);
        return m_entries.insert(it, entry { std::forward<K>(k), std::forward<V>(v) });
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
template<typename K, typename V>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
emplace_hint(const_iterator hint, K&& k, V&& v)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_constructible_v<T, V>
{
    try {
        typename Traits::order cmp {};
        project<&entry::key> p {};

        const_iterator pos {};

        // N1780
        if (hint == m_entries.end() || !cmp(hint->key, k)) {
            if (hint == m_entries.begin() || !cmp(k, (hint-1)->key)) {
                pos = hint;
            }
            else {
                pos = rng::upper_bound(m_entries.begin(), hint, k, cmp, p);
            }
        }
        else {
            pos = rng::lower_bound(hint, m_entries.end(), k, cmp, p);
        }

        return m_entries.insert(pos, entry { std::forward<K>(k), std::forward<V>(v) });
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
erase(const_iterator it) noexcept
{
    return m_entries.erase(it);
}

template<typename Key, typename T, typename Traits>
sorted_table<Key, T, Traits>::iterator sorted_table<Key, T, Traits>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_entries.erase(first, last);
}

template<typename Key, typename T, typename Traits>
template<typename K>
sorted_table<Key, T, Traits>::size_type sorted_table<Key, T, Traits>::
erase(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    auto it = find(k);
    if (it == end()) {
        return 0;
    }
    else {
        erase(it);
        return 1;
    }
}

template<typename Key, typename T, typename Traits>
void sorted_table<Key, T, Traits>::
clear() noexcept
{
    return m_entries.clear();
}

template<typename Key, typename T, typename Traits>
void sorted_table<Key, T, Traits>::
reserve(size_type n)
{
    try {
        return m_entries.reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
void sorted_table<Key, T, Traits>::
shrink_to_fit()
{
    try {
        return m_entries.shrink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
bool sorted_table<Key, T, Traits>::
operator==(sorted_table const& o) const
    noexcept(noexcept(std::declval<Key>() == std::declval<Key>())
          && noexcept(std::declval<T>() == std::declval<T>()) )
{
    return std::equal(begin(), end(), o.begin(), o.end());
}

template<typename Key, typename T, typename Traits>
auto sorted_table<Key, T, Traits>::
operator<=>(sorted_table const& o) const
    noexcept(noexcept(std::declval<Key>() <=> std::declval<Key>())
          && noexcept(std::declval<T>() <=> std::declval<T>()) )
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        o.begin(), o.end()
    );
}

template<typename K, typename V, typename T>
std::ostream&
operator<<(std::ostream& os,
           sorted_table<K, V, T> const& t)
{
    return os << json::value_from(t);
}

template<size_t I, typename K, typename V, typename T>
auto
get(sorted_table<K, V, T>& t) noexcept
    requires (I == 0 || I == 1)
{
    if constexpr (I == 0) {
        return t.begin();
    }
    else if constexpr (I == 1) {
        return t.end();
    }
}

template<size_t I, typename K, typename V, typename T>
auto
get(sorted_table<K, V, T> const& t) noexcept
    requires (I == 0 || I == 1)
{
    if constexpr (I == 0) {
        return t.begin();
    }
    else if constexpr (I == 1) {
        return t.end();
    }
}

namespace json {

template<typename K, typename V, typename T>
void
tag_invoke(value_from_tag,
           value& jv,
           sorted_table<K, V, T> const& t)
{
    auto& arr = jv.emplace_array();
    for (auto& [k, v]: t) {
        json::array e;
        e.push_back(k);
        e.push_back(v);
        arr.push_back(std::move(e));
    }
}

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_SORTED_TABLE_IPP
