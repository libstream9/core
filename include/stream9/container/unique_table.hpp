#ifndef STREAM9_CONTAINER_UNIQUE_TABLE_HPP
#define STREAM9_CONTAINER_UNIQUE_TABLE_HPP

#include "key_value.hpp"

#include <stream9/array.hpp>
#include <stream9/json.hpp>
#include <stream9/less.hpp>
#include <stream9/optional.hpp>

#include <compare>
#include <cstdint>
#include <initializer_list>
#include <memory>
#include <ostream>
#include <type_traits>

namespace stream9 {

struct unique_table_traits
{
    template<typename T>
    using allocator = std::allocator<T>;

    using order = less;
    using size_type = std::int64_t;

    static constexpr auto min_capacity = std::numeric_limits<size_type>::min;
    static constexpr auto max_capacity = std::numeric_limits<size_type>::max;
};

template<typename Key, typename T,
         typename Traits = unique_table_traits>
class unique_table
{
public:
    using entry = key_value<Key, T>;

    using array_t = array<entry>;

    using size_type = Traits::size_type;

    using iterator = array_t::iterator;
    using const_iterator = array_t::const_iterator;

    struct insert_result_t
    {
        iterator position;
        bool inserted;

        T& operator*() const noexcept;
        T* operator->() const noexcept;

        operator bool () const noexcept;
        operator iterator () const noexcept;
    };

    template<typename K>
    static constexpr bool is_comparable_with_key_v =
        requires (Key const& x, K const& y) {
            { typename Traits::order()(x, y) } -> std::convertible_to<bool>;
        };

    template<typename K>
    static constexpr bool is_nothrow_comparable_with_key_v =
        noexcept(typename Traits::order()(std::declval<Key>(), std::declval<K>()));

    static_assert(std::is_nothrow_destructible_v<Key>);
    static_assert(std::is_nothrow_destructible_v<T>);
    static_assert(std::is_nothrow_move_constructible_v<Key>);
    static_assert(std::is_nothrow_move_constructible_v<T>);

public:
    // essentials
    unique_table() noexcept;

    unique_table(std::initializer_list<entry>);

    unique_table(unique_table const&) = default;
    unique_table& operator=(unique_table const&) = default;

    unique_table(unique_table&&) noexcept = default;
    unique_table& operator=(unique_table&&) noexcept = default;

    ~unique_table() noexcept = default;

    void swap(unique_table&) noexcept;

    // accessor
    const_iterator begin() const noexcept;
    iterator begin() noexcept;

    const_iterator end() const noexcept;
    iterator end() noexcept;

    template<typename K>
    T& operator[](K const&)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_default_constructible_v<T>;

    iterator remove_const(const_iterator) noexcept;

    // query
    size_type size() const noexcept;

    template<typename K>
    const_iterator find(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator find(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    const_iterator lower_bound(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator lower_bound(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    const_iterator upper_bound(K const&) const
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    template<typename K>
    iterator upper_bound(K const&)
        noexcept(is_nothrow_comparable_with_key_v<K>)
        requires is_comparable_with_key_v<K>;

    // modifier
    insert_result_t insert(entry const&);

    insert_result_t insert(entry&&);

    template<typename K, typename V>
    insert_result_t
    emplace(K&& k, V&& v)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_constructible_v<T, V>;

    template<typename K, typename V>
    insert_result_t
    emplace_or_assign(K&&, V&&)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_constructible_v<T, V>;

    template<typename K, typename V>
    insert_result_t
    emplace_hint(const_iterator hint, K&& k, V&& v)
        requires is_comparable_with_key_v<K>
              && std::is_constructible_v<Key, K>
              && std::is_constructible_v<T, V>;

    iterator erase(const_iterator) noexcept;

    iterator erase(const_iterator first, const_iterator last) noexcept;

    template<typename K>
    size_type erase(K const& k) noexcept
        requires is_comparable_with_key_v<K>;

    void clear() noexcept;

    void reserve(size_type);

    void shrink_to_fit();

    bool operator==(unique_table const&) const
        noexcept(noexcept(std::declval<Key>() == std::declval<Key>())
              && noexcept(std::declval<T>() == std::declval<T>()) );

    auto operator<=>(unique_table const&) const
        noexcept(noexcept(std::declval<Key>() <=> std::declval<Key>())
              && noexcept(std::declval<T>() <=> std::declval<T>()) );

private:
    array_t m_entries;
};

template<typename K, typename V, typename T>
std::ostream& operator<<(std::ostream&, unique_table<K, V, T> const&);

namespace json {

template<typename K, typename V, typename T>
void
tag_invoke(value_from_tag, value&, unique_table<K, V, T> const&)
    requires requires (K const& k) { json::string(k); }
          && requires (V const& v) { json::value_from(v); };

} // namespace json

} // namespace stream9

namespace std {

template<typename K, typename V, typename T>
struct tuple_size<stream9::unique_table<K, V, T>>
{
    static constexpr auto value = 2;
};

template<typename K, typename V, typename T>
struct tuple_element<0, stream9::unique_table<K, V, T>>
{
    using type = stream9::ranges::iterator_t<stream9::unique_table<K, V, T>>;
};

template<typename K, typename V, typename T>
struct tuple_element<1, stream9::unique_table<K, V, T>>
{
    using type = stream9::ranges::sentinel_t<stream9::unique_table<K, V, T>>;
};

} // namespace std

#endif // STREAM9_CONTAINER_UNIQUE_TABLE_HPP

#include "unique_table.ipp"
