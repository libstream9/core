#ifndef STREAM9_CONTAINER_UNIQUE_TABLE_IPP
#define STREAM9_CONTAINER_UNIQUE_TABLE_IPP

#include <stream9/errors.hpp>
#include <stream9/projection.hpp>
#include <stream9/ranges/binary_find.hpp>
#include <stream9/ranges/lower_bound.hpp>
#include <stream9/ranges/upper_bound.hpp>

#include <algorithm>

namespace stream9 {

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::
unique_table() noexcept = default;

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::
unique_table(std::initializer_list<entry> il)
    try : m_entries { std::move(il) }
{
    typename Traits::order o {};
    project<&entry::key> p {};

    std::ranges::sort(m_entries, o, p);
}
catch (...) {
    rethrow_error();
}

template<typename Key, typename T, typename Traits>
void unique_table<Key, T, Traits>::
swap(unique_table& o) noexcept
{
    m_entries.swap(o.m_entries);
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::const_iterator unique_table<Key, T, Traits>::
begin() const noexcept
{
    return m_entries.begin();
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
begin() noexcept
{
    return m_entries.begin();
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::const_iterator unique_table<Key, T, Traits>::
end() const noexcept
{
    return m_entries.end();
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
end() noexcept
{
    return m_entries.end();
}

template<typename Key, typename T, typename Traits>
template<typename K>
T& unique_table<Key, T, Traits>::
operator[](K const& k)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_default_constructible_v<T>
{
    try {
        auto it = lower_bound(k);
        if (it == end() || it->key != k) {
            auto it2 = m_entries.insert(it, entry { k, T() });
            return it2->value;
        }
        else {
            return it->value;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
remove_const(const_iterator it) noexcept
{
    return m_entries.remove_const(it);
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::size_type unique_table<Key, T, Traits>::
size() const noexcept
{
    return m_entries.size();
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::const_iterator unique_table<Key, T, Traits>::
find(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::binary_find(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
find(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::binary_find(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::const_iterator unique_table<Key, T, Traits>::
lower_bound(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::lower_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
lower_bound(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::lower_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::const_iterator unique_table<Key, T, Traits>::
upper_bound(K const& k) const
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::upper_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
upper_bound(K const& k)
    noexcept(is_nothrow_comparable_with_key_v<K>)
    requires is_comparable_with_key_v<K>
{
    typename Traits::order o {};
    project<&entry::key> p {};

    return rng::upper_bound(m_entries, k, o, p);
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::insert_result_t unique_table<Key, T, Traits>::
insert(entry const& e)
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it1 = rng::lower_bound(m_entries, e.key, o, p);

        if (it1 != m_entries.end() && it1->key == e.key) {
            return { it1, false };
        }
        else {
            auto it2 = m_entries.insert(it1, e);
            return { it2, true };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::insert_result_t unique_table<Key, T, Traits>::
insert(entry&& e)
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it1 = rng::lower_bound(m_entries, e.key, o, p);

        if (it1 != m_entries.end() && it1->key == e.key) {
            return { it1, false };
        }
        else {
            auto it2 = m_entries.insert(it1, std::move(e));
            return { it2, true };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
template<typename K, typename V>
unique_table<Key, T, Traits>::insert_result_t unique_table<Key, T, Traits>::
emplace(K&& k, V&& v)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_constructible_v<T, V>
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it1 = rng::lower_bound(m_entries, k, o, p);

        if (it1 != m_entries.end() && it1->key == k) {
            return { it1, false };
        }
        else {
            auto it2 = m_entries.insert(it1, entry { k, v });
            return { it2, true };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
template<typename K, typename V>
unique_table<Key, T, Traits>::insert_result_t unique_table<Key, T, Traits>::
emplace_or_assign(K&& k, V&& v)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_constructible_v<T, V>
{
    try {
        typename Traits::order o {};
        project<&entry::key> p {};

        auto it1 = rng::lower_bound(m_entries, k, o, p);

        if (it1 != m_entries.end() && it1->key == k) {
            it1->value = std::forward<V>(v);
            return { it1, false };
        }
        else {
            auto it2 = m_entries.insert(it1, entry { std::forward<K>(k), std::forward<V>(v) });
            return { it2, true };
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
template<typename K, typename V>
unique_table<Key, T, Traits>::insert_result_t unique_table<Key, T, Traits>::
emplace_hint(const_iterator hint_, K&& k, V&& v)
    requires is_comparable_with_key_v<K>
          && std::is_constructible_v<Key, K>
          && std::is_constructible_v<T, V>
{
    try {
        typename Traits::order cmp {};
        project<&entry::key> p {};

        auto hint = remove_const(hint_);

        auto insert_at = [&](auto i) -> insert_result_t {
            i = m_entries.insert(
                i,
                entry { std::forward<K>(k), std::forward<V>(v) }
            );
            return { i, true };
        };

        auto insert_at_lower_bound_of =
            [&](auto f, auto l) -> insert_result_t {
                auto i = rng::lower_bound(f, l, k, cmp, p);
                if (i != m_entries.end() && i->key == k) {
                    return { i, false };
                }
                else {
                    return insert_at(i);
                }
            };

        if (hint != m_entries.end()) {
            if (hint != m_entries.begin()) {
                if (cmp(k, hint->key)) {
                    if (cmp((hint-1)->key, k)) {
                        return insert_at(hint);
                    }
                    else if (!cmp(k, (hint-1)->key)) { // k == (hint-1)->key
                        return { hint-1, false };
                    }
                    else { // k < (hint-1)->key
                        return insert_at_lower_bound_of(m_entries.begin(), hint-1);
                    }
                }
                else if (!cmp(hint->key, k)) { // k == hint->key
                    return { hint, false };
                }
                else { // hint->key < k
                    return insert_at_lower_bound_of(hint, m_entries.end());
                }
            } else { // hint == m_entries.begin()
                if (cmp(k, hint->key)) {
                    return insert_at(hint);
                }
                else if (!cmp(hint->key, k)) { // k == hint->key
                    return { hint, false };
                }
                else { // hint->key < k
                    return insert_at_lower_bound_of(m_entries.begin(), m_entries.end());
                }
            }
        }
        else { // hint == m_entries.end()
            if (hint != m_entries.begin()) {
                if (cmp((hint-1)->key, k)) {
                    return insert_at(hint);
                }
                else if (!cmp(k, (hint-1)->key)) { // (hint-1)->key == k
                    return { hint-1, false };
                }
                else { // k < (hint-1)->key
                    return insert_at_lower_bound_of(m_entries.begin(), hint-1);
                }
            }
            else { // hint == m_entries.begin()
                return insert_at(hint);
            }
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
erase(const_iterator it) noexcept
{
    return m_entries.erase(it);
}

template<typename Key, typename T, typename Traits>
unique_table<Key, T, Traits>::iterator unique_table<Key, T, Traits>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_entries.erase(first, last);
}

template<typename Key, typename T, typename Traits>
template<typename K>
unique_table<Key, T, Traits>::size_type unique_table<Key, T, Traits>::
erase(K const& k) noexcept
    requires is_comparable_with_key_v<K>
{
    auto it = find(k);
    if (it == end()) {
        return 0;
    }
    else {
        erase(it);
        return 1;
    }
}

template<typename Key, typename T, typename Traits>
void unique_table<Key, T, Traits>::
clear() noexcept
{
    return m_entries.clear();
}

template<typename Key, typename T, typename Traits>
void unique_table<Key, T, Traits>::
reserve(size_type n)
{
    try {
        return m_entries.reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
void unique_table<Key, T, Traits>::
shrink_to_fit()
{
    try {
        return m_entries.shrink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename Key, typename T, typename Traits>
bool unique_table<Key, T, Traits>::
operator==(unique_table const& o) const
    noexcept(noexcept(std::declval<Key>() == std::declval<Key>())
          && noexcept(std::declval<T>() == std::declval<T>()) )
{
    return std::equal(begin(), end(), o.begin(), o.end());
}

template<typename Key, typename T, typename Traits>
auto unique_table<Key, T, Traits>::
operator<=>(unique_table const& o) const
    noexcept(noexcept(std::declval<Key>() <=> std::declval<Key>())
          && noexcept(std::declval<T>() <=> std::declval<T>()) )
{
    return std::lexicographical_compare_three_way(
        begin(), end(),
        o.begin(), o.end()
    );
}

template<typename K, typename V, typename T>
std::ostream&
operator<<(std::ostream& os,
           unique_table<K, V, T> const& t)
{
    try {
        return os << json::value_from(t);
    }
    catch (...) {
        rethrow_error();
    }
}

template<size_t I, typename K, typename V, typename T>
auto
get(unique_table<K, V, T>& t) noexcept
    requires (I == 0 || I == 1)
{
    if constexpr (I == 0) {
        return t.begin();
    }
    else if constexpr (I == 1) {
        return t.end();
    }
}

template<size_t I, typename K, typename V, typename T>
auto
get(unique_table<K, V, T> const& t) noexcept
    requires (I == 0 || I == 1)
{
    if constexpr (I == 0) {
        return t.begin();
    }
    else if constexpr (I == 1) {
        return t.end();
    }
}

namespace json {

template<typename K, typename V, typename T>
void
tag_invoke(value_from_tag,
           value& jv,
           unique_table<K, V, T> const& t)
    requires requires (K const& k) { json::string(k); }
          && requires (V const& v) { json::value_from(v); }
{
    try {
        auto& obj = jv.emplace_object();
        for (auto const& [k, v]: t) {
            obj[k] = v;
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename K, typename V, typename T>
void
tag_invoke(value_from_tag,
           value& jv,
           unique_table<K, V, T> const& t)
    requires (!requires (K const& k) { json::string(k); })
          && requires (K const& k) { json::value_from(k); }
          && requires (V const& v) { json::value_from(v); }
{
    try {
        auto& arr = jv.emplace_array();
        for (auto const& [k, v]: t) {
            json::array a { k, v };
            arr.push_back(std::move(a));
        }
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_UNIQUE_TABLE_IPP
