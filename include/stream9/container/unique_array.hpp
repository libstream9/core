#ifndef STREAM9_CONTAINER_UNIQUE_ARRAY_HPP
#define STREAM9_CONTAINER_UNIQUE_ARRAY_HPP

#include <concepts>
#include <cstdint>
#include <functional>
#include <type_traits>

#include <stream9/array.hpp>
#include <stream9/is_comparable.hpp>
#include <stream9/projection.hpp>
#include <stream9/nothrow_swap.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

/*
 * model std::contiguous_range
 */
template<typename T,
         typename Compare = std::less<>, // strict weak ordering
         typename Proj = std::identity,
         std::int64_t MinCapacity = 0,
         typename Allocator = std::allocator<T> >
class unique_array
{
public:
    using array_type = array<T, MinCapacity, Allocator>;
    using pointer = T*;
    using const_pointer = T const*;
    using reference = T&;
    using const_reference = T const&;
    using size_type = safe_integer<std::int64_t, 0>;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using allocator_type = Allocator;
    using value_type = T;
    using project = Proj;
    using compare = Compare;

    constexpr static bool is_comparable_v =
        stream9::is_comparable_v<projected_t<T, Proj>,
                                 projected_t<T, Proj>, Compare >;

    constexpr static bool is_nothrow_comparable_v =
        stream9::is_nothrow_comparable_v<projected_t<T, Proj>,
                                         projected_t<T, Proj>, Compare >;

    template<typename U>
    constexpr static bool is_comparable_with_v =
        stream9::is_comparable_v<projected_t<T, Proj>, U, Compare>;

    template<typename U>
    constexpr static bool is_nothrow_comparable_with_v =
        stream9::is_nothrow_comparable_v<projected_t<T, Proj>, U, Compare>;

    template<typename>
    struct is_insertable : std::false_type {};

    template<typename U>
        requires std::is_constructible_v<T, U>
    struct is_insertable<U> : std::true_type {};

    template<typename U>
    constexpr static bool is_insertable_v = is_insertable<U>::value;

    static_assert(!std::is_reference_v<T>);
    static_assert(!std::is_const_v<T>);
    static_assert(MinCapacity >= 0);
    static_assert(std::is_nothrow_move_constructible_v<T>);
    static_assert(std::is_nothrow_destructible_v<T>);
    static_assert(is_nothrow_swappable_v<T>);
    static_assert(std::is_nothrow_default_constructible_v<Allocator>);
    static_assert(std::is_nothrow_move_constructible_v<Allocator>);
    static_assert(std::is_nothrow_destructible_v<Allocator>);
    static_assert(is_nothrow_swappable_v<Allocator>);
    static_assert(projection<Proj, T>);
    static_assert(is_comparable_v);

public:
    // essential
    unique_array() = default;

    unique_array(allocator_type const& alloc);

    unique_array(unique_array const&) = default;
    unique_array& operator=(unique_array const&) = default;

    unique_array(unique_array&&) = default;
    unique_array& operator=(unique_array&&) = default;

    unique_array(std::initializer_list<T>);

    template<typename... Us>
    unique_array(Us&&... values)
        requires (is_insertable_v<Us> && ...);

    ~unique_array() noexcept = default;

    // accessor
    iterator       begin() noexcept;
    iterator       end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    reference       at(size_type pos) noexcept;
    const_reference at(size_type pos) const noexcept;

    reference       operator[](size_type pos) noexcept;
    const_reference operator[](size_type pos) const noexcept;

    reference       front() noexcept;
    const_reference front() const noexcept;

    reference back() noexcept;
    const_reference back() const noexcept;

    pointer       data() noexcept;
    const_pointer data() const noexcept;

    // query
    bool empty() const noexcept;
    size_type size() const noexcept;

    iterator find(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator find(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator find(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    template<typename U>
    const_iterator find(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    iterator lower_bound(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator lower_bound(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator lower_bound(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;
    template<typename U>
    const_iterator lower_bound(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    iterator upper_bound(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator upper_bound(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator upper_bound(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;
    template<typename U>
    const_iterator upper_bound(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    std::ranges::subrange<iterator>
        equal_range(T const&)
            noexcept(is_nothrow_comparable_v);
    std::ranges::subrange<const_iterator>
        equal_range(T const&) const
            noexcept(is_nothrow_comparable_v);

    template<typename U>
    std::ranges::subrange<iterator>
        equal_range(U const&)
            noexcept(is_nothrow_comparable_with_v<U>)
            requires is_comparable_with_v<U>;
    template<typename U>
    std::ranges::subrange<const_iterator>
        equal_range(U const&) const
            noexcept(is_nothrow_comparable_with_v<U>)
            requires is_comparable_with_v<U>;

    size_type capacity() const noexcept;
    size_type min_capacity() const noexcept;
    size_type max_capacity() const noexcept;

    allocator_type get_allocator() const noexcept;

    iterator remove_const(const_iterator i);

    // modifier
    struct insert_result_t
    {
        iterator position;
        bool inserted;

        T& operator*() const noexcept;
        T* operator->() const noexcept;

        operator bool () const noexcept;
        operator iterator () const noexcept;
    };

    insert_result_t
        insert(T const& value);
    insert_result_t
        insert(T&& value);

    template<typename U>
    insert_result_t
    insert(U&& value)
        requires std::is_constructible_v<T, U>;

    insert_result_t
        insert_or_replace(T const& value);
    insert_result_t
        insert_or_replace(T&& value);

    template<typename U>
    insert_result_t
    insert_or_replace(U&& value)
        requires std::is_constructible_v<T, U>;

    // There is no emplace function, because
    // 1. We need the value before place it in the array so we can project(value)
    //    and decide where to place the value.
    // 2. But if we fail to place the value (ex. duplication), we have to destruct
    //    the value we had just constructed in 1.
    // 3. But if we destruct the value, then values we use to construct the value
    //    (possibly R-value) are gone.
    //TODO BUT there should be a emplace() on this class regardless of a problem above.

    iterator
    erase(const_iterator pos) noexcept;

    iterator
    erase(const_iterator first, const_iterator last) noexcept;

    void
    clear() noexcept;

    void
    reserve(size_type n);

    void
    shrink_to_fit();

    void
    swap(unique_array&) noexcept;

    // comparison
    template<std::ranges::input_range R>
    bool
    operator==(R const& other) const
        noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
        requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    auto
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
        requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    std::partial_ordering
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
        requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
              && requires (T v1, std::ranges::range_reference_t<R> v2) {
                  { v1 < v2 } -> std::convertible_to<bool>;
              };

    // friend functions
    template<typename CharT, typename Traits,
             typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
    friend std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>&, unique_array<T2, C2, P2, M2, A2> const&)
        requires requires (std::basic_ostream<CharT, Traits>& os, T2 v) { os << v; };

    template<typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
    friend void
    json::tag_invoke(value_from_tag, value&, stream9::unique_array<T2, C2, P2, M2, A2> const&);

private:
    array_type m_array;
};

template<typename T, typename C, typename P, std::int64_t M, typename A>
void
swap(unique_array<T, C, P, M, A>& lhs, unique_array<T, C, P, M, A>& rhs) noexcept; //TODO

} // namespace stream9

#endif // STREAM9_CONTAINER_UNIQUE_ARRAY_HPP

#include "unique_array.ipp"
