#ifndef STREAM9_CONTAINER_ARRAY_VIEW_IPP
#define STREAM9_CONTAINER_ARRAY_VIEW_IPP

#include "../array_view.hpp"

#include <cassert>

#include <stream9/errors.hpp>

namespace stream9 {

template<typename T>
template<std::size_t N>
constexpr array_view<T>::
array_view(T (&arr)[N]) noexcept
    : m_first { arr }
    , m_last { arr + N }
{
    assert(m_first <= m_last);
}

template<typename T>
template<std::size_t N>
constexpr array_view<T>::
array_view(T (&arr)[N], size_type offset) noexcept
    : m_first { arr + offset }
    , m_last { arr + N }
{
    if (m_first > m_last) m_first = m_last;
    assert(m_first <= m_last);
}

template<typename T>
template<std::size_t N>
constexpr array_view<T>::
array_view(T (&arr)[N], size_type offset, size_type length) noexcept
    : m_first { arr + offset }
    , m_last { m_first + length }
{
    if (m_last > &arr[N]) m_last = &arr[N];
    if (m_first > &arr[N]) m_first = m_last;
    assert(m_first <= m_last);
}

template<typename T>
template<std::ranges::contiguous_range R>
constexpr array_view<T>::
array_view(R&& range) noexcept
    requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>
    : m_first { std::ranges::data(range) }
    , m_last { m_first + std::ranges::size(range) }
{
    assert(m_first <= m_last);
}

template<typename T>
template<std::ranges::contiguous_range R>
constexpr array_view<T>::
array_view(R&& range, size_type offset) noexcept
    requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>
    : m_first { std::ranges::data(range) + offset }
    , m_last { std::ranges::data(range) + std::ranges::size(range) }
{
    if (m_first > m_last) m_first = m_last;

    assert(m_first <= m_last);
}

template<typename T>
template<std::ranges::contiguous_range R>
constexpr array_view<T>::
array_view(R&& range, size_type offset, size_type length) noexcept
    requires std::is_convertible_v<decltype(std::ranges::data(range)), pointer>
    : m_first { std::ranges::data(range) + offset }
    , m_last { m_first + length }
{
    auto e = std::ranges::data(range) + std::ranges::size(range);
    if (m_first > e) m_first = e;
    if (m_last > e) m_last = e;

    assert(m_first <= m_last);
}

template<typename T>
template<std::contiguous_iterator It>
constexpr array_view<T>::
array_view(It first, size_type count) noexcept
    requires std::is_convertible_v<std::iter_reference_t<It>, reference>
    : m_first { std::to_address(first) }
    , m_last { m_first + count }
{
    if (!m_first) {
        m_last = m_first;
    }

    assert(m_first <= m_last);
}

template<typename T>
template<std::contiguous_iterator It>
constexpr array_view<T>::
array_view(It first, It last) noexcept
    requires std::is_convertible_v<std::iter_reference_t<It>, reference>
    : m_first { std::to_address(first) }
    , m_last { std::to_address(last) }
{
    if (m_first > m_last) {
        m_last = m_first;
    }

    assert(m_first <= m_last);
}

template<typename T>
constexpr array_view<T>::reference array_view<T>::
front() const
{
    if (m_first == m_last) {
        throw_error(array_view_errc::reference_empty_view);
    }

    return *m_first;
}

template<typename T>
constexpr array_view<T>::reference array_view<T>::
back() const
{
    if (m_first == m_last) {
        throw_error(array_view_errc::reference_empty_view);
    }

    return *(m_last - 1);
}

template<typename T>
constexpr array_view<T>::reference array_view<T>::
operator[](size_type idx) const
{
    auto p = m_first + idx;
    if (p >= m_last) {
        throw_error(array_view_errc::index_out_of_range, {
            { "idx", idx },
            { "size", m_last - m_first },
        });
    }

    return *p;
}

template<typename T>
constexpr array_view<T>::pointer array_view<T>::
data() const noexcept
{
    return m_first;
}

template<typename T>
constexpr array_view<T>::iterator array_view<T>::
begin() const noexcept
{
    return m_first;
}

template<typename T>
constexpr array_view<T>::iterator array_view<T>::
end() const noexcept
{
    return m_last;
}

template<typename T>
constexpr array_view<T>::size_type array_view<T>::
size() const noexcept
{
    return m_last - m_first;
}

template<typename T>
constexpr bool array_view<T>::
empty() const noexcept
{
    return m_first == m_last;
}

template<typename T>
constexpr void array_view<T>::
advance_begin(difference_type n)
{
    auto it = m_first + n;
    if (it <= m_last) {
        m_first = it;
    }
    else {
        m_first = m_last;
    }
}

template<typename T>
constexpr void array_view<T>::
advance_begin(difference_type n, iterator left_boundary)
{
    auto it = m_first + n;
    if (it < left_boundary) {
        m_first = left_boundary;
    }
    else if (it > m_last) {
        m_first = m_last;
    }
    else {
        m_first = it;
    }
}

template<typename T>
constexpr void array_view<T>::
advance_end(difference_type n)
{
    auto it = m_last + n;
    if (it >= m_first) {
        m_last = it;
    }
    else {
        m_last = m_first;
    }
}

template<typename T>
constexpr void array_view<T>::
advance_end(difference_type n, iterator right_boundary)
{
    auto it = m_last + n;
    if (it > right_boundary) {
        m_last = right_boundary;
    }
    else if (it < m_first) {
        m_last = m_first;
    }
    else {
        m_last = it;
    }
}

inline std::error_category const&
array_view_error_category()
{
    static struct impl : std::error_category
    {
        char const* name() const noexcept { return "stream9::array_view"; }

        std::string message(int ec) const
        {
            switch (static_cast<array_view_errc>(ec)) {
                using enum array_view_errc;
                case ok:
                    return "ok";
                case reference_empty_view:
                    return "reference empty view";
                case index_out_of_range:
                    return "index out of range";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

inline std::error_code
make_error_code(array_view_errc e) noexcept
{
    return { static_cast<int>(e), array_view_error_category() };
}

} // namespace stream9

#endif // STREAM9_CONTAINER_ARRAY_VIEW_IPP
