#ifndef STREAM9_CONTAINER_SORTED_ARRAY_HPP
#define STREAM9_CONTAINER_SORTED_ARRAY_HPP

#include <concepts>
#include <cstdint>
#include <iterator>
#include <memory>
#include <ostream>
#include <ranges>
#include <type_traits>
#include <utility>

#include <stream9/array.hpp>
#include <stream9/errors.hpp>
#include <stream9/is_comparable.hpp>
#include <stream9/less.hpp>
#include <stream9/nothrow_swap.hpp>
#include <stream9/projection.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9 {

/*
 * model std::contiguous_range
 */
template<typename T,
         typename Compare = less, // strict weak ordering
         typename Proj = std::identity,
         std::int64_t MinCapacity = 0,
         typename Allocator = std::allocator<T> >
class sorted_array
{
public:
    using array_type = array<T, MinCapacity, Allocator>;
    using pointer = T*;
    using const_pointer = T const*;
    using reference = T&;
    using const_reference = T const&;
    using size_type = safe_integer<std::int64_t, 0>;
    using iterator = pointer;
    using const_iterator = const_pointer;
    using allocator_type = Allocator;
    using value_type = T;
    using compare = Compare;
    using project = Proj;

    template<typename U>
    constexpr static bool is_insertable_v =
                                   array_type::template is_insertable<U>::value;

    template<typename U>
    constexpr static bool is_nothrow_insertable_v =
                           array_type::template is_nothrow_insertable<U>::value;

    constexpr static bool is_comparable_v =
        stream9::is_comparable_v<projected_t<T, Proj>,
                                 projected_t<T, Proj>, Compare >;

    constexpr static bool is_nothrow_comparable_v =
        stream9::is_nothrow_comparable_v<projected_t<T, Proj>,
                                         projected_t<T, Proj>, Compare >;

    template<typename U>
    constexpr static bool is_comparable_with_v =
        stream9::is_comparable_v<projected_t<T, Proj>, U, Compare>;

    template<typename U>
    constexpr static bool is_nothrow_comparable_with_v =
        stream9::is_nothrow_comparable_v<projected_t<T, Proj>, U, Compare>;

    static_assert(!std::is_reference_v<T>);
    static_assert(!std::is_const_v<T>);
    static_assert(MinCapacity >= 0);
    static_assert(std::is_nothrow_move_constructible_v<T>);
    static_assert(std::is_nothrow_destructible_v<T>);
    static_assert(is_nothrow_swappable_v<T>);
    static_assert(std::is_nothrow_default_constructible_v<Allocator>);
    static_assert(std::is_nothrow_move_constructible_v<Allocator>);
    static_assert(std::is_nothrow_destructible_v<Allocator>);
    static_assert(is_nothrow_swappable_v<Allocator>);
    static_assert(projection<Proj, T>);
    static_assert(is_comparable_v);

public:
    // essential
    sorted_array() = default;

    sorted_array(allocator_type const& alloc);

    sorted_array(std::initializer_list<T>);

    template<typename... Us>
    sorted_array(Us&&... values)
        requires (is_insertable_v<Us> && ...);

    sorted_array(sorted_array const&) = default;
    sorted_array& operator=(sorted_array const&) = default;

    sorted_array(sorted_array&&) = default;
    sorted_array& operator=(sorted_array&&) = default;

    ~sorted_array() noexcept = default;

    // accessor
    iterator       begin() noexcept;
    iterator       end() noexcept;

    const_iterator begin() const noexcept;
    const_iterator end() const noexcept;

    reference       at(size_type pos) noexcept;
    const_reference at(size_type pos) const noexcept;

    reference       operator[](size_type pos) noexcept;
    const_reference operator[](size_type pos) const noexcept;

    reference       front() noexcept;
    const_reference front() const noexcept;

    reference back() noexcept;
    const_reference back() const noexcept;

    pointer       data() noexcept;
    const_pointer data() const noexcept;

    // query
    bool empty() const noexcept;
    size_type size() const noexcept;

    iterator find(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator find(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator find(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;
    template<typename U>
    const_iterator find(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    iterator lower_bound(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator lower_bound(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator lower_bound(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;
    template<typename U>
    const_iterator lower_bound(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    iterator upper_bound(T const&)
        noexcept(is_nothrow_comparable_v);
    const_iterator upper_bound(T const&) const
        noexcept(is_nothrow_comparable_v);

    template<typename U>
    iterator upper_bound(U const&)
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;
    template<typename U>
    const_iterator upper_bound(U const&) const
        noexcept(is_nothrow_comparable_with_v<U>)
        requires is_comparable_with_v<U>;

    std::ranges::subrange<iterator>
        equal_range(T const&)
            noexcept(is_nothrow_comparable_v);
    std::ranges::subrange<const_iterator>
        equal_range(T const&) const
            noexcept(is_nothrow_comparable_v);

    template<typename U>
    std::ranges::subrange<iterator>
        equal_range(U const&)
            noexcept(is_nothrow_comparable_with_v<U>)
            requires is_comparable_with_v<U>;
    template<typename U>
    std::ranges::subrange<const_iterator>
        equal_range(U const&) const
            noexcept(is_nothrow_comparable_with_v<U>)
            requires is_comparable_with_v<U>;

    size_type capacity() const noexcept;
    size_type min_capacity() const noexcept;
    size_type max_capacity() const noexcept;

    allocator_type get_allocator() const noexcept;

    iterator remove_const(const_iterator i);

    // modifier
    template<typename U>
    iterator
    insert(U&& value)
        requires std::is_constructible_v<T, U>;

    template<rng::input_range R>
    void
    insert(R&& range)
        requires (!std::is_constructible_v<T, R>)
              && std::is_constructible_v<T, rng::reference_t<R>>;

    template<typename... Us>
    void
    insert(Us&&... values)
        requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...);

    template<typename... Args>
    iterator
    emplace(Args&&... args)
        requires std::is_constructible_v<T, Args...>;

    iterator
    erase(const_iterator pos) noexcept;

    iterator
    erase(const_iterator first, const_iterator last) noexcept;

    void
    clear() noexcept;

    template<typename... Args>
    void
    resize(size_type count, Args&&... args)
        requires std::is_constructible_v<T, Args...>;

    void
    reserve(size_type n);

    void
    shrink_to_fit();

    void
    swap(sorted_array&) noexcept;

    // comparison
    template<std::ranges::input_range R>
    bool
    operator==(R const& other) const
        noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
        requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    auto
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
        requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>;

    template<std::ranges::input_range R>
    std::partial_ordering
    operator<=>(R const& other) const
        noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
        requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
              && requires (T v1, std::ranges::range_reference_t<R> v2) {
                  { v1 < v2 } -> std::convertible_to<bool>;
              };

    // friend functions
    template<typename CharT, typename Traits>
    friend std::basic_ostream<CharT, Traits>&
    operator<<(std::basic_ostream<CharT, Traits>& os, sorted_array const& a)
        requires requires (std::basic_ostream<CharT, Traits>& os, T const& v) { os << v; }
    {
        try {
            return os << a.m_array;
        }
        catch (...) {
            rethrow_error();
        }
    }

    template<typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
    friend void
    swap(sorted_array<T2, C2, P2, M2, A2>& lhs,
         sorted_array<T2, C2, P2, M2, A2>& rhs) noexcept;

    template<typename T2, typename C2, typename P2, std::int64_t M2, typename A2>
    friend void
    json::tag_invoke(json::value_from_tag, value& v,
                     sorted_array<T2, C2, P2, M2, A2> const&);

private:
    array_type m_array;
};

} // namespace stream9

#endif // STREAM9_CONTAINER_SORTED_ARRAY_HPP

#include "sorted_array.ipp"
