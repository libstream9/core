#ifndef STREAM9_CONTAINER_MODIFIER_APPEND_HPP
#define STREAM9_CONTAINER_MODIFIER_APPEND_HPP

#include "../namespace.hpp"
#include "insert.hpp"

#include <iterator>
#include <ranges>
#include <utility>

#include <stream9/errors.hpp>

namespace stream9::container {

namespace append_ {

    struct api
    {
        template<typename C, typename... Ts>
        std::ranges::subrange<rng::iterator_t<C>>
        operator()(C& s, Ts&&... v) const
            requires insertable<C, Ts...>
        {
            try {
                return (insert)(s, rng::end(s), std::forward<Ts>(v)...);
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace append_

inline constexpr append_::api append;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_APPEND_HPP
