#ifndef STREAM9_CONTAINER_MODIFIER_INSERT_HPP
#define STREAM9_CONTAINER_MODIFIER_INSERT_HPP

#include <concepts>
#include <iterator>
#include <ranges>

#include <stream9/container/converter/remove_const.hpp>
#include <stream9/move_iterator.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/end.hpp>

namespace stream9::container {

namespace insert_ {

    /*
     * concepts
     */
    template<typename Seq, typename T>
    concept value_insertable =
        requires (Seq& s, T&& v) {
            { s.insert(rng::begin(s), std::forward<T>(v)) } -> std::same_as<rng::iterator_t<Seq>>;
        };

    template<typename Seq, typename R>
    concept range_insertable =
        rng::input_range<R> &&
        requires (Seq& s, R&& v) {
            { s.insert(rng::begin(s), std::forward<R>(v)) } -> std::same_as<std::ranges::subrange<rng::iterator_t<Seq>>>;
        };

    template<typename Seq, typename I, typename S = I>
    concept iter_insertable =
        std::input_iterator<I> &&
        std::sentinel_for<S, I> &&
        requires (Seq& s, I const& f, S const& l) {
            { s.insert(rng::begin(s), f, l) } -> std::same_as<std::ranges::subrange<rng::iterator_t<Seq>>>;
        };

    template<typename Seq, typename I>
    concept legacy_iter_insertable =
        std::input_iterator<I> &&
        requires (Seq& s, I const& f, I const& l) {
            { s.insert(rng::begin(s), f, l) } -> std::same_as<rng::iterator_t<Seq>>;
        };

    template<typename Seq, typename R>
    concept range_insertable_by_iterator =
        rng::input_range<R> &&
        requires (Seq& s, R&& v) {
            { s.insert(rng::begin(s), rng::begin(v), rng::end(v)) } -> std::same_as<std::ranges::subrange<rng::iterator_t<Seq>>>;
        };

    template<typename Seq, typename R>
    concept range_insertable_by_legacy_iterator =
        rng::input_range<R> &&
        requires (Seq& s, R&& v) {
            { s.insert(rng::begin(s), rng::begin(v), rng::end(v)) } -> std::same_as<rng::iterator_t<Seq>>;
        };

    template<typename Seq, typename... Ts>
    concept variadic_insertable =
        requires (Seq& s, Ts&&... vs) {
            { s.insert(rng::begin(s), std::forward<Ts>(vs)...) } -> std::same_as<std::ranges::subrange<rng::iterator_t<Seq>>>;
        };

    /*
     * functions
     */
    template<typename Seq, std::input_iterator I>
    std::ranges::subrange<rng::iterator_t<Seq>>
    legacy_iter_insert(Seq& s,
                       rng::iterator_t<Seq const> i,
                       I const& first, I const& last)
        requires (!iter_insertable<Seq, I, I>)
              && rng::forward_range<Seq>
              && std::sized_sentinel_for<I, I>
              && legacy_iter_insertable<Seq, I>
              // && has_strong_exception_safety_on_insert<Seq> //TODO
              && std::is_nothrow_move_constructible_v<rng::value_t<Seq>>
    {
        auto len = last - first;

        auto j = s.insert(i, first, last);

        return { j, std::next(j, len) };
    }

    template<typename Seq, std::input_iterator I, std::sentinel_for<I> S>
    std::ranges::subrange<rng::iterator_t<Seq>>
    legacy_iter_insert(Seq& s,
                       rng::iterator_t<Seq const> i,
                       I const& first, S const& last)
        requires (!iter_insertable<Seq, I, S>)
              && std::is_default_constructible_v<Seq>
              && rng::forward_range<Seq>
              && rng::sized_range<Seq>
              && legacy_iter_insertable<Seq, I>
              // && has_strong_exception_safety_on_insert_at_end<Seq> //TODO
              // && has_strong_exception_safety_on_insert_at_middle<Seq> //TODO
              && std::is_nothrow_move_constructible_v<rng::value_t<Seq>>
    {
        auto off = i - rng::begin(s);

        if (i != s.end()) {
            Seq tmp;
            for (auto f = first; f != last; ++f) {
                tmp.insert(tmp.end(), *f);
            }

            // noexcept from here
            auto j = i;
            for (auto& v: tmp) {
                j = s.insert(j, std::move(v));
                ++j;
            }

            return { std::next(rng::begin(s), off), remove_const(s, j) };
        }
        else {
            for (auto f = first; f != last; ++f) {
                s.insert(s.end(), *f);
            }

            return { std::next(rng::begin(s), off), s.end() };
        }
    }

    template<typename Seq, rng::input_range R>
    std::ranges::subrange<rng::iterator_t<Seq>>
    range_insert_by_iter(Seq& s, rng::iterator_t<Seq const> i, R&& v)
        requires iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>
              // && has_strong_exception_safety_on_insert<Seq> //TODO
              && std::is_nothrow_move_constructible_v<rng::value_t<Seq>>
    {
        if constexpr (std::is_lvalue_reference_v<R>) {
            return s.insert(i, rng::begin(v), rng::end(v));
        }
        else if constexpr (std::input_iterator<rng::sentinel_t<R>>) {
            return s.insert(i,
                move_iterator(rng::begin(v)), move_iterator(rng::end(v))
            );
        }
        else {
            return s.insert(i, move_iterator(rng::begin(v)), rng::end(v));
        }
    }

    template<typename Seq, rng::input_range R>
    std::ranges::subrange<rng::iterator_t<Seq>>
    range_insert_by_iter(Seq& s, rng::iterator_t<Seq const> i, R&& v)
        requires (!iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>)
              && legacy_iter_insertable<Seq, rng::iterator_t<R>>
    {
        auto f = rng::begin(v);
        auto l = rng::end(v);

        if constexpr (std::is_lvalue_reference_v<R>) {
            return legacy_iter_insert(s, i, f, l);
        }
        else if constexpr (std::input_iterator<rng::sentinel_t<R>>) {
            return legacy_iter_insert(s, i,
                move_iterator(f), move_iterator(l)
            );
        }
        else {
            return legacy_iter_insert(s, i, move_iterator(f), l);
        }
    }

    template<typename Seq>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i)
    {
        auto j = remove_const(s, i);
        return { j, j };
    }

    template<typename Seq, typename R, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i, R&& v, Ts&&... rest)
        requires (!value_insertable<Seq, R>)
              && range_insertable<Seq, R>
              && rng::forward_range<Seq>
              && rng::sized_range<Seq>;

    template<typename Seq, typename R, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i, R&& v, Ts&&... rest)
        requires (!value_insertable<Seq, R>)
              && (!range_insertable<Seq, R>)
              && rng::forward_range<R>
              && (iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>
                  || legacy_iter_insertable<Seq, rng::iterator_t<R>>)
              && rng::sized_range<Seq>;

    template<typename Seq, typename T, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i, T&& v, Ts&&... rest)
        requires value_insertable<Seq, T>
              && rng::forward_range<Seq>
              && rng::sized_range<Seq>
    {
        auto off = std::distance(rng::cbegin(s), i);

        auto j = s.insert(i, std::forward<T>(v));
        auto [_, l] = variadic_insert(s, std::next(j), std::forward<Ts>(rest)...);

        auto f = std::next(rng::begin(s), off);
        return { f, l };
    }

    template<typename Seq, typename R, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i, R&& v, Ts&&... rest)
        requires (!value_insertable<Seq, R>)
              && range_insertable<Seq, R>
              && rng::forward_range<Seq>
              && rng::sized_range<Seq>
    {
        auto off = std::distance(rng::cbegin(s), i);

        auto [_1, e1] = s.insert(i, std::forward<R>(v));
        auto [_2, e2] = variadic_insert(s, e1, std::forward<Ts>(rest)...);

        return { std::next(rng::begin(s), off), e2 };
    }

    template<typename Seq, typename R, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert(Seq& s, rng::iterator_t<Seq const> i, R&& v, Ts&&... rest)
        requires (!value_insertable<Seq, R>)
              && (!range_insertable<Seq, R>)
              && rng::forward_range<R>
              && (iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>
                  || legacy_iter_insertable<Seq, rng::iterator_t<R>>)
              && rng::sized_range<Seq>
    {
        auto off = std::distance(rng::cbegin(s), i);

        auto [_1, e1] = range_insert_by_iter(s, i, std::forward<R>(v));
        auto [_2, e2] = variadic_insert(s, e1, std::forward<Ts>(rest)...);

        return { std::next(rng::begin(s), off), e2 };
    }

    template<typename Seq, typename... Ts>
    std::ranges::subrange<rng::iterator_t<Seq>>
    variadic_insert_main(Seq& s, rng::iterator_t<Seq const> i, Ts&&... rest)
    {
        Seq tmp;
        variadic_insert(tmp, tmp.end(), std::forward<Ts>(rest)...);

        return variadic_insert(s, i, std::move(tmp));
    }


    struct api
    {
        /*
         * insert value
         */
        template<typename Seq, typename T>
        rng::iterator_t<Seq>
        operator()(Seq& s, rng::iterator_t<Seq const> i, T&& v) const
            requires value_insertable<Seq, T>
        {
            return s.insert(i, std::forward<T>(v));
        }

        /*
         * insert range of values
         */
        template<typename Seq, rng::input_range R>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i, R&& v) const
            requires range_insertable<Seq, R>
        {
            return s.insert(i, std::forward<R>(v));
        }

        template<typename Seq, rng::input_range R>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i, R&& v) const
            requires (!range_insertable<Seq, R>)
                  && iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>
        {
            return range_insert_by_iter(s, i, std::forward<R>(v));
        }

        template<typename Seq, rng::forward_range R>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i, R&& v) const
            requires (!range_insertable<Seq, R>)
                  && (!iter_insertable<Seq, rng::iterator_t<R>, rng::sentinel_t<R>>)
                  && std::sized_sentinel_for<rng::sentinel_t<R>, rng::iterator_t<R>>
                  && legacy_iter_insertable<Seq, rng::iterator_t<R>>
        {
            return range_insert_by_iter(s, i, std::forward<R>(v));
        }

        /*
         * insert range of values by a pair of iterator
         */
        template<typename Seq, std::input_iterator I, std::sentinel_for<I> S>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i,
                   I const& first, S const& last) const
            requires iter_insertable<Seq, I, S>
        {
            return s.insert(i, first, last);
        }

        template<typename Seq, std::input_iterator I, std::sentinel_for<I> S>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i,
                   I const& first, S const& last) const
            requires (!iter_insertable<Seq, I, S>)
                  && rng::forward_range<Seq>
                  && legacy_iter_insertable<Seq, I>
                  && ((std::same_as<I, S> && std::sized_sentinel_for<S, I>) || rng::sized_range<Seq>)
        {
            return legacy_iter_insert(s, i, first, last);
        }


        /*
         * insert values with variadic arguments
         */
        template<typename Seq, typename... Ts>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i, Ts&&... values) const
            requires (sizeof...(Ts) > 1)
                  && variadic_insertable<Seq, Ts...>
        {
            return s.insert(i, std::forward<Ts>(values)...);
        }

        template<typename Seq, typename... Ts>
        std::ranges::subrange<rng::iterator_t<Seq>>
        operator()(Seq& s, rng::iterator_t<Seq const> i, Ts&&... values) const
            requires (sizeof...(Ts) > 1)
                  && (!variadic_insertable<Seq, Ts...>)
                  && ((value_insertable<Seq, Ts>
                       || range_insertable<Seq, Ts>
                       || range_insertable_by_iterator<Seq, Ts>
                       || range_insertable_by_legacy_iterator<Seq, Ts>)
                      && ...)
        {
            return variadic_insert_main(s, i, std::forward<Ts>(values)...);
        }

    };

} // namespace insert_

inline constexpr insert_::api insert;

template<typename Seq, typename... Args>
concept insertable = requires (Seq& s, Args&&... v) {
    (insert)(s, rng::end(s), std::forward<Args>(v)...);
};

//TODO exception, noexcept

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_INSERT_HPP
