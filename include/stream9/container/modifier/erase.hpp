#ifndef STREAM9_CONTAINER_MODIFIER_ERASE_HPP
#define STREAM9_CONTAINER_MODIFIER_ERASE_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/errors.hpp>

#include <concepts>

namespace stream9 {

namespace erase_ {

    using con::erasable;
    using con::nothrow_erasable;
    using con::nothrow_range_erasable;
    using con::range_erasable;
    using ranges::iterator_t;
    using std::convertible_to;

    struct api
    {
        /*
         * erase single element
         */
        template<erasable S>
        constexpr iterator_t<S>
        operator()(S& s, iterator_t<S const> pos) const
            noexcept(nothrow_erasable<S>)
        {
            try {
                return s.erase(pos);
            }
            catch (...) {
                rethrow_error();
            }
        }

        /*
         * erase range of elements
         */
        template<range_erasable S, input_range R>
        constexpr iterator_t<S>
        operator()(S& s, R const& r) const
            noexcept(nothrow_range_erasable<S>)
            requires convertible_to<iterator_t<R>, iterator_t<S const>>
        {
            try {
                return s.erase(rng::begin(r), rng::end(r));
            }
            catch (...) {
                rethrow_error();
            }
        }

        template<range_erasable S>
        constexpr iterator_t<S>
        operator()(S& s,
                   iterator_t<S const> f,
                   iterator_t<S const> l) const
            noexcept(nothrow_range_erasable<S>)
        {
            try {
                return s.erase(f, l);
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace erase_

inline constexpr erase_::api erase;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_ERASE_HPP
