#ifndef STREAM9_CONTAINER_MODIFIER_EXPAND_HPP
#define STREAM9_CONTAINER_MODIFIER_EXPAND_HPP

#include <stream9/errors.hpp>

#include <concepts>

namespace stream9 {

namespace expand_ {

    struct api
    {
        template<typename C, std::integral I>
        void
        operator()(C& c, I n) const
            requires requires (C& c, I n) {
                { c.reserve(n) };
                { c.size() } -> std::integral;
            }
        {
            try {
                c.reserve(c.size() + n);
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace expand_

inline constexpr expand_::api expand;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_EXPAND_HPP
