#ifndef STREAM9_CONTAINER_MODIFIER_PUSH_BACK_HPP
#define STREAM9_CONTAINER_MODIFIER_PUSH_BACK_HPP

#include "../concepts2.hpp"

#include <stream9/errors.hpp>

namespace stream9::container {

namespace push_back_ {

    using std::forward;
    using std::ranges::range_reference_t;

    struct api
    {
        template<typename T, typename C>
        range_reference_t<C>&
        operator()(C& s, T&& v) const
            requires insertable_sequence<C, T>
        {
            try {
                auto it = s.insert(s.end(), forward<T>(v));
                return *it;
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace push_back_

inline constexpr push_back_::api push_back;

} // namespace stream9::container

namespace stream9 {

using container::push_back;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_PUSH_BACK_HPP
