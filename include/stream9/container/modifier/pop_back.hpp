#ifndef STREAM9_CONTAINER_MODIFIER_POP_BACK_HPP
#define STREAM9_CONTAINER_MODIFIER_POP_BACK_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include "pop_from.hpp"

#include <stream9/errors.hpp>
#include <stream9/prev.hpp>

namespace stream9 {

// template<typename T>
// value_t<T>
// pop_back(T& x)
//     requires nothrow_erasable<T>
//         && bidirectional_range<T>
//         && nothrow_move_constructible<value_t<T>>
//         && nothrow_destructible<value_t<T>>;

namespace pop_back_ {

    using rng::bidirectional_range;
    using rng::begin;
    using rng::end;
    using con::nothrow_erasable;
    using rng::value_t;

    struct api
    {
        template<typename T>
        static value_t<T>
        operator()(T& x)
            requires nothrow_erasable<T>
                  && bidirectional_range<T>
                  && nothrow_move_constructible<value_t<T>>
                  && nothrow_destructible<value_t<T>>
        {
            auto f = rng::begin(x);
            auto l = rng::end(x);
            if (f == l) {
                throw error {
                    errc::invalid_argument, {
                        { "detail", "the range is empty." }
                    }
                };
            }

            return pop_from(x, prev(l));
        }
    };

} // namespace pop_back_

inline constexpr pop_back_::api pop_back;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_POP_BACK_HPP
