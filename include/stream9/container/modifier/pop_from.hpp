#ifndef STREAM9_CONTAINER_MODIFIER_POP_FROM_HPP
#define STREAM9_CONTAINER_MODIFIER_POP_FROM_HPP

#include "erase.hpp"

#include "../concepts.hpp"

namespace stream9 {

// template<nothrow_erasable T>
// value_t<T>
// push_from(T& x, iterator_t<T>) nothrow
//     requires nothrow_move_constructible<value_t<T>>
//           && nothrow_destructible<value_t<T>>;

namespace pop_from_ {

    using con::nothrow_erasable;
    using rng::iterator_t;
    using rng::value_t;

    struct api
    {
        template<nothrow_erasable T>
        static value_t<T>
        operator()(T& x, iterator_t<T> i) noexcept
            requires nothrow_move_constructible<value_t<T>>
                  && nothrow_destructible<value_t<T>>
        {
            auto v = std::move(*i);
            erase(x, i);
            return v;
        }
    };

} // namespace pop_from_

inline constexpr pop_from_::api pop_from;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_POP_FROM_HPP
