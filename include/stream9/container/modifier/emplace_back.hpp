#ifndef STREAM9_CONTAINER_MODIFIER_EMPLACE_BACK_HPP
#define STREAM9_CONTAINER_MODIFIER_EMPLACE_BACK_HPP

#include "../concepts2.hpp"

#include <ranges>
#include <utility>

#include <stream9/errors.hpp>

namespace stream9::container {

namespace emplace_back_ {

    using std::ranges::range_reference_t;
    using std::forward;

    struct api
    {
        template<typename C, typename... Args>
        range_reference_t<C>
        operator()(C& s, Args&&... args) const
            requires emplacable_sequence<C, Args...>
        {
            try {
                auto it = s.emplace(s.end(), forward<Args>(args)...);
                return *it;
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace emplace_back_

inline constexpr emplace_back_::api emplace_back;

} // namespace stream9::container

namespace stream9 {

using container::emplace_back;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_EMPLACE_BACK_HPP
