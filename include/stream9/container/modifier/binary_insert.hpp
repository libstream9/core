#ifndef STREAM9_CONTAINER_MODIFIER_BINARY_INSERT_HPP
#define STREAM9_CONTAINER_MODIFIER_BINARY_INSERT_HPP

#include "../concepts2.hpp"

#include <algorithm>
#include <concepts>
#include <functional>

#include <stream9/errors.hpp>
#include <stream9/optional.hpp>

namespace stream9::container {

namespace binary_insert_ {

    using std::ranges::iterator_t;
    using std::ranges::lower_bound;
    using std::projected;
    using std::identity;
    using std::ranges::less;

    template<typename C, typename T, typename Proj, typename Comp>
    concept indirect_strict_weak_order =
        std::indirect_strict_weak_order<
            Comp,
            T const*,
            projected<typename C::const_iterator, Proj> >;

    struct api
    {
        template<typename C, typename T,
                 typename Proj = identity,
                 typename Comp = less >
        opt<iterator_t<C>>
        operator()(C& c, T&& v, Comp comp = {}, Proj proj = {}) const
            requires insertable_sequence<C, T>
                  && indirect_strict_weak_order<C, T, Proj, Comp>
        {
            try {
                auto const it = lower_bound(c.begin(), c.end(), v, comp, proj);
                if (it == c.end() || comp(v, *it)) {
                    return c.insert(it, std::forward<T>(v));
                }
                else {
                    return {};
                }
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace binary_insert_

inline constexpr binary_insert_::api binary_insert;

} // namespace stream9::container

#endif // STREAM9_CONTAINER_MODIFIER_BINARY_INSERT_HPP
