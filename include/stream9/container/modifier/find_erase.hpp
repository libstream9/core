#ifndef STREAM9_CONTAINER_MODIFIER_FIND_ERASE_HPP
#define STREAM9_CONTAINER_MODIFIER_FIND_ERASE_HPP

#include "../namespace.hpp"

#include <stream9/erase.hpp>
#include <stream9/errors.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/ranges/find.hpp>

namespace stream9 {

template<forward_range C, typename T>
bool
find_erase(C& c, T const& v)
    requires requires (C& c, T const& v) {
        rng::find(c, v);
        st9::erase(c, rng::end(c));
    }
{
    try {
        auto i = rng::find(c, v);
        auto j = st9::erase(c, i);

        return j != rng::end(c);
    }
    catch (...) {
        rethrow_error();
    }
}

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_FIND_ERASE_HPP
