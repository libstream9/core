#ifndef STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP
#define STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <algorithm>
#include <concepts>
#include <functional>
#include <iterator>
#include <ranges>

#include <stream9/range.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/size.hpp>

namespace stream9 {

namespace erase_if_ {

    using con::range_erasable;
    using con::nothrow_range_erasable;

    template<typename Proj, typename I>
    concept projection =
       std::indirectly_regular_unary_invocable<Proj, I>;

    template<typename Pred, typename Proj, typename I>
    concept predicate_on_projection =
        std::indirect_unary_predicate<Pred, std::projected<I, Proj>>;

    template<typename Pred, typename Proj>
    constexpr auto
    projected_predicate(Pred pred, Proj proj)
    {
        return [pred, proj](auto& v) { return pred(proj(v)); };
    }

    template<typename C, typename Pred, typename Proj>
    concept has_adl_erase_if =
        requires (C&& r, Pred pred, Proj proj) {
            { erase_if(r, projected_predicate(pred, proj)) }
                    -> std::same_as<rng::size_t<C>>;
        };

    template<typename C, typename Pred, typename Proj>
    concept has_nothrow_adl_erase_if =
        requires (C&& r, Pred pred, Proj proj) {
            { erase_if(r, projected_predicate(pred, proj)) } noexcept
                    -> std::same_as<rng::size_t<C>>;
        };

    struct api {

        template<range_erasable C, typename Pred, typename Proj = std::identity>
        constexpr rng::difference_t<C>
        operator()(C& c, Pred pred, Proj proj = {}) const
            noexcept(nothrow_range_erasable<C>)
            requires (!has_adl_erase_if<C, Pred, Proj>)
                  && std::permutable<rng::iterator_t<C>>
                  && projection<Proj, rng::iterator_t<C>>
                  && predicate_on_projection<Pred, Proj, rng::iterator_t<C>>
        {
            range removed = std::ranges::remove_if(c, pred, proj);
            c.erase(removed.begin(), removed.end());

            return rng::size(removed);
        }

        template<rng::forward_range C, typename Pred, typename Proj = std::identity>
        constexpr auto
        operator()(C& c, Pred pred, Proj proj = {}) const
            noexcept(has_nothrow_adl_erase_if<C, Pred, Proj>)
            requires has_adl_erase_if<C, Pred, Proj>
        {
            return erase_if(c, projected_predicate(pred, proj));
        }

    };

} // namespace erase_if_

inline constexpr erase_if_::api erase_if;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_ERASE_IF_HPP
