#ifndef STREAM9_CONTAINER_MODIFIER_ERASE_PREFIX_HPP
#define STREAM9_CONTAINER_MODIFIER_ERASE_PREFIX_HPP

#include "../concepts.hpp"
#include "../namespace.hpp"

#include <stream9/errors.hpp>
#include <stream9/ranges/begin.hpp>
#include <stream9/ranges/concepts.hpp>
#include <stream9/ranges/end.hpp>
#include <stream9/ranges/prefix.hpp>

namespace stream9 {

namespace erase_prefix_ {

    using con::range_erasable;
    using con::nothrow_range_erasable;
    using rng::iterator_t;

    struct api
    {
        template<range_erasable C>
        static constexpr iterator_t<C>
        operator()(C&& c, rng::size_t<C> n)
            noexcept(nothrow_range_erasable<C>)
        {
            try {
                auto r = rng::prefix(c, n);
                return c.erase(r.begin(), r.end());
            }
            catch (...) {
                rethrow_error();
            }
        }
    };

} // namespace erase_prefix_

inline constexpr erase_prefix_::api erase_prefix;

} // namespace stream9

#endif // STREAM9_CONTAINER_MODIFIER_ERASE_PREFIX_HPP
