#ifndef STREAM9_CONTAINER_DETAIL_TABLE_IPP
#define STREAM9_CONTAINER_DETAIL_TABLE_IPP

#include <stream9/find_if.hpp>
#include <stream9/json.hpp>
#include <stream9/ranges/tuple.hpp>
#include <stream9/to_string.hpp>

namespace stream9 {

template<typename K, typename V, typename T>
table<K, V, T>::
table(std::initializer_list<entry> x)
    : m_entries { std::move(x) }
{}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
begin() noexcept
{
    return m_entries.begin();
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
end() noexcept
{
    return m_entries.end();
}

template<typename K, typename V, typename T>
table<K, V, T>::const_iterator table<K, V, T>::
begin() const noexcept
{
    return m_entries.begin();
}

template<typename K, typename V, typename T>
table<K, V, T>::const_iterator table<K, V, T>::
end() const noexcept
{
    return m_entries.end();
}

template<typename K, typename V, typename T>
template<typename K2>
V& table<K, V, T>::
operator[](K2 const& x)
    requires equality_comparable<K, K2>
          && constructible<K, K2>
          && default_constructible<V>
{
    try {
        auto i = find(x);
        if (i == end()) {
            i = emplace(x, V());
            return i->value;
        }
        else {
            return i->value;
        }
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
remove_const(const_iterator x)
{
    return const_cast<iterator>(x);
}

template<typename K, typename V, typename T>
table<K, V, T>::size_type table<K, V, T>::
size() const noexcept
{
    return m_entries.size();
}

template<typename K, typename V, typename T>
bool table<K, V, T>::
empty() const noexcept
{
    return m_entries.empty();
}

template<typename K, typename V, typename T>
template<typename K2>
table<K, V, T>::const_iterator table<K, V, T>::
find(K2 const& x) const
    noexcept(nothrow_equality_comparable<K, K2>)
    requires equality_comparable<K, K2>
{
    try {
        return st9::find_if(m_entries, [&](auto& e) { return e.key == x; });
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
template<typename K2>
table<K, V, T>::iterator table<K, V, T>::
find(K2 const& x)
    noexcept(nothrow_equality_comparable<K, K2>)
    requires equality_comparable<K, K2>
{
    try {
        return st9::find_if(m_entries, [&](auto& e) { return e.key == x; });
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
insert(entry&& x)
{
    try {
        return m_entries.insert(std::move(x));
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
insert_at(const_iterator i, entry&& x)
{
    try {
        return m_entries.insert(i, std::move(x));
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
template<typename K2, typename V2>
table<K, V, T>::iterator table<K, V, T>::
emplace(K2&& k, V2&& v)
    requires constructible<K, K2>
          && constructible<V, V2>
{
    try {
        return m_entries.insert(
            entry(std::forward<K2>(k), std::forward<V2>(v)) );
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
template<typename K2, typename V2>
table<K, V, T>::iterator table<K, V, T>::
emplace_at(const_iterator i, K2&& k, V2&& v)
    requires constructible<K, K2>
          && constructible<V, V2>
{
    try {
        return m_entries.insert(i,
            entry(std::forward<K2>(k), std::forward<V2>(v)) );
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
erase(const_iterator x) noexcept
{
    return m_entries.erase(x);
}

template<typename K, typename V, typename T>
table<K, V, T>::iterator table<K, V, T>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_entries.erase(first, last);
}

template<typename K, typename V, typename T>
void table<K, V, T>::
clear() noexcept
{
    m_entries.clear();
}

template<typename K, typename V, typename T>
void table<K, V, T>::
resize(size_type x)
{
    try {
        m_entries.resize(x);
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
void table<K, V, T>::
reserve(size_type x)
{
    try {
        m_entries.reserve(x);
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
void table<K, V, T>::
shrink_to_fit()
{
    try {
        m_entries.shrink_to_fit();
    }
    catch (...) { rethrow_error(); }
}

template<typename K, typename V, typename T>
bool table<K, V, T>::
operator==(table const& x) const
    noexcept(nothrow_equality_comparable<entry>)
    requires equality_comparable<entry>
{
    return m_entries == x.m_entries;
}

template<typename K, typename V, typename T>
std::ostream&
operator<<(std::ostream& os, table<K, V, T> const& x)
{
    try {
        return os << json::value_from(x);
    }
    catch (...) { rethrow_error(); }
}

namespace json {

template<typename K, typename V, typename T>
void
tag_invoke(value_from_tag, value& jv, table<K, V, T> const& x)
    requires requires (K const& k) { to_string(k); }
          && requires (V const& v) { json::value_from(v); }
{
    try {
        auto& obj = jv.emplace_object();

        for (auto& [k, v]: x) {
            obj.emplace(to_string(k), v);
        }
    }
    catch (...) { rethrow_error(); }
}

} // namespace json

} // namespace stream9

#endif // STREAM9_CONTAINER_DETAIL_TABLE_IPP
