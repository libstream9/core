#ifndef STREAM9_CONTAINER_SORTED_ARRAY_IPP
#define STREAM9_CONTAINER_SORTED_ARRAY_IPP

#include <stream9/ranges/binary_find.hpp>

#include <algorithm>

namespace stream9 {

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::
sorted_array(allocator_type const& alloc)
    : m_array { alloc }
{}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::
sorted_array(std::initializer_list<T> il)
    try : m_array { std::move(il) }
{
    std::ranges::sort(m_array, C(), P());
}
catch (...) {
    rethrow_error();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename... Us>
sorted_array<T, C, P, M, A>::
sorted_array(Us&&... values)
    requires (is_insertable_v<Us> && ...)
    try : m_array { std::forward<Us>(values)... }
{
    std::ranges::sort(m_array, C(), P());
}
catch (...) {
    rethrow_error();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
begin() noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
end() noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
begin() const noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
end() const noexcept
{
    return m_array.begin();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::reference  sorted_array<T, C, P, M, A>::
at(size_type pos) noexcept
{
    return m_array.at(pos);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_reference sorted_array<T, C, P, M, A>::
at(size_type pos) const noexcept
{
    return m_array.at(pos);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::reference  sorted_array<T, C, P, M, A>::
operator[](size_type pos) noexcept
{
    return m_array[pos];
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_reference sorted_array<T, C, P, M, A>::
operator[](size_type pos) const noexcept
{
    return m_array[pos];
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::reference sorted_array<T, C, P, M, A>::
front() noexcept
{
    return m_array.front();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_reference sorted_array<T, C, P, M, A>::
front() const noexcept
{
    return m_array.front();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::reference sorted_array<T, C, P, M, A>::
back() noexcept
{
    return m_array.back();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_reference sorted_array<T, C, P, M, A>::
back() const noexcept
{
    return m_array.back();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::pointer  sorted_array<T, C, P, M, A>::
data() noexcept
{
    return m_array.data();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_pointer sorted_array<T, C, P, M, A>::
data() const noexcept
{
    return m_array.data();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
bool sorted_array<T, C, P, M, A>::
empty() const noexcept
{
    return m_array.empty();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::size_type sorted_array<T, C, P, M, A>::
size() const noexcept
{
    return m_array.size();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
find(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
find(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
find(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
find(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    return rng::binary_find(*this, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
lower_bound(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::lower_bound;
    P proj {};
    return lower_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
lower_bound(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::lower_bound;
    P proj {};
    return lower_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
lower_bound(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::lower_bound;

    return lower_bound(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
lower_bound(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::lower_bound;

    return lower_bound(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
upper_bound(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::upper_bound;
    P proj {};
    return upper_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
upper_bound(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::upper_bound;
    P proj {};
    return upper_bound(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
upper_bound(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::upper_bound;

    return upper_bound(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::const_iterator sorted_array<T, C, P, M, A>::
upper_bound(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::upper_bound;

    return upper_bound(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
std::ranges::subrange<typename sorted_array<T, C, P, M, A>::iterator> sorted_array<T, C, P, M, A>::
equal_range(T const& v)
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::equal_range;
    P proj {};
    return equal_range(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
std::ranges::subrange<typename sorted_array<T, C, P, M, A>::const_iterator> sorted_array<T, C, P, M, A>::
equal_range(T const& v) const
    noexcept(is_nothrow_comparable_v)
{
    using std::ranges::equal_range;
    P proj {};
    return equal_range(m_array, proj(v), C(), proj);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
std::ranges::subrange<typename sorted_array<T, C, P, M, A>::iterator> sorted_array<T, C, P, M, A>::
equal_range(U const& v)
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::equal_range;

    return equal_range(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
std::ranges::subrange<typename sorted_array<T, C, P, M, A>::const_iterator> sorted_array<T, C, P, M, A>::
equal_range(U const& v) const
    noexcept(is_nothrow_comparable_with_v<U>)
    requires is_comparable_with_v<U>
{
    using std::ranges::equal_range;

    return equal_range(m_array, v, C(), P());
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::size_type sorted_array<T, C, P, M, A>::
capacity() const noexcept
{
    return m_array.capacity();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::size_type sorted_array<T, C, P, M, A>::
min_capacity() const noexcept
{
    return m_array.min_capacity();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::size_type sorted_array<T, C, P, M, A>::
max_capacity() const noexcept
{
    return m_array.max_capacity();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::allocator_type sorted_array<T, C, P, M, A>::
get_allocator() const noexcept
{
    return m_array.get_allocator();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
remove_const(const_iterator i)
{
    return m_array.remove_const(i);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename U>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
insert(U&& value)
    requires std::is_constructible_v<T, U>
{
    try {
        using std::ranges::upper_bound;

        T v { std::forward<U>(value) };
        C comp {};
        P proj {};
        auto it = upper_bound(m_array, proj(v), comp, proj);
        it = m_array.insert(it, std::move(v));

        return it;
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<rng::input_range R>
void sorted_array<T, C, P, M, A>::
insert(R&& range)
    requires (!std::is_constructible_v<T, R>)
          && std::is_constructible_v<T, rng::reference_t<R>>
{
    using std::ranges::sort;
    using std::ranges::inplace_merge;

    try {
        auto [mid, _] = m_array.insert(m_array.end(), std::forward<R>(range));

        // nothrow from here
        C comp {};
        P proj {};
        sort(mid, m_array.end(), comp, proj);
        inplace_merge(m_array.begin(), mid, m_array.end(), comp, proj);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename... Us>
void sorted_array<T, C, P, M, A>::
insert(Us&&... values)
    requires (sizeof...(Us) > 1) && (is_insertable_v<Us> && ...)
{
    using std::ranges::sort;
    using std::ranges::inplace_merge;

    try {
        auto [mid, _] = m_array.insert(m_array.end(), std::forward<Us>(values)...);

        // nothrow from here
        C comp {};
        P proj {};
        sort(mid, m_array.end(), comp, proj);
        inplace_merge(m_array.begin(), mid, m_array.end(), comp, proj);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename... Args>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
emplace(Args&&... args)
    requires std::is_constructible_v<T, Args...>
{
    try {
        using std::ranges::upper_bound;

        T v { std::forward<Args>(args)... };
        C comp {};
        P proj {};
        auto it = upper_bound(m_array, proj(v), comp, proj);
        it = m_array.insert(it, std::move(v));

        return it;
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
erase(const_iterator pos) noexcept
{
    return m_array.erase(pos);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
sorted_array<T, C, P, M, A>::iterator sorted_array<T, C, P, M, A>::
erase(const_iterator first, const_iterator last) noexcept
{
    return m_array.erase(first, last);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void sorted_array<T, C, P, M, A>::
clear() noexcept
{
    m_array.clear();
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<typename... Args>
void sorted_array<T, C, P, M, A>::
resize(size_type count, Args&&... args)
    requires std::is_constructible_v<T, Args...>
{
    using std::ranges::upper_bound;
    using std::rotate;

    try {
        auto n = size();
        m_array.resize(count, std::forward<Args>(args)...);
        if (count > n) {
            C comp {};
            P proj {};
            auto f = m_array.begin();
            auto i1 = f + n;
            auto i2 = upper_bound(f, i1, proj(*i1), comp, proj);
            rotate(i2, i1, m_array.end());
        }
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void sorted_array<T, C, P, M, A>::
reserve(size_type n)
{
    try {
        m_array.reserve(n);
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void sorted_array<T, C, P, M, A>::
shrink_to_fit()
{
    try {
        m_array.shrink_to_fit();
    }
    catch (...) {
        rethrow_error();
    }
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void sorted_array<T, C, P, M, A>::
swap(sorted_array& o) noexcept
{
    m_array.swap(o);
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<std::ranges::input_range R>
bool sorted_array<T, C, P, M, A>::
operator==(R const& other) const
    noexcept(noexcept(std::declval<T>() == std::declval<std::ranges::range_value_t<R>>()))
    requires std::equality_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return m_array == other.m_array;
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<std::ranges::input_range R>
auto sorted_array<T, C, P, M, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() <=> std::declval<std::ranges::range_value_t<R>>()))
    requires std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>
{
    return m_array <=> other.m_array;
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
template<std::ranges::input_range R>
std::partial_ordering sorted_array<T, C, P, M, A>::
operator<=>(R const& other) const
    noexcept(noexcept(std::declval<T>() < std::declval<std::ranges::range_value_t<R>>()))
    requires (!std::three_way_comparable_with<T, std::ranges::range_reference_t<R>>)
          && requires (T v1, std::ranges::range_reference_t<R> v2) {
              { v1 < v2 } -> std::convertible_to<bool>;
          }
{
    return m_array <=> other.m_array;
}

template<typename T, typename C, typename P, std::int64_t M, typename A>
void
swap(sorted_array<T, C, P, M, A>& lhs,
     sorted_array<T, C, P, M, A>& rhs) noexcept
{
    lhs.swap(rhs);
}

} // namespace stream9

namespace stream9::json {

template<typename T, typename C, typename P, std::int64_t M, typename A>
void
tag_invoke(value_from_tag, value& v,
           sorted_array<T, C, P, M, A> const& a)
{
    v = json::value_from(a.m_array);
}

} // namespace stream9::json

#endif // STREAM9_CONTAINER_SORTED_ARRAY_IPP
