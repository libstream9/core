#ifndef STREAM9_CONTAINER_KEY_VALUE_HPP
#define STREAM9_CONTAINER_KEY_VALUE_HPP

#include <ostream>

namespace stream9 {

template<typename K, typename V>
struct key_value
{
    K key;
    V value;

    bool operator==(key_value const&) const = default;
    auto operator<=>(key_value const&) const = default;
};

template<typename K, typename V>
std::ostream&
operator<<(std::ostream& os, key_value<K, V> const& x)
{
    os << x.key << ": " << x.value;
    return os;
}

} // namespace stream9

#endif // STREAM9_CONTAINER_KEY_VALUE_HPP
