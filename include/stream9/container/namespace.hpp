#ifndef STREAM9_CONTAINER_NAMESPACE_HPP
#define STREAM9_CONTAINER_NAMESPACE_HPP

namespace stream9::container {}

namespace stream9 {

namespace con { using namespace container; }

} // namespace stream9

#endif // STREAM9_CONTAINER_NAMESPACE_HPP
