#ifndef STREAM9_SOURCE_LOCATION_HPP
#define STREAM9_SOURCE_LOCATION_HPP

#include <stream9/json.hpp>

#include <source_location>

namespace stream9 {

class source_location
{
public:
    // essential
    source_location(std::source_location l = std::source_location::current()) noexcept
        : m_loc { std::move(l) }
    {}

    source_location(source_location const&) = default;
    source_location& operator=(source_location const&) = default;

    source_location(source_location&&) noexcept = default;
    source_location& operator=(source_location&&) noexcept = default;

    operator std::source_location () const noexcept { return m_loc; }

    ~source_location() noexcept = default;

    // accessor
    auto line() const noexcept { return m_loc.line(); }
    auto column() const noexcept { return m_loc.column(); }
    auto file_name() const noexcept { return m_loc.file_name(); }
    auto function_name() const noexcept { return m_loc.function_name(); }

private:
    std::source_location m_loc;
};

namespace json {

inline void
tag_invoke(value_from_tag, value& jv, source_location const& l)
{
    jv = json::object {
        { "line", l.line() },
        { "column", l.column() },
        { "file_name", l.file_name() },
        { "function_name", l.function_name() },
    };
}

} // namespace json

} // namespace stream9

#endif // STREAM9_SOURCE_LOCATION_HPP
