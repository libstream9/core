#ifndef STREAM9_OPTIONAL_FREE_FUNCTION_HPP
#define STREAM9_OPTIONAL_FREE_FUNCTION_HPP

#include <concepts>
#include <ostream>
#include <type_traits>
#include <utility>

#include <stream9/null.hpp>

namespace stream9 {

template<typename T, typename U>
constexpr bool
operator==(optional<T> const& lhs, optional<U> const& rhs)
    noexcept(noexcept(std::declval<T>() == std::declval<U>()))
    requires requires (T t, U u) { { t == u } -> std::same_as<bool>; };

template<typename T, typename U>
constexpr auto operator<=>(optional<T> const& lhs, optional<U> const& rhs)
    noexcept(noexcept(std::declval<T>() <=> std::declval<U>()))
    requires requires (T t, U u) { t <=> u; };

template<typename T, typename U>
constexpr bool operator==(optional<T> const& lhs, U const& rhs)
    noexcept(noexcept(std::declval<T>() == std::declval<U>()))
    requires requires (T t, U u) { { t == u } -> std::same_as<bool>; };

template<typename T, typename U>
constexpr auto operator<=>(optional<T> const& lhs, U const& rhs)
    noexcept(noexcept(std::declval<T>() <=> std::declval<U>()))
    requires requires (T t, U u) { t <=> u; };

template<typename T>
constexpr bool operator==(optional<T> const&, null_t) noexcept;

template<typename T>
constexpr auto operator<=>(optional<T> const&, null_t) noexcept;

template<typename T, std::invocable<T const&> F>
constexpr optional<std::invoke_result_t<F, T const&>>
transform(optional<T> const&, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T const&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T const&>>,
                std::invoke_result_t<F, T const&> >);

template<typename T, std::invocable<T&> F>
constexpr optional<std::invoke_result_t<F, T&>>
transform(optional<T>&, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T&>>,
                std::invoke_result_t<F, T&> >);

template<typename T, std::invocable<T const&> F>
constexpr optional<std::invoke_result_t<F, T const&>>
operator|(optional<T> const& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T const&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T const&>>,
                std::invoke_result_t<F, T const&> >);

template<typename T, std::invocable<T&> F>
constexpr optional<std::invoke_result_t<F, T&>>
operator|(optional<T>& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T&>>,
                std::invoke_result_t<F, T&> >);

template<typename T, typename U>
constexpr optional<T>
static_optional_cast(optional<U> const&)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { static_cast<T>(u) } -> std::same_as<T>;
    };

template<typename T, typename U>
constexpr optional<T>
dynamic_optional_cast(optional<U> const&)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { dynamic_cast<T>(u) } -> std::same_as<T>;
    };

template<typename T, typename U>
constexpr optional<T>
const_optional_cast(optional<U> const&)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { const_cast<T>(u) } -> std::same_as<T>;
    };

template<typename T, typename U>
constexpr optional<T>
reinterpret_optional_cast(optional<U> const&)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { reinterpret_cast<T>(u) } -> std::same_as<T>;
    };

template<typename CharT, typename Traits, typename T>
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, optional<T> const& v)
    requires requires (std::basic_ostream<CharT, Traits>& os, T v) { os << v; };

} // namespace stream9

#include "free_function.ipp"

#endif // STREAM9_OPTIONAL_FREE_FUNCTION_HPP
