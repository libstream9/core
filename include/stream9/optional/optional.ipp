#ifndef STREAM9_OPTIONAL_OPTIONAL_IPP
#define STREAM9_OPTIONAL_OPTIONAL_IPP

#include <cassert>

namespace stream9 {

/*
 * class optional<T>
 */
template<typename T>
constexpr optional<T>::
optional(null_t) noexcept
{}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U const&>
          && (!std::constructible_from<T, optional<U> const&>)
          && (!std::convertible_to<optional<U> const&, T>)
constexpr optional<T>::
optional(optional<U> const& v)
    noexcept(std::is_nothrow_constructible_v<T, U const&>)
    : base_type { v }
{}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U&>
          && (!std::constructible_from<T, optional<U&> const&>)
          && (!std::convertible_to<optional<U&> const&, T>)
constexpr optional<T>::
optional(optional<U&> const& v)
    noexcept(std::is_nothrow_constructible_v<T, U&>)
{
    if (v) {
        this->construct(*v);
    }
}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U&>
          && (!std::constructible_from<T, optional<U&> const&>)
          && (!std::convertible_to<optional<U&> const&, T>)
constexpr optional<T>& optional<T>::
operator=(optional<U&> const& v)
    noexcept(std::is_nothrow_assignable_v<T, U&>)
{
    if (this->engaged()) {
        if (v.has_value()) {
            this->assign(*v);
        }
        else {
            this->reset();
        }
    }
    else {
        if (v.has_value()) {
            this->construct(*v);
        }
    }

    return *this;
}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U const&>
          && (!std::constructible_from<T, optional<U> const&>)
          && (!std::convertible_to<optional<U> const&, T>)
constexpr optional<T>& optional<T>::
operator=(optional<U> const& v)
    noexcept(std::is_nothrow_assignable_v<T, U const&>)
{
    if (this->engaged()) {
        if (v.has_value()) {
            this->assign(*v);
        }
        else {
            this->reset();
        }
    }
    else {
        if (v.has_value()) {
            this->construct(*v);
        }
    }

    return *this;
}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U&&>
          && (!std::constructible_from<T, optional<U>&&>)
          && (!std::convertible_to<optional<U>&&, T>)
constexpr optional<T>::
optional(optional<U>&& v)
    noexcept(std::is_nothrow_constructible_v<T, U&&>)
    : base_type { std::move(v) }
{}

template<typename T>
template<typename U>
    requires std::constructible_from<T, U&&>
          && (!std::constructible_from<T, optional<U>&&>)
          && (!std::convertible_to<optional<U>&&, T>)
constexpr optional<T>& optional<T>::
operator=(optional<U>&& v)
    noexcept(std::is_nothrow_assignable_v<T, U&&>)
{
    if (this->engaged()) {
        if (v.has_value()) {
            this->assign(std::move(*v));
        }
        else {
            this->reset();
        }
    }
    else {
        if (v.has_value()) {
            this->construct(std::move(*v));
        }
    }

    return *this;
}

template<typename T>
template<typename U>
    requires (std::constructible_from<T, U&&>)
          && (!std::same_as<std::remove_cvref_t<U>, optional<T>>)
          && (!std::same_as<std::remove_cvref_t<U>, std::in_place_t>)
constexpr optional<T>::
optional(U&& v)
    noexcept(std::is_nothrow_constructible_v<T, U&&>)
    : base_type { std::in_place, std::forward<U>(v) }
{}

template<typename T>
template<typename U>
    requires (std::constructible_from<T, U&&>)
          && (!std::same_as<std::remove_cvref_t<U>, optional<T>>)
          && (!std::same_as<std::remove_cvref_t<U>, std::in_place_t>)
constexpr optional<T>& optional<T>::
operator=(U&& v)
    noexcept(std::is_nothrow_assignable_v<T, U&&>)
{
    if (this->engaged()) {
        this->assign(std::forward<U>(v));
    }
    else {
        this->construct(std::forward<U>(v));
    }

    return *this;
}

template<typename T>
template<typename... Args>
    requires std::constructible_from<T, Args...>
constexpr optional<T>::
optional(std::in_place_t i, Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, Args...>)
    : base_type { i, std::forward<Args>(args)... }
{}

template<typename T>
template<typename U, typename... Args>
    requires std::constructible_from<T, std::initializer_list<U>&, Args...>
constexpr optional<T>::
optional(std::in_place_t i, std::initializer_list<U> ilist, Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, std::initializer_list<U>, Args...>)
    : base_type { i, ilist, std::forward<Args>(args)... }
{}

template<typename T>
template<typename X>
constexpr auto&& optional<T>::
value(this X&& self) noexcept
{
    assert(self.engaged());
    return std::forward<X>(self).get();
}

template<typename T>
template<typename X, typename Fn>
constexpr auto&& optional<T>::
or_throw(this X&& self, Fn&& fn)
    requires std::invocable<Fn>
{
    if (self.has_value()) {
        return std::forward<X>(self).value();
    }
    else {
        throw fn();
    }
}

template<typename T>
template<typename X>
constexpr auto&& optional<T>::
operator*(this X&& self) noexcept
{
    assert(self.engaged());
    return std::forward<X>(self).get();
}

template<typename T>
constexpr optional<T>::const_pointer optional<T>::
operator->() const noexcept
    requires (!has_transitive_arrow<T>)
{
    return &value();
}

template<typename T>
constexpr optional<T>::pointer optional<T>::
operator->() noexcept
    requires (!has_transitive_arrow<T>)
{
    return &value();
}

template<typename T>
constexpr decltype(auto) optional<T>::
operator->() const
    noexcept(noexcept(value().operator->()))
    requires has_transitive_arrow<T>
{
    return value().operator->();
}

template<typename T>
constexpr decltype(auto) optional<T>::
operator->()
    noexcept(noexcept(value().operator->()))
    requires has_transitive_arrow<T>
{
    return value().operator->();
}

template<typename T>
constexpr bool optional<T>::
has_value() const noexcept
{
    return this->engaged();
}

template<typename T>
template<typename... Args>
constexpr T& optional<T>::
emplace(Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, Args...>)
    requires std::constructible_from<T, Args...>
{
    this->reset();
    this->construct(std::forward<Args>(args)...);
    return this->get();
}

template<typename T>
template<typename U, typename... Args>
constexpr T& optional<T>::
emplace(std::initializer_list<U> ilist, Args&&... args)
    noexcept(std::is_nothrow_constructible_v<T, std::initializer_list<U>&, Args...>)
    requires std::constructible_from<T, std::initializer_list<U>&, Args...>
{
    this->reset();
    this->construct(ilist, std::forward<Args>(args)...);
    return this->get();
}

template<typename T>
constexpr void optional<T>::
reset() noexcept
{
    base_type::reset();
}

template<typename T>
constexpr void optional<T>::
swap(optional& other) noexcept
{
    using std::swap;

    if (has_value() == other.has_value()) {
        if (has_value()) {
            swap(this->get(), other.get());
        }
    }
    else {
        if (has_value()) {
            other.construct(std::move(*this).get());
            reset();
        }
        else {
            this->construct(std::move(other).get());
            other.reset();
        }
    }
}

template<typename T>
constexpr optional<T>::
operator bool () const noexcept
{
    return has_value();
}

} // namespace stream9

#endif // STREAM9_OPTIONAL_OPTIONAL_IPP
