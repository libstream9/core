#ifndef STREAM9_OPTIONAL_REFERENCE_IPP
#define STREAM9_OPTIONAL_REFERENCE_IPP

#include <memory>
#include <type_traits>
#include <utility>

namespace stream9 {

template<typename T>
constexpr optional<T&>::
optional(null_t) noexcept
{}

template<typename T>
constexpr optional<T&>& optional<T&>::
operator=(null_t) noexcept
{
    reset();
    return *this;
}

template<typename T>
template<typename U>
    requires std::is_convertible_v<U, T&>
constexpr optional<T&>::
optional(optional<U> const& v) noexcept
{
    if (v.has_value()) {
        m_ptr = std::addressof(v.value());
    }
}

template<typename T>
template<typename U>
    requires std::is_convertible_v<U, T&>
constexpr optional<T&>& optional<T&>::
operator=(optional<U> const& v) noexcept
{
    if (v.has_value()) {
        m_ptr = std::addressof(v.value());
    }
    else {
        m_ptr = nullptr;
    }

    return *this;
}

template<typename T>
template<typename U>
    requires std::is_convertible_v<U, T&>
          && (!is_optional_v<U>)
constexpr optional<T&>::
optional(U&& v) noexcept
    : m_ptr { std::addressof(v) }
{}

template<typename T>
template<typename U>
    requires std::is_convertible_v<U, T&>
          && (!is_optional_v<U>)
constexpr optional<T&>& optional<T&>::
operator=(U&& v) noexcept
{
    m_ptr = std::addressof(v);

    return *this;
}

template<typename T>
constexpr T& optional<T&>::
value() const noexcept
{
    return *m_ptr;
}

template<typename T>
constexpr T& optional<T&>::
operator*() const noexcept
{
    return value();
}

template<typename T>
constexpr optional<T&>::pointer optional<T&>::
operator->() const noexcept
{
    return m_ptr;
}

template<typename T>
constexpr bool optional<T&>::
has_value() const noexcept
{
    return m_ptr;
}

template<typename T>
constexpr void optional<T&>::
reset() noexcept
{
    m_ptr = nullptr;
}

template<typename T>
constexpr void optional<T&>::
swap(optional& other) noexcept
{
    using std::swap;
    swap(m_ptr, other.m_ptr);
}

template<typename T>
constexpr optional<T&>::
operator bool () const noexcept
{
    return has_value();
}

} // namespace stream9

#endif // STREAM9_OPTIONAL_REFERENCE_IPP
