#ifndef STREAM9_OPTIONAL_OPTIONAL_HPP
#define STREAM9_OPTIONAL_OPTIONAL_HPP

#include "base.hpp"

#include <stream9/null.hpp>
#include <stream9/transitive_arrow.hpp>

namespace stream9 {

template<typename T>
class optional : private optional_::base<T>
{
    static_assert(!std::is_reference_v<T>);

    using base_type = optional_::base<T>;
public:
    using const_pointer = base_type::const_pointer;
    using pointer = base_type::pointer;

    static constexpr bool transitive_arrow = true;

public:
    constexpr optional() noexcept = default;

    constexpr optional(optional const&)
        noexcept(std::is_nothrow_copy_constructible_v<T>) = default;

    constexpr optional& operator=(optional const&)
        noexcept(std::is_nothrow_copy_assignable_v<T>) = default;

    constexpr optional(optional&&)
        noexcept(std::is_nothrow_move_constructible_v<T>) = default;

    constexpr optional& operator=(optional&&)
        noexcept(std::is_nothrow_move_assignable_v<T>) = default;

    constexpr ~optional() noexcept = default;

    constexpr optional(null_t) noexcept;

    template<typename U>
        requires std::constructible_from<T, U const&>
              && (!std::constructible_from<T, optional<U> const&>)
              && (!std::convertible_to<optional<U> const&, T>)
    constexpr optional(optional<U> const&)
        noexcept(std::is_nothrow_constructible_v<T, U const&>);

    template<typename U>
        requires std::constructible_from<T, U const&>
              && (!std::constructible_from<T, optional<U> const&>)
              && (!std::convertible_to<optional<U> const&, T>)
    constexpr optional& operator=(optional<U> const&)
        noexcept(std::is_nothrow_assignable_v<T, U const&>);

    template<typename U>
        requires std::constructible_from<T, U&&>
              && (!std::constructible_from<T, optional<U>&&>)
              && (!std::convertible_to<optional<U>&&, T>)
    constexpr optional(optional<U>&&)
        noexcept(std::is_nothrow_constructible_v<T, U&&>);

    template<typename U>
        requires std::constructible_from<T, U&&>
              && (!std::constructible_from<T, optional<U>&&>)
              && (!std::convertible_to<optional<U>&&, T>)
    constexpr optional& operator=(optional<U>&&)
        noexcept(std::is_nothrow_assignable_v<T, U&&>);

    template<typename U = T>
        requires (std::constructible_from<T, U&&>)
              && (!std::same_as<std::remove_cvref_t<U>, optional<T>>)
              && (!std::same_as<std::remove_cvref_t<U>, std::in_place_t>)
    constexpr optional(U&&)
        noexcept(std::is_nothrow_constructible_v<T, U&&>);

    template<typename U = T>
        requires (std::constructible_from<T, U&&>)
              && (!std::same_as<std::remove_cvref_t<U>, optional<T>>)
              && (!std::same_as<std::remove_cvref_t<U>, std::in_place_t>)
    constexpr optional& operator=(U&&)
        noexcept(std::is_nothrow_assignable_v<T, U&&>);

    template<typename... Args>
        requires std::constructible_from<T, Args...>
    constexpr optional(std::in_place_t, Args&&...)
        noexcept(std::is_nothrow_constructible_v<T, Args...>);

    template<typename U, typename... Args>
        requires std::constructible_from<T, std::initializer_list<U>&, Args...>
    constexpr optional(std::in_place_t, std::initializer_list<U>, Args&&...)
        noexcept(std::is_nothrow_constructible_v<T, std::initializer_list<U>, Args...>);

    // conversion from optional reference
    template<typename U>
        requires std::constructible_from<T, U&>
              && (!std::constructible_from<T, optional<U&> const&>)
              && (!std::convertible_to<optional<U&> const&, T>)
    constexpr optional(optional<U&> const&)
        noexcept(std::is_nothrow_constructible_v<T, U&>);

    template<typename U>
        requires std::constructible_from<T, U&>
              && (!std::constructible_from<T, optional<U&> const&>)
              && (!std::convertible_to<optional<U&> const&, T>)
    constexpr optional& operator=(optional<U&> const&)
        noexcept(std::is_nothrow_assignable_v<T, U&>);

    // query
    template<typename X>
    constexpr auto&&
    value(this X&& self) noexcept;

    template<typename X, typename Fn>
    constexpr auto&&
    or_throw(this X&& self, Fn&& fn)
        requires std::invocable<Fn>;

    template<typename X>
    constexpr auto&&
    operator*(this X&& self) noexcept;

    constexpr const_pointer operator->() const noexcept
        requires (!has_transitive_arrow<T>);

    constexpr pointer operator->() noexcept
        requires (!has_transitive_arrow<T>);

    constexpr decltype(auto) operator->() const
        noexcept(noexcept(value().operator->()))
        requires has_transitive_arrow<T>;

    constexpr decltype(auto) operator->()
        noexcept(noexcept(value().operator->()))
        requires has_transitive_arrow<T>;

    constexpr bool has_value() const noexcept;

    // modifier
    template<typename... Args>
    constexpr T& emplace(Args&&...)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        requires std::constructible_from<T, Args...>;

    template<typename U, typename... Args>
    constexpr T& emplace(std::initializer_list<U>, Args&&...)
        noexcept(std::is_nothrow_constructible_v<T, std::initializer_list<U>&, Args...>)
        requires std::constructible_from<T, std::initializer_list<U>&, Args...>;

    constexpr void reset() noexcept;

    constexpr void swap(optional&) noexcept;

    // conversion
    constexpr explicit operator bool () const noexcept;

private:
    template<typename> friend class optional;
};

} // namespace stream9

#include "optional.ipp"
#include "free_function.hpp"

#endif // STREAM9_OPTIONAL_OPTIONAL_HPP
