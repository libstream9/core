#ifndef STREAM9_OPTIONAL_BASE_HPP
#define STREAM9_OPTIONAL_BASE_HPP

#include <stream9/null.hpp>

#include <memory>
#include <utility>

namespace stream9::optional_ {

template<typename T> struct storage_base;

template<typename T>
    requires std::is_trivially_destructible_v<T>
struct storage_base<T>
{
    union {
        char m_null;
        T m_value;
    };
    bool m_engaged = false;

    constexpr storage_base() noexcept : m_null {} {}

    template<typename... Args>
    constexpr storage_base(std::in_place_t, Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        : m_value { std::forward<Args>(args)... }
        , m_engaged { true }
    {}

    constexpr void destruct() noexcept {}
};

template<typename T>
    requires (!std::is_trivially_destructible_v<T>)
struct storage_base<T>
{
    union {
        char m_null;
        T m_value;
    };
    bool m_engaged = false;

    constexpr storage_base() noexcept : m_null {} {}

    template<typename... Args>
    constexpr storage_base(std::in_place_t, Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        : m_value { std::forward<Args>(args)... }
        , m_engaged { true }
    {}

    constexpr ~storage_base() noexcept
    {
        if (m_engaged) {
            m_value.~T();
        }
    }

    constexpr void destruct() noexcept
    {
        m_value.~T();
    }
};

/*
 * storage
 */
template<typename T> struct storage;

template<typename T>
    requires (!nullable<T>)
struct storage<T> : storage_base<T>
{
    using pointer = T*;
    using const_pointer = T const*;

    using storage_base<T>::storage_base;

    storage() = default;

    template<typename U>
    constexpr storage(storage<U> const& v)
        noexcept(std::is_nothrow_constructible_v<T, U const&>)
    {
        if (v.m_engaged) {
            construct(v.m_value);
        }
    }

    template<typename U>
    constexpr storage(storage<U>&& v)
        noexcept(std::is_nothrow_constructible_v<T, U&&>)
    {
        if (v.m_engaged) {
            construct(std::move(v.m_value));
        }
    }

    constexpr bool engaged() const noexcept
    {
        return this->m_engaged;
    }

    constexpr T& get() & noexcept { return this->m_value; }
    constexpr T const& get() const& noexcept { return this->m_value; }
    constexpr T&& get() && noexcept { return std::move(this->m_value); }
    constexpr T const&& get() const&& noexcept { return std::move(this->m_value); }

    template<typename... Args>
    constexpr void construct(Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
    {
        std::construct_at(
            std::addressof(this->m_value), std::forward<Args>(args)...);
        this->m_engaged = true;
    }

    template<typename U>
    constexpr void assign(U&& v)
        noexcept(std::is_nothrow_assignable_v<T, U&&>)
    {
        this->m_value = std::forward<U>(v);
    }

    constexpr void reset() noexcept
    {
        if (this->m_engaged) {
            this->destruct();
            this->m_engaged = false;
        }
    }

    template<typename> friend struct storage;
};

template<nullable T>
struct storage<T>
{
    using pointer = T*;
    using const_pointer = T const*;

    T m_value;

    constexpr storage()
        noexcept(noexcept(stream9::make_null<T>()))
        : m_value { stream9::make_null<T>() }
    {}

    template<typename... Args>
    constexpr storage(std::in_place_t, Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
        : m_value { std::forward<Args>(args)... }
    {}

    template<typename U>
    constexpr storage(storage<U> const& v)
        noexcept(std::is_nothrow_constructible_v<T, U const&>)
    {
        if (engaged()) {
            construct(v.m_value);
        }
    }

    template<typename U>
    constexpr storage(storage<U>&& v)
        noexcept(std::is_nothrow_constructible_v<T, U&&>)
    {
        if (engaged()) {
            construct(std::move(v.m_value));
        }
    }

    constexpr bool engaged() const noexcept
    {
        return !stream9::is_null(m_value);
    }

    constexpr T& get() & noexcept { return m_value; }
    constexpr T const& get() const& noexcept { return m_value; }
    constexpr T&& get() && noexcept { return std::move(m_value); }
    constexpr T const&& get() const&& noexcept { return std::move(m_value); }

    template<typename... Args>
    constexpr void construct(Args&&... args)
        noexcept(std::is_nothrow_constructible_v<T, Args...>)
    {
        std::construct_at(
            std::addressof(m_value), std::forward<Args>(args)...);
    }

    template<typename U>
    constexpr void assign(U&& v)
        noexcept(std::is_nothrow_assignable_v<T, U&&>)
    {
        m_value = std::forward<U>(v);
    }

    constexpr void reset() noexcept
    {
        stream9::set_null(m_value);
    }

    template<typename> friend struct storage;
};

template<typename T>
struct base : storage<T>
{
    using storage<T>::storage;

    base() = default;

    // copy constructor
    base(base const&) noexcept
        requires std::is_trivially_copy_constructible_v<T> = default;

    constexpr base(base const& v)
        noexcept(std::is_nothrow_copy_constructible_v<T>)
        requires std::is_copy_constructible_v<T>
              && (!std::is_trivially_copy_constructible_v<T>)
        : storage<T> {}
    {
        if (v.engaged()) {
            this->construct(v.get());
        }
    }

    base(base const&)
        requires (!std::is_copy_constructible_v<T>) = delete;

    // copy assignment
    base& operator=(base const&) noexcept
        requires std::is_trivially_copy_assignable_v<T> = default;

    constexpr base& operator=(base const& v)
        noexcept(std::is_nothrow_copy_assignable_v<T>)
        requires std::is_copy_assignable_v<T>
              && (!std::is_trivially_copy_assignable_v<T>)
    {
        if (this->engaged() == v.engaged()) {
            if (this->engaged()) {
                this->assign(v.get());
            }
        }
        else {
            if (this->engaged()) {
                this->reset();
            }
            else {
                this->construct(v.get());
            }
        }

        return *this;
    }

    base& operator=(base const&)
        requires (!std::is_copy_assignable_v<T>) = delete;

    // move constructor
    base(base&&) noexcept
        requires std::is_trivially_move_constructible_v<T> = default;

    constexpr base(base&& v)
        noexcept(std::is_nothrow_move_constructible_v<T>)
        requires std::is_move_constructible_v<T>
              && (!std::is_trivially_move_constructible_v<T>)
        : storage<T> {}
    {
        if (v.engaged()) {
            this->construct(std::move(v).get());
        }
    }

    base(base&&)
        requires (!std::is_move_constructible_v<T>) = delete;

    // move assignment
    base& operator=(base&&) noexcept
        requires std::is_trivially_move_assignable_v<T> = default;

    constexpr base& operator=(base&& v)
        noexcept(std::is_nothrow_move_assignable_v<T>)
        requires std::is_move_assignable_v<T>
              && (!std::is_trivially_move_assignable_v<T>)
    {
        if (this->engaged() == v.engaged()) {
            if (this->engaged()) {
                this->assign(std::move(v).get());
            }
        }
        else {
            if (this->engaged()) {
                this->reset();
            }
            else {
                this->construct(std::move(v).get());
            }
        }

        return *this;
    }

    base& operator=(base&&)
        requires (!std::is_move_assignable_v<T>) = delete;
};

} // namespace stream9::optional_

#endif // STREAM9_OPTIONAL_BASE_HPP
