#ifndef STREAM9_OPTIONAL_TYPE_TRAITS_HPP
#define STREAM9_OPTIONAL_TYPE_TRAITS_HPP

#include <type_traits>

namespace stream9 {

template<typename T> class optional;

template<typename T>
struct is_optional : std::false_type {};

template<typename T>
struct is_optional<optional<T>> : std::true_type {};

template<typename T>
constexpr bool is_optional_v = is_optional<T>::value;

} // namespace stream9

#endif // STREAM9_OPTIONAL_TYPE_TRAITS_HPP
