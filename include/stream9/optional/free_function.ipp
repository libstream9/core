#ifndef STREAM9_OPTIONAL_FREE_FUNCTION_IPP
#define STREAM9_OPTIONAL_FREE_FUNCTION_IPP

namespace stream9 {

template<typename T, typename U>
constexpr bool
operator==(optional<T> const& lhs, optional<U> const& rhs)
    noexcept(noexcept(std::declval<T>() == std::declval<U>()))
    requires requires (T t, U u) { { t == u } -> std::same_as<bool>; }
{
    if (lhs.has_value() && rhs.has_value()) {
        return lhs.value() == rhs.value();
    }
    else {
        return false;
    }
}

template<typename T, typename U>
constexpr auto
operator<=>(optional<T> const& lhs, optional<U> const& rhs)
    noexcept(noexcept(std::declval<T>() <=> std::declval<U>()))
    requires requires (T t, U u) { t <=> u; }
{
    using category_t = decltype(lhs.value() <=> rhs.value());

    if (lhs.has_value()) {
        if (rhs.has_value()) {
            return lhs.value() <=> rhs.value();
        }
        else {
            return category_t::greater;
        }
    }
    else {
        if (rhs.has_value()) {
            return category_t::less;
        }
        else {
            return category_t::equal;
        }
    }
}

template<typename T, typename U>
constexpr bool
operator==(optional<T> const& lhs, U const& rhs)
    noexcept(noexcept(std::declval<T>() == std::declval<U>()))
    requires requires (T t, U u) { { t == u } -> std::same_as<bool>; }
{
    if (lhs.has_value()) {
        return lhs.value() == rhs;
    }
    else {
        return false;
    }
}

template<typename T, typename U>
constexpr auto
operator<=>(optional<T> const& lhs, U const& rhs)
    noexcept(noexcept(std::declval<T>() <=> std::declval<U>()))
    requires requires (T t, U u) { t <=> u; }
{
    using category_t = decltype(lhs.value() <=> rhs);

    if (lhs.has_value()) {
        return lhs.value() <=> rhs;
    }
    else {
        return category_t::less;
    }
}

template<typename T>
constexpr bool
operator==(optional<T> const& lhs, null_t) noexcept
{
    return !lhs.has_value();
}

template<typename T>
constexpr auto
operator<=>(optional<T> const& lhs, null_t) noexcept
{
    if (lhs.has_value()) {
        return std::strong_ordering::greater;
    }
    else {
        return std::strong_ordering::equal;
    }
}

template<typename T, std::invocable<T const&> F>
constexpr optional<std::invoke_result_t<F, T const&>>
transform(optional<T> const& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T const&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T const&>>,
                std::invoke_result_t<F, T const&> >)
{
    if (v.has_value()) {
        return func(v.value());
    }
    else {
        return {};
    }
}

template<typename T, std::invocable<T&> F>
constexpr optional<std::invoke_result_t<F, T&>>
transform(optional<T>& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T&>>,
                std::invoke_result_t<F, T&> >)
{
    if (v.has_value()) {
        return func(v.value());
    }
    else {
        return {};
    }
}

template<typename T, std::invocable<T const&> F>
constexpr optional<std::invoke_result_t<F, T const&>>
operator|(optional<T> const& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T const&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T const&>>,
                std::invoke_result_t<F, T const&> >)
{
    return transform(v, func);
}

template<typename T, std::invocable<T&> F>
constexpr optional<std::invoke_result_t<F, T&>>
operator|(optional<T>& v, F&& func)
    noexcept(std::is_nothrow_invocable_v<F, T&>
          && std::is_nothrow_constructible_v<
                optional<std::invoke_result_t<F, T&>>,
                std::invoke_result_t<F, T&> >)
{
    return transform(v, func);
}

template<typename T, typename U>
constexpr optional<T>
static_optional_cast(optional<U> const& v)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { static_cast<T>(u) } -> std::same_as<T>;
    }
{
    if (v.has_value()) {
        return static_cast<T>(v.value());
    }
    else {
        return {};
    }
}

template<typename T, typename U>
constexpr optional<T>
dynamic_optional_cast(optional<U> const& v)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { dynamic_cast<T>(u) } -> std::same_as<T>;
    }
{
    if (v.has_value()) {
        return dynamic_cast<T>(v.value());
    }
    else {
        return {};
    }
}

template<typename T, typename U>
constexpr optional<T>
const_optional_cast(optional<U> const& v)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { const_cast<T>(u) } -> std::same_as<T>;
    }
{
    if (v.has_value()) {
        return const_cast<T>(v.value());
    }
    else {
        return {};
    }
}

template<typename T, typename U>
constexpr optional<T>
reinterpret_optional_cast(optional<U> const& v)
    noexcept(std::is_nothrow_constructible_v<optional<T>, T>)
    requires requires (T&& t, U&& u) {
        { reinterpret_cast<T>(u) } -> std::same_as<T>;
    }
{
    if (v.has_value()) {
        return reinterpret_cast<T>(v.value());
    }
    else {
        return {};
    }
}

template<typename CharT, typename Traits, typename T>
std::basic_ostream<CharT, Traits>&
operator<<(std::basic_ostream<CharT, Traits>& os, optional<T> const& v)
    requires requires (std::basic_ostream<CharT, Traits>& os, T v) { os << v; }
{
    if (v.has_value()) {
        return os << v.value();
    }
    else {
        return os << "[null]";
    }
}

} // namespace stream9

#endif // STREAM9_OPTIONAL_FREE_FUNCTION_IPP
