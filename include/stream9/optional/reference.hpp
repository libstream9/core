#ifndef STREAM9_OPTIONAL_REFERENCE_HPP
#define STREAM9_OPTIONAL_REFERENCE_HPP

#include "type_traits.hpp"

#include <type_traits>

#include <stream9/null.hpp>

namespace stream9 {

template<typename T> class optional;

template<typename T>
class optional<T&>
{
public:
    using pointer = std::add_pointer_t<T>;

public:
    constexpr optional() noexcept = default;

    constexpr optional(optional const&) noexcept = default;
    constexpr optional& operator=(optional const&) noexcept = default;

    constexpr optional(optional&&) noexcept = default;
    constexpr optional& operator=(optional&&) noexcept = default;

    constexpr ~optional() noexcept = default;

    constexpr optional(null_t) noexcept;
    constexpr optional& operator=(null_t) noexcept;

    template<typename U>
        requires std::is_convertible_v<U, T&>
    constexpr optional(optional<U> const&) noexcept;

    template<typename U>
        requires std::is_convertible_v<U, T&>
    constexpr optional& operator=(optional<U> const&) noexcept;

    template<typename U>
        requires std::is_convertible_v<U, T&>
              && (!is_optional_v<U>)
    constexpr optional(U&&) noexcept;

    template<typename U>
        requires std::is_convertible_v<U, T&>
              && (!is_optional_v<U>)
    constexpr optional& operator=(U&&) noexcept;

    // query
    constexpr T& value() const noexcept;

    constexpr T& operator*() const noexcept;

    constexpr pointer operator->() const noexcept;

    constexpr bool has_value() const noexcept;

    // modifier
    constexpr void reset() noexcept;

    constexpr void swap(optional&) noexcept;

    // conversion
    constexpr operator bool () const noexcept;

private:
    pointer m_ptr = nullptr;
};

} // namespace stream9

#include "reference.ipp"
#include "free_function.hpp"

#endif // STREAM9_OPTIONAL_REFERENCE_HPP
