#include <stream9/safe_integer/any_integer.hpp>

#include <ostream>
#include <sstream>

#include <boost/core/demangle.hpp>

namespace stream9::safe_integers {

std::string any_safe_integer::
type_name() const
{
    std::ostringstream oss;

    oss << "safe_integer<";

    std::visit([&](auto&& v) {
        oss << boost::core::demangle(typeid(v).name());
    }, m_value);

    std::visit([&](auto&& v) {
        oss << ", " << v;
    }, m_min);

    std::visit([&](auto&& v) {
        oss << ", " << v;
    }, m_max);

    oss << ">";

    return oss.str();
}

std::ostream&
operator<<(std::ostream& os, any_safe_integer const& v)
{
    std::visit([&](auto&& v) {
        os << v;
    }, v.value());

    return os;
}

std::ostream&
operator<<(std::ostream& os, any_integer const& v)
{
    std::visit([&](auto&& v) {
        os << v;
    }, v.value());

    return os;
}

} // namespace stream9::safe_integers
