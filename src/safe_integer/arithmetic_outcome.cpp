#include <stream9/safe_integer/arithmetic_outcome.hpp>

#include <ostream>

namespace stream9::safe_integers {

std::ostream&
operator<<(std::ostream& os, arithmetic_errc const e)
{
    switch (e) {
    case arithmetic_errc::ok:
        os << "ok";
        break;
    case arithmetic_errc::overflow:
        os << "overflow";
        break;
    case arithmetic_errc::underflow:
        os << "underflow";
        break;
    case arithmetic_errc::divide_by_zero:
        os << "divide_by_zero";
        break;
    default:
        os << "unknown error: " << static_cast<int>(e);
    }

    return os;
}

} // namespace stream9::safe_integers
