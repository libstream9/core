#include <stream9/safe_integer/errors.hpp>

#include <ostream>

namespace stream9::safe_integers {

template<typename T>
std::ostream&
stream_out(std::ostream& os, T const& e)
{
    os << e.what() << "\n"
       << "operation: " << e.operation() << "\n"
       << "operand 1: " << e.operand1() << "\n";

    auto const& op2 = e.operand2();
    if (op2) {
        os << "operand 2: " << *op2 << "\n";
    }

    return os;
}

std::ostream&
operator<<(std::ostream& os, operation const op)
{
    using o = operation;

    switch (op) {
        case o::construction:
            return os << "construction";
        case o::assignment:
            return os << "assignment";
        case o::addition:
            return os << "addition";
        case o::subtraction:
            return os << "subtraction";
        case o::multiplication:
            return os << "multiplication";
        case o::division:
            return os << "division";
        case o::modulo:
            return os << "modulo";
        case o::negation:
            return os << "negation";
        case o::conversion:
            return os << "conversion";
    }

    return os << "unknown";
}

std::ostream&
operator<<(std::ostream& os, overflow_error const& e)
{
    return stream_out(os, e);
}

std::ostream&
operator<<(std::ostream& os, underflow_error const& e)
{
    return stream_out(os, e);
}

std::ostream&
operator<<(std::ostream& os, divided_by_zero_error const& e)
{
    os << e.what() << "\n"
       << "operand: " << e.operand() << "\n";

    return os;
}

} // namespace stream9::safe_integers
