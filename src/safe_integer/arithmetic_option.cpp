#include <stream9/safe_integer/arithmetic_option.hpp>

#include <ostream>
#include <type_traits>

namespace stream9::safe_integers {

std::ostream&
operator<<(std::ostream& os, arithmetic_option const e)
{
    using o = arithmetic_option;
    using value_t = std::underlying_type_t<arithmetic_option>;

    if (e == o::none) {
        os << "arithmetic_option::none";
    }
    else {
        auto found = false;

        if (test_option(e, o::dont_check_overflow)) {
            os << "arithmetic_option::dont_check_overflow";
            found = true;
        }
        if (test_option(e, o::dont_check_underflow)) {
            if (found) os << " | ";
            os << "arithmetic_option::dont_check_underflow";
            found = true;
        }
        if (test_option(e, o::dont_check_zero_division)) {
            if (found) os << " | ";
            os << "arithmetic_option::dont_check_zero_division";
            found = true;
        }

        if (!found) {
            os << "unknown arithmetic_option: " << static_cast<value_t>(e);
        }
    }

    return os;
}

} // namespace stream9::safe_integers
