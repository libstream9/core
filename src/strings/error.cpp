#include <stream9/strings/error.hpp>

#include <string>
#include <system_error>

namespace stream9::strings {

std::error_category const&
error_category() noexcept
{
    static struct impl : std::error_category
    {
        char const*
        name() const noexcept
        {
            return "stream9::strings";
        }

        std::string
        message(int ec) const
        {
            using enum errc;

            switch (static_cast<errc>(ec)) {
                case ok:
                    return "ok";
                case invalid_character:
                    return "invalid character";
                case invalid_hex_character:
                    return "invalid hex character";
                case invalid_bool_string:
                    return "invalid bool string";
                case invalid_format_string:
                    return "invalid format string";
                case fail_to_convert_from_istream:
                    return "fail to convert with istream";
                case not_terminated_with_null:
                    return "not terminated with null";
                case index_is_out_of_range:
                    return "index is out of range";
                case not_enough_memory:
                    return "not enough memory";
                case invalid_pointer:
                    return "invalid pointer";
                case invalid_range:
                    return "invalid range";
                case invalid_iterator:
                    return "invalid iterator";
                case try_to_access_inside_of_empty_string:
                    return "try to access inside of empty string";
            }

            return "unknown error";
        }
    } instance;

    return instance;
}

} // namespace stream9::strings
