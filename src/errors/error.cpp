#include <stream9/errors/error.hpp>

#include <stream9/errors/namespace.hpp>

namespace stream9::errors {

error::
error(std::error_code why,
      source_location where) noexcept
    : m_why { std::move(why) }
    , m_where { std::move(where) }
{}

error::
error(std::error_code why,
      json::object context,
      source_location where) noexcept
    : m_why { std::move(why) }
    , m_context { std::move(context) }
    , m_where { std::move(where) }
{}

error::
error(std::string what,
      std::error_code why,
      source_location where) noexcept
    : m_what { std::move(what) }
    , m_why { std::move(why) }
    , m_where { std::move(where) }
{}

error::
error(std::string what,
      std::error_code why,
      json::object context,
      source_location where) noexcept
    : m_what { std::move(what) }
    , m_why { std::move(why) }
    , m_context { std::move(context) }
    , m_where { std::move(where) }
{}

char const* error::
what() const noexcept
{
    if (m_what.empty()) {
        return m_where.function_name();
    }
    else {
        return m_what.c_str();
    }
}

bool error::
operator==(error const& o) const noexcept
{
    return m_what == o.m_what
        && m_why == o.m_why
        && m_context == o.m_context
        && m_where.line() == o.m_where.line()
        && m_where.column() == o.m_where.column()
        && m_where.file_name() == o.m_where.file_name()
        && m_where.function_name() == o.m_where.function_name();
}

[[noreturn]] void
throw_error(std::error_code why,
            source_location loc/*= {}*/)
{
    if (std::current_exception()) {
        std::throw_with_nested(error {
            std::move(why),
            std::move(loc),
        });
    }
    else {
        throw error {
            std::move(why),
            std::move(loc),
        };
    }
}

[[noreturn]] void
throw_error(std::error_code why,
            json::object cxt,
            source_location loc/*= {}*/)
{
    if (std::current_exception()) {
        std::throw_with_nested(error {
            std::move(why),
            std::move(cxt),
            std::move(loc),
        });
    }
    else {
        throw error {
            std::move(why),
            std::move(cxt),
            std::move(loc),
        };
    }
}

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            source_location loc/*= {}*/)
{
    if (std::current_exception()) {
        std::throw_with_nested(error {
            std::move(what),
            std::move(why),
            std::move(loc),
        });
    }
    else {
        throw error {
            std::move(what),
            std::move(why),
            std::move(loc),
        };
    }
}

[[noreturn]] void
throw_error(std::string what,
            std::error_code why,
            json::object cxt,
            source_location loc/*= {}*/)
{
    if (std::current_exception()) {
        std::throw_with_nested(error {
            std::move(what),
            std::move(why),
            std::move(cxt),
            std::move(loc),
        });
    }
    else {
        throw error {
            std::move(what),
            std::move(why),
            std::move(cxt),
            std::move(loc),
        };
    }
}

} // namespace stream9::errors
