#include <stream9/errors/error_code.hpp>

namespace stream9::errors {

std::error_category const&
error_category()
{
    static struct impl : std::error_category {
        char const* name() const noexcept
        {
            return "stream9::errors";
        }

        std::string message(int condition) const
        {
            switch (static_cast<errc>(condition)) {
                using enum errc;
                case ok:
                    return "ok";
                case invalid_argument:
                    return "invalid argument";
                case argument_out_of_domain:
                    return "argument out of domain";
                case result_out_of_range:
                    return "result out of range";
                case invalid_data:
                    return "invalid data";
                case internal_error:
                    return "internal error";
                default:
                case unknown_error:
                    return "unknown_error";
            }
        }
    } instance;

    return instance;
}

} // namespace stream9::errors
