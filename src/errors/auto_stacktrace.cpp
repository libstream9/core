#include <cassert>
#include <stacktrace>

#include <dlfcn.h>

/*
 * types
 */
struct __cxa_eh_globals {
    void* caughtException;
    unsigned int uncaughtException;
};

/*
 * variables
 */
thread_local static std::stacktrace s_stacktrace {};

static bool s_auto_stacktrace = true;

/*
 * functions
 */
extern "C" __cxa_eh_globals* __cxa_get_globals();
extern "C" void* __cxa_get_exception_ptr(void* exceptionObject);

[[noreturn]] static void
real_cxa_throw(void* exception, void* typeinfo, void (*dest)(void*))
{
    using func_t = void*(*)(void*, void*, void(*)(void*));

    static auto original =
        reinterpret_cast<func_t>(::dlsym(RTLD_NEXT, "__cxa_throw"));

    original(exception, typeinfo, dest);

    assert(false);
}

static void
real_cxa_end_catch()
{
    using func_t = void(*)();

    static auto original =
        reinterpret_cast<func_t>(::dlsym(RTLD_NEXT, "__cxa_end_catch"));

    original();
}

/*
 * overrides
 */
extern "C" void
__cxa_throw(void *exception, void *typeinfo, void (*dest)(void *))
{
    if (s_auto_stacktrace) {
        if (s_stacktrace == std::stacktrace()) {
            s_stacktrace = std::stacktrace::current(1);
        }
    }

    real_cxa_throw(exception, typeinfo, dest);
}

extern "C" void
__cxa_end_catch()
{
    real_cxa_end_catch();

    if (s_auto_stacktrace) {
        auto* global = __cxa_get_globals();
        if (global->caughtException == nullptr &&
            global->uncaughtException == 0) // there is no more exception to handle.
        {
            s_stacktrace = {};
        }
    }
}

/*
 * public API
 */
namespace stream9 {

bool
auto_stacktrace() noexcept
{
    return s_auto_stacktrace;
}

void
set_auto_stacktrace(bool x) noexcept
{
    s_auto_stacktrace = x;
}

std::stacktrace const&
stacktrace_at_throw() noexcept
{
    return s_stacktrace;
}

} // namespace stream9
