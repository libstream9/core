#include <stream9/errors/print_error.hpp>

#include <stream9/errors/error.hpp>
#include <stream9/errors/auto_stacktrace.hpp>

#include <exception>
#include <ostream>
#include <string>

namespace stream9::errors {

static void
print_error_impl_(std::ostream& os, std::exception_ptr const& ptr)
{
    try {
        std::rethrow_exception(ptr);
    }
    catch (error const& e) {
        auto const& where = e.where();
        auto const& context = e.context();

        os << where.file_name() << ':' << where.line()
           << '|' << e.what()
           << '|' << e.why().message();

        if (!context.empty()) {
            os << '|' << context;
        }
    }
    catch (std::exception const& e) {
        os << e.what();
    }
    catch (std::string_view const& e) {
        os << e;
    }
    catch (std::string const& e) {
        os << e;
    }
    catch (char const* const e) {
        os << e;
    }
    catch (...) {
        os << "unknown error";
    }

    os << std::endl;

    try {
        std::rethrow_exception(ptr);
    }
    catch (std::nested_exception const& e) {
        print_error_impl_(os, e.nested_ptr());
    }
    catch (...)
    {}
}

static void
print_error_impl(std::ostream& os, std::exception_ptr const& ptr)
{
    print_error_impl_(os, ptr);

    if (auto_stacktrace()) {
        os << "[Stacktrace]\n";
        os << stacktrace_at_throw() << std::flush;
    }
}

void
print_error(std::ostream& os,
            source_location where/*= {}*/)
{
    assert(std::current_exception());

    os << where.file_name() << ':' << where.line()
       << "|error" << std::endl;

    print_error_impl(os, std::current_exception());
}

void
print_error(std::ostream& os,
            json::object const& context,
            source_location where/*= {}*/)
{
    assert(std::current_exception());

    os << where.file_name() << ':' << where.line()
       << "|error|" << context
       << std::endl;

    print_error_impl(os, std::current_exception());
}

void
print_error(std::ostream& os,
            error const& e,
            source_location where/*= {}*/)
{
    try {
        throw e;
    }
    catch (...) {
        print_error(os, std::move(where));
    }
}

void
print_error(std::ostream& os,
            error const& e,
            json::object const& context,
            source_location where/*= {}*/)
{
    try {
        throw e;
    }
    catch (...) {
        os << where.file_name() << ':' << where.line()
           << "|error|" << context
           << std::endl;

        print_error_impl(os, std::current_exception());
    }
}

void
print_error(std::ostream& os,
            std::exception_ptr const& ex,
            source_location where/* = {}*/)
{
    try {
        std::rethrow_exception(ex);
    }
    catch (...) {
        print_error(os, std::move(where));
    }
}

void
print_error(std::ostream& os,
            std::exception_ptr const& ex,
            json::object const& context,
            source_location where/* = {}*/)
{
    try {
        std::rethrow_exception(ex);
    }
    catch (...) {
        os << where.file_name() << ':' << where.line()
           << "|error|" << context
           << std::endl;

        print_error_impl(os, std::current_exception());
    }
}

std::ostream&
operator<<(std::ostream& os, error const& e)
{
    try {
        throw e;
    }
    catch (...) {
        print_error_impl(os, std::current_exception());
    }
    return os;
}

} // namespace stream9::errors
