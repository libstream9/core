#include <stream9/errors/json.hpp>

namespace stream9::errors {

static void
assign_or_push_back(json::value& v1, json::value&& v2)
{
    if (v1.is_array()) {
        auto& a = v1.get_array();
        a.push_back(std::move(v2));
    }
    else {
        v1 = std::move(v2);
    }
}

static void
push_back(json::value& v1, json::value&& v2)
{
    if (v1.is_array()) {
        auto& a = v1.get_array();
        a.push_back(std::move(v2));
    }
    else {
        auto& a = v1.emplace_array();
        a.push_back(std::move(v2));
    }
}

void
tag_invoke(json::value_from_tag t,
           json::value& v,
           error const& e)
{
    auto& where = e.where();
    json::object obj {
        { "what", e.what() },
        { "why", e.why() },
        { "where", {
            { "filename", where.file_name() },
            { "function name", where.function_name() },
            { "line", where.line() },
            { "column", where.column() },
        }},
        { "context", e.context() }
    };

    if (auto* const ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));

        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

} // namespace stream9::errors

namespace stream9::json {

void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::exception_ptr const& ptr)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    try {
        std::rethrow_exception(ptr);
    }
    catch (stream9::errors::error const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::system_error const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::exception const& e) {
        tag_invoke(t, v, e);
    }
    catch (std::nested_exception const& e) {
        json::object obj;

        obj["type"] = "unknown";

        push_back(v, std::move(obj));

        tag_invoke(t, v, e.nested_ptr());
    }
    catch (...) {
        json::object obj;

        obj["type"] = "unknown";

        assign_or_push_back(v, std::move(obj));
    }
}

void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::exception const& e)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    json::object obj;

    obj["what"] = e.what();

    if (auto* const ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));
        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

void
tag_invoke(json::value_from_tag t,
           json::value& v,
           std::system_error const& e)
{
    using stream9::errors::assign_or_push_back;
    using stream9::errors::push_back;

    json::object obj {
        { "what", e.what() },
        { "why", json::value_from(e.code()) },
    };

    if (auto* ex = dynamic_cast<std::nested_exception const*>(&e)) {
        push_back(v, std::move(obj));
        tag_invoke(t, v, ex->nested_ptr());
    }
    else {
        assign_or_push_back(v, std::move(obj));
    }
}

void
tag_invoke(json::value_from_tag,
           json::value& v,
           std::error_code const& e)
{
    v = json::object {
        { "value", e.value() },
        { "category", e.category().name() },
        { "message", e.message() },
    };
}

} // namespace stream9::json
