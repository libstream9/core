#include <stream9/errors/rethrow_error.hpp>

#include <stream9/errors/error.hpp>

namespace stream9::errors {

static void
merge_context(json::object&& src, json::object& dest)
{
    for (auto&& [k, v]: src) {
        dest[std::move(k)] = std::move(v);
    }
}

[[noreturn]] void
rethrow_error(source_location where/*= {}*/)
{
    assert(std::current_exception());

    try {
        throw;
    }
    catch (error const& e) {
        if (e.where().function_name() == where.function_name()) {
            throw;
        }
        else {
            std::throw_with_nested(error {
                e.why(),
                std::move(where),
            });
        }
    }
    catch (...) {
        std::throw_with_nested(error {
            errc::internal_error,
            std::move(where),
        });
    }
}

[[noreturn]] void
rethrow_error(json::object context,
              source_location where/*= {}*/)
{
    assert(std::current_exception());

    try {
        throw;
    }
    catch (error& e) {
        if (e.where().function_name() == where.function_name()) {
            merge_context(std::move(context), e.context());

            throw;
        }
        else {
            std::throw_with_nested(error {
                e.why(),
                std::move(context),
                std::move(where),
            });
        }
    }
    catch (...) {
        std::throw_with_nested(error {
            errc::internal_error,
            std::move(context),
            std::move(where),
        });
    }
}

[[noreturn]] void
rethrow_error(std::error_category const&,
              source_location where/*= {}*/)
{
    rethrow_error(std::move(where));
}

[[noreturn]] void
rethrow_error(std::error_category const&,
              json::object context,
              source_location where/*= {}*/)
{
    rethrow_error(std::move(context), std::move(where));
}

} // namespace stream9::errors
