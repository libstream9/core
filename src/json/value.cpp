#include <stream9/json/value.hpp>

#include <stream9/json/namespace.hpp>
#include <stream9/json/parse.hpp>

#include <ostream>

#include <stream9/strings/stream.hpp>

namespace stream9::json {

static void
indent(std::ostream& s, std::string_view indent, int const level)
{
    for (int i = 0; i < level; ++i) {
        s << indent;
    }
}

static void
print(std::ostream& s, value const& v, std::string_view i, int level);

static void
print(std::ostream& s, object const& obj, std::string_view i, int level)
{
    if (obj.empty()) {
        s << "{}";
        return;
    }

    s << "{" << std::endl;

    bool first = true;

    for (auto&& [k, v]: obj) {
        if (!first) {
            s << "," << std::endl;
        }
        else {
            first = false;
        }

        indent(s, i, level + 1);
        s << '"' << k << "\": ";
        print(s, v, i, level + 1);
    }

    s << std::endl;
    indent(s, i, level);
    s << "}";
}

static void
print(std::ostream& s, array const& arr, std::string_view i, int level)
{
    if (arr.empty()) {
        s << "[]";
        return;
    }

    s << "[" << std::endl;

    bool first = true;

    for (auto&& v: arr) {
        if (!first) {
            s << "," << std::endl;
        }
        else {
            first = false;
        }

        indent(s, i, level + 1);
        print(s, v, i, level + 1);
    }

    s << std::endl;
    indent(s, i, level);
    s << "]";
}

static void
print(std::ostream& s, value const& v, std::string_view i, int level)
{
    switch (v.kind()) {
        case json::kind::object:
            print(s, v.get_object(), i, level);
            break;
        case json::kind::array:
            print(s, v.get_array(), i, level);
            break;
        default:
            s << v.base();
            break;
    }
}

std::string value::
to_string(std::string_view indent/*= "  "*/) const
{
    std::string result;
    str::ostream os { result };

    serialize(os, indent);

    return result;
}

void value::
serialize(std::ostream& os, std::string_view indent/*= "  "*/) const
{
    if (indent.empty()) {
        os << m_base;
    }
    else {
        print(os, *this, indent, 0);
    }
}

std::ostream&
operator<<(std::ostream& os, value const& v)
{
    if (os.width() > 0) {
        std::string space(static_cast<size_t>(os.width()), ' ');
        os.width(0);

        v.serialize(os, space);
    }
    else {
        v.serialize(os, "");
    }

    return os;
}

namespace literals {

    json::value
    operator ""_js (char const* const ptr, size_t const len)
    {
        return parse(std::string_view(ptr, len));
    }

} // namespace literals

} // namespace stream9::json
