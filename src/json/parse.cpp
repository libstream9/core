#include <stream9/json/parse.hpp>

#include <stream9/json/namespace.hpp>

#include <system_error>

#include <stream9/errors.hpp>
#include <stream9/ranges/size.hpp>
#include <stream9/safe_integer.hpp>

namespace stream9::json {

using pos_t = stream9::safe_integer<ptrdiff_t, 0>;
using line_no_t = stream9::safe_integer<ptrdiff_t, 1>;
using column_t = stream9::safe_integer<ptrdiff_t, 0>;

static json::object
make_context(std::string_view const js, pos_t const pos)
{
    if (pos > js.size()) return {};

    line_no_t line_no = 1;
    pos_t bol = 0;

    for (pos_t i = 0; i < pos; ++i) {
        if (js[i] == '\n') {
            bol = i + 1;
            ++line_no;
        }
    }

    pos_t eol = rng::ssize(js);

    for (pos_t i = pos; i < rng::ssize(js); ++i) {
        if (js[i] == '\n') {
            eol = i;
            break;
        }
    }

    assert(bol <= pos);
    assert(bol <= eol);

    column_t column = pos - bol;

    return {
        { "line", line_no },
        { "column", column },
    };
}

json::value
parse(std::string_view s, json::storage_ptr sp/*= {}*/,
                          json::parse_options const& opt/*= {}*/)
{
    std::error_code ec;
    unsigned char temp[BOOST_JSON_STACK_BUFFER_SIZE];

    boost::json::parser p(storage_ptr(), opt, temp);
    p.reset(std::move(sp));

    pos_t pos = p.write(s, ec);
    if (ec) {
        throw_error(ec, make_context(s, pos));
    }

    try {
        return json::value { p.release() };
    }
    catch (...) {
        rethrow_error(make_context(s, pos));
    }
}

json::value
parse(std::istream& s, json::storage_ptr sp/*= {}*/,
                       json::parse_options const& opt/*= {}*/)
{
    if (!s.good()) return {};

    char buf[8192];
    std::error_code ec;
    unsigned char temp[BOOST_JSON_STACK_BUFFER_SIZE];

    boost::json::stream_parser p(storage_ptr(), opt, temp);
    p.reset(std::move(sp));

    pos_t pos = 0;

    while (s.good()) {
        s.read(buf, sizeof(buf));
        pos_t const n_read = s.gcount();

        pos += p.write(buf, n_read, ec);
        if (ec) {
            throw_error(ec, {
                { "pos", pos },
            });
        }
    }

    try {
        return json::value { p.release() };
    }
    catch (...) {
        rethrow_error({ { "pos", pos }, });
    }
}

} // namespace stream9::json
