#include <stream9/json/format.hpp>

#include <stream9/json/value.hpp>

namespace stream9::json {

std::string
format(std::initializer_list<boost::json::value_ref> ini,
       std::string_view const space/*= ""*/)
{
    unsigned char buf[1024];
    boost::json::monotonic_resource mr { buf };

    return json::value(
        boost::json::value(std::move(ini)), &mr
    ).to_string(space);
}

} // namespace stream9::json
